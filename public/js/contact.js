// CONTACT MAP

var PageContact = function() {

	var _init = function() {

		var mapbg = new GMaps({
			div: '#gmapbg',
			lat: 32.543009,
			lng: -110.876571,
			scrollwheel: false,
			zoom: 13,
		});


		mapbg.addMarker({
            lat: 32.543009,
            lng: -110.876571,
			title: 'Your Location',
			infoWindow: {
				content: '<h3>Parish House</h3><p>36768 South Aaron Ln<br>Tucson, AZ 85739</p>'
			}
		});

        mapbg.addMarker({
            lat: 32.514304,
            lng: -110.903466,
            title: 'Your Location',
            infoWindow: {
                content: '<h3>Desertview Performing Arts Center</h3><p>39900 S Clubhouse Drive<br>Tucson, AZ 85739</p>'
            }
        });
	}

    return {
        //main function to initiate the module
        init: function() {

            _init();

        }

    };
}();

$(document).ready(function() {
    PageContact.init();
    $( window ).resize(function() {
		PageContact.init();
	});
});