<div class="blank-block"></div>
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-3 -->
<div class="c-layout-breadcrumbs-1 c-bgimage c-subtitle c-fonts-uppercase c-fonts-bold c-bg-img-center" style="background-image: url({{ asset('storage/backgrounds/bc-bg.jpg') }})">
    <div class="container">
        <div class="c-page-title c-pull-left">
            <h3 class="c-font-uppercase c-font-bold ">{!! $bc_title !!}</h3>
            <h4 class="c-font-dark">{!! $bc_subtitle !!}</h4>
        </div>
        <!-- Breadcrumbs -->
        {!! $bc !!}
    </div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-3 -->