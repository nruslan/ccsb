@if (count($breadcrumbs))
    <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                <li>/</li>
            @else
                <li class="c-state_active">{{ $breadcrumb->title }}</li>
            @endif
        @endforeach
    </ul>
@endif