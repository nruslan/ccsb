<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span></button>
    <h4 class="modal-title" id="exampleModalLabel">Message has been sent successfully!</h4>
</div>
<div class="modal-body c-center">
    <i class="icon-like"></i>
    <p>The message has been sent successfully!</p>
</div>
<div class="modal-footer">
    <button type="button" class="btn c-theme-btn c-btn-border-2x c-btn-square c-btn-bold c-btn-uppercase" data-dismiss="modal">Close</button>
</div>