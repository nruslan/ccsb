@extends('layouts.master')

@section('title', 'Church Directory')

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('show-member', $user),
        'bc_title' => "$user->members_pretty_name",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content c-square" id="modalContactForm">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">New message to</h4>
                </div>
                {!! Form::open(['url' => 'member/message', 'id' => 'messageForm', 'onsubmit' => 'return false;']) !!}
                <div class="modal-body">
                    <input type="hidden" id="memberId" value="">
                    <input type="hidden" id="fromEmail" value="{{ $currentUserEmail }}">
                    <input type="hidden" id="fromName" value="{!! $currentUserName !!}">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Recipient:</label>
                        <input type="text" class="form-control c-square" id="recipient-name" readonly>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Message:</label>
                        <textarea class="form-control  c-square" id="messageText" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn c-theme-btn c-btn-square c-btn-bold c-btn-uppercase" id="submitBtn"><i class="fa fa-fw fa-send-o"></i>Submit</button>
                    <button type="button" class="btn c-theme-btn c-btn-border-2x c-btn-square c-btn-bold c-btn-uppercase" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <!-- END: CONTENT/MISC/TEAM-3 -->
    <div class="c-content-box c-size-md c-bg-grey-1">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-team-1-slider" data-slider="owl" data-items="3">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-font-uppercase c-font-bold">{!! $user->members_pretty_name !!}</h3>
                    <div class="c-line-left c-theme-bg"></div>
                </div><!-- End-->
                <div class="row">
                    @foreach($user->members as $member)
                        <div class="col-md-4 col-sm-6 c-margin-b-30">
                            <div class="c-content-person-1 c-option-2">
                                <div class="c-caption c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ $member->profile_picture_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-4">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $member->profile_picture_url }}" alt="" width="100%">
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold">{!! $member->full_name !!}</div>
                                        <ul class="c-socials c-theme-ul">
                                            <li>
                                                <a href="#" class="envelopeBtn" id="{{ $member->id }}" data-toggle="modal" data-target="#exampleModal2" data-name="{{ $member->full_name }}" data-id="{{ $member->id }}"><i class="icon-envelope"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="c-position">Member Since {{ $user->since_year }}</div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div><!-- End ROW-->

                <div class="row">
                    <div class="col-md-8 col-md-push-4">
                        <div class="c-content-title-3 c-theme-border">
                            <h3 class="c-left c-font-uppercase">About</h3>
                            <div class="c-line c-dot c-dot-left "></div>
                        </div>
                        @if($user->story){!! $user->story->description !!}@endif
                    </div>
                    <div class="col-md-4 col-md-pull-8">
                        <div class="c-content-title-3 c-theme-border">
                            <h3 class="c-left c-font-uppercase">Address</h3>
                            <div class="c-line c-dot c-dot-left "></div>
                        </div>
                        @foreach($addresses as $adr)
                            {{ $adr->address1 }}<br>
                            @if($adr->address2){{ $adr->address2 }}<br>@endif
                            {{ $adr->city }}@if($adr->state_id) {{ $adr->state->name }}@endif {{ $adr->zip_code }}<br>
                            @if($adr->country->id != 244){{ $adr->country->name }}@endif
                            @if(isset($adr->homePhone)) <i class="glyphicon glyphicon-phone-alt"></i> ({{ $adr->homePhone->area_code }}) {{ $adr->homePhone->number }} @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div><!-- END: CONTENT/MISC/TEAM-3 -->
    </div>
@endsection

@section('scripts')
<script>
    // JavaScript Document
    (function () {
        //let output = document.getElementById('messageResponse');
        let submitBtn = document.getElementById('submitBtn');
        submitBtn.onclick = function () {
            this.disabled = true;
            let data = new FormData();
            data.append('id', document.getElementById('memberId').value);
            data.append('message', document.getElementById('messageText').value);
            data.append('from_name', document.getElementById('fromName').value);
            data.append('from_email', document.getElementById('fromEmail').value);
            axios.post('/api/member/message', data)
                .then(function (response) {
                    document.getElementById('messageForm').remove();
                    //console.log(response);
                    document.getElementById('modalContactForm').innerHTML = response.data;
                })
                .catch(function (err) {
                    output.className = 'container text-danger';
                    output.innerHTML = err.message;
                });
        };
    })();

    $(document).ready(function() {
        $('.envelopeBtn').on('click', function(){
            //console.log($(this).data('name'));
            $('#recipient-name').val($(this).data('name'));
            $('#memberId').val($(this).data('id'));
        });
    });
</script>
@endsection