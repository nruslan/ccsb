@extends('layouts.master')

@section('title', 'Church Directory')

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('directory'),
        'bc_title' => "Church Members Directory",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- END: CONTENT/MISC/TEAM-3 -->
    <div class="c-content-box c-size-md c-bg-grey-1">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-team-1-slider" data-slider="owl" data-items="3">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Directory</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
                <div class="row">
                    @foreach($users as $user)
                        <div class="col-md-4 col-sm-6 c-margin-b-30">
                            <div class="c-content-person-1 c-option-2">
                                <div class="c-caption c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ url("directory/member/$user->id") }}"><i class="icon-link"></i></a>
                                            <a href="{{ $user->picture_filename_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-4">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $user->picture_filename_url }}" alt="" width="100%">
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold">
                                            <a href="{{ url("directory/member/$user->id") }}">{!! $user->members_pretty_name !!}</a></div>
                                    </div>
                                    <div class="c-position">Member Since {{ $user->since_year }}</div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        @if($i++ % 3 == 0)</div><div class="row">@endif
                    @endforeach
                </div><!-- End-->
                <div class="row">
                    <div class="col-lg-12 text-center">
                        {{ $users->links('vendor.pagination.jango') }}
                    </div>
                </div>
            </div>
        </div><!-- END: CONTENT/MISC/TEAM-3 -->
    </div>
@endsection