@extends('layouts.master')
@section('title', 'Church Happenings')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('happenings'),
        'bc_title' => "Happenings",
        'bc_subtitle' => 'News, Events & Calendar'
    ])

    <!-- BEGIN: CONTENT/BLOG/RECENT-POSTS-2 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="c-content-title-1">
                        <h3 class="c-left c-font-uppercase">News</h3>
                        <div class="c-line-left c-theme-bg"></div>
                    </div>
                    <div class="c-content-tab-1 c-theme c-margin-t-30">
                        <ul class="c-content-recent-posts-1">
                            @foreach($news as $news)
                            <li>
                                <div class="c-image">
                                    <a href="{{ $news->url }}"><img src="{{ $news->thumbnail_picture }}" alt="image thumbnail" class="img-responsive"></a>
                                </div>
                                <div class="c-post">
                                    <a href="{{ $news->url }}" class="c-title">{{ $news->title }} {{ $news->subtitle }}</a>
                                    <p>{!! $news->text_limit($news->text) !!}... <a href="{{ $news->url }}" class="aBtn">read more <i class="fa fa-fw fa-angle-double-right"></i></a></p>
                                    <div class="c-date c-font-bold">Published on {{ $news->pub_date }}</div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p class="c-margin-t-40">
                                <a href="{{ url("/happenings/news") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                                    <i class="fa fa-fw fa-bullhorn" aria-hidden="true"></i> View More
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="c-content-title-1">
                        <h3 class="c-left c-font-uppercase">Upcoming Events</h3>
                        <div class="c-line-left c-theme-bg"></div>
                    </div>
                    <div class="c-content-tab-1 c-theme c-margin-t-30">
                        <ul class="c-content-recent-posts-1">
                            @foreach($events as $event)
                            <li>
                                <div class="c-image">
                                    <img src="{{ asset("storage/events/thumbnail-default.jpg") }}" alt="" class="img-responsive">
                                </div>
                                <div class="c-post">
                                    <a href="" class="c-title">{{ $event->data->title }}</a>
                                    <p>{!! $event->data->text_limit($event->data->description) !!}... <a href="{{ $event->data->url }}" class="aBtn">read more <i class="fa fa-fw fa-angle-double-right"></i></a></p>
                                    <div class="c-date c-font-bold">{{ $event->date }} at {{ $event->time }}</div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p class="c-margin-t-40">
                                <a href="{{ url("/happenings/events") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                                    <i class="fa fa-fw fa-bullhorn" aria-hidden="true"></i> View More
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/BLOG/RECENT-POSTS-2 -->

    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="c-content-title-1">
                        <h3 class="c-left c-font-uppercase">Church Calendar</h3>
                        <div class="c-line-left c-theme-bg"></div>
                    </div>
                    <iframe src="https://www.google.com/calendar/embed?mode=MONTH&amp;height=500&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=communitychurchatsaddlebrooke%40gmail.com&amp;color=%232952A3&amp;ctz=America%2FPhoenix" width="100%" height="500" frameborder="0" scrolling="no"></iframe>
                </div>
            </div>
        </div>
    </div>

@endsection