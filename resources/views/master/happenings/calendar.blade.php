@extends('layouts.master')
@section('title', 'Events')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('calendar'),
        'bc_title' => "Calendar",
        'bc_subtitle' => 'Community Church at Saddlebrooke'
    ])

    <!-- BEGIN: CONTENT/BLOG/RECENT-POSTS-2 -->
    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="c-content-title-1">
                        <h3 class="c-left c-font-uppercase">Church Calendar</h3>
                        <div class="c-line-left c-theme-bg"></div>
                    </div>
                    <iframe src="https://www.google.com/calendar/embed?mode=MONTH&amp;height=500&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=communitychurchatsaddlebrooke%40gmail.com&amp;color=%232952A3&amp;ctz=America%2FPhoenix" width="100%" height="500" frameborder="0" scrolling="no"></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection