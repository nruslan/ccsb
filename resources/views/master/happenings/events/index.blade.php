@extends('layouts.master')
@section('title', 'Events')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('events'),
        'bc_title' => "Events",
        'bc_subtitle' => 'Community Church at Saddlebrooke'
    ])

    <!-- BEGIN: BLOG LISTING EVENTS -->
    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="c-content-title-1">
                        <h3 class="c-left c-font-uppercase">Upcoming Events</h3>
                        <div class="c-line-left c-theme-bg"></div>
                    </div>
                    <div class="c-content-blog-post-1-list">
                        @foreach($events as $event)
                            <div class="c-content-blog-post-1">
                                <div class="c-media">
                                    <div class="item">
                                        <div class="c-content-media-2" style="background: url({{ $event->data->thumbnail_picture }}) no-repeat center center; background-size: cover; min-height: 360px;"></div>
                                    </div>
                                </div>
                                <div class="c-title c-font-bold c-font-uppercase">
                                    <a href="{{ $event->data->url }}">{!! $event->data->title_subtitle !!}</a>
                                </div>
                                <div class="c-desc">
                                    <!--{!! $event->data->text_limit($event->data->description, 0, 221) !!}...<a href="{{ url($event->data->url) }}" title="read more"><i class="fa fa-fw fa-angle-double-right"></i></a>-->
                                    {!! $event->data->description !!}
                                </div>
                                <div class="c-panel">
                                    <div class="c-author"><a href="#">By <span class="c-font-uppercase">CCSB</span></a></div>
                                    <div class="c-date">on <span class="c-font-uppercase">{{ $event->date }}</span> at <span class="c-font-uppercase">{{ $event->time }}</span></div>
                                    <ul class="c-tags c-theme-ul-bg">
                                        <li>{{ $event->data->category->title }}</li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                        {{ $events->links('vendor.pagination.jango') }}
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
@endsection