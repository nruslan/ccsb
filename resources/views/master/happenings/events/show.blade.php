@extends('layouts.master')
@section('title', 'Events')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('event_show', $event),
        'bc_title' => "$event->title",
        'bc_subtitle' => 'Community Church at Saddlebrooke'
    ])

    <!-- BEGIN: BLOG LISTING EVENTS -->
    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="c-content-blog-post-1-list">
                        <div class="c-content-blog-post-1">
                            <div class="c-media">
                                <div class="item">
                                    <div class="c-content-media-2" style="background: url({{ asset("storage/events/thumbnail-default.jpg") }}) no-repeat center center; background-size: cover; min-height: 360px;"></div>
                                </div>
                            </div>
                            <div class="c-title c-font-bold c-font-uppercase">
                                {!! $event->title_subtitle !!}
                            </div>
                            <div class="c-desc">
                                {!! $event->description !!}

                                <ul class="c-content-list-1 c-theme">
                                    @foreach($dates as $date)
                                    <li class="c-bg-before-blue">{{ $date->date }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="c-panel">
                                <div class="c-author"><a>By <span class="c-font-uppercase">CCSB</span></a></div>
                                <ul class="c-tags c-theme-ul-bg">
                                    <li>{{ $event->category->title }}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>

@endsection