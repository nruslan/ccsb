@extends('layouts.master')
@section('title', 'News')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('news_show', $newsPost),
        'bc_title' => "$newsPost->title",
        'bc_subtitle' => 'Community Church at Saddlebrooke'
    ])

    <!-- BEGIN: BLOG LISTING -->
    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="c-content-title-1">
                        <h3 class="c-left c-font-uppercase">{!! $newsPost->title_subtitle !!}</h3>
                        <div class="c-line-left c-theme-bg"></div>
                    </div>
                    <div class="c-content-blog-post-1-list">
                        <div class="c-content-blog-post-1">
                            <div class="c-media">
                                <div class="item">
                                    <div class="c-content-media-2" style="background-image: url({{ $newsPost->thumbnail_picture }}); min-height: 400px;"></div>
                                </div>
                            </div>
                            <div class="c-title c-font-bold c-font-uppercase"></div>
                            <div class="c-desc">{!! $newsPost->text !!}</div>
                            <div class="c-panel">
                                <div class="c-author"><a href="#">Published By <span class="c-font-uppercase">CCSB</span></a></div>
                                <div class="c-date">on <span class="c-font-uppercase">{{ $newsPost->pub_date }}</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
@endsection