@extends('layouts.master')
@section('title', 'News')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('news'),
        'bc_title' => "News",
        'bc_subtitle' => 'Community Church at Saddlebrooke'
    ])

    <!-- BEGIN: BLOG LISTING -->
    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="c-content-title-1">
                        <h3 class="c-left c-font-uppercase">News</h3>
                        <div class="c-line-left c-theme-bg"></div>
                    </div>
                    <div class="c-content-blog-post-1-list">
                        @foreach($news as $n)
                            <div class="c-content-blog-post-1">
                                <div class="c-media">
                                    <div class="item">
                                        <div class="c-content-media-2" style="background: url({{ $n->thumbnail_picture }}) no-repeat center center; background-size: cover; min-height: 360px;"></div>
                                    </div>
                                </div>
                                <div class="c-title c-font-bold c-font-uppercase">
                                    <a href="{{ url("happenings/news/$n->slug") }}">{!! $n->title_subtitle !!}</a>
                                </div>
                                <div class="c-desc">
                                    <!--{!! $n->text_limit($n->text, 0, 221) !!}...<a href="{{ url("happenings/news/$n->slug") }}" title="read more"><i class="fa fa-fw fa-angle-double-right"></i></a>-->
                                    {!! $n->text !!}
                                </div>
                                <div class="c-panel">
                                    <div class="c-author"><a href="#">By <span class="c-font-uppercase">CCSB</span></a></div>
                                    <div class="c-date">on <span class="c-font-uppercase">{{ $n->pub_date }}</span></div>
                                    <ul class="c-tags c-theme-ul-bg">
                                        <li>CCSB NEWS</li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                        {{ $news->links('vendor.pagination.jango') }}
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
@endsection