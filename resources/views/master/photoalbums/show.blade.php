@extends('layouts.master')

@section('title', $album->title)

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('photoalbum', $album),
        'bc_title' => "Photo Album: $album->title",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/MISC/TEAM-3 -->
    <div class="c-content-box c-size-md c-bg-grey-1">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-team-1-slider" data-slider="owl" data-items="3">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-left c-font-uppercase">{{ $album->title }} <small>{{ $album->date }}</small></h3>
                    <div class="c-line-left c-theme-bg"></div>
                </div><!-- End-->
                <div class="row">
                    @foreach($album->pictures as $pic)
                        <div class="col-md-4 col-sm-6 c-margin-b-30">
                            <div class="c-content-person-1 c-option-2">
                                <div class="c-caption c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ asset("$album->album_url/$pic->filename") }}" data-lightbox="fancybox" data-fancybox-group="gallery-church">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ asset("$album->album_url/thumbnails/$pic->filename") }}" alt="">
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold">{{ $album->title }} {{ $album->year }}</div>
                                    </div>
                                    <div class="c-position"></div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    @endforeach<!-- End-->
                </div><!-- End-->
            </div>
        </div><!-- END: CONTENT/MISC/TEAM-3 -->
    </div>
@endsection