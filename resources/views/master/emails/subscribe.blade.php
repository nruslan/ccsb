@component('mail::message-custom')
# Hello!
<p>Could you subscribe me to your newsletter, please?</p>
<p>Below you can find my email address.</p>

@component('mail::promotion')
### New Subscriber
{{ $email }}
@endcomponent

<i>Thank you and blessings!</i>
@component('mail::subcopy')
***This request came from the CCSB website, please do not reply.
@endcomponent
@endcomponent