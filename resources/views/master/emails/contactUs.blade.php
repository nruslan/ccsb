@component('mail::message-custom')
# Message from CCSB.US website
<p>This message has been sent by <b>{{ $name }}</b> from Community Church website.</p>

@component('mail::promotion')
### Message
{{ $message }}
### Reply to the message: <a href="mailto:{{ $email }}">{{ $email }}</a>
@endcomponent

<i>Thank you and blessings!</i>
@component('mail::subcopy')
***This email came from the CCSB.US website, Contact Us page.
@endcomponent
@endcomponent