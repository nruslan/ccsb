@component('mail::message-custom')
# Message from CCSB Member
<p>This message has been sent by the member of the Community Church at SaddleBrooke - <b>{{ $from_name }}</b></p>

@component('mail::promotion')
### Message
{{ $message }}
### Reply to the message: <a href="mailto:{{ $from_email }}">{{ $from_email }}</a>
@endcomponent

<i>Thank you and blessings!</i>
@component('mail::subcopy')
***This email came from the CCSB.US website.
@endcomponent
@endcomponent