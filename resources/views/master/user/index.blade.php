@extends('layouts.master')

@section('title', 'Church Directory')

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('profile', $user),
        'bc_title' => "$user->username's Profile",
        'bc_subtitle' => $user->just_members_names
    ])

    <div class="container">
        <div class="c-layout-sidebar-menu c-theme ">
            <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
            <div class="c-sidebar-menu-toggler">
                <h3 class="c-title c-font-uppercase c-font-bold">Profile</h3>
                <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                    <span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
                </a>
            </div>

            <ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
                <li class="c-dropdown c-open">
                    <a href="javascript:;" class="c-toggler">My Profile<span class="c-arrow"></span></a>
                    <ul class="c-dropdown-menu">
                        <li class="c-active">
                            <a href="shop-customer-dashboard.html">My Dashbord</a>
                        </li>
                        <li class="">
                            <a href="shop-customer-profile.html">Edit Profile</a>
                        </li>
                        <li class="">
                            <a href="shop-customer-addresses.html">My Addresses</a>
                        </li>
                    </ul>
                </li>
            </ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
        </div>
        <div class="c-layout-sidebar-content">
            @include("master.parts.under-construction")
            <!-- BEGIN: PAGE CONTENT -->
            <!-- BEGIN: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
            <div class="c-content-title-1">
                <h3 class="c-font-uppercase c-font-bold">My Dashboard</h3>
                <div class="c-line-left"></div>
                <p class="">Hello <a href="#" class="c-theme-link">{!! $user->just_members_names !!}</a>!</p>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 c-margin-b-20">
                    <h3 class="c-font-uppercase c-font-bold">{{ $user->username }}</h3>
                    <ul class="list-unstyled">
                        <li> Username: {{ $user->username }}</li>
                        <li>Email: {{ $user->email }}</li>
                        <li>Member Since: {{ $user->since_date }}</li>
                        <li>Registration date: {{ $user->registration_date }}</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <img src="{{ $user->picture_filename_url }}" class="media-object img-responsive img-thumbnail img-rounded" alt="Responsive image">
                </div>
            </div><!-- END: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
            <!-- END: PAGE CONTENT -->
        </div>
    </div>

@endsection