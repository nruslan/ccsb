@extends('layouts.master')
@section('title', "Men's Ministry")
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('mens'),
        'bc_title' => "Men's Ministry",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/BLOG/RECENT-POSTS-2 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 wow animated fadeInRight">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1 text-center">
                        <img class="img-responsive center-block" src="{{ asset('storage/logotypes/Empowerment_Logo.png') }}" alt="Empowerment">
                        <h3 class="c-center c-font-uppercase c-font-bold">Men's Ministry</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div><!-- End-->
                    <p class="c-center">Men tend to share a common fear of small group Bible studies. So, how do you break through this attitude to help men jump in? Empowerment has found a good answer that might help you or your church. We have discovered a learning and group process that motivates men to participate and unleashes their desire to play a significant role in the lives of those they love.<br>If you look at the elements in our logo, you will be able to catch our vision: Empowering Men for Christ. The methods we use help fulfill this dream and are so easy that any man our group can use them.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN: CONTENT/TABS/TAB-1 -->
    <div class="c-content-box c-size-md c-no-bottom-padding c-overflow-hide">
        <div class="c-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="c-content-tab-2 c-theme c-opt-1">
                        <ul class="nav c-tab-icon-stack c-font-sbold c-font-uppercase">
                            <li class="active">
                                <a href="#c-tab2-opt1-1" data-toggle="tab">
                                    <span class="c-content-line-icon c-icon-13"></span>
                                    <span class="c-title">Why Participate?</span>
                                </a>
                                <div class="c-arrow"></div>
                            </li>
                            <li>
                                <a href="#c-tab2-opt1-2" data-toggle="tab">
                                    <span class="c-content-line-icon c-icon-11"></span>
                                    <span class="c-title">How Empowerment Works</span>
                                </a>
                                <div class="c-arrow"></div>
                            </li>
                            <li>
                                <a href="#c-tab2-opt1-3" data-toggle="tab">
                                    <span class="c-content-line-icon c-icon-31"></span>
                                    <span class="c-title">Leadership Roles</span>
                                </a>
                                <div class="c-arrow"></div>
                            </li>
                            <li>
                                <a href="#c-tab2-opt1-4" data-toggle="tab">
                                    <span class="c-content-line-icon c-icon-48"></span>
                                    <span class="c-title">Joining a New Group</span>
                                </a>
                                <div class="c-arrow"></div>
                            </li>
                            <li>
                                <a href="#c-tab2-opt1-5" data-toggle="tab">
                                    <span class="c-content-line-icon c-icon-23"></span>
                                    <span class="c-title">S.O.A.P.</span>
                                </a>
                                <div class="c-arrow"></div>
                            </li>
                        </ul>
                        <div class="c-tab-content">
                            <div class="container">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="c-tab2-opt1-1">
                                        <div class="c-tab-pane">
                                            <img class="img-responsive" src="{{ asset("storage/ministries/men/Why-Participate.jpg") }}" alt=""/>

                                            <h4 class="c-font-30 c-font-thin c-font-uppercase c-font-bold">Why Participate?</h4>

                                            <p class="c-font-17 c-margin-b-20 c-margin-t-20 c-font-thin ">
                                                You will discover that the Empowerment approach removes your fears and places you on a path toward spiritual leadership. Yes, we said “spiritual leadership.” Deep in our hearts we realize that obligation for our marriages and families, but just don’t know where to begin. No one ever trained in this lost art. The simplicity of Empowerment will help change that for you. Here are some life-changing outcomes you will discover:
                                            <ol class="c-font-white">
                                                <li><strong>Bible study</strong>: Each week you will read and journal a chapter in the Bible with the other group members. The group setting provides accountability for the regular Bible reading you have wanted to do.</li>
                                                <li><strong>Journaling</strong>: All the group members use the same journaling method so that you can learn from each other. It is so easy anyone can participate.</li>
                                                <li><strong>Group Discussion</strong>: You will participate in group interaction about the biblical passage under study. Most men do not have a place to discuss spiritual things with other men or ask their questions.</li>
                                                <li><strong>Example</strong>: Your participation will capture the imaginations of those you love as they watch and listen to you talk about your journey. It will motivate them to follow the Lord more closely.</li>
                                                <li><strong>Outreach</strong>: Don’t be surprised if you become so excited about Empowerment that you recruit other guys to join. You will wish this joy on others.</li>
                                                <li><strong>Legacy</strong>: One day your family will cherish your journal and pour over the entries to see what you learned from the Lord that week. Your spiritual legacy will out last you!</li>
                                            </ol>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="c-tab2-opt1-2">
                                        <div class="c-tab-pane">
                                            <img class="img-responsive" src="{{ asset("storage/ministries/men/How-Empowerment-Works.jpg") }}" alt=""/>

                                            <h4 class="c-font-30 c-font-thin c-font-uppercase c-font-bold">How Empowerment Works?</h4>

                                            <p class="c-font-17 c-margin-b-20 c-margin-t-20 c-font-thin ">
                                                Empowerment uses a small group discussion model:
                                            </p>
                                            <ol class="c-font-white">
                                                <li>The groups meet each week in a home, restaurant or generic meeting room. If the group meets in a home, it is good to rotate locations to share the responsibility and to discover each man’s surroundings.</li>
                                                <li>Group sizes may vary, but the optimal discussion size is 4-9 men. Some have grown larger since everyone may not be in attendance each week. Once a group grows beyond twelve, it is suggested that they spilt. After all, we want to make room for others to discover the joy.</li>
                                                <li>Before the group gathering, each man reads the assigned biblical passage. He then selects a verse that stands out or is meaningful to him and journals about it using the S.O.A.P. method. This homework prepares each man to participate well in the group.</li>
                                                <li>During the group session, each man reads his journal to the others and shares his comments about what he discovered. This leads the men into a fuller discussion of the passage. After each man’s contribution, the discussion leader gives the men the opportunity to share a “take-away” suggesting ways they plan to apply what they learned that day.</li>
                                                <li>Often, the group session will end with men sharing some prayer requests before the closing prayer.</li>
                                                <li>Then, we go out the door to practice what we learned.</li>
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="c-tab2-opt1-3">
                                        <div class="c-tab-pane">
                                            <img class="img-responsive" src="{{ asset("storage/ministries/men/Leadership-Roles.jpg") }}" alt=""/>

                                            <h4 class="c-font-30 c-font-thin c-font-uppercase c-font-bold">Leadership Roles</h4>

                                            <p class="c-font-17 c-margin-b-20 c-margin-t-20 c-font-thin ">
                                                Since nothing happens without leadership, key leadership roles are necessary to bring Empowerment to life? Here are three critical roles in this ministry:
                                            </p>

                                            <ol class="c-font-white">
                                                <li>Group Leader: Each group has a leader to help them follow the Empowerment strategy. He is responsible for the general welfare of the group and coordinating with the pastoral team.</li>
                                                <li>Session Guide: This person leads the weekly group session. He does not take the role of a teacher; he is simply a guide. Two questions foster good group discussion:&nbsp;<br>Question 1: Who wants to go first? This opens the door for each man to share his journal entry. After this, the guide leads discussion to champion what each man shares.&nbsp;<br>Question 2: What is your take-away? Each man share what application stands out to him and how he would like to practice it in his life. Rotating the role of session guide allows men to grow in their leadership and prevents the group process from becoming stale or routine.</li>
                                                <li>Host: He welcomes the group into his home and prepares the room setting and any refreshment. It is best to separate the role of host from session guide so that the discussion is not interrupted by phone calls or hosting needs. Again, it is best if this role rotates from man to man to foster freshness.</li>
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="c-tab2-opt1-4">
                                        <div class="c-tab-pane">
                                            <img class="img-responsive" src="{{ asset("storage/ministries/men/Joining-a-Group.jpg") }}" alt=""/>

                                            <h4 class="c-font-30 c-font-thin c-font-uppercase c-font-bold">Joining a New Group</h4>

                                            <p class="c-font-17 c-margin-b-20 c-margin-t-20 c-font-thin ">
                                                Pastor Ron Gannett provides leadership to the Empowerment ministry and is anxious to help you find a group that works for you. New groups are always in the works, so contact him to get empowered. You will be glad you did!
                                            </p>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="c-tab2-opt1-5">
                                        <div class="c-tab-pane">
                                            <img class="img-responsive" src="http://img.youtube.com/vi/BzqbJqqVhQg/0.jpg" alt=""/>

                                            <h4 class="c-font-30 c-font-thin c-font-uppercase c-font-bold">Journaling and the S.O.A.P. Method</h4>

                                            <p class="c-font-17 c-margin-b-20 c-margin-t-20 c-font-thin">Empowerment groups share a common method for
                                                journaling that provides familiarity and a comfort level to the
                                                participants. Each week we get better at journaling as we learn
                                                from each other. Our journaling uses the simple S.O.A.P. method</p>
                                            <p class="c-font-17 c-margin-b-20 c-margin-t-20 c-font-thin c-font-white"><strong>S</strong> ~ Scripture: After reading the assigned passage, you choose a verse that stands out or is meaningful to you. You actually write the verse out after the letter “S.” You will discover that this is more than a manual exercise; it guides your mind and heart into greater understanding.</p>
                                            <p class="c-font-17 c-margin-b-20 c-margin-t-20 c-font-thin c-font-white"><strong>O</strong> ~ Observation: Then you record the factual observations you see in the verse. The goal is to uncover the biblical truth from the actual words in the Bible. You are simply recording what God is saying in the verse. This process is not about opinions or suggestions, but just the facts. Do you remember Dragnet? “Just the facts, ma’am.”</p>
                                            <p class="c-font-17 c-margin-b-20 c-margin-t-20 c-font-thin c-font-white"><strong>A</strong> ~ Application: If God has said what you observed in the verse, then what are the implications for your life. How could or should you apply that truth. Our desire is to put shoe leather to our faith so that it changes us. This step is critical to reposition us as followers of the Lord. The key is to be personal and practical.</p>
                                            <p class="c-font-17 c-margin-b-20 c-margin-t-20 c-font-thin c-font-white"><strong>P</strong> ~ Prayer: You conclude by writing a brief prayer of commitment based on the truth in your verse. Let the Lord know what you desire or plan to do as you walk out the door into your life.</p>
                                            <p class="c-font-17 c-margin-b-20 c-margin-t-20 c-font-thin">Just as soap cleanses the body, our prayer is that this process will help us cleanse our lives as we seek to honor God and radiate his love to others.</p>
                                            <a class="btn btn-sm c-theme-btn c-btn-square c-btn-bold aBtn" href="{{ url('media/videos/category/lecture/journaling-made-easy') }}"><i class="fa fa-play fa-fw"></i> Watch</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div><!-- END: CONTENT/TABS/TAB-1 -->
    <!-- BEGIN: EMPOWERMENT GROUPS -->
    <div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url({{ asset('storage/ministries/men/empowerment.jpg') }})">
        <div class="container">
            <div class="c-content-blog-post-card-1-slider" data-slider="owl">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Empowerment Groups</h3>
                    <div class="c-line-center c-theme-bg"></div>
                    <p class="c-font-center">Here is a current list of groups, meeting times and leaders. Feel free to contact any group leader to discover what is a good fit for you.</p>
                </div>
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-items="3" data-slide-speed="8000" data-rtl="false">
                    @foreach($groups as $group)
                        <div class="item">
                            <div class="c-content-testimonial-3 c-option-light">
                                <div class="c-content">
                                    <h3>{{ $group->name }}<br><small>{{ $group->time }}</small></h3>
                                    <ul class="fa-ul">
                                        <li><i class="fa-li fa fa-user"></i>{!! $group->group_leaders !!}</li>
                                        @if($group->location)<li><i class="fa-li fa fa-map-marker"></i>{{ $group->location ?? "---" }}</li>@endif
                                        @if($group->description)<li><i class="fa-li fa fa-book"></i>{!! $group->description ?? "---" !!}</li>@endif
                                    </ul>
                                </div>
                                @foreach($group->leaders as $leader)
                                    <div class="c-person">
                                        <img src="{{ $leader->member->profile_picture_url }}" class="img-responsive">
                                        <div class="c-person-detail c-font-uppercase">
                                            <h4 class="c-name"><a href="{{ url("directory/member/".$leader->member->userAccount->id) }}">{{ $leader->member->full_name }}</a></h4>
                                            <p class="c-position c-font-bold c-theme-font">{{ $group->name }} Group Leader</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="c-margin-t-60">
                        <a href="{{ url("happenings/calendar") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                            <i class="fa fa-fw fa-calendar" aria-hidden="true"></i> Calendar
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div><!-- END: EMPOWERMENT GROUPS -->

    <!-- BEGIN: CONTENT/BLOG/RECENT-POSTS NEWS -->
    <div class="c-content-box c-size-md c-bg-dark">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-blog-post-card-1-slider" data-slider="owl">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold c-font-white">News</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
                <!-- Begin: Owlcarousel -->
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-items="3" data-slide-speed="8000" data-rtl="false">
                    @foreach($news as $n)
                        <div class="item">
                            <div class="c-content-blog-post-card-1 c-option-2">
                                <div class="c-media c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ url("happenings/news/$n->slug") }}"><i class="icon-link"></i></a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $n->thumbnail_picture }}" alt="">
                                </div>
                                <div class="c-body">
                                    <div class="c-title c-font-uppercase c-font-bold"><a href="{{ url("happenings/news/$n->slug") }}">{!! $n->title_subtitle !!}</a></div>
                                    <div class="c-author"><span class="c-font-uppercase">{{ $n->pub_date }}</span></div>
                                    <div class="c-panel">
                                        <ul class="c-tags c-theme-ul-bg"><li>news</li></ul>
                                        <div class="c-comments"><a href="{{ url("happenings/news/$n->slug") }}"><i class="icon-emoticon-smile"></i></a></div>
                                    </div>
                                    <p>{{ $n->text_limit($n->text) }}... <a href="{{ url("happenings/news/$n->slug") }}">read more</a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div><!-- End-->
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <p class="c-margin-t-60">
                            <a href="{{ url("/happenings/news") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                                <i class="fa fa-fw fa-bullhorn" aria-hidden="true"></i> View More News
                            </a>
                        </p>
                    </div>
                </div>
            </div><!-- End-->
        </div>
    </div><!-- END: CONTENT/BLOG/RECENT-POSTS -->

    <!-- BEGIN: CONTENT/MISC/TEAM-3 -->
    <div class="c-content-box c-size-md c-bg-grey-1">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-team-1-slider" data-slider="owl" data-items="3">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Photo Albums</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <!-- End-->
                <div class="row">
                    @foreach($photoalbums as $photoalbum)
                        <div class="col-md-4 col-sm-6 c-margin-b-30">
                            <div class="c-content-person-1 c-option-2">
                                <div class="c-caption c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ $photoalbum->url }}"><i class="icon-link"></i></a>
                                            <a href="{{ $photoalbum->picture_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-church">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $photoalbum->album_thumbnail_uri }}" alt="">
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold">
                                            <a href="{{ $photoalbum->url }}">{{ $photoalbum->title }} {{ $photoalbum->year }}</a>
                                        </div>
                                    </div>
                                    <div class="c-position">{{ $photoalbum->date }}</div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                @endforeach<!-- End-->
                </div><!-- End-->
            </div>
        </div><!-- END: CONTENT/MISC/TEAM-3 -->
    </div>
@endsection