@extends('layouts.master')
@section('title', "Care Ministry")
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('care'),
        'bc_title' => "Care Ministry",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/FEATURES/FEATURES-5 -->
    <div class="c-content-box c-size-lg c-bg-grey-1">
        <div class="container">
            <div class="">
                <div class="row">
                    <div class="col-md-6">
                        <div class="c-content-feature-5">
                            <div class="c-content-title-1">
                                <h3 class="c-left c-font-dark c-font-uppercase c-font-bold">Grief<br/>Share</h3>
                                <div class="c-line-left c-bg-blue-3"></div>
                            </div>
                            <div class="c-text c-font-16 c-font-sbold c-font-uppercase">
                                Designed to help and encourage people after they suffer the loss of a spouse, child, family member or friend.
                            </div>
                            <a class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn" href="{{ url('ministries/care/grief-share') }}"><i class="fa fa-fw fa-info-circle"></i> EXPLORE</a>
                            <img class="c-photo img-responsive" style="width: 310px" src="{{ asset('storage/logotypes/grief-share.jpg') }}" alt=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="c-content-feature-5">
                            <div class="c-content-title-1">
                                <h3 class="c-left c-font-dark c-font-uppercase c-font-bold">In Touch<br/>Ministry</h3>
                                <div class="c-line-left c-theme-bg"></div>
                            </div>
                            <div class="c-text c-font-16 c-font-sbold c-font-uppercase">
                                Seeks to maintain meaningful contact with our church family during seasons of physical and spiritual need.
                            </div>
                            <a class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn" href="{{ url('ministries/care/in-touch') }}"><i class="fa fa-fw fa-info-circle"></i> EXPLORE</a>
                            <img class="c-photo img-responsive" style="width: 310px" src="{{ asset('storage/logotypes/in-touch.jpg') }}" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/FEATURES/FEATURES-5 -->
    <!-- BEGIN: CONTENT/BLOG/RECENT-POSTS NEWS -->
    <div class="c-content-box c-size-md c-bg-grey-1">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-blog-post-card-1-slider" data-slider="owl">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">News</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
                <!-- Begin: Owlcarousel -->
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-items="3" data-slide-speed="8000" data-rtl="false">
                    @foreach($news as $n)
                        <div class="item">
                            <div class="c-content-blog-post-card-1 c-option-2">
                                <div class="c-media c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ url("happenings/news/$n->slug") }}"><i class="icon-link"></i></a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $n->thumbnail_picture }}" alt="">
                                </div>
                                <div class="c-body">
                                    <div class="c-title c-font-uppercase c-font-bold"><a href="{{ url("happenings/news/$n->slug") }}">{!! $n->title_subtitle !!}</a></div>
                                    <div class="c-author"><span class="c-font-uppercase">{{ $n->pub_date }}</span></div>
                                    <div class="c-panel">
                                        <ul class="c-tags c-theme-ul-bg"><li>news</li></ul>
                                        <div class="c-comments"><a href="{{ url("happenings/news/$n->slug") }}"><i class="icon-emoticon-smile"></i></a></div>
                                    </div>
                                    <p>{{ $n->text_limit($n->text) }}... <a href="{{ url("happenings/news/$n->slug") }}">read more</a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div><!-- End-->
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <p class="c-margin-t-60">
                            <a href="{{ url("/happenings/news") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                                <i class="fa fa-fw fa-bullhorn" aria-hidden="true"></i> View More News
                            </a>
                        </p>
                    </div>
                </div>
            </div><!-- End-->
        </div>
    </div><!-- END: CONTENT/BLOG/RECENT-POSTS NEWS -->
@endsection