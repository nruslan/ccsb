@extends('layouts.master')

@section('title', 'Supported Ministries')

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('care_supported_ministry', $ministry),
        'bc_title' => "$ministry->title",
        'bc_subtitle' => "Care Ministry"
    ])

    <!-- BEGIN: CONTENT/FEATURES/FEATURES-14-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-push-4">
                    <div class="c-content-title-3 c-theme-border">
                        <h3 class="c-left c-font-uppercase">{!! $ministry->title_subtitle !!}</h3>
                        <div class="c-line c-dot c-dot-left "></div>
                        <p></p>
                    </div>
                    {!! $ministry->description !!}
                </div>
                <div class="col-md-4 col-md-pull-8">
                    <img class="img-responsive" src="{{ asset($ministry->logotype_url) }}" width="100%" alt="logotype of {{ $ministry->title }}"/>

                    <div class="c-content-ver-nav">

                    </div><!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->

                    <div class="c-content-ver-nav">
                        <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
                            <h3 class="c-font-bold c-font-uppercase">Contact Person</h3>
                            <div class="c-line-left c-theme-bg"></div>
                        </div>
                        <ul class="c-menu c-arrow-dot c-theme">
                            <li>{!! $liaison->members_pretty_name !!}</li>
                        </ul>
                    </div><!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN: LEADERSHIP -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-person-1-slider" data-slider="owl">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Leadership</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
                <!-- Begin: Owlcarousel -->
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-rtl="false" data-items="3" data-slide-speed="8000">
                    @foreach($members as $member)
                        <div class="item">
                            <div class="c-content-person-1">
                                <div class="c-caption c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ $member->profile_picture_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-1">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $member->profile_picture_url }}" alt="{{ $member->full_name }}">
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold">{{ $member->full_name }}</div>
                                    </div>
                                    <div class="c-position">
                                        Member
                                    </div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div><!-- End-->
            </div><!-- End-->
        </div>
    </div><!-- END: LEADERSHIP -->
@endsection