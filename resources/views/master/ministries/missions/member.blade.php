@extends('layouts.master')

@section('title', "About $member->full_name")

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('missions_member', $member),
        'bc_title' => "About $member->full_name",
        'bc_subtitle' => "Missions Ministry"
    ])

    <!-- BEGIN: CONTENT/FEATURES/FEATURES-14-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-push-4">
                    <div class="c-content-title-3 c-theme-border">
                        <h3 class="c-left c-font-uppercase">{{ $member->full_name }}</h3>
                        <div class="c-line c-dot c-dot-left "></div>
                        <p>{!! $member->titles_list !!} &bull; Missions Member</p>
                    </div>
                    {!! $member->about_story !!}
                </div>
                <div class="col-md-4 col-md-pull-8">
                    <img class="img-responsive" src="{{ $member->profile_picture_url }}" width="100%" alt="picture of {{ $member->full_name }}"/>

                    <div class="c-content-ver-nav">
                        <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
                            <h3 class="c-font-bold c-font-uppercase">{{ $member->full_name }}</h3>
                            <div class="c-line-left c-theme-bg"></div>
                        </div>
                        <ul class="c-menu c-arrow-dot c-theme">
                            @foreach($member->phones as $phone)
                                <li>P: {{ $phone->full_number }}</li>
                            @endforeach
                        </ul>
                    </div><!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->

                    <div class="c-content-ver-nav">
                        <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
                            <h3 class="c-font-bold c-font-uppercase">Ministries Liaison</h3>
                            <div class="c-line-left c-theme-bg"></div>
                        </div>
                        <ul class="c-menu c-arrow-dot c-theme">
                            @foreach($member->ministries as $ministry)
                                <li><a href="{{ url("ministries/missions/$ministry->slug") }}" title="{{ $ministry->title }}">{{ $ministry->title }}</a></li>
                            @endforeach
                        </ul>
                    </div><!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
                </div>
            </div>
        </div>
    </div>

@endsection