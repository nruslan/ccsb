@extends('layouts.master')
@section('title', 'Missions Ministry')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('missions'),
        'bc_title' => "Missions Ministry",
        'bc_subtitle' => "Missions in Action"
    ])

    <!-- BEGIN: CONTENT/BLOG/RECENT-POSTS-2 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 wow animated fadeInLeft">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-bold">What is Missions Ministry?<br><small>Therefore go and make disciples of all nations, baptizing them in the name of the Father and of the Son and of the Holy Spirit. - <em>Matthew 28:19</em></small></h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div><!-- End-->
                    <p class="c-center">Missions Ministry is our church’s response to the Great Commission to make Christ-like disciples in all the nations. We become the hands and feet of Jesus as we reach out to help people in need. Here at Community Church, we make it our top priority to serve others in our local Tucson area and around the world. Our financial support of missionaries in various countries, providing annually to support and contribute to Operation Christmas Child and monthly collection of food products for our local food banks are just a few ways we serve the needs of others.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: CONTENT/SLIDERS/TEAM-1 COMMITTEE -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-person-1-slider" data-slider="owl">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">{{ $ministry->name }} Committee</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
                <!-- Begin: Owlcarousel -->
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-rtl="false" data-items="3" data-slide-speed="8000">
                    @foreach($members as $member)
                        <div class="item">
                            <div class="c-content-person-1">
                                <div class="c-caption c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ url("ministries/missions/member/$member->slug") }}"><i class="icon-link"></i></a>
                                            <a href="{{ $member->profile_picture_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-1">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $member->profile_picture_url }}" alt="{{ $member->full_name }}">
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold">{{ $member->full_name }}</div>
                                    </div>
                                    <div class="c-position">
                                        Missions Committee Member
                                    </div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div><!-- End-->
            </div><!-- End-->
        </div>
    </div><!-- END: CONTENT/SLIDERS/TEAM-1 -->

    <!-- BEGIN: CONTENT/MISC/SERVICES-1 MINISTRIES -->
    <div class="c-content-box c-size-md c-bg-grey-1">
        <div class="container">
            <div class="row">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">MISSION PARTNERS</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
            </div>
            <div class="c-content-feature-2-grid" data-auto-height="true" data-mode="base-height">
                <div class="row">
                    @foreach($ministry->supportedMinistries as $sm)
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2" data-height="height">
                            <div class="c-icon-wrapper">
                                <a class="" href="{{ url("ministries/missions/$sm->slug") }}">
                                    <img src="{{ asset($sm->logotype_url) }}" alt="logo" width="60">
                                </a>
                            </div>
                            <div class="c-title c-font-bold c-font-uppercase">
                                <a href="{{ url("ministries/missions/$sm->slug") }}">{{ $sm->title }}</a>
                            </div>
                            <p>{{ $sm->subtitle }}</p>
                        </div>

                    </div>
                    @if($i++ == 4)
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-v-center c-theme-bg wow bounceInUp" data-wow-delay1="2s" data-height="height">
                            <div class="c-wrapper">
                                <div class="c-body c-padding-20 c-center">
                                    <h3 class="c-font-19 c-line-height-28 c-font-uppercase c-font-white c-font-bold">
                                        You can make a special donation to one of the supported ministries
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif($i == 10)
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-v-center c-theme-bg wow bounceInUp" data-wow-delay1="4s" data-height="height">
                            <div class="c-wrapper">
                                <div class="c-body c-padding-20 c-center">
                                    <a class="btn c-btn-border-1x c-btn-grey-1 c-btn-square c-btn-uppercase btn-lg c-btn-bold wow animate fadeIn aBtn" href="{{ url('give') }}">
                                        <i class="fa fa-fw fa-heart"></i> Donate</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/MISC/SERVICES-1 -->

    <!-- BEGIN: CONTENT/MISC/TEAM-3 PHOTOALBUM -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-team-1-slider" data-slider="owl" data-items="3">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Photo Albums</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <!-- End-->
                <div class="row">
                    @foreach($photoalbums as $photoalbum)
                        <div class="col-md-4 col-sm-6 c-margin-b-30">
                            <div class="c-content-person-1 c-option-2">
                                <div class="c-caption c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ $photoalbum->url }}"><i class="icon-link"></i></a>
                                            <a href="{{ $photoalbum->picture_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-church">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $photoalbum->album_thumbnail_uri }}" alt="">
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold">
                                            <a href="{{ $photoalbum->url }}">{{ $photoalbum->title }} {{ $photoalbum->year }}</a>
                                        </div>
                                    </div>
                                    <div class="c-position">{{ $photoalbum->date }}</div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                @endforeach<!-- End-->
                </div><!-- End-->
            </div>
        </div><!-- END: CONTENT/MISC/TEAM-3 -->
    </div><!-- END: CONTENT/MISC/TEAM-3 PHOTOALBUM -->

    <!-- BEGIN: CONTENT/BLOG/RECENT-POSTS NEWS -->
    <div class="c-content-box c-size-md c-bg-grey-1">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-blog-post-card-1-slider" data-slider="owl">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">News</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
                <!-- Begin: Owlcarousel -->
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-items="3" data-slide-speed="8000" data-rtl="false">
                    @foreach($news as $n)
                        <div class="item">
                            <div class="c-content-blog-post-card-1 c-option-2">
                                <div class="c-media c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ url("happenings/news/$n->slug") }}"><i class="icon-link"></i></a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $n->thumbnail_picture }}" alt="">
                                </div>
                                <div class="c-body">
                                    <div class="c-title c-font-uppercase c-font-bold"><a href="{{ url("happenings/news/$n->slug") }}">{!! $n->title_subtitle !!}</a></div>
                                    <div class="c-author"><span class="c-font-uppercase">{{ $n->pub_date }}</span></div>
                                    <div class="c-panel">
                                        <ul class="c-tags c-theme-ul-bg"><li>news</li></ul>
                                        <div class="c-comments"><a href="{{ url("happenings/news/$n->slug") }}"><i class="icon-emoticon-smile"></i></a></div>
                                    </div>
                                    <p>{{ $n->text_limit($n->text) }}... <a href="{{ url("happenings/news/$n->slug") }}">read more</a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div><!-- End-->
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <p class="c-margin-t-60">
                            <a href="{{ url("/happenings/news") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                                <i class="fa fa-fw fa-bullhorn" aria-hidden="true"></i> View More News
                            </a>
                        </p>
                    </div>
                </div>
            </div><!-- End-->
        </div>
    </div><!-- END: CONTENT/BLOG/RECENT-POSTS NEWS -->
@endsection