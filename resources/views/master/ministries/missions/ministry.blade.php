@extends('layouts.master')

@section('title', 'Supported Ministries')

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('supported_ministry', $ministry),
        'bc_title' => "$ministry->title",
        'bc_subtitle' => "Ministry Supported by Missions"
    ])

    <!-- BEGIN: CONTENT/FEATURES/FEATURES-14-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-push-4">
                    <div class="c-content-title-3 c-theme-border">
                        <h3 class="c-left c-font-uppercase">{!! $ministry->title_subtitle !!}</h3>
                        <div class="c-line c-dot c-dot-left "></div>
                        <p></p>
                    </div>
                    {!! $ministry->description !!}
                </div>
                <div class="col-md-4 col-md-pull-8">
                    <img class="img-responsive" src="{{ asset($ministry->logotype_url) }}" width="100%" alt="logotype of {{ $ministry->title }}"/>

                    <div class="c-content-ver-nav">
                        <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
                            <h3 class="c-font-bold c-font-uppercase">CCSB Liaison</h3>
                            <div class="c-line-left c-theme-bg"></div>
                        </div>
                        <ul class="c-menu c-arrow-dot c-theme">
                            @foreach($liaisons as $liaison)
                                <li><a href="{{ url("ministries/missions/member/$liaison->slug") }}" title="{{ $liaison->full_name }}">{{ $liaison->full_name }}</a></li>
                            @endforeach
                        </ul>
                    </div><!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
                    <!--<div class="c-content-ver-nav">
                        <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
                            <h3 class="c-font-bold c-font-uppercase">Web Resources</h3>
                            <div class="c-line-left c-theme-bg"></div>
                        </div>
                        <ul class="c-menu c-arrow-dot c-theme">
                            <li></li>
                        </ul>
                    </div>--><!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->

                    <div class="c-content-tab-1 c-theme c-margin-t-30">
                        <div class="nav-justified">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active"><a href="#blog_recent_posts" data-toggle="tab">News</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="blog_recent_posts">
                                    <ul class="c-content-recent-posts-1">
                                        @foreach($news as $news)
                                            <li>
                                                <div class="c-image">
                                                    <a href="{{ $news->url }}"><img src="{{ $news->thumbnail_picture }}" alt="image thumbnail" class="img-responsive"></a>
                                                </div>
                                                <div class="c-post">
                                                    <a href="{{ $news->url }}" class="c-title">{{ $news->title }} {{ $news->subtitle }}</a>
                                                    <p>{!! $news->text_limit($news->text) !!}... <a href="{{ $news->url }}" class="aBtn">read more <i class="fa fa-fw fa-angle-double-right"></i></a></p>
                                                    <div class="c-date c-font-bold">Published on {{ $news->pub_date }}</div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <p class="c-margin-t-40">
                                        <a href="{{ url("/happenings/news") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                                            <i class="fa fa-fw fa-bullhorn" aria-hidden="true"></i> View More News
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection