@extends('layouts.master')
@section('title', "Women's Ministry")
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('womens_ministry', $ministry),
        'bc_title' => $ministry->title,
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: ADDITIONAL WOMENS MINISTRY -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-content-blog-post-card-1-slider" data-slider="owl">
                <div class="row c-margin-b-40">
                    <div class="col-sm-10 col-sm-offset-1 wow animated fadeInLeft">
                        <!-- Begin: Title 1 component -->
                        <div class="c-content-title-1 text-center">
                            <h3 class="c-center c-font-uppercase c-font-bold">{{ $ministry->title }}</h3>
                            <div class="c-line-center c-theme-bg"></div>
                        </div><!-- End-->
                        <p class="c-center"></p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END: ADDITIONAL WOMENS MINISTRY -->
@endsection