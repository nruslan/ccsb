@extends('layouts.master')
@section('title', "Women's Ministry")
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('womens'),
        'bc_title' => "Women's Ministry",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row c-margin-b-40">
                <div class="col-sm-10 col-sm-offset-1 wow animated fadeInLeft">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1 text-center">
                        <img src="{{ asset('storage/logotypes/ladies_ministry_logo.png') }}" alt="Women's Ministry">
                        <h3 class="c-center c-font-uppercase c-font-bold">Women Encouraging Women</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div><!-- End-->
                    <p class="c-center">The purpose of the Women’s Ministry of Community Church at SaddleBrooke is to provide avenues for spiritual growth and opportunities to minister inside and outside of the church body.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: BIBLE STUDIES -->
    <div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url({{ asset('storage/backgrounds/bg-bible.jpg') }})">
        <div class="container">
            @if($bibleStudies->count())
                <div class="c-content-blog-post-card-1-slider" data-slider="owl">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Bible Studies</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-items="3" data-slide-speed="8000" data-rtl="false">
                    @foreach($bibleStudies as $event)
                        @foreach($event->dates as $date)
                            <div class="item">
                                <div class="c-content-testimonial-3 c-option-light">
                                    <div class="c-content">
                                        <h3>{{ $event->title }}<br><small>{{ $event->subtitle }}</small></h3>
                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-calendar"></i> {{ $date->date }}</li>
                                            <li><i class="fa-li fa fa-map-marker"></i> {{ $date->location_name }}</li>
                                            @if($event->price)<li><i class="fa-li fa fa-money"></i> {{ $event->price_description }}</li>@endif
                                        </ul>
                                    </div>
                                    @if($event->main_member)
                                        <div class="c-person">
                                            <img src="{{ $event->main_member->profile_picture_url }}" class="img-responsive">
                                            <div class="c-person-detail c-font-uppercase">
                                                <h4 class="c-name">{{ $event->main_member->full_name }}</h4>
                                                <p class="c-position c-font-bold c-theme-font">{{ $event->main_member->pivot->title }}</p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
                </div>
            @endif
        </div>
    </div><!-- END: BIBLE STUDIES -->

    @if($retreats->count())
    <!-- BEGIN: RETREATS -->
    <div id="feature-15-2" class="c-content-feature-15 c-bg-img-center c-bg-parallax" style="background-image: url({{ asset('storage/backgrounds/sb-bg-top.jpg') }})">
        <div class="container">
            @foreach($retreats as $retreat)
                @foreach($retreat->dates as $date)
                <div class="row">
                    <div class="col-md-offset-4 col-md-8 col-xs-12">
                        <div class="c-feature-15-container c-bg-white">
                            <h2 class="c-feature-15-title c-font-bold c-font-uppercase c-theme-border c-font-dark">
                                <span class="c-theme-font">2018 {{ $retreat->title }}:</span> {{ $retreat->subtitle }}
                            </h2>
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <img src="{{ asset("storage/ministries/women/Sue-Heimer.jpg") }}" class="c-feature-15-img"/>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="c-feature-15-desc c-font-dark">
                                        {{ $date->date }}<br>
                                        {{ $date->location_name }} &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; $65<br><br>
                                        {!! $retreat->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            @endforeach
        </div>
    </div><!-- END: RETREATS -->
    @endif
    <!-- BEGIN: LEADERSHIP -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-person-1-slider" data-slider="owl">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Leadership</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
                <!-- Begin: Owlcarousel -->
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-rtl="false" data-items="3" data-slide-speed="8000">
                    @foreach($members as $member)
                        <div class="item">
                            <div class="c-content-person-1">
                                <div class="c-caption c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ url("ministries/missions/member/$member->slug") }}"><i class="icon-link"></i></a>
                                            <a href="{{ $member->profile_picture_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-1">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $member->profile_picture_url }}" alt="{{ $member->full_name }}">
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold">{{ $member->full_name }}</div>
                                    </div>
                                    <div class="c-position">
                                        Member
                                    </div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div><!-- End-->
            </div><!-- End-->
        </div>
    </div><!-- END: LEADERSHIP -->
    <!-- BEGIN: MINISTRIES -->
    <div class="c-content-box c-size-md c-bg-grey-1">
        <div class="container">
            <div class="row">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Ministries</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
            </div>
            <div class="c-content-feature-2-grid" data-auto-height="true" data-mode="base-height">
                <div class="row">
                    @foreach($ministry->supportedMinistries as $sm)
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2" data-height="height">
                                <div class="c-icon-wrapper">
                                    <a class="" href="{{ url("ministries/women/$sm->slug") }}">
                                        <img src="{{ asset($sm->logotype_url) }}" alt="logo" width="60">
                                    </a>
                                </div>
                                <div class="c-title c-font-bold c-font-uppercase">
                                    <a href="{{ url("ministries/women/$sm->slug") }}">{{ $sm->title }}</a>
                                </div>
                                <p>{{ $sm->subtitle }}</p>
                            </div>

                        </div>
                        @if($i++ == 4)
                            <div class="col-md-4 col-sm-6">
                                <div class="c-content-v-center c-theme-bg wow bounceInUp" data-wow-delay1="2s" data-height="height">
                                    <div class="c-wrapper">
                                        <div class="c-body c-padding-20 c-center">
                                            <h3 class="c-font-19 c-line-height-28 c-font-uppercase c-font-white c-font-bold">
                                                Women Encouraging Women<br>Join One of Our Ministries!
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div><!-- END: MINISTRIES -->
    <!-- BEGIN: CONTENT/BLOG/RECENT-POSTS NEWS -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-blog-post-card-1-slider" data-slider="owl">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">News</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
                <!-- Begin: Owlcarousel -->
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-items="3" data-slide-speed="8000" data-rtl="false">
                    @foreach($news as $n)
                        <div class="item">
                            <div class="c-content-blog-post-card-1 c-option-2">
                                <div class="c-media c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ url("happenings/news/$n->slug") }}"><i class="icon-link"></i></a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $n->thumbnail_picture }}" alt="">
                                </div>
                                <div class="c-body">
                                    <div class="c-title c-font-uppercase c-font-bold"><a href="{{ url("happenings/news/$n->slug") }}">{!! $n->title_subtitle !!}</a></div>
                                    <div class="c-author"><span class="c-font-uppercase">{{ $n->pub_date }}</span></div>
                                    <div class="c-panel">
                                        <ul class="c-tags c-theme-ul-bg"><li>news</li></ul>
                                        <div class="c-comments"><a href="{{ url("happenings/news/$n->slug") }}"><i class="icon-emoticon-smile"></i></a></div>
                                    </div>
                                    <p>{{ $n->text_limit($n->text) }}... <a href="{{ url("happenings/news/$n->slug") }}">read more</a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div><!-- End-->
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <p class="c-margin-t-60">
                            <a href="{{ url("/happenings/news") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                                <i class="fa fa-fw fa-bullhorn" aria-hidden="true"></i> View More News
                            </a>
                        </p>
                    </div>
                </div>
            </div><!-- End-->
        </div>
    </div><!-- END: CONTENT/BLOG/RECENT-POSTS -->
    <!-- BEGIN: PHOTOALBUMS -->
    <div class="c-content-box c-size-md c-bg-grey-1">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-team-1-slider" data-slider="owl" data-items="3">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Photo Albums</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <!-- End-->
                <div class="row">
                    @foreach($photoalbums as $photoalbum)
                        <div class="col-md-4 col-sm-6 c-margin-b-30">
                            <div class="c-content-person-1 c-option-2">
                                <div class="c-caption c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ $photoalbum->url }}"><i class="icon-link"></i></a>
                                            <a href="{{ $photoalbum->picture_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-church">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $photoalbum->album_thumbnail_uri }}" alt="">
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold">
                                            <a href="{{ $photoalbum->url }}">{{ $photoalbum->title }} {{ $photoalbum->year }}</a>
                                        </div>
                                    </div>
                                    <div class="c-position">{{ $photoalbum->date }}</div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                @endforeach<!-- End-->
                </div><!-- End-->
            </div>
        </div><!-- END: CONTENT/MISC/TEAM-3 -->
    </div><!-- END: PHOTOALBUMS -->
@endsection