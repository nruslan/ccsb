@extends('layouts.master')
@section('title', 'Ministries')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('ministries'),
        'bc_title' => "Ministries",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/FEATURES/FEATURES-5 -->
    <div class="c-content-box c-size-lg c-bg-grey-1">
        <div class="container">
            <div class="row c-margin-b-60">
                @foreach($ministries as $ministry)
                <div class="col-md-6">
                    <div class="c-content-feature-5">
                        <div class="c-content-title-1">
                            <h3 class="c-left c-font-dark c-font-uppercase c-font-bold">{{ $ministry->title }}<br>ministry</h3>
                            <div class="c-line-left c-bg-blue-3"></div>
                        </div>
                        <div class="c-text c-font-16 c-font-sbold c-font-uppercase">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, saepe.</div>
                        <a class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn" href="{{ url("ministries/$ministry->slug") }}"><i class="fa fa-fw fa-info-circle"></i> EXPLORE</a>
                        <img class="c-photo img-responsive" style="width: 310px" src="{{ asset('storage/logotypes/grief-share.jpg') }}" alt=""/>
                    </div>
                </div>
                @if($i++ % 2 == 0)</div><div class="row">@endif
                @endforeach
            </div>
        </div>
    </div><!-- END: CONTENT/FEATURES/FEATURES-5 -->
@endsection