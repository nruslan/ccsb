@extends('layouts.master')

@section('title', 'Community Church at SaddleBrooke')

@section('content')
<!-- BEGIN: LAYOUT/SLIDERS/REVO-SLIDER-13 -->
<section class="c-layout-revo-slider c-layout-revo-slider-13" dir="ltr">
    <div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile c-arrow-darken" data-bullets-pos="center">
        <div class="tp-banner rev_slider" data-version="5.0">
            <ul>
                <!--BEGIN: SLIDE #1 -->
                <li data-index="rs-16" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="{{ asset('storage/slider/slide4.jpg') }}"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Welcome to Our Church" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('storage/slider/slide4.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-0 c-font-white c-font-bold"
                         id="slide-16-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['70','70','70','45']"
                         data-lineheight="['70','70','70','50']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"

                         data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="1000"
                         data-splitin="chars"
                         data-splitout="none"
                         data-responsive_offset="on"

                         data-elementdelay="0.05"

                         style="z-index: 5; white-space: nowrap;">COMMUNITY CHURCH<br>at SADDLEBROOKE
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-0 c-font-white"
                         id="slide-16-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"

                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="1500"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"

                         style="z-index: 6; white-space: nowrap;">
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0"
                         id="slide-16-layer-8"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-style_hover="cursor:default;"

                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="2000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"

                         style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                    </div>
                </li>
                <!--END -->
                <!-- BEGIN: SLIDE #2 -->
                <li data-index="rs-17" data-transition="fadetotopfadefrombottom" data-slotamount="default"  data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500"  data-thumb="{{ asset('storage/slider/slide2.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="A Community of Grace" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('storage/slider/slide2.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-3 c-font-white c-font-bold"
                         id="slide-17-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['70','70','70','45']"
                         data-lineheight="['70','70','70','50']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"

                         data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:0px;"
                         data-mask_out="x:inherit;y:inherit;"
                         data-start="1000"
                         data-splitin="chars"
                         data-splitout="none"
                         data-responsive_offset="on"

                         data-elementdelay="0.05"

                         style="z-index: 5; white-space: nowrap;">A COMMUNITY of GRACE
                    </div>
                </li>
                <!-- END -->
                <!-- SLIDE #3  -->
                <li data-index="rs-19" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="{{ asset('storage/slider/slider001.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Receive, Live and Share" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('storage/slider/slider001.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- BACKGROUND VIDEO LAYER -->
                    <div class="rs-background-video-layer"
                         data-forcerewind="on"
                         data-volume="mute"
                         data-videowidth="100%"
                         data-videoheight="100%"
                         data-videomp4="{{ asset('storage/slider/video-3.mov') }}"
                         data-videopreload="preload"
                         data-videoloop="none"
                         data-forceCover="1"
                         data-aspectratio="16:9"
                         data-autoplay="true"
                         data-autoplayonlyfirsttime="false"
                         data-nextslideatend="true"></div>
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-shape tp-shapewrapper   rs-parallaxlevel-0"
                         id="slide-19-layer-10"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"

                         data-transform_in="opacity:0;s:2000;e:Power3.easeInOut;"
                         data-transform_out="opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-start="2000"
                         data-basealign="slide"
                         data-responsive_offset="on"
                         data-responsive="off"

                         style="z-index: 5;background-color:rgba(0, 0, 0, 0.25);border-color:rgba(0, 0, 0, 0);">
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-0 c-font-white c-font-bold"
                         id="slide-19-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['70','70','70','45']"
                         data-lineheight="['70','70','70','50']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"

                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="1000"
                         data-splitin="chars"
                         data-splitout="none"
                         data-responsive_offset="on"

                         data-elementdelay="0.05"

                         style="z-index: 6; white-space: nowrap;">Receive, Live and Share<br>the love of Jesus Christ.
                    </div>
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-0 c-font-white"
                         id="slide-19-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"

                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="1500"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"

                         style="z-index: 7; white-space: nowrap;">
                    </div>
                </li>
                <!-- END -->
                <!-- SLIDE #4  -->
                <li data-index="rs-20" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="{{ asset('storage/slider/slide1-2.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Join Us" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('storage/slider/slide1-2.jpg') }}"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="15000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-0 c-font-white c-font-bold"
                         id="slide-20-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['70','70','70','45']"
                         data-lineheight="['70','70','70','50']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"

                         data-transform_in="x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="1000"
                         data-splitin="chars"
                         data-splitout="none"
                         data-responsive_offset="on"

                         data-elementdelay="0.1"

                         style="z-index: 5; white-space: nowrap;">Join us on our journey<br>towards Jesus
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-0 c-font-white"
                         id="slide-20-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"

                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="1500"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"

                         style="z-index: 6; white-space: nowrap;">
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section><!-- END: LAYOUT/SLIDERS/REVO-SLIDER-13 -->

<!-- BEGIN: CONTENT/FEATURES/FEATURES-1 -->
<div class="c-content-box c-size-md c-bg-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 text-center">
                <div class="c-content-feature-1 wow animate fadeInUp">
                    <div class="c-content-line-icon c-theme c-icon-31"></div>
                    <h3 class="c-font-uppercase c-font-bold">Mission and Vision</h3>
                    <p class="c-font-thin">The mission of Community Church at SaddleBrooke is to provide varied opportunities for people to establish and develop a spiritual faith in Jesus Christ.</p>
                </div>
            </div>
            <div class="col-sm-4 text-center">
                <div class="c-content-feature-1 wow animate fadeInUp" data-wow-delay="0.2s">
                    <div class="c-content-line-icon c-theme c-icon-10"></div>
                    <h3 class="c-font-uppercase c-font-bold">Our Beliefs</h3>
                    <p class="c-font-thin">We believe in the Triune God, acknowledge Jesus Christ as Lord and Savior, and accept the Holy Scriptures as the only norm and rule of faith and practice.</p>
                </div>
            </div>
            <div class="col-sm-4  text-center c-card">
                <div class="c-content-feature-1 wow animate fadeInUp" data-wow-delay="0.4s">
                    <div class="c-content-line-icon c-theme c-icon-29"></div>
                    <h3 class="c-font-uppercase c-font-bold">Hope and Dream</h3>
                    <p class="c-font-thin">The dream of Community Church is to glorify God by becoming a community of grace where people will receive, live and share the love of Jesus Christ.</p>
                </div>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/FEATURES/FEATURES-1 -->

<!-- BEGIN: CONTENT/FEATURES/FEATURES-12 WELCOME -->
<div class="c-content-box c-size-md c-bg-parallax" style="background-image: url({{ asset('storage/backgrounds/sb-bg-top.jpg') }})">
    <div class="container">
        <div class="c-content-feature-8 c-opt-2">
            <ul class="c-grid">
                <li>
                    <div class="c-card c-font-right c-bg-opacity-2">
                        <h3 class="c-font-40 c-font-bold c-font-uppercase"><span class="c-theme-font">WELCOME</span> TO COMMUNITY CHURCH AT SADDLEBROOKE</h3>
                        <p class="c-font-18">
                            We invite you to join us on our journey toward Jesus.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="c-card c-bg-opacity-2">
                        <h3 class="c-font-40 c-font-bold c-font-uppercase">Come and see <br/>what <span class="c-theme-font">God</span> is doing<br/>at our Church!</h3>
                        <p class="c-font-18">
                            God has an amazing plan for each of us.
                        </p>
                    </div>
                </li>
            </ul>
            <ul class="c-grid">
                <li>
                    <div class="c-card c-font-right c-bg-opacity-2">
                        <h3 class="c-font-40 c-font-bold c-font-uppercase">Join us in this exciting <span class="c-theme-font">adventure</span> with God</h3>
                        <p class="c-font-18">
                            We have a great group of people!
                        </p>
                    </div>
                </li>
                <li>
                    <iframe width="100%" height="335" src="{{ $welcomingVideo->video_url }}" frameborder="0" allowfullscreen></iframe>
                </li>
            </ul>
        </div>
    </div>
</div><!-- END: CONTENT/FEATURES/FEATURES-12 -->

<!-- BEGIN: CONTENT/BLOG/RECENT-POSTS NEWS EVENTS -->
<div class="c-content-box c-size-md c-bg-grey-1">
    <div class="container">
        <!-- Begin: Testimonals 1 component -->
        <div class="c-content-blog-post-card-1-slider" data-slider="owl">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Happenings</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->
        @if(count($news) > 3)
            <!-- Begin: Owlcarousel -->
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-items="3" data-slide-speed="8000" data-rtl="false">
                    @foreach($news as $n)
                        <div class="item">
                            <div class="c-content-blog-post-card-1 c-option-2">
                                <div class="c-media c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ url("happenings/news/$n->slug") }}"><i class="icon-link"></i></a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $n->thumbnail_picture }}" alt="">
                                </div>
                                <div class="c-body">
                                    <div class="c-title c-font-uppercase c-font-bold">
                                        <a href="{{ url("happenings/news/$n->slug") }}">{!! $n->title_subtitle !!}</a>
                                    </div>
                                    <div class="c-author">
                                        <span class="c-font-uppercase">{{ $n->pub_date }}</span>
                                    </div>

                                    <div class="c-panel">
                                        <ul class="c-tags c-theme-ul-bg">
                                            <li>news</li>
                                        </ul>
                                        <div class="c-comments"><a href="{{ url("happenings/news/$n->slug") }}"><i class="icon-emoticon-smile"></i></a></div>
                                    </div>
                                    <p>{{ $n->text_limit($n->text) }}... <a href="{{ url("happenings/news/$n->slug") }}">read more</a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- End-->
            @endif
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="c-margin-t-60">
                        <a href="{{ url("/happenings") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                            <i class="fa fa-fw fa-bullhorn" aria-hidden="true"></i> View More
                        </a>
                    </p>
                </div>
            </div>

        </div>
        <!-- End-->
    </div>
</div><!-- END: CONTENT/BLOG/RECENT-POSTS -->

<!-- BEGIN: CONTENT/MISC/SERVICES-4 WEEKLY VIDEO -->
<div class="c-content-box c-size-md c-bg-img-top c-bg-parallax" style="background-image: url({{ asset('storage/backgrounds/sb-bg-top.jpg') }})">
    <div class="container">
        <div class="c-content-feature-11">
            <div class="c-content-title-1">
                <h3 class="c-left c-font-dark c-font-uppercase c-font-bold">Weekly Video</h3>
                <div class="c-line-left c-theme-bg"></div>
            </div>
            <div class="row">
                <div class="col-md-6 c-grid" data-auto-height="true" data-related="#c-video-card-3">
                    <ul class="c-grid-row">
                        <li>
                            <div class="c-bg-opacity-1 c-card" data-height="height">
                                <h3 class="c-font-uppercase c-font-bold">Title</h3>
                                <p>{{ $weeklyVideo->title }}</p>
                            </div>
                        </li>
                        <li>
                            <div class="c-bg-opacity-1 c-card" data-height="height">
                                <h3 class="c-font-uppercase c-font-bold"><i class="fa fa-calendar fa-fw"></i> Date</h3>
                                <p>{{ $weeklyVideo->text_date }}</p>
                            </div>
                        </li>
                    </ul>
                    <ul class="c-grid-row">
                        <li>
                            <div class="c-bg-opacity-1 c-card" data-height="height">
                                <h3 class="c-font-uppercase c-font-bold">Pastor</h3>
                                <p>{{ $weeklyVideo->video_members }}</p>
                            </div>
                        </li>
                        <li>
                            <div class="c-bg-opacity-1 c-card" data-height="height">
                                <h3 class="c-font-uppercase c-font-bold">Watch More</h3>
                                <p>
                                    <a href="{{ url("/media/videos") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                                        <i class="fa fa-fw fa-play" aria-hidden="true"></i> Videos
                                    </a>
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 c-video">
                    <div id="c-video-card-3" class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{ $weeklyVideo->video_url }}" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/MISC/SERVICES-4 -->

<!-- BEGIN: Sermons -->

<!-- BEGIN: CONTENT/TILES/TILE-3 GIVING -->

<!-- BEGIN: CONTENT/MISC/TEAM-3 PHOTOALBUMS -->
<div class="c-content-box c-size-md c-bg-grey-1">
    <div class="container">
        <!-- Begin: Testimonals 1 component -->
        <div class="c-content-team-1-slider" data-slider="owl" data-items="3">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Photo Albums</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->
            <div class="row">
                @foreach($latestPics as $pic)
                <div class="col-md-4 col-sm-6 c-margin-b-30">
                    <div class="c-content-person-1 c-option-2">
                        <div class="c-caption c-content-overlay">
                            <div class="c-overlay-wrapper">
                                <div class="c-overlay-content">
                                    <a href="{{ $pic->photoalbum->url }}"><i class="icon-link"></i></a>
                                    <a href="{{ $pic->picture_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-church">
                                        <i class="icon-magnifier"></i>
                                    </a>
                                </div>
                            </div>
                            <img class="c-overlay-object img-responsive" src="{{ $pic->picture_thumb_url }}" alt="">
                        </div>
                        <div class="c-body">
                            <div class="c-head">
                                <div class="c-name c-font-uppercase c-font-bold">{{ $pic->photoalbum->title }} {{ $pic->photoalbum->year }}</div>
                            </div>
                            <div class="c-position"></div>
                            <p></p>
                        </div>
                    </div>
                </div>
                @endforeach<!-- End-->
            </div><!-- End-->
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="c-margin-t-60">
                        <a href="{{ url("/photoalbums") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                            <i class="fa fa-fw fa-play-circle" aria-hidden="true"></i> More Photos
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/MISC/TEAM-3 -->
</div>

<!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-3 WE BELIEVE -->
<div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url({{ asset('storage/backgrounds/bg-17.jpg') }})">
    <div class="container">
        <!-- Begin: testimonials 1 component -->
        <div class="c-content-testimonials-1" data-slider="owl">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-white c-font-uppercase c-font-bold">We Believe</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->
            <div class="c-testimonial">
                <p class="c-margin-b-30 c-left">
                    We believe that Jesus gives us an invitation to live life to the fullest. So we invite you to join us on our journey toward Jesus. We believe He really lived, really died, and really rose from the dead on the 3rd day. God has an amazing plan for each of us that both glorifies Him and gives us peace and joy. We are not so concerned with where you have been but more with where you are going!
                </p>
            </div>
        </div>
        <!-- End-->
    </div>
</div><!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-3 -->
@endsection