<!-- BEGIN: CONTENT/SLIDERS/TEAM-1 NEW MEMBERS -->
<div class="c-content-box c-size-md c-bg-white">
    <div class="container">
        <!-- Begin: Testimonals 1 component -->
        <div class="c-content-person-1-slider" data-slider="owl">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Welcome, New Members!</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            <!-- End-->

            <!-- Begin: Owlcarousel -->
            <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-rtl="false" data-items="3" data-slide-speed="8000">
                @foreach($users as $user)
                    <div class="item">
                        <div class="c-content-person-1">
                            <div class="c-caption c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="#"><i class="icon-link"></i></a>
                                        <a href="{{ asset($user->picture_filename_url) }}" data-lightbox="fancybox" data-fancybox-group="gallery-1">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </div>
                                </div>
                                <img class="c-overlay-object img-responsive" src="{{ asset($user->picture_filename_url) }}" alt="">
                            </div>
                            <div class="c-body">
                                <div class="c-head">
                                    <div class="c-name c-font-uppercase c-font-bold">{{ $user->members_pretty_name }}</div>
                                </div>
                                <div class="c-position">{{ $user->just_members_names }}</div>
                                <p></p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- End-->
        </div>
        <!-- End-->
    </div>
</div><!-- END: CONTENT/SLIDERS/TEAM-1 -->