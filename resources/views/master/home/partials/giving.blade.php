<!-- BEGIN: CONTENT/TILES/TILE-3 GIVING -->
<div class="c-content-box c-size-md c-bg-white" id="giving">
    <div class="c-content-tile-grid c-bs-grid-reset-space" data-auto-height="true">
        <div class="c-content-title-1 wow animate fadeInDown">
            <h3 class="c-font-uppercase c-center c-font-bold">Giving<br><small>Using our resources to further God's kingdom</small></h3>
            <div class="c-line-center"></div>
        </div>
        <div class="row wow animate fadeInUp">
            <div class="col-md-6">
                <div class="c-content-tile-1 c-bg-green">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="c-tile-content c-content-v-center" data-height="height">
                                <div class="c-wrapper">
                                    <div class="c-body c-center">
                                        <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white">Give In-Person</h3>
                                        <p class="c-tile-body c-font-white">Simply place cash or checks in the offering plate during the collection at any weekend service.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="c-tile-content c-arrow-right c-arrow-green c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content c-font-white">
                                        GIVE IN-PERSON
                                    </div>
                                </div>
                                <div class="c-image c-overlay-object" data-height="height" style="background-image: url({{ asset('storage/backgrounds/in-person.jpg') }})"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="c-content-tile-1 c-bg-brown-2">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="c-tile-content c-content-v-center" data-height="height">
                                <div class="c-wrapper">
                                    <div class="c-body c-center">
                                        <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white">
                                            Give Online
                                        </h3>
                                        <p class="c-tile-body c-font-white">It allows for a specific dollar amount to be deducted automatically from your Credit Card, a checking and or savings account.</p>
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="28MXNJTDY2VV6">
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="c-tile-content c-arrow-right c-arrow-brown-2 c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" title="Give Online">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="28MXNJTDY2VV6">
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                        </form>
                                    </div>
                                </div>
                                <div class="c-image c-overlay-object" data-height="height" style="background-image: url({{ asset('storage/backgrounds/give-online.jpg') }})"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="c-content-tile-1 c-bg-red-2">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="c-tile-content c-arrow-left c-arrow-red-2 c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content c-font-white">
                                        GIVE BY MAIL
                                    </div>
                                </div>
                                <div class="c-image c-overlay-object" data-height="height" style="background-image: url({{ asset('storage/backgrounds/by-mail.jpg') }})"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="c-tile-content c-content-v-center" data-height="height">
                                <div class="c-wrapper">
                                    <div class="c-body c-center">
                                        <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white">Give by Mail</h3>
                                        <p class="c-tile-body c-font-white">Mail your donation to <abbr title="Community Church at SaddleBrooke">CCSB</abbr> at<br>
                                            36768 S. Aaron Lane<br>Tucson, AZ 85739.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="c-content-tile-1 c-bg-blue-3">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="c-tile-content c-arrow-left c-arrow-blue-3 c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="https://smile.amazon.com/ch/86-0979306" target="_blank" title="Give with AmazonSmile">
                                            <i class="fa fa-amazon" aria-hidden="true"></i> </a>
                                    </div>
                                </div>
                                <div class="c-image c-overlay-object" data-height="height" style="background-image: url({{ asset('storage/backgrounds/amazon-smile.jpg') }})"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="c-tile-content c-content-v-center" data-height="height">
                                <div class="c-wrapper">
                                    <div class="c-body c-center">
                                        <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white">
                                            Give with AmazonSmile
                                        </h3>
                                        <p class="c-tile-body c-font-white">Shop at AmazonSmile and Amazon will make a donation to Community Church at SaddleBrooke.</p>
                                        <a href="https://smile.amazon.com/ch/86-0979306" target="_blank" title="Give with AmazonSmile" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Give</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/TILES/TILE-3 -->