<!-- BEGIN: CONTENT/FEATURES/FEATURES-5 SERMONS -->
<div class="c-content-box c-size-lg c-bg-grey-1">
    <div class="container-fluid">
        <div class="c-content-title-1">
            <h3 class="c-center c-font-dark c-font-uppercase c-font-bold">Latest Sermons</h3>
            <div class="c-line-center c-theme-bg"></div>
        </div>
        <div class="row">
            @foreach($latestSermons as $latestSermon)
                <div class="col-md-4">
                    <div class="c-content-feature-5">
                        <div class="c-content-title-1">
                            <h3 class="c-left c-font-dark c-font-uppercase c-font-bold"><span class="c-theme-font">Latest</span><br/>Sermon #{{ $i++ }}</h3>
                            <div class="c-line-left c-theme-bg"></div>
                        </div>
                        <div class="c-text c-font-16 c-font-sbold c-font-uppercase">
                            {{ $latestSermon->title }}<br>
                            <p class="c-font-12">by {{ $latestSermon->sermon_pastors }}<br/>
                                <i class="fa fa-calendar" aria-hidden="true"></i> {{ $latestSermon->text_date }}
                            </p>
                        </div>
                        <ul class="list-inline">
                            <li>
                                <a href="{{ url("/media/sermons/$latestSermon->slug") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                                    <i class="fa fa-fw fa-headphones" aria-hidden="true"></i> Listen
                                </a>
                            </li>
                        </ul>
                        <img class="c-photo img-responsive" style="width: 310px" src="{{ asset($latestSermon->thumbnail) }}" alt="Bible with Headphones"/>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <p class="c-margin-t-60">
                    <a href="{{ url("/media/sermons") }}" class="btn c-btn-uppercase btn-md c-btn-sbold c-btn-square c-theme-btn aBtn">
                        <i class="fa fa-fw fa-play-circle" aria-hidden="true"></i> More Sermons
                    </a>
                </p>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/FEATURES/FEATURES-5 SERMONS -->