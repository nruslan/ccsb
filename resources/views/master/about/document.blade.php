@extends('layouts.master')

@section('title', "$doc->title")

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('bylaws'),
        'bc_title' => "$doc->title",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/SLIDERS/TEAM-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <!-- Begin: Title 1 component -->
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">BYLAWS of Community Church at SaddleBrooke</h3>
                <div class="c-line-center c-theme-bg"></div>
            </div>
            {!! $doc->description !!}
            <iframe src="{{ asset("storage/documents/$doc->year/$doc->filename") }}" width="100%" height="750px" frameborder="0"></iframe>
        </div>
    </div>
@endsection