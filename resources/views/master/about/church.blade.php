@extends('layouts.master')

@section('title', 'About')

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('church'),
        'bc_title' => "About",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/MISC/ABOUT-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 wow animated fadeInLeft">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-bold">Our Mission and Vision</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div><!-- End-->
                    <p class="c-center">The mission of Community Church at SaddleBrooke is to provide varied opportunities for people to establish and develop a spiritual faith in Jesus Christ. We are a church that is open and inclusive, caring and sharing, serving the spiritual needs of SaddleBrooke and the larger community, deploying our talents and gifts.</p>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 wow animated fadeInRight">
                    <div class="c-content-client-logos-1">
                        <!-- Begin: Title 1 component -->
                        <div class="c-content-title-1">
                            <h3 class="c-center c-font-uppercase c-font-bold">Our Beliefs</h3>
                            <div class="c-line-center c-theme-bg"></div>
                        </div><!-- End-->
                        <p class="c-center">We believe in the Triune God, acknowledge Jesus Christ as Lord and Savior, and accept the Holy Scriptures as the only norm and rule of faith and practice. A fuller description may be found in the bylaws and creeds.</p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/MISC/ABOUT-1 -->

    <!-- BEGIN: FEATURES 13.4 -->
    <div class="c-content-box c-size-md c-no-padding c-bg-img-center" style="background-image: url({{ asset('storage/backgrounds/cross-bg.jpg') }})">
        <div class="c-content-feature-13">
            <div class="row c-reset">
                <div class="col-md-6 col-md-offset-6 c-bg-grey-1">
                    <div class="c-feature-13-container">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold">What Our Beliefs <span class="c-theme-font">Mean</span></h3>
                            <div class="c-line-left c-theme-bg"></div>
                        </div>
                        <div class="row">
                            <div class="c-feature-13-tile">
                                <i class="icon-user c-theme-font c-font-24"></i>
                                <div class="c-feature-13-content">
                                    <h4 class="c-font-uppercase c-theme-font c-font-bold">Christ is Our Savior</h4>
                                    <p class="c-font-dark">We seek to help people believe in Christ as their Savior and then encourage them to mature in their walk with God.</p>
                                </div>
                            </div>
                            <div class="c-feature-13-tile">
                                <i class="icon-eye c-theme-font c-font-24"></i>
                                <div class="c-feature-13-content">
                                    <h4 class="c-font-uppercase c-theme-font c-font-bold">Believers from all Denominations</h4>
                                    <p class="c-font-dark">We welcome believers from various church backgrounds who share this faith from all denominations.</p>
                                </div>
                            </div>
                            <div class="c-feature-13-tile">
                                <i class="icon-book-open c-theme-font c-font-24"></i>
                                <div class="c-feature-13-content">
                                    <h4 class="c-font-uppercase c-theme-font c-font-bold">Bible as God’s Authority</h4>
                                    <p class="c-font-dark">We view the Bible as God’s authority to guide us in our teaching and practice.</p>
                                </div>
                            </div>
                            <div class="c-feature-13-tile">
                                <i class="icon-heart c-theme-font c-font-24"></i>
                                <div class="c-feature-13-content">
                                    <h4 class="c-font-uppercase c-theme-font c-font-bold">Love of Christ</h4>
                                    <p class="c-font-dark">Our calling is to take the love of Christ to our community and world so that others believe in Jesus Christ as Savior and Lord.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END: FEATURES 13-4 -->

    <!-- BEGIN: CONTENT/MISC/ABOUT-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow animated fadeInLeft">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-bold">Our History and Life Together</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div><!-- End-->
                    <p class="c-center">Community Church at SaddleBrooke was founded in 2000 to provide a worship experience for the families in the community. The fellowship has always had an “all denominations” flavor welcoming people from a variety of backgrounds who share a common faith. The church has enjoyed leadership from the board of directors who provide governance to the church and pastors who serve the spiritual needs of the congregation.</p>
                    <p class="c-center">Our congregation elects the leaders and then grants them freedom to lead us in doing God’s work. Throughout the years a variety of ministries have been developed within the church to foster fellowship and spiritual growth. The church as always been known for its generous missions activity in the community and around the world. The combination of these traits has enabled Community Church to honor the Lord in fresh ways through the years.</p>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12 wow animated fadeInLeft">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-bold">Committees and Bylaws</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div><!-- End-->
                    <p class="c-center">Community Church at SaddleBrooke has a number of <a href="{{ url('about/committees') }}" class="c-theme-color">Committees Teams</a> that help the church function smoothly as it spreads the love of God.<br>You can download a copy of our <a href="{{ url('about/bylaws') }}" class="c-theme-color">Bylaws here</a>.</p>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/MISC/ABOUT-1 -->

    <!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-3 -->
    <div class="c-content-box c-size-lg c-bg-parallax" style="background-image: url({{ asset('storage/backgrounds/bg-17.jpg') }})">
        <div class="container">
            <!-- Begin: testimonials 1 component -->
            <div class="c-content-testimonials-1" data-slider="owl">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-white c-font-uppercase c-font-bold">Our Hope and Dream</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <!-- End-->
                <div class="c-testimonial">
                    <p>The dream of Community Church is to glorify God by becoming a community of grace where people will receive, live and share the love of Jesus Christ. It is our conviction that spiritual life is meant to be done together. That is why we encourage Bible studies to help us all grow in the faith.</p>
                </div>
            </div>
            <!-- End-->
        </div>
    </div><!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-3 -->
@endsection