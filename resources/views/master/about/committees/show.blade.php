@extends('layouts.master')

@section('title', 'About Committees')

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('committee', $committee),
        'bc_title' => "$committee->name Committee",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/MISC/ABOUT-2 COMMITTEES -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="c-content-tile-grid c-bs-grid-reset-space" data-auto-height="true">
            <div class="c-content-title-1 wow animate fadeInDown">
                <h3 class="c-font-uppercase c-center c-font-bold">{{ $committee->name }}<br><small>Committee</small></h3>
                <div class="c-line-center"></div>
                <div class="c-center lead">{!! $committee->description !!}</div>
            </div>
        </div>
    </div><!-- END: CONTENT/MISC/ABOUT-2 COMMITTEES -->
@endsection