@extends('layouts.master')

@section('title', 'About Committees')

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('committees'),
        'bc_title' => "About Committees",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/MISC/ABOUT-2 COMMITTEES -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow animate fadeInUp">
                    <p class="c-center">Community Church at SaddleBrooke has a number of committees teams that help the church function smoothly as it spreads the love of God. Below is a list of the committees and a very brief statement of their purpose. To serve on a committee is a great way to get to know others and to help the church to operate with enthusiasm and efficiency. Snowbirds who attend our church are welcome to serve on any of the committees while they are in SaddleBrooke.</p>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/MISC/ABOUT-2 COMMITTEES -->

    <!-- BEGIN: CONTENT/MISC/SERVICES-2 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-content-feature-2-grid">
                <div class="c-content-title-1 c-center wow animated fadeIn">
                    <h3 class="c-font-uppercase c-font-bold">Committees</h3>
                    <div class="c-line-center"></div>
                </div>
                <div class="row">
                    @foreach($committees as $committee)
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow animated fadeInUp">
                                <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                    <div class="c-content-line-icon c-theme {{ $committee->icon_class }}"></div>
                                </div>
                                <h3 class="c-font-uppercase c-title">{{ $committee->name }}</h3>
                                {!! $committee->description !!}
                            </div>
                        </div>
                        @if($i++ % 3 == 0)</div><div class="row">@endif
                    @endforeach
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/MISC/SERVICES-2 -->

@endsection