@extends('layouts.master')

@section('title', "About $member->full_name")

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render($segment.'_member', $member),
        'bc_title' => "About $member->full_name",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/FEATURES/FEATURES-14-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-push-4">
                    <div class="c-content-title-3 c-theme-border">
                        <h3 class="c-left c-font-uppercase">{{ $member->full_name }}</h3>
                        <div class="c-line c-dot c-dot-left "></div>
                        <p>{!! $member->titles_list !!}</p>
                    </div>
                    {!! $member->about_story !!}
                </div>
                <div class="col-md-4 col-md-pull-8">
                    <img class="img-responsive" src="{{ $member->profile_picture_url }}" width="100%" alt="picture of {{ $member->full_name }}"/>
                    <div class="c-content-ver-nav">
                        <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
                            <h3 class="c-font-bold c-font-uppercase">Contact Info</h3>
                            <div class="c-line-left c-theme-bg"></div>
                        </div>
                        <ul class="c-menu c-arrow-dot c-theme">
                        @foreach($member->emails as $email)
                            @if($email->visibility_id == 1)
                                <li>{{ Html::mailto($email->address) }}</li>
                            @elseif($email->visibility_id == 2)
                                @if (Auth::guest())
                                    <li>Please <a href="{{ url('login') }}">login</a> to see the email.</li>
                                @else
                                    <li>{{ Html::mailto($email->address) }}</li>
                                @endif
                            @endif
                        @endforeach

                        @foreach($member->phones as $phone)
                            @if($phone->visibility_id == 1)
                                <li>{{ $phone->full_number }}</li>
                            @elseif($phone->visibility_id == 2)
                                @if (Auth::guest())
                                    <li>Please <a href="{{ url('login') }}">login</a> to see the phone number.</li>
                                @else
                                    <li>{{ $phone->full_number }}</li>
                                @endif
                            @endif
                        @endforeach
                        </ul>
                    </div>
                    @if($media)
                        <div class="c-content-ver-nav">
                            <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
                                <h3 class="c-font-bold c-font-uppercase">Media</h3>
                                <div class="c-line-left c-theme-bg"></div>
                            </div>
                            <ul class="c-menu c-arrow-dot c-theme">
                                @if($videos)<li><a href="{{ url("media/videos/pastor/$member->slug") }}" title="videos by {{ $member->full_name }}">Videos by {{ $member->full_name }}</a></li>@endif
                                @if($sermons)<li><a href="{{ url("media/sermons/pastor/$member->slug") }}" title="sermons by {{ $member->full_name }}">Sermons by {{ $member->full_name }}</a></li>@endif
                            </ul>
                        </div><!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection