<div class="alert alert-success alert-dismissible" role="alert">
    <i class="icon-like"></i> Your message has been sent successfully. Thank you!
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
</div>