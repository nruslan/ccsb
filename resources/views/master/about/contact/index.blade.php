@extends('layouts.master')

@section('title', 'Contact Us')

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('contact'),
        'bc_title' => "Contact Us",
        'bc_subtitle' => 'Community Church at Saddlebrooke'
    ])

    <!-- BEGIN: CONTENT/CONTACT/FEEDBACK-1 -->
    <div class="c-content-box c-size-md c-bg-grey">
        <div class="container">
            <div class="c-content-feedback-1 c-option-1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="c-container c-bg-green c-bg-img-bottom-right">
                            <div class="c-content-title-1 c-inverse">
                                <h3 class="c-font-uppercase c-font-bold">Join a Committee</h3>
                                <div class="c-line-left"></div>
                                <p class="c-font-lowercase">To serve on a committee is a great way to get to know others and to help the church to operate with enthusiasm and efficiency!</p>
                                <a href="{{ url('about/committees') }}" class="btn btn-md c-btn-border-2x c-btn-white c-btn-uppercase c-btn-square c-btn-bold">Learn More</a>
                            </div>
                        </div>
                        <div class="c-container c-bg-grey-1 c-bg-img-bottom-right" style="">
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold">Community Church<br><small>at SaddleBrooke</small></h3>
                                <div class="c-line-left"></div>
                                <div class="c-section">
                                    <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">SUNDAY SERVICE</div>
                                    <p><a href="https://www.google.com/maps/place/DesertView+Performing+Arts+Center/@32.514247,-110.9054327,17z/data=!4m13!1m7!3m6!1s0x86d610207d8bac1b:0x716b26530ffb8778!2s39900+S+Clubhouse+Dr,+Tucson,+AZ+85739!3b1!8m2!3d32.514247!4d-110.903244!3m4!1s0x86d610207d8bac1b:0xe75bfd19b9d4a09a!8m2!3d32.514295!4d-110.903499?hl=en&authuser=0" target="_blank">39900 S Clubhouse Drive<br/>
                                        Tucson, AZ 85739</a><br>
                                        <small>Sunday morning services at 8:30 AM</small>
                                        <br>
                                        <br>
                                    </p>
                                </div>
                                <div class="c-section">
                                    <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Parish House</div>
                                    <p><a href="https://www.google.com/maps/place/36768+S+Aaron+Ln,+Tucson,+AZ+85739/@32.5430033,-110.8787618,17z/data=!3m1!4b1!4m5!3m4!1s0x86d61a9fd2281d6b:0xff34c0f272da5fca!8m2!3d32.5430033!4d-110.8765731?hl=en&authuser=0" target="_blank">
                                            36768 South Aaron Ln<br/>Tucson, AZ 85739
                                        </a>
                                        <br>
                                        <br>
                                    </p>
                                </div>
                                <div class="c-section">
                                    <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Contacts</div>
                                    <p><strong>Phone:</strong> (520) 825-5394</p>
                                    <p><strong>Email:</strong> {!! Html::mailto('ccsbsaddlebrooke@gmail.com') !!}</p>
                                </div>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="c-contact">
                            <div class="c-content-title-1">
                                <h3  class="c-font-uppercase c-font-bold">Reach out to us</h3>
                                <div class="c-line-left"></div>
                                <p class="c-font-lowercase">Are you interested in learning more about our church? Do you have a specific question regarding our vision, beliefs, or ministries? Would you like to subscribe to our email newsletter? We'd love to hear from you!</p>
                            </div>
                            {!! Form::open(['url' => 'about/contact', 'id' => 'contactForm', 'autocomplete' => 'off', 'onsubmit' => 'return false;']) !!}
                                <div class="form-group">
                                    <input type="text" name="name" id="c-name" placeholder="Your Name" class="form-control c-square c-theme input-lg" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" id="c-email" placeholder="Your Email" class="form-control c-square c-theme input-lg" required>
                                </div>
                                <div class="form-group">
                                    <textarea rows="8" name="message" id="c-message" placeholder="Write comment here ..." class="form-control c-theme c-square input-lg" required></textarea>
                                </div>
                                <button type="submit" id="submitBtn" class="btn c-theme-btn c-btn-uppercase btn-lg c-btn-bold c-btn-square">Submit</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/CONTACT/FEEDBACK-1 -->
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        function disableField() {
            const invalidForm = document.querySelector('form#contactForm:invalid');
            const submitBtn = document.getElementById('submitBtn');
            if (invalidForm) {
                submitBtn.setAttribute('disabled', true);
            } else {
                submitBtn.disabled = false;
            }
        }

        disableField();

        //const inputs = document.getElementsByTagName("input");
        const inputs = document.getElementById('contactForm').elements;
        for (let input of inputs) {
            input.addEventListener('change', disableField);
        }

        (function () {
            let submitBtn = document.getElementById('submitBtn');
            submitBtn.onclick = function () {

                let name = document.getElementById('c-name');
                let email = document.getElementById('c-email');
                let message = document.getElementById('c-message');

                this.disabled = true;
                this.innerHTML = "Sending...";
                let data = new FormData();
                data.append('name', name.value);
                data.append('email', email.value);
                data.append('message', message.value);
                axios.post('/api/about/contact', data)
                    .then(function (response) {
                        console.log(response);
                        document.getElementById('contactForm').innerHTML = response.data;
                    })
                    .catch(function (error) {
                        //console.log(error.response.data.message);
                        document.getElementById('contactForm').innerHTML = error.response.data.message;
                    });
            };
        })();
    </script>
@endsection