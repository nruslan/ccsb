@extends('layouts.master')

@section('title', 'Church Staff')

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('staff'),
        'bc_title' => "About Church Staff",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/SLIDERS/TEAM-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-person-1-slider" data-slider="owl">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Pastoral Staff</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div><!-- End-->
                <!-- Begin: Owlcarousel -->
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-rtl="false" data-items="3" data-slide-speed="8000">
                    @foreach($pastors as $pastor)
                        <div class="item">
                            <div class="c-content-person-1">
                                <div class="c-caption c-content-overlay">
                                    <div class="c-overlay-wrapper">
                                        <div class="c-overlay-content">
                                            <a href="{{ url("about/staff/$pastor->slug") }}"><i class="icon-link"></i></a>
                                            <a href="{{ $pastor->profile_picture_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-1">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <img class="c-overlay-object img-responsive" src="{{ $pastor->profile_picture_url }}" alt="{{ $pastor->full_name }}">
                                </div>
                                <div class="c-body">
                                    <div class="c-head">
                                        <div class="c-name c-font-uppercase c-font-bold"><a href="{{ url("about/staff/$pastor->slug") }}">{{ $pastor->full_name }}</a></div>
                                    </div>
                                    <div class="c-position">{{ $title->title }}</div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div><!-- End-->
            </div><!-- End-->
        </div>
    </div><!-- END: CONTENT/SLIDERS/TEAM-1 -->
    <!-- END: CONTENT/MISC/TEAM-3 -->
    <div class="c-content-box c-size-md c-bg-grey-1">
        <div class="container">
            <!-- Begin: Testimonals 1 component -->
            <div class="c-content-team-1-slider" data-slider="owl" data-items="3">
                <!-- Begin: Title 1 component -->
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Ministry Staff</h3>
                    <div class="c-line-center c-theme-bg"></div>
                </div>
                <!-- End-->
                <div class="row">
                    @foreach($titles as $title)
                        @foreach($title->active_members as $member)
                        <div class="col-md-4 col-sm-6 c-margin-b-30">
                        <div class="c-content-person-1 c-option-2">
                            <div class="c-caption c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="{{ url("about/staff/$member->slug") }}"><i class="icon-link"></i></a>
                                        <a href="{{ $member->profile_picture_url }}" data-lightbox="fancybox" data-fancybox-group="gallery-4">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </div>
                                </div>
                                <img class="c-overlay-object img-responsive" src="{{ $member->profile_picture_url }}" alt="" width="100%">
                            </div>
                            <div class="c-body">
                                <div class="c-head">
                                    <div class="c-name c-font-uppercase c-font-bold"><a href="{{ url("about/staff/$member->slug") }}">{{ $member->full_name }}</a></div>
                                </div>
                                <div class="c-position">{{ $title->title }}</div>
                                <p></p>
                            </div>
                        </div>
                    </div>
                        @endforeach
                    @endforeach
                    <!-- End-->
                </div>
                <!-- End-->
            </div>
        </div><!-- END: CONTENT/MISC/TEAM-3 -->
    </div>

@endsection