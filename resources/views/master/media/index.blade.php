@extends('layouts.master')
@section('title', 'Media')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('media'),
        'bc_title' => "Media",
        'bc_subtitle' => 'Community Church at Saddlebrooke'
    ])

    <!-- BEGIN: CONTENT/BLOG/RECENT-POSTS-2 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 wow animated fadeInLeft">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-bold">Media<br><small>Sermons & Videos</small></h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div><!-- End-->
                    <p class="c-center"></p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            //Delete button
            $("#search").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection