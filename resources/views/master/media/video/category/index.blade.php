@extends('layouts.master')

@section('title', $category->title)

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('category', $category),
        'bc_title' => "$category->title",
        'bc_subtitle' => 'The Pastors Will Encourage You Every Week'
    ])

    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="c-content-blog-post-card-1-grid">
                        <div class="row">
                            @foreach($videos as $video)
                            <div class="col-md-6">
                                <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                                    <div class="c-media c-content-overlay">
                                        <div class="c-overlay-wrapper">
                                            <div class="c-overlay-content">
                                                <a href="{{ url("/media/videos/category/$category->slug/$video->slug") }}">
                                                    <i class="icon-social-youtube"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <img class="c-overlay-object img-responsive" src="{{ $video->image_video_url }}" alt="">
                                    </div>
                                    <div class="c-body">
                                        <div class="c-title c-font-bold c-font-uppercase">
                                            <a href="{{ url("/media/videos/category/$category->slug/$video->slug") }}">{{ $video->title }}</a>
                                        </div>
                                        <div class="c-author">
                                            By <a href="#"><span class="c-font-uppercase">{{ $video->starring_members }}</span></a>
                                            on <span class="c-font-uppercase">{{ $video->mdy_date }}</span>
                                        </div>

                                        <div class="c-panel">
                                            <ul class="c-tags c-theme-ul-bg">
                                                <li>{{ $video->related_categories }}</li>
                                            </ul>
                                            <div class="c-comments">
                                                {{--<a href="{{ url("/media/videos/category/$video->slug") }}"><i class="icon-eye"></i> {{ $video->youtube_video_views_count }} views</a>--}}
                                            </div>
                                        </div>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        {{ $videos->links('vendor.pagination.jango') }}
                    </div>
                </div>
                <div class="col-md-3">
                    @include('master.media.video.partials.sidenav')
                </div>
            </div>
        </div>
    </div>
@endsection