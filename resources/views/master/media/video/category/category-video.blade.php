@extends('layouts.master')

@section('title', $video->title)

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('category', $category),
        'bc_title' => $category->title,
        'bc_subtitle' => $video->title
    ])

    <!-- BEGIN: BLOG LISTING -->
    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="c-content-blog-post-1-view">
                        <div class="c-content-blog-post-1">
                            <iframe width="100%" height="465" src="{{ $video->video_url }}" frameborder="0"></iframe>
                            <div class="c-title c-font-bold c-font-uppercase">
                                <a href="{{ url("/media/videos/category/$category->slug/$video->slug") }}">{{ $video->title }}</a>
                            </div>

                            <div class="c-panel c-margin-b-30">
                                <div class="c-author"><a href="#">By <span class="c-font-uppercase">{{ $video->starring_members }}</span></a></div>
                                <div class="c-date">on <span class="c-font-uppercase">{{ $video->mdy_date }}</span></div>
                                <ul class="c-tags c-theme-ul-bg">
                                    <li>{{ $video->related_categories }}</li>
                                </ul>
                                <div class="c-comments"><a href="{{ url("/media/videos/category/$category->slug/$video->slug") }}"><i class="icon-eye"></i> {{ $video->youtube_video_views_count }} views</a></div>
                            </div>
                            <div class="c-desc">{!! $video->description or '---' !!}</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    @include('master.media.video.partials.sidenav')
                </div>
            </div>
            @include('master.media.video.partials.random-videos', ['v_url' => "media/videos/category/$category->slug", 'videos' => $videos])
        </div>
        </div>
    </div><!-- END: BLOG LISTING  -->
@endsection