<div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
    <h3 class="c-font-bold c-font-uppercase">Other Videos</h3>
    <div class="c-line-left c-theme-bg"></div>
</div>
<div class="row">
    <? $i = 1 ?>
    @foreach($videos as $video)
        <div class="col-md-4">
            <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                <div class="c-media c-content-overlay">
                    <div class="c-overlay-wrapper">
                        <div class="c-overlay-content">
                            <a href="{{ url("$v_url/$video->slug") }}">
                                <i class="icon-social-youtube"></i>
                            </a>
                        </div>
                    </div>
                    <img class="c-overlay-object img-responsive" src="{{ $video->image_video_url }}" alt="">
                </div>
                <div class="c-body">
                    <div class="c-title c-font-bold c-font-uppercase">
                        <a href="{{ url("$v_url/$video->slug") }}">{{ $video->title }}</a>
                    </div>
                    <div class="c-author">
                        By <a href="#"><span class="c-font-uppercase">{{ $video->starring_members }}</span></a>
                        on <span class="c-font-uppercase">{{ $video->mdy_date }}</span>
                    </div>

                    <div class="c-panel">
                        <ul class="c-tags c-theme-ul-bg">
                            <li>{{ $video->related_categories }}</li>
                        </ul>
                        <div class="c-comments">
                            {{--<a href="{{ url("$v_url/$video->slug") }}"><i class="icon-eye"></i> {{ $video->youtube_video_views_count }} views</a>--}}
                        </div>
                    </div>
                    <p></p>
                </div>
            </div>
        </div>
        @if($i++ == 3)</div><br><div class="row">@endif
    @endforeach
</div>