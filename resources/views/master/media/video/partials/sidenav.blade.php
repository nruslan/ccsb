<!-- BEGIN: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
{!! Form::open(['method' => 'GET', 'url' => 'media/videos/search', 'id' => 'search']) !!}
    <div class="input-group">
        {!! Form::text('keywords', null, ['class' => 'form-control c-square c-theme-border', 'id' => 'searchQuery', 'placeholder' => 'Search for video...']) !!}
        <span class="input-group-btn">
            <button class="btn c-theme-btn c-theme-border c-btn-square" type="submit"><i class="fa fa-search fa-fw"></i></button>
        </span>
    </div>
{!! Form::close() !!}

<div class="c-content-ver-nav">
    <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
        <h3 class="c-font-bold c-font-uppercase">Categories</h3>
        <div class="c-line-left c-theme-bg"></div>
    </div>
    <ul class="c-menu c-arrow-dot1 c-theme">
        @foreach($categories as $category)
        <li><a href="{{ url("media/videos/category/$category->slug") }}" title="{{ $category->title }}">{{ $category->title }}</a></li>
        @endforeach
    </ul>
</div>

<div class="c-content-ver-nav">
    <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
        <h3 class="c-font-bold c-font-uppercase">Pastors</h3>
        <div class="c-line-left c-theme-bg"></div>
    </div>
    <ul class="c-menu c-arrow-dot c-theme">
        @foreach($pastors as $pastor)
            <li><a href="{{ url("media/videos/pastor/$pastor->slug") }}" title="Pastor {{ $pastor->full_name }}">{{ $pastor->full_name }}</a></li>
        @endforeach
    </ul>
</div><!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->