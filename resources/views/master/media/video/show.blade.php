@extends('layouts.master')

@section('title', $video->title)

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('video', $video),
        'bc_title' => "Video: $video->title",
        'bc_subtitle' => $video->title
    ])

    <!-- BEGIN: BLOG LISTING -->
    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="c-content-blog-post-1-view">
                        <div class="c-content-blog-post-1">
                            <iframe width="100%" height="465" src="{{ $video->video_url }}" frameborder="0"></iframe>
                            <div class="c-title c-font-bold c-font-uppercase">
                                <a href="#">{{ $video->title }}</a>
                            </div>

                            <div class="c-panel c-margin-b-30">
                                <div class="c-author">By <a href="{{ url("media/videos/pastor/$video->starring_member_slug") }}"><span class="c-font-uppercase">{{ $video->starring_members }}</span></a></div>
                                <div class="c-date">on <span class="c-font-uppercase">{{ $video->mdy_date }}</span></div>
                                <ul class="c-tags c-theme-ul-bg">
                                    <li>{{ $video->related_categories }}</li>
                                </ul>
                                <div class="c-comments"><a href="#"><i class="icon-eye"></i> {{ $video->youtube_video_views_count }} views</a></div>
                            </div>
                            <div class="c-desc">
                                {!! $video->description ?? '---' !!}
                            </div>
                            <nav>
                                <ul class="pager">
                                    <li class="previous{{ $prevDisabled ?? '' }}">
                                        <a class="aBtn" href="{{ $video->previous_video }}" data-toggle="tooltip" data-placement="top" title="previous video">
                                            <i class="fa fa-fw fa-angle-double-left" aria-hidden="true"></i> Previous
                                        </a>
                                    </li>
                                    <li class="next{{ $nextDisabled ?? '' }}">
                                        <a class="aBtn" href="{{ $video->next_video }}" data-toggle="tooltip" data-placement="top" title="next video">
                                            Next <i class="fa fa-fw fa-angle-double-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    @include('master.media.video.partials.sidenav')
                </div>
            </div>
            @include('master.media.video.partials.random-videos', ['v_url' => 'media/videos', 'videos' => $randVideos])
        </div>
    </div>
    <!-- END: BLOG LISTING  -->
@endsection