@extends('layouts.master')
@section('title', 'Sermons')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('sermon_search'),
        'bc_title' => "Sermons Search Result ".$sermons->total(),
        'bc_subtitle' => "The Word of God Provided Each Week"
    ])
    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="c-content-blog-post-card-1-grid">
                        <div class="row">
                            <div class="col-md-6">
                                @foreach($sermons as $sermon)
                                    <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                                        <div class="c-media c-content-overlay">
                                            <div class="c-overlay-wrapper">
                                                <div class="c-overlay-content">
                                                    <a href="{{ url("/media/sermons/$sermon->slug") }}" data-lightbox="fancybox" data-fancybox-group="gallery">
                                                        <i class="icon-earphones"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <img class="c-overlay-object img-responsive" src="{{ asset($sermon->thumbnail) }}" alt="">
                                        </div>
                                        <div class="c-body">
                                            <div class="c-title c-font-bold c-font-uppercase">
                                                <a href="{{ url("/media/sermons/$sermon->slug") }}">{!! $sermon->title_subtitle !!}</a>
                                            </div>
                                            <div class="c-author">
                                                By <a href="#"><span class="c-font-uppercase">{{ $sermon->sermon_pastors }}</span></a>
                                                on <span class="c-font-uppercase">{{ $sermon->text_date }}</span>
                                            </div>
                                            <div class="c-panel">
                                                <a href="{{ url("/media/sermons/$sermon->slug") }}"><i class="icon-earphones"></i> Listen ({{ $sermon->duration }})</a>
                                                <a href="{{ $sermon->sermon_url }}" download="{{ $sermon->filename }}" class="pull-right"><i class="icon-cloud-download"></i> Download</a>
                                            </div>
                                            <p></p>
                                        </div>
                                    </div>
                                    @if($i++ == $sermons->perPage() / 2)</div><div class="col-md-6">@endif
                                @endforeach
                            </div>
                        </div>
                        {{ $sermons->appends(['keywords' => $search])->links('vendor.pagination.jango') }}
                    </div>
                </div>
                <div class="col-md-3">
                    @include('master.media.sermon.partials.sidenav')
                </div>
            </div>
        </div>
    </div>
    <!-- END: BLOG LISTING  -->

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            //Delete button
            $("#search").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection