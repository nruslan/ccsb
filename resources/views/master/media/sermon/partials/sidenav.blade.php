<!-- BEGIN: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
{!! Form::open(['method' => 'GET', 'url' => 'media/sermons/search', 'id' => 'search']) !!}
<div class="input-group">
    {!! Form::text('keywords', null, ['class' => 'form-control c-square c-theme-border', 'id' => 'searchQuery', 'placeholder' => 'Search for sermon...']) !!}
    <span class="input-group-btn"><button class="btn c-theme-btn c-theme-border c-btn-square" type="submit"><i class="fa fa-search fa-fw"></i></button></span>
</div>
{!! Form::close() !!}

<div class="c-content-ver-nav">
    <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
        <h3 class="c-font-bold c-font-uppercase">Pastors</h3>
        <div class="c-line-left c-theme-bg"></div>
    </div>
    <ul class="c-menu c-arrow-dot1 c-theme">
        @foreach($pastors as $pastor)
            <li><a href="{{ url("media/sermons/pastor/$pastor->slug") }}" title="Pastor {{ $pastor->full_name }}">{{ $pastor->full_name }}</a></li>
        @endforeach
    </ul>
</div>