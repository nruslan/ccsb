<div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
    <h3 class="c-font-bold c-font-uppercase">Other Sermons</h3>
    <div class="c-line-left c-theme-bg"></div>
</div>
<div class="row">
    <? $i = 1 ?>
    @foreach($sermons as $sermon)
        <div class="col-md-4">
            <div class="c-content-blog-post-card-1 c-option-2 c-bordered">
                <div class="c-media c-content-overlay">
                    <div class="c-overlay-wrapper">
                        <div class="c-overlay-content">
                            <a href="{{ url("$s_url/$sermon->slug") }}" data-lightbox="fancybox" data-fancybox-group="gallery">
                                <i class="icon-earphones"></i>
                            </a>
                        </div>
                    </div>
                    <img class="c-overlay-object img-responsive" src="{{ asset($sermon->thumbnail) }}" alt="">
                </div>
                <div class="c-body">
                    <div class="c-title c-font-bold c-font-uppercase">
                        <a href="{{ url("$s_url/$sermon->slug") }}">{!! $sermon->title_subtitle !!}</a>
                    </div>
                    <div class="c-author">
                        By <a href="#"><span class="c-font-uppercase">{{ $sermon->sermon_pastors }}</span></a>
                        on <span class="c-font-uppercase">{{ $sermon->text_date }}</span>
                    </div>
                    <div class="c-panel">
                        <a href="{{ url("$s_url/$sermon->slug") }}"><i class="icon-earphones"></i> Listen ({{ $sermon->duration }})</a>
                        <a href="{{ $sermon->sermon_url }}" download="{{ $sermon->filename }}" class="pull-right"><i class="icon-cloud-download"></i> Download</a>
                    </div>
                    <p></p>
                </div>
            </div>
        </div>
        @if($i++ == 3)</div><br><div class="row">@endif
    @endforeach
</div>