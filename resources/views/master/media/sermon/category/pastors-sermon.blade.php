@extends('layouts.master')

@section('title', $sermon->title)

@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('sermons_pastor', $pastor),
        'bc_title' => "$sermon->title",
        'bc_subtitle' => "By $pastor->full_name"
    ])

    <!-- BEGIN: BLOG LISTING -->
    <div class="c-content-box c-size-md">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="c-content-blog-post-1-view">
                        <div class="c-content-blog-post-1">
                            <audio controls="controls">
                                Your browser does not support the <code>audio</code> element.
                                <source src="{{ $sermon->sermon_url }}" type="audio/mp3">
                            </audio>
                            <div class="c-title c-font-bold c-font-uppercase">
                                <a href="{{ url("/media/sermons/pastor/$pastor->slug/$sermon->slug") }}">{!! $sermon->title_subtitle !!}</a>
                            </div>

                            <div class="c-panel c-margin-b-30">
                                <div class="c-author">By <a href="{{ url("/media/sermons/pastor/$pastor->slug") }}"><span class="c-font-uppercase">{{ $sermon->sermon_pastors }}</span></a></div>
                                <div class="c-date">on <span class="c-font-uppercase">{{ $sermon->text_date }}</span></div>
                                <ul class="c-tags c-theme-ul-bg">
                                    <li>Sermon</li>
                                </ul>
                                <div class="c-comments"><a href="{{ url("/media/sermons/pastor/$pastor->slug/$sermon->slug") }}"><i class="icon-clock"></i> {{ $sermon->duration }}</a></div>
                            </div>
                            <div class="c-desc">
                                {!! $sermon->description or '---' !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    @include('master.media.sermon.partials.sidenav')
                </div>
            </div>
            @include('master.media.sermon.partials.random-sermons', ['s_url' => "media/sermons/pastor/$pastor->slug", 'sermons' => $sermons])
        </div>
    </div><!-- END: BLOG LISTING  -->
@endsection