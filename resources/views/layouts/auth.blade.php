<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CCSB Admin Panel</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}"/>
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>

<body class="hold-transition login-page">
<div class="wrapper">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/') }}"><b>Admin</b>CCSB</a>
        </div>
        <!-- /.login-logo -->
        @yield('content')
        <br>
        <p class="text-center">
            Community Church at SaddleBrooke<br>
            <small>developed by <a href="https://nruslan.com" title="NRuslan - web developer" target="_blank">NRuslan</a> &copy; 2017 @if(date('Y') != 2017)- {{ date('Y') }}@endif</small>
        </p>
    </div>
</div>
<!-- Scripts -->
<script src="{{ asset('js/admin.js') }}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
    $(document).ready(function() {
        $(".form-signin").on('submit', function(){
            $(this).children("button[type=submit]").prop('disabled',true).html('processing...');
        });
    });
</script>
</body>
</html>