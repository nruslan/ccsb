<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-83659460-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-83659460-1');
    </script>
    <meta charset="utf-8"/>
    <title>@yield('title') | CCSB.US</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="Community Church at SaddleBrooke-Community of Grace" name="description" />
    <meta content="Ruslan Niyazimbetov" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="{{ asset('css/jango-mandatory.css') }}" rel="stylesheet">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN: BASE PLUGINS  -->
    <link href="{{ asset('css/jango-base-plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl-carousel/assets/owl.carousel.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END: BASE PLUGINS -->

    <!-- BEGIN THEME STYLES -->
    <link href="{{ asset('css/jango-theme.css') }}" rel="stylesheet">
    <!-- END THEME STYLES -->

    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}"/>
</head>
<body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-fullscreen">

<!-- BEGIN: LAYOUT/HEADERS/HEADER-1 -->
<!-- BEGIN: HEADER -->
<header class="c-layout-header c-layout-header-4 c-bordered c-layout-header-default-mobile c-header-transparent-dark" data-minimize-offset="80">
    <div class="c-navbar">
        <div class="container-fluid">
            <!-- BEGIN: BRAND -->
            <div class="c-navbar-wrapper clearfix">
                <div class="c-brand c-pull-left">
                    <a href="{{ url('/') }}" class="c-logo">
                        <img src="{{ asset("storage/logo-brand.png") }}" alt="CCSB logo" class="c-desktop-logo">
                        <img src="{{ asset("storage/logo-brand.png") }}" alt="CCSB logo" class="c-desktop-logo-inverse">
                        <img src="{{ asset("storage/logo-brand.png") }}" alt="CCSB logo" class="c-mobile-logo">
                    </a>
                    <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
                        <span class="c-line"></span>
                        <span class="c-line"></span>
                        <span class="c-line"></span>
                    </button>
                    <button class="c-topbar-toggler" type="button">
                        <i class="fa fa-ellipsis-v"></i>
                    </button>
                </div>
                <!-- END: BRAND -->
                <!-- BEGIN: HOR NAV -->
                <!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU -->
                <!-- BEGIN: MEGA MENU -->
                <!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
                <nav class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
                    <ul class="nav navbar-nav c-theme-nav">
                        <li class="@if(request()->segment(1) == '')c-active @endif">
                            <a href="{{ url('/') }}" class="c-link">Home</a>
                        </li>
                        <li class="c-menu-type-classic @if(request()->segment(1) == 'about')c-active @endif">
                            <a href="javascript:;" class="c-link dropdown-toggle">About<span class="c-arrow c-toggler"></span></a>
                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                <li @if(request()->segment(2) == 'church')class="c-active"@endif>
                                    <a href="{{ url('about/church') }}">Church</a>
                                </li>
                                <li @if(request()->segment(2) == 'staff')class="c-active"@endif>
                                    <a href="{{ url('about/staff') }}">Staff</a>
                                </li>
                                <li @if(request()->segment(2) == 'leaders')class="c-active"@endif>
                                    <a href="{{ url('about/leaders') }}">Leaders</a>
                                </li>
                                <li @if(request()->segment(2) == 'bylaws')class="c-active"@endif>
                                    <a href="{{ url('about/bylaws') }}">Bylaws</a>
                                </li>
                                <li @if(request()->segment(2) == 'committees')class="c-active"@endif>
                                    <a href="{{ url('about/committees') }}">Committees</a>
                                </li>
                                <li @if(request()->segment(2) == 'contact-us')class="c-active"@endif>
                                    <a href="{{ url('about/contact-us') }}">Contact Us</a>
                                </li>
                            </ul>
                        </li><!-- /ABOUT -->
                        <li class="c-menu-type-classic @if(request()->segment(1) == 'ministries')c-active @endif">
                            <a href="javascript:;" class="c-link dropdown-toggle">Ministries<span class="c-arrow c-toggler"></span></a>
                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                <li @if(request()->segment(2) == 'missions')class="c-active"@endif>
                                    <a href="{{ url('ministries/missions') }}">Missions</a>
                                </li>
                                <li @if(request()->segment(2) == 'mens')class="c-active"@endif>
                                    <a href="{{ url('ministries/mens') }}">Men's Ministry</a>
                                </li>
                                <li @if(request()->segment(2) == 'women')class="c-active"@endif>
                                    <a href="{{ url('ministries/women') }}">Women's Ministry</a>
                                </li>
                                <li @if(request()->segment(2) == 'care')class="c-active"@endif>
                                    <a href="{{ url('ministries/care') }}">Care Ministry</a>
                                </li>
                            </ul>
                        </li><!-- /MINISTRIES -->
                        <li class="c-menu-type-classic @if(request()->segment(1) == 'media')c-active @endif">
                            <a href="javascript:;" class="c-link dropdown-toggle">Media<span class="c-arrow c-toggler"></span></a>
                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                <li class="@if(request()->segment(2) == 'videos')c-active @endif">
                                    <a href="{{ url('/media/videos') }}" class="c-link">Videos</a>
                                </li>
                                <li class="@if(request()->segment(2) == 'sermons')c-active @endif">
                                    <a href="{{ url('/media/sermons') }}" class="c-link">Sermons</a>
                                </li>
                            </ul>
                        </li><!-- /MEDIA -->
                        <li class="@if(request()->segment(1) == 'give')c-active @endif">
                            <a href="{{ url('/give') }}" class="c-link">Give</a>
                        </li><!-- /GIVE -->
                        <li class="c-menu-type-classic @if(request()->segment(1) == 'happenings')c-active @endif">
                            <a href="javascript:;" class="c-link dropdown-toggle">Happenings<span class="c-arrow c-toggler"></span></a>
                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                <li @if(request()->segment(2) == 'news')class="c-active"@endif>
                                    <a href="{{ url('happenings/news') }}">News</a>
                                </li>
                                <li @if(request()->segment(2) == 'events')class="c-active"@endif>
                                    <a href="{{ url('happenings/events') }}">Events</a>
                                </li>
                                <li @if(request()->segment(2) == 'calendar')class="c-active"@endif>
                                    <a href="{{ url('happenings/calendar') }}">Calendar</a>
                                </li>
                            </ul>
                        </li><!-- /HAPPENING -->
                        @if (Auth::guest())
                        <li>
                            <a href="{{ route('login') }}" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold"><i class="icon-user"></i> Login</a>
                        </li>
                        @else
                        <li class="c-menu-type-classic">
                            <a href="#" class="c-link dropdown-toggle">
                                <i class="icon-user"></i> {{ Auth::user()->username }} <span class="c-arrow c-toggler"></span>
                            </a>
                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                <li>
                                    <a href="{{ route('directory') }}"><i class="fa fa-btn fa-users"></i> Directory</a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </nav>
                <!-- END: MEGA MENU --><!-- END: LAYOUT/HEADERS/MEGA-MENU -->
                <!-- END: HOR NAV -->
            </div>
        </div>
    </div>
</header>
<!-- END: HEADER --><!-- END: LAYOUT/HEADERS/HEADER-1 -->

<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
    <!-- BEGIN: PAGE CONTENT -->
    @yield('content')
</div>

<footer class="c-layout-footer c-layout-footer-7">
    <div class="container">
        <div class="c-prefooter">
            <div class="c-body">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <ul class="c-links c-theme-ul">
                            <li><a href="{{ url('about/church') }}" title="">About CCSB</a></li>
                            <li><a href="{{ url('ministries') }}" title="">Ministries</a></li>
                            <li><a href="{{ url('about/committees') }}" title="">Committees</a></li>
                            <li><a href="{{ url('about/contact-us') }}" title="">Contact Us</a></li>
                            <li><a href="{{ url('give') }}" title="">Give</a></li>
                        </ul>
                        <ul class="c-links c-theme-ul">
                            <li><a href="{{ url('happenings/news') }}" title="">News</a></li>
                            <li><a href="{{ url('happenings/events') }}" title="">Events</a></li>
                            <li><a href="{{ url('media/sermons') }}" title="">Sermons</a></li>
                            <li><a href="{{ url('media/videos') }}" title="">Videos</a></li>
                            <li><a href="{{ url('photoalbums') }}" title="">Photoalbums</a></li>
                        </ul>
                    </div>
                    <div class="col-md-5 col-sm-6 col-xs-12">
                        <div class="c-content-title-1 c-title-md">
                            <h3 class="c-title c-font-uppercase c-font-bold"><span class="c-theme-font">COME CELEBRATE </span>WITH US</h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <p class="c-address c-font-16">
                            Sunday morning services at 8:30 AM<br/>
                            DesertView Theater in SaddleBrooke!<br/>
                            39900 S. Clubhouse Drive<br/>
                            Tucson, AZ 85739
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="c-content-title-1 c-title-md">
                            <h3 class="c-title c-font-uppercase c-font-bold"><span class="c-theme-font">REACH OUT</span> TO US</h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <p class="c-address c-font-16">
                            Parish House
                            <br/>36768 S Aaron Ln
                            <br/> Tucson, AZ 85739
                            <br/> Phone: (520) 825-5394
                            <br/> Email: <a href="{{ url('about/contact-us') }}" class="c-theme-color">contact us</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="c-line"></div>

            <div class="c-head">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="c-content-title-1 c-title-md">
                            <h3 class="c-title c-font-uppercase c-font-bold">Follow <span class="c-theme-font">US</span></h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <div class="c-left">
                            <div class="socicon">
                                <a href="https://www.facebook.com/CommunityChurchSaddleBrooke" target="_blank" class="socicon-btn socicon-btn-circle socicon-solid c-font-dark-1 c-theme-on-hover socicon-facebook tooltips" data-original-title="Facebook" data-container="body"></a>
                                <a href="https://www.youtube.com/channel/UCIq6iP7rO7BZUDAsKY__V8w" target="_blank" class="socicon-btn socicon-btn-circle socicon-solid c-font-dark-1 c-theme-on-hover socicon-youtube tooltips" data-original-title="Youtube" data-container="body"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p class="c-font-white">For optimal website performance, we recommend using <a class="c-theme-color" href="https://www.google.com/chrome" target="_blank">Chrome</a> or
                            <a class="c-theme-color" href="https://www.mozilla.org/en-US/firefox/new" target="_blank">Firefox</a> browser.</p>
                        <!--<div class="c-content-title-1 c-title-md">
                            <h3 class="c-title c-font-uppercase c-font-bold">Subscribe to Newsletter</h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <div class="c-line-left hide"></div>
                        {!! Form::open(['url' => 'subscribe', 'id' => 'subscribeForm', 'onsubmit' => 'return false;']) !!}
                            <div class="input-group input-group-lg c-square">
                                <input type="email" name="email" id="subscribersEmail" class="c-input form-control c-square c-theme" placeholder="Your Email Here" autocomplete="off" required>
                                <span class="input-group-btn">
						            <button class="btn c-theme-btn c-theme-border c-btn-square c-btn-uppercase c-font-16" type="button" id="subscribeBtn">Subscribe</button>
						        </span>
                            </div>
                        {!! Form::close() !!}
                        <div id="subscribeResponse"></div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="c-postfooter c-bg-dark-2">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 c-col">
                    <p class="c-copyright c-font-grey">2016 @if(date('Y') != 2016)- {{ date('Y') }}@endif &copy; CCSB
                        <span class="c-font-grey-3">All Rights Reserved.
                            Developed by <a href="http://www.nruslan.com" title="websites developer" target="_blank"><span class="c-theme-color">NRuslan</span></a> | Designed by <a href="https://www.kseniadesign.com" title="Graphic Designer" target="_blank"><span class="c-theme-color">NKathy</span></a>
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>

</footer><!-- END: LAYOUT/FOOTERS/FOOTER-7 -->

<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div><!-- END: LAYOUT/FOOTERS/GO2TOP -->

<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
<!-- BEGIN: CORE PLUGINS -->
<script src="{{ asset('js/app.js') }}" type="text/javascript" ></script>
<script src="{{ asset('js/reveal-animate/wow.js') }}" type="text/javascript" ></script>
<script src="{{ asset('js/reveal-animate/reveal-animate.js') }}" type="text/javascript" ></script>
<!-- END: CORE PLUGINS -->

<!-- BEGIN: LAYOUT PLUGINS -->
<script src="{{ asset('js/revo-slider/js/jquery.themepunch.tools.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/revo-slider/js/jquery.themepunch.revolution.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/revo-slider/js/extensions/revolution.extension.slideanims.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/revo-slider/js/extensions/revolution.extension.layeranimation.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/revo-slider/js/extensions/revolution.extension.navigation.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/revo-slider/js/extensions/revolution.extension.video.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/revo-slider/js/extensions/revolution.extension.parallax.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.cubeportfolio.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.waypoints.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.counterup.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/fancybox/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.smooth-scroll.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/typed.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/slider-for-bootstrap/js/bootstrap-slider.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/js.cookie.js') }}" type="text/javascript"></script>
<!-- END: LAYOUT PLUGINS -->

<!-- BEGIN: THEME SCRIPTS -->
<script src="{{ asset('js/base/js/components.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/base/js/components-shop.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/base/js/app.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        App.init(); // init core
    });
</script>
<!-- END: THEME SCRIPTS -->

<!-- BEGIN: PAGE SCRIPTS -->
<script src="{{ asset('js/revo-slider/js/extensions/revolution.extension.kenburn.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/revo-slider/js/extensions/revolution.extension.parallax.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/revo-slider/slider-13.js') }}" type="text/javascript"></script>
@yield('scripts')
<script>
    // JavaScript Document
    /*(function () {
        let output = document.getElementById('subscribeResponse');
        let submitBtn = document.getElementById('subscribeBtn');
        submitBtn.onclick = function () {
            let data = new FormData();
            data.append('email', document.getElementById('subscribersEmail').value);
            axios.post('/api/subscribe', data)
                .then(function (response) {
                    document.getElementById('subscribeForm').remove();
                    console.log(response);
                    output.innerHTML = response.data;
                })
                .catch(function (err) {
                    output.className = 'container text-danger';
                    output.innerHTML = err.message;
                });
        };
    })();*/

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('.aBtn').on('click', function(){
            $(this).addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
        });
    });
</script>

<!-- END: PAGE SCRIPTS -->
<!-- END: LAYOUT/BASE/BOTTOM -->
</body>
</html>