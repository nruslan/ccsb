@extends('layouts.master')
@section('title', 'Reset Password')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('reset-password'),
        'bc_title' => "Reset Password",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/SHOPS/SHOP-LOGIN-REGISTER-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-shop-login-register-1">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-default c-panel">
                            <div class="panel-body c-panel-body">
                                @if (session('status'))<div class="alert alert-success">{{ session('status') }}</div>@endif
                                <form class="c-form-login" method="POST" action="{{ url('/password/email') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                                        <input class="form-control c-square c-theme input-lg" placeholder="Email" name="email" value="{{ old('email') }}">
                                        <span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
                                        @if ($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
                                    </div>

                                    <button type="submit" class="pull-right btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">
                                        <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/SHOPS/SHOP-LOGIN-REGISTER-1 -->
@endsection
