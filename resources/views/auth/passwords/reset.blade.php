@extends('layouts.master')
@section('title', 'Reset Password')
@section('content')
    @include('master.parts.breadcrumbs-template', [
            'bc' => Breadcrumbs::render('reset-password'),
            'bc_title' => "Reset Password",
            'bc_subtitle' => "Community Church at SaddleBrooke"
        ])

    <!-- BEGIN: CONTENT/SHOPS/SHOP-LOGIN-REGISTER-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-shop-login-register-1">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-default c-panel">
                            <div class="panel-body c-panel-body">
                                <form class="c-form-login" method="POST" action="{{ url('/password/reset') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="token" value="{{ $token }}">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                                        <input id="email" type="email" class="form-control c-square c-theme input-lg" name="email" placeholder="E-Mail Address" value="{{ $email or old('email') }}">
                                        <span class="glyphicon glyphicon-envelope form-control-feedback c-font-grey"></span>
                                        @if ($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                                        <input id="password" type="password" class="form-control c-square c-theme input-lg" placeholder="Password" name="password">
                                        <span class="glyphicon glyphicon-lock form-control-feedback c-font-grey"></span>
                                        @if ($errors->has('password'))<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>@endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} has-feedback">
                                        <input id="password-confirm" type="password" class="form-control c-square c-theme input-lg" placeholder="Confirm Password" name="password_confirmation">
                                        <span class="glyphicon glyphicon-lock form-control-feedback c-font-grey"></span>
                                        @if ($errors->has('password_confirmation'))<span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>@endif
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class=" btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">
                                                <i class="fa fa-btn fa-refresh"></i> Reset Password
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/SHOPS/SHOP-LOGIN-REGISTER-1 -->
@endsection
