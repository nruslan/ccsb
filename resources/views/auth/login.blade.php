@extends('layouts.master')
@section('title', 'Member Authentication')
@section('content')
    @include('master.parts.breadcrumbs-template', [
        'bc' => Breadcrumbs::render('login'),
        'bc_title' => "Member Authentication",
        'bc_subtitle' => "Community Church at SaddleBrooke"
    ])

    <!-- BEGIN: CONTENT/SHOPS/SHOP-LOGIN-REGISTER-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-shop-login-register-1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default c-panel">
                            <div class="panel-body c-panel-body">
                                <form class="c-form-login" method="POST" action="{{ url('/login') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                                        <input class="form-control c-square c-theme input-lg" placeholder="Email" name="email" value="{{ old('email') }}">
                                        <span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
                                        @if ($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                                        <input type="password" class="form-control c-square c-theme input-lg" placeholder="Password" name="password">
                                        <span class="glyphicon glyphicon-lock form-control-feedback c-font-grey"></span>
                                        @if ($errors->has('password'))<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>@endif
                                    </div>
                                    <div class="row c-margin-t-40">
                                        <div class="col-xs-8">
                                            <div class="c-checkbox">
                                                <input type="checkbox" id="checkbox1-77" class="c-check" name="remember">
                                                <label for="checkbox1-77"> <span class="inc"></span>
                                                    <span class="check"></span> <span class="box"></span> Remember me
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <button type="submit" class="pull-right btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">
                                                <i class="fa fa-fw fa-sign-in"></i> Login</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="c-content-title-1">
                                    <div class="c-line-left c-theme-bg"></div>
                                    <a href="{{ url('/password/reset') }}" class="c-btn-forgot">Forgot Your Password?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default c-panel">
                            <div class="panel-body c-panel-body">
                                <div class="c-content-title-1">
                                    <h3 class="c-left"><i class="icon-user"></i> Don't have an account yet?</h3>
                                    <div class="c-line-left c-theme-bg"></div>
                                    <p>Please <a class="c-theme-font" href="{{ url('about/contact-us') }}">contact</a> CCSB office to become a member.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/SHOPS/SHOP-LOGIN-REGISTER-1 -->
@endsection
