@extends('layouts.auth')
@section('title', 'Reset Password')
@section('content')
    <div class="login-box-body">
        @if (session('status'))<div class="alert alert-success">{{ session('status') }}</div>@endif
        <p class="login-box-msg">Type your email address</p>
        <form method="POST" action="{{ route('admin.password.email') }}" id="logInForm">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
            </div>
            <button type="submit" class="btn btn-primary btn-block btn-flat">
                <i class="fa fa-fw fa-sign-in"></i> Send Password Reset Link</button>
        </form>
    </div><!-- /.login-box-body -->
@endsection
