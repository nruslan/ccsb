@component('mail::message-custom')
# Hello, {{ $name }}!
<p>We glad that you have decided to join our Community Church at SaddleBrooke.</p>
<p>Below you can find temporary password to access the website's member section.</p>

@component('mail::promotion')
<strong>Your temporary password is: {{ $temp_password }}</strong><br>
<i>This password will be valid for 72 hours only</i>.
@endcomponent

@component('mail::button', ['url' => 'http://ccsb.loc'])
http://ccsb.loc
@endcomponent

Thank you and blessings!<br>
<i>Community Church at SaddleBrooke</i>
@component('mail::subcopy')
***This is an automatically generated email, please do not reply
@endcomponent
@endcomponent