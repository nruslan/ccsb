@component('mail::message-custom')
# Hello, {{ $name }}!
<p>Your password has been changed!</p>

@component('mail::promotion')
<strong>Password hint: {!! $hint !!}</strong>.
@endcomponent

@component('mail::button', ['url' => 'http://ccsb.loc/admin'])
http://ccsb.loc/admin
@endcomponent

Thank you and blessings!<br>
<i>Community Church at SaddleBrooke</i>
@component('mail::subcopy')
***This is an automatically generated email, please do not reply
@endcomponent
@endcomponent