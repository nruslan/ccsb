<!-- search form -->
{!! Form::open(['url' => $url, 'id' => 'search', 'class' => 'sidebar-form', 'onsubmit' => 'return false;']) !!}
<div class="input-group">
    {!! Form::text('search', null, ['class' => 'form-control', 'id' => 'searchQuery', 'placeholder' => 'Search...']) !!}
    <span class="input-group-btn"><button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button></span>
</div>
{!! Form::close() !!}
<!-- /.search form -->

