{!! Html::script('/ckeditor/ckeditor.js') !!}
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    {!! Form::label($name, $title) !!}
    {!! Form::textarea($name, null, ['class' => 'form-control', 'id' => 'textarea1', 'autocomplete' => 'off']) !!}
    @if ($errors->has($name))<span class="help-block"><strong>{{ $errors->first($name) }}</strong></span>@endif
</div>
<script>CKEDITOR.replace( 'textarea1' );</script>