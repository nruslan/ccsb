@if (session()->has('message_success'))
    <div class="alert alert-success alert-dismissible text-center lead" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Well done!</strong> {!! session()->get('message_success') !!} <i class="fa fa-fw fa-thumbs-up fa-lg" aria-hidden="true"></i>
    </div>
@endif

@if (session()->has('message_info'))
    <div class="alert alert-info alert-dismissible text-center lead" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {!! session()->get('message_info') !!} <i class="fa fa-fw fa-info fa-lg" aria-hidden="true"></i>
    </div>
@endif