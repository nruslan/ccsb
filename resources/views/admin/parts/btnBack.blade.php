<a class="btn btn-primary aBtn pull-left {{ $btnClass ?? '' }}" href="{{ $url }}" data-toggle="tooltip" data-placement="top" title="back to {{ $title ?? '' }}">
    <i class="fa fa-fw fa-angle-double-left" aria-hidden="true"></i> Back
</a>