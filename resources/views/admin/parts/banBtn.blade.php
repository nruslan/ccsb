{!! Form::open(['method' => 'DELETE', 'action' => $action, 'id' => 'banForm']) !!}
<button type="submit" class="btn btn-danger btn-xs"
        data-toggle="confirmation"
        data-placement="left"
        data-popout="true" title="Delete {{ $name }}?">
    <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
</button>
{!! Form::close() !!}