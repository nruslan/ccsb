<div class="row">
    <div class="col-sm-4">
        <button type="submit"  class="btn btn-primary btn-lg" id="submitBtn">
            <i class="fa fa-fw fa-floppy-o"></i> {{ $btnName }}
        </button>
    </div>
    <div class="col-sm-5 col-sm-push-3">
        <a href="{{ url($cancelUrl) }}" class="btn btn-danger btn-lg pull-right aBtn">
            <i class="fa fa-fw fa-times-circle"></i> Cancel
        </a>
    </div>
</div>