{!! Form::open(['method' => 'DELETE', 'action' => $action, 'class' => 'deleteBtnForm']) !!}
<button type="submit" class="btn btn-danger {{ $class ?? '' }}" data-toggle="confirmation"
        data-btn-ok-label="Continue" data-btn-ok-icon="glyphicon glyphicon-share-alt"
        data-btn-ok-class="btn-success" data-singleton="true"
        data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
        data-btn-cancel-class="btn-danger"
        data-content=""
        data-placement="{{ $position ?? 'left' }}"
        data-popout="true" title="{{ $btnTitle }}?">
    <i class="fa fa-trash-o fa-fw"></i>
</button>
{!! Form::close() !!}