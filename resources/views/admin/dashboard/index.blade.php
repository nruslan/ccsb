@extends('layouts.admin')

@section('title', 'Dashboard')

@section('hTitle')
    Dashboard <small>Home page</small>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-fw fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ count($members) }}</div>
                            <div>Total Members!</div>
                        </div>
                    </div>
                </div>
                <a href="{{ url('/admin/accounts/members') }}">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-file-audio-o fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ count($sermons) }}</div>
                            <div>Total Sermons</div>
                        </div>
                    </div>
                </div>
                <a href="{{ url('/admin/sermons') }}">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-file-video-o fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ count($videos) }}</div>
                            <div>Total Videos</div>
                        </div>
                    </div>
                </div>
                <a href="{{ url('/admin/videos') }}">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-calendar fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ count($events) }}</div>
                            <div>Upcoming Events</div>
                        </div>
                    </div>
                </div>
                <a href="{{ url('/admin/events') }}">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-4">
            <h2 class="page-header">Latest Sermon</h2>
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-black" style="background: url({{ $sermon->thumbnail }}) center center;">
                    <h3 class="widget-user-username">{{ $sermon->title }} {{ $sermon->subtitle }}</h3>
                    <h5 class="widget-user-desc">by {{ $sermon->sermon_pastors }}</h5>
                </div>
                <div class="widget-user-image">
                    @foreach($sermon->pastors as $pastor)
                        <img class="img-circle" src="{{ $pastor->profile_picture_url }}" alt="User Avatar">
                    @endforeach
                </div>
                <div class="box-footer">
                    <div class="description-block">
                        <h5 class="description-header">{!! $sermon->title_subtitle !!}</h5>
                        @if($sermon->filename)
                            <audio controls="controls" title="{{ $sermon->title }}">
                                Your browser does not support the <code>audio</code> element.
                                <source src="{{ $sermon->sermon_url }}" type="audio/mp3">
                            </audio>
                        @else
                            <a href="{{ url("admin/sermons/file/create?id=$sermon->id") }}" class="btn btn-block btn-primary aBtn" data-toggle="tooltip" data-placement="top" title="upload audio file">
                                <i class="fa fa-upload fa-fw"></i> Upload
                            </a>
                        @endif
                    </div>
                    <!-- /.description-block -->
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{ $sermon->compact_date }}</h5>
                                <span class="description-text">DATE</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{ $sermon->duration }}</h5>
                                <span class="description-text">DURATION</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header">{{ $sermon->file_size }} MB</h5>
                                <span class="description-text">SIZE</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <h2 class="page-header">Latest Video</h2>
            <!-- 16:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $video->filename }}" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-lg-4">

        </div>
    </div>
@endsection

@section('scripts')

@endsection