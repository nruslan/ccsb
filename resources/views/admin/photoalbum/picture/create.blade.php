@extends('layouts.admin')

@section('title', 'Add New Pictures')

@section('hTitle')
    Add New Pictures
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/photoalbums') }}">Photoalbums</a></li>
    <li><a href="{{ url("/admin/photoalbums/$album->id") }}">{{ $album->title }}</a></li>
    <li class="active">Add New Pictures</li>
@endsection

@section('content')
    <div class="box box-primary">
        {!! Form::open(['url' => 'admin/photoalbums/pictures', 'files' => true, 'id' => 'myForm']) !!}
        {!! Form::hidden('album_id', $album->id) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-4 col-lg-offset-4">
                    <div class="form-group{{ $errors->has('input_files') ? ' has-error has-feedback' : '' }}">
                        <span class="fa fa-fw fa-image"></span> {!! Form::label('input_files', 'Picture Files') !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="required field"></i>
                        {!! Form::file('input_files[]',['class' => 'form-control input-lg', 'id' => 'inputFiles', 'accept' => '.jpg, .png', 'multiple', 'required']) !!}
                        @if ($errors->has('input_files'))
                            <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                            <span class="sr-only">(error)</span>
                            <span class="help-block"><strong>{{ $errors->first('input_files') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="progress hidden" id="progress">
                        <div class="progress-bar progress-bar-primary progress-bar-striped" id="output" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                            <span id="completed"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url('/admin/photoalbums')])
        </div>
        {!! Form::close() !!}
    </div>
@endsection