@extends('layouts.admin')

@section('title', 'Picture')

@section('hTitle')
    Picture
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/photoalbums') }}">Photoalbums</a></li>
    <li><a href="{{ url("/admin/photoalbums/$album->id") }}">{{ $album->title }}</a></li>
    <li class="active">Add New Pictures</li>
@endsection

@section('content')