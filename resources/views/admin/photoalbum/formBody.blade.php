<div class="row">
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title', 'Title') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group input-group-lg">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Sermon's title"><i class="fa fa-chevron-right fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('title', null, ['class' => 'form-control input-lg', 'placeholder' => 'sermons title', 'id' => 'title', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('title'))
                <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
                <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
            {!! Form::label('date', 'Date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group input-group-lg date" id="datepicker">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the video was recorded"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('date', null, ['class' => 'form-control input-lg', 'placeholder' => 'photo session date', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('date'))<span class="help-block"><strong>{{ $errors->first('date') }}</strong></span>@endif
        </div>
        @include('admin.parts.textarea', ['name' => 'description', 'title' => 'Photoalbum Description'])
    </div>
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
            {!! Form::label('event-location', 'Event Location') !!}
            <div class="row">
                @foreach($locations as $location)
                    <div class="col-lg-2">
                        <div class="radio">
                            <label>
                                {!! Form::radio('location_id', $location->id) !!}
                                <strong>{{ $location->addressName->name }}</strong><br>
                                {{ $location->address1 }}<br>{{ $location->city }}, {{ $location->state->name }} {{ $location->zip_code }}
                            </label>
                        </div>
                    </div>
                @endforeach

                @if ($errors->has('location_id'))
                    <span class="help-block"><strong>{{ $errors->first('location_id') }}</strong></span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('ministry_id') ? ' has-error' : '' }}">
            {!! Form::label('ministry_id', 'Ministries') !!}
            @foreach($ministries as $ministry)
                <div class="radio">
                    <label>
                        {!! Form::checkbox("ministry_id[$ministry->id]", $ministry->id) !!}
                        <strong>{{ $ministry->name }}</strong>
                    </label>
                </div>
            @endforeach
            @if ($errors->has('ministry_id'))<span class="help-block"><strong>{{ $errors->first('ministry_id') }}</strong></span>@endif
        </div>
    </div>
</div>