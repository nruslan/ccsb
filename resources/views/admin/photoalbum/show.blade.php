@extends('layouts.admin')

@section('title', $album->title)

@section('hTitle')
    Photoalbum: {{ $album->title }} <small>{{ $album->date }}</small>
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/photoalbums') }}">Photoalbums</a></li>
    <li class="active">{{ $album->title }}</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            {{ $album->title }} <span class="badge" data-toggle="tooltip" data-placement="top" title="Total pictures">{{ $album->pictures->count() }}</span>
            <a class="btn btn-primary btn-xs aBtn pull-right" href="{{ url("admin/photoalbums/$album->id/edit") }}" data-toggle="tooltip" data-placement="top" title="edit photo album's info"><i class="fa fa-fw fa-pencil"></i> Edit</a></div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! $album->description or 'n/a' !!}
            <div class="row">
                @foreach($album->pictures as $picture)
                    <div class="col-md-2 text-center">
                        <a href="{{ asset($album->album_url.$picture->filename) }}" target="_blank">
                            <img src="{{ asset("$album->album_url/thumbnails/$picture->filename") }}" alt="{{ $picture->alt or null }}" class="img-responsive">
                        </a>
                        @include('admin.parts.deleteBtn', ['action' => ['Admin\Photoalbum\PictureController@destroy', $picture->id], 'class' => 'btn-xs', 'btnTitle' => "Delete", 'position' => 'top'])
                    </div>
                @endforeach
            </div>
            <br>
            <a href="{{ url("admin/photoalbums/pictures/create?id=$album->id") }}" class="btn btn-block btn-primary aBtn" data-toggle="tooltip" data-placement="top" title="upload file">
                <i class="fa fa-upload fa-fw"></i> Upload
            </a>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix"></div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection