@extends('layouts.admin')

@section('title', 'Photoalbums')

@section('hTitle')
    Photoalbums
@endsection

@section('breadcrumbs')
    <li class="active">Photoalbums</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        Total <span class="badge">{{ $photoalbums->total() }}</span>
        <a href="{{ url('admin/photoalbums/create') }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="create new photoalbum">
            <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i> Add</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding table-responsive">
        @include('admin.parts.message')
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th></th>
                <th>Title</th>
                <th>Location</th>
                <th colspan="3"></th>
            </tr>
            </thead>
            <tbody id="result">
            @foreach($photoalbums as $album)
                <tr>
                    <td scope="col">{{ $i++ }}</td>
                    <td>{{ $album->date }}</td>
                    <td><a href="{{ url("/admin/photoalbums/$album->id") }}" data-toggle="tooltip" data-placement="left" title="view details"><img src="{{ $album->album_thumbnail_uri }}" alt="" width="20"></a></td>
                    <td><a href="{{ url("/admin/photoalbums/$album->id") }}">{{ $album->title }}</a></td>
                    <td>{{ $album->locationName->name }}</td>
                    <td class="text-center">
                        <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/photoalbums/$album->id") }}" data-toggle="tooltip" data-placement="left" title="view details">
                            <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/photoalbums/$album->id/edit") }}" data-toggle="tooltip" data-placement="left" title="edit">
                            <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-right">
                        @include('admin.parts.deleteBtn', ['action' => ['Admin\Photoalbum\IndexController@destroy', $album->id], 'class' => 'btn-xs', 'btnTitle' => "Delete $album->title"])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">{{ $photoalbums->links('vendor.pagination.admin-lte') }}</div>
</div>
@endsection