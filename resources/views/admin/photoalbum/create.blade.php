@extends('layouts.admin')

@section('title', 'Add New Photoalbum')

@section('hTitle')
    Add New Photoalbum
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/photoalbums') }}">Photoalbums</a></li>
    <li class="active">Add New Photoalbum</li>
@endsection

@section('content')
    <div class="box box-primary">
        {!! Form::open(['url' => 'admin/photoalbums', 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.photoalbum.formBody')
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url('/admin/photoalbums')])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({format: 'MM/DD/YYYY'});

            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection