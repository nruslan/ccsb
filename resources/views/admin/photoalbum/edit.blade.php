@extends('layouts.admin')

@section('title', 'Edit Photoalbum')

@section('hTitle')
    Edit Photoalbum
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/photoalbums') }}">Photoalbums</a></li>
    <li class="active">Add New Photoalbum</li>
@endsection

@section('content')
    <div class="box box-primary">
        {!! Form::model($album, ['method' => 'PATCH', 'action' => ['Admin\Photoalbum\IndexController@update', $album->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.photoalbum.formBody')
            <div class="form-group{{ $errors->has('picture_id') ? ' has-error' : '' }}">
                <h4>Album Thumbnail {{ $album->picture_id }}</h4>
                @foreach($album->pictures as $picture)
                    <div class="radio radio-inline">
                        <label>
                            @if($album->picture_id == $picture->id)
                                {!! Form::radio('picture_id', $picture->id, true); !!}
                            @else
                                {!! Form::radio('picture_id', $picture->id); !!}
                            @endif
                            <img src="{{ asset("$album->album_url/thumbnails/$picture->filename") }}" alt="{{ $picture->alt or null }}" class="img-responsive" width="70">
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url('/admin/photoalbums')])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({format: 'MM/DD/YYYY'});

            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection