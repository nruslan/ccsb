@extends('layouts.admin')

@section('title')
    {{ $committee->name }}
@endsection

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/committees?page=$pageId"), 'title' => 'committees list'])
    Committee: {{ $committee->name or '- title -' }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/committees') }}"><i class="fa fa-table"></i> Committees</a></li>
    <li class="active">{{ $committee->name }}</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $committee->name }}</h3>
            <a href="{{ url("/admin/committees/$committee->id/edit") }}" class="btn btn-primary btn-xs aBtn pull-right" data-toggle="tooltip" data-placement="top" title="edit committee">
                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i> edit
            </a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @include('admin.parts.message')
            <div class="row">
                <div class="col-lg-6">
                    <h3>Description</h3>
                    <p class="lead">
                        {!! $committee->description !!}
                    </p>
                </div>
                <div class="col-lg-6">
                    <h3>
                        Active Committee members <span class="badge">{{ $committee->total_active_members }}</span>
                        <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/committees/member/create?id=$committee->id") }}" data-container="body" data-toggle="tooltip" data-placement="top" title="add member">
                            <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i>
                        </a>
                    </h3>
                    <table class="table">
                        <tbody>
                        @foreach($committee->active_members as $member)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $member->first_name }} {{ $member->last_name }}</td>
                                <td><small>member since</small> {{ date("F j, Y", strtotime($member->pivot->term_started)) }}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs aBtn" href="{{ url('/admin/committees/member/'.$member->pivot->id.'/edit') }}">
                                        <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <h3>Past Committee members</h3>
                    <table class="table">
                        <tbody>
                        @foreach($committee->past_members as $member)
                            <tr>
                                <td>{{ $ii++ }}</td>
                                <td>{{ $member->first_name }} {{ $member->last_name }}</td>
                                <td>{{ date("F j, Y", strtotime($member->pivot->term_started)) }}</td>
                                <td>{{ date("F j, Y", strtotime($member->pivot->term_ended)) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include("admin.parts.deleteBtn", ['action' => ['Admin\Committees\IndexController@destroy', $committee->id], 'class' => 'pull-right', 'btnTitle' => "$committee->name committee"])
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection