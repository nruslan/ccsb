<div class="row">
    <div class="col-lg-8 col-lg-offset-2">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', 'Committee Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group input-group-lg">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify name"><i class="fa fa-info fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('name', null, ['class' => 'form-control input-lg', 'placeholder' => 'committee name', 'autocomplete' => 'off', 'autofocus']) !!}
            </div>
            @if ($errors->has('name'))<span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>@endif
        </div>
        @include('admin.parts.textarea', ['name' => 'description', 'title' => 'Committee Description'])
    </div>
</div>


