@extends('layouts.admin')

@section('title')
    Committees
@endsection

@section('hTitle')
    Church Committees
@endsection

@section('breadcrumbs')
    <li class="active">Committees</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            Total committees <span class="badge">{{ $committees->total() }}</span>
            <a href="{{ url('/admin/committees/create') }}" class="btn btn-primary btn-xs aBtn pull-right" data-toggle="tooltip" data-placement="top" title="add new committee">
                <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i> Add
            </a>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            @include('admin.parts.message')
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Committee name</th>
                    <th class="text-center">Total Members</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($committees as $committee)
                    <tr>
                        <th scope="col">{{ $i++ }}</th>
                        <td><a class="btn btn-link btn-xs aBtn" href="{{ url("/admin/committees/$committee->id") }}"><i class="fa fa-fw fa-eye" aria-hidden="true"></i> {{ $committee->name }}</a></td>
                        <td class="text-center">{{ $committee->total_active_members }}</td>
                        <td class="text-center">
                            <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/committees/$committee->id") }}" data-toggle="tooltip" data-placement="left" title="view details">
                                <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-right">
                            @include('admin.parts.deleteBtn', ['action' => ['Admin\Committees\IndexController@destroy', $committee->id], 'class' => 'btn-xs', 'btnTitle' => "Delete $committee->name"])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $committees->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection