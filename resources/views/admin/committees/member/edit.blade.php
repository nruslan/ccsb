@extends('layouts.admin')

@section('title')
    Edit Committee member info
@endsection

@section('hTitle')
    Edit Terms, {{ $committeeMember->committee->name }} member: {{ $committeeMember->member->full_name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/committees') }}"><i class="fa fa-table"></i> Committees</a></li>
    <li><a href="{{ url("/admin/committees/$committeeMember->committee_id") }}"><i class="fa fa-table"></i> {{ $committeeMember->committee->name }}</a></li>
    <li class="active">Edit Terms</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header"></div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::model($committeeMember, ['method' => 'PATCH', 'action' => ['Admin\Committees\MembersController@update', $committeeMember->id], 'id' => 'myForm']) !!}
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group{{ $errors->has('term_started') ? ' has-error' : '' }}">
                        {!! Form::label('term_started', 'Term started') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg date" data-provide="datepicker">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the term is started"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('term_started', null, ['class' => 'form-control input-lg', 'placeholder' => 'term started', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('term_started'))<span class="help-block"><strong>{{ $errors->first('term_started') }}</strong></span>@endif
                    </div>
                    <div class="form-group{{ $errors->has('term_ended') ? ' has-error' : '' }}">
                        {!! Form::label('term_ended', 'Term Ended') !!}
                        <div class="input-group input-group-lg date" data-provide="datepicker">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the term is ended"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('term_ended', null, ['class' => 'form-control input-lg', 'placeholder' => 'term ended', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('term_ended'))<span class="help-block"><strong>{{ $errors->first('term_ended') }}</strong></span>@endif
                    </div>
                </div>
            </div>
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/committees/$committeeMember->committee_id")])
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('.date').datepicker({orientation: "bottom auto"});

            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection