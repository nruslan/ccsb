@extends('layouts.admin')

@section('title')
    Add new members to the committee
@endsection

@section('hTitle')
    Add new member to {{ $committee->name }} committee
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/committees') }}"><i class="fa fa-table"></i> Committees</a></li>
    <li><a href="{{ url("/admin/committees/$committee->id") }}"><i class="fa fa-table"></i> {{ $committee->name }}</a></li>
    <li class="active">Add Member</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header"></div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    {!! Form::open(['url' => '/admin/search/member', 'id' => 'search']) !!}
                    <div class="form-group{{ $errors->has('member_name') ? ' has-error' : '' }}">
                        {!! Form::label('member_name', 'Get Member') !!}
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify username"><i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('member_name', null, ['class' => 'form-control input-lg', 'id' => 'memberName', 'placeholder' => 'name', 'autocomplete' => 'off', 'autofocus']) !!}
                            <span class="input-group-btn"><button type="submit" class="btn btn-info btn-flat" id="searchMember"><i class="fa fa-search fa-fw" aria-hidden="true"></i></button></span>
                        </div>
                        @if ($errors->has('member_name'))
                            <span class="help-block"><strong>{{ $errors->first('member_name') }}</strong></span>
                        @endif
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            {!! Form::open(['url' => '/admin/committees/member', 'id' => 'myForm']) !!}
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    {!! Form::hidden('committee_id', $committee->id) !!}
                    <div class="form-group{{ $errors->has('member_id') ? ' has-error' : '' }}">
                        <div id="memberResult"></div>
                        @if ($errors->has('member_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('member_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('term_started') ? ' has-error' : '' }}">
                        {!! Form::label('term_started', 'Term started') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg date" data-provide="datepicker">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the term is started"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('term_started', null, ['class' => 'form-control input-lg', 'placeholder' => 'term started', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('term_started'))<span class="help-block"><strong>{{ $errors->first('term_started') }}</strong></span>@endif
                    </div>
                </div>
            </div>
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/committees/$committee->id")])
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('.date').datepicker({orientation: "bottom auto"});

            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });

            // Get members by AJAX
            $("#search").on('submit', function(e) {
                e.preventDefault();
                $('#searchMember').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                $.ajaxSetup({
                    headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
                });
                $.ajax({
                    method: 'POST',
                    url: $("#search").attr('action'),
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data){
                        $('#memberResult').html(data);
                        $('#searchMember').prop('disabled',false).html('<i class="fa fa-search fa-fw" aria-hidden="true"></i>');
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
        });
    </script>
@endsection