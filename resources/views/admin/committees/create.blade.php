@extends('layouts.admin')

@section('title')
    Add new committee
@endsection

@section('hTitle')
    Add new committee
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/committees', 'id' => 'myForm']) !!}
                    @include('admin.committees.formBody')
                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => '/admin/committees'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection