@extends('layouts.admin')

@section('title')
   Edit committee
@endsection

@section('hTitle')
    Edit {{ $committee->name }} committee
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/committees') }}"><i class="fa fa-table"></i> Committees</a></li>
    <li><a href="{{ url("/admin/committees/$committee->id") }}"><i class="fa fa-table"></i> {{ $committee->name }}</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">{{ $committee->name }}</div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::model($committee, ['method' => 'PATCH', 'action' => ['Admin\Committees\IndexController@update', $committee->id], 'id' => 'myForm']) !!}
            @include('admin.committees.formBody')
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/committees/$committee->id")])
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection