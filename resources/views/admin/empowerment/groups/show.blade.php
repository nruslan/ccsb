@extends('layouts.admin')

@section('title', "$group->name Empowerment Group")

@section('hTitle')
    {{ $group->name }} Empowerment Group <small>{{ $group->time }}</small>
@endsection

@section('breadcrumbs')
    <li class="active">Empowerment Group</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h1 class="box-title">{{ $group->name }} <small>Total Members <span class="badge">{{ $group->total_members }}</span></small></h1>
            <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/groups/$group->id/edit") }}" data-toggle="tooltip" data-placement="top" title="edit group"><i class="fa fa-fw fa-pencil"></i></a>
            <a href="{{ url("admin/groups/$group->id/member/create") }}" class="btn btn-primary btn-xs aBtn pull-right" data-toggle="tooltip" data-placement="left" title="add new member">
                <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i> add</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding table-responsive">
            @include('admin.parts.message')
            <h3 class="text-center">Leaders</h3>
            <table class="table table-striped">
                <tbody>
                @foreach($leaders as $leader)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>@include('admin.empowerment.partials.memberBtn', ['action' => ['Admin\Empowerment\MembersController@update',$group->id ,$leader->id], 'name' => $leader->member->full_name])</td>
                        <td>{{ $leader->member->full_name }}</td>
                        <td>@include('admin.parts.deleteBtn', ['action' => ['Admin\Empowerment\MembersController@destroy',$group->id ,$leader->id], 'btnTitle' => "Delete from the group", 'class' => 'btn-xs'])</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <h3 class="text-center">Members</h3>
            <table class="table table-striped">
                <tbody>
                @foreach($members as $member)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>@include('admin.empowerment.partials.leaderBtn', ['action' => ['Admin\Empowerment\MembersController@update',$group->id ,$member->id], 'name' => $member->member->full_name])</td>
                        <td>{{ $member->member->full_name }}</td>
                        <td>@include('admin.parts.deleteBtn', ['action' => ['Admin\Empowerment\MembersController@destroy',$group->id ,$member->id], 'btnTitle' => "Delete from the group", 'class' => 'btn-xs'])</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix"></div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
            $(".leaderBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
            $(".memberBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection