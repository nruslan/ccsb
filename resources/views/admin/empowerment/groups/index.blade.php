@extends('layouts.admin')

@section('title', "Empowerment Groups")

@section('hTitle')
    Empowerment Groups
@endsection

@section('breadcrumbs')
    <li class="active">Empowerment Groups</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            Total Groups <span class="badge">{{ $groups->total() }}</span>
            <a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/groups/create") }}" data-toggle="tooltip" data-placement="top" title="add new group"><i class="fa fa-fw fa-plus"></i> add</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            @include('admin.parts.message')
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Group Name</th>
                    <th>Time</th>
                    <th>Location</th>
                    <th class="text-center">Total Members</th>
                    <th colspan="2"></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($groups as $group)
                    <tr>
                        <th scope="col">{{ $i++ }}</th>
                        <td>
                            <a class="btn btn-link btn-xs aBtn" href="{{ url("/admin/groups/$group->id") }}" data-toggle="tooltip" data-placement="left" title="view {{ $group->name }}'s details">
                                <i class="fa fa-fw fa-eye"></i> {{ $group->name }}
                            </a>
                        </td>
                        <td>{{ $group->time ?? '---' }}</td>
                        <td></td>
                        <td class="text-center">{{ $group->total_members }}</td>
                        <td class="text-center">
                            <a class="btn btn-default btn-xs" href="{{ url("/admin/groups/$group->id") }}" data-toggle="tooltip" data-placement="left" title="view {{ $group->name }}'s details">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/groups/$group->id/edit") }}" data-toggle="tooltip" data-placement="top" title="edit group">
                                <i class="fa fa-fw fa-pencil"></i></a>
                        </td>
                        <td class="text-right">
                            @include('admin.parts.deleteBtn', ['action' => ['Admin\Empowerment\GroupsController@destroy', $group->id], 'class' => 'btn-xs', 'btnTitle' => "Delete $group->name group"])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $groups->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection