@extends('layouts.admin')

@section('title', "Empowerment Group")

@section('hTitle')
    Empowerment Group {{ $group->name }} {{ $group->time }}
@endsection

@section('breadcrumbs')
    <li class="active">Empowerment Groups</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        Edit Group {{ $group->name }} {{ $group->time }}
    </div><!-- /.box-header -->
    {!! Form::model($group, ['method' => 'PATCH', 'url' => "admin/groups/$group->id", 'id' => 'myForm']) !!}
    <div class="box-body">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('name', 'Group Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify the group name"><i class="fa fa-info fa-fw" aria-hidden="true"></i></span>
                        {!! Form::text('name', null, ['class' => 'form-control input-lg', 'placeholder' => 'group name', 'autocomplete' => 'off', 'autofocus']) !!}
                    </div>
                    @if ($errors->has('name'))<span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>@endif
                </div>
                <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                    {!! Form::label('time', "Time") !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="field is required "></i>
                    <div class="input-group input-group-lg date" id="datepicker0">
                        <span class="input-group-addon"><i class="fa fa-fw fa-clock-o"></i></span>
                        {!! Form::text('time', null, ['class' => 'form-control input-lg','placeholder' => 'time']) !!}
                    </div>
                    @if ($errors->has('time'))<span class="help-block"><strong>{{ $errors->first('time') }}</strong></span>@endif
                </div>
                @include('admin.parts.textarea', ['name' => 'description', 'title' => 'Group Description'])
            </div>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("admin/groups/$group->id")])
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker0").datetimepicker({ format: 'LT' });

            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection