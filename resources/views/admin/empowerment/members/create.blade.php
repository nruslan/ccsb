@extends('layouts.admin')

@section('title', 'Add new members to the Empowerment Group')

@section('hTitle')
    Add new member to the {{ $group->name }} {{ $group->time }} Empowerment Group
@endsection

@section('breadcrumbs')
    <li class="active">Add New Member</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header"></div><!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    {!! Form::open(['url' => 'admin/search/member', 'id' => 'search']) !!}
                    <div class="form-group{{ $errors->has('member_name') ? ' has-error' : '' }}">
                        {!! Form::label('member_name', 'Get Member') !!}
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify member name"><i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('member_name', null, ['class' => 'form-control input-lg', 'id' => 'memberName', 'placeholder' => 'name', 'autocomplete' => 'off', 'autofocus']) !!}
                            <span class="input-group-btn"><button type="submit" class="btn btn-info btn-flat" id="searchMember"><i class="fa fa-search fa-fw" aria-hidden="true"></i></button></span>
                        </div>
                        @if ($errors->has('member_name'))<span class="help-block"><strong>{{ $errors->first('member_name') }}</strong></span>@endif
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            {!! Form::open(['url' => "admin/groups/$group->id/member", 'id' => 'myForm']) !!}
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group{{ $errors->has('member_id') ? ' has-error' : '' }}">
                        <div id="memberResult"></div>
                        @if ($errors->has('member_id'))<span class="help-block">{{ $errors->first('member_id') }}</span>@endif
                    </div>
                </div>
            </div>
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("admin/groups/$group->id")])
            {!! Form::close() !!}
        </div><!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });

            // Get members by AJAX
            $("#search").on('submit', function(e) {
                e.preventDefault();
                $('#searchMember').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                $.ajaxSetup({
                    headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
                });
                $.ajax({
                    method: 'POST',
                    url: $("#search").attr('action'),
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data){
                        $('#memberResult').html(data);
                        $('#searchMember').prop('disabled',false).html('<i class="fa fa-search fa-fw" aria-hidden="true"></i>');
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
        });
    </script>
@endsection