{!! Form::open(['method' => 'PATCH', 'action' => $action, 'class' => 'leaderBtnForm']) !!}
<button type="submit" class="btn btn-default btn-xs" data-toggle="confirmation"
        data-placement="right"
        data-content="Do you want to make {{ $name ?? 'Member' }} the leader of the group?"
        data-singleton="true"
        data-popout="true" title="Make Him a Leader">
    <i class="fa fa-fw fa-graduation-cap" aria-hidden="true"></i>
</button>
{!! Form::close() !!}