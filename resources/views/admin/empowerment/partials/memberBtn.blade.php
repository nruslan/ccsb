{!! Form::open(['method' => 'PATCH', 'action' => $action, 'class' => 'memberBtnForm']) !!}
<button type="submit" class="btn btn-default btn-xs" data-toggle="confirmation"
        data-placement="right"
        data-content="Do you want to remove {{ $name ?? 'Member' }} from the leaders of the group?"
        data-singleton="true"
        data-popout="true" title="Remove Him from the Leaders">
    <i class="fa fa-fw fa-graduation-cap" aria-hidden="true"></i>
</button>
{!! Form::close() !!}