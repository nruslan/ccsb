<div class="row">
    <div class="col-md-4 col-md-offset-2">
        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
            {!! Form::label('address', 'Email address') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::text('address', null, ['class' => 'form-control input-lg', 'placeholder' => 'email address']) !!}
            @if ($errors->has('address'))
                <span class="help-block">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('visibility_id') ? ' has-error' : '' }}">
            {!! Form::label('visibility_id', 'Visibility mode') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('visibility_id', $visibility, null, ['class' => 'form-control input-lg']) !!}
            @if ($errors->has('visibility_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('visibility_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>