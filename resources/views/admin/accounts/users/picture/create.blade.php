@extends('layouts.admin')

@section('title')
    User: {{ $user->username }}
@endsection

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/accounts/users/$user->id"), 'title' => "$user->username's account"])
    User's account: {{ $user->username }}
@endsection

@section('styles')
    <style type="text/css">
        .imageBox
        {
            position: relative;
            height: 800px;
            width: 800px;
            border:1px solid #aaa;
            background: #fff;
            overflow: hidden;
            background-repeat: no-repeat;
            cursor:move;
        }

        .imageBox .thumbBox
        {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 502px;
            height: 502px;
            margin-top: -251px;
            margin-left: -251px;
            box-sizing: border-box;
            border: 1px solid rgb(102, 102, 102);
            box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
            background: none repeat scroll 0% 0% transparent;
        }

        .imageBox .spinner
        {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            text-align: center;
            line-height: 400px;
            background: rgba(0,0,0,0.7);
        }

        .container
        {
            position: absolute;
            top: 10%; left: 10%; right: 0; bottom: 0;
        }
        .action
        {
            width: 600px;
            height: 50px;
            margin: 10px 0;
        }
        .cropped>img
        {
            margin-right: 10px;
        }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-6">
            <div class="imageBox">
                <div class="thumbBox"></div>
                <div class="spinner" style="display: none">Loading...</div>
            </div>
            <div class="action">
                <div class="form-group">
                    <label class="sr-only" for="file">Image</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-file-image-o" aria-hidden="true"></i></div>
                        <input type="file"  class="form-control input-lg" id="file" autofocus>
                    </div>
                </div>
                <div class="form-inline text-center">
                    <div class="form-group">
                        <button class="btn btn-default btn-lg" id="btnZoomIn"><i class="fa fa-fw fa-search-plus" aria-hidden="true"></i></button>
                        <button class="btn btn-default btn-lg" id="btnZoomOut"><i class="fa fa-fw fa-search-minus" aria-hidden="true"></i></button>
                    </div>
                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" id="btnCrop">crop <i class="fa fa-crop" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cropped">
                        <img src="http://via.placeholder.com/500x500" alt="thumbnail image">
                    </div>
                    {!! Form::open(['url' => '/admin/accounts/users/picture']) !!}
                    {!! Form::hidden('id', $user->id) !!}
                    {!! Form::textarea('imageData', null, ['class' => 'form-control hidden', 'readonly']) !!}
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        var cropbox = function(options){
            var el = document.querySelector(options.imageBox),
                obj =
                    {
                        state : {},
                        ratio : 1,
                        options : options,
                        imageBox : el,
                        thumbBox : el.querySelector(options.thumbBox),
                        spinner : el.querySelector(options.spinner),
                        image : new Image(),
                        getDataURL: function ()
                        {
                            var width = this.thumbBox.clientWidth,
                                height = this.thumbBox.clientHeight,
                                canvas = document.createElement("canvas"),
                                dim = el.style.backgroundPosition.split(' '),
                                size = el.style.backgroundSize.split(' '),
                                dx = parseInt(dim[0]) - el.clientWidth/2 + width/2,
                                dy = parseInt(dim[1]) - el.clientHeight/2 + height/2,
                                dw = parseInt(size[0]),
                                dh = parseInt(size[1]),
                                sh = parseInt(this.image.height),
                                sw = parseInt(this.image.width);

                            canvas.width = width;
                            canvas.height = height;
                            var context = canvas.getContext("2d");
                            context.drawImage(this.image, 0, 0, sw, sh, dx, dy, dw, dh);
                            var imageData = canvas.toDataURL('image/png');
                            return imageData;
                        },
                        getBlob: function()
                        {
                            var imageData = this.getDataURL();
                            var b64 = imageData.replace('data:image/png;base64,','');
                            var binary = atob(b64);
                            var array = [];
                            for (var i = 0; i < binary.length; i++) {
                                array.push(binary.charCodeAt(i));
                            }
                            return  new Blob([new Uint8Array(array)], {type: 'image/png'});
                        },
                        zoomIn: function ()
                        {
                            this.ratio*=1.1;
                            setBackground();
                        },
                        zoomOut: function ()
                        {
                            this.ratio*=0.9;
                            setBackground();
                        }
                    },
                attachEvent = function(node, event, cb)
                {
                    if (node.attachEvent)
                        node.attachEvent('on'+event, cb);
                    else if (node.addEventListener)
                        node.addEventListener(event, cb);
                },
                detachEvent = function(node, event, cb)
                {
                    if(node.detachEvent) {
                        node.detachEvent('on'+event, cb);
                    }
                    else if(node.removeEventListener) {
                        node.removeEventListener(event, render);
                    }
                },
                stopEvent = function (e) {
                    if(window.event) e.cancelBubble = true;
                    else e.stopImmediatePropagation();
                },
                setBackground = function()
                {
                    var w =  parseInt(obj.image.width)*obj.ratio;
                    var h =  parseInt(obj.image.height)*obj.ratio;

                    var pw = (el.clientWidth - w) / 2;
                    var ph = (el.clientHeight - h) / 2;

                    el.setAttribute('style',
                        'background-image: url(' + obj.image.src + '); ' +
                        'background-size: ' + w +'px ' + h + 'px; ' +
                        'background-position: ' + pw + 'px ' + ph + 'px; ' +
                        'background-repeat: no-repeat');
                },
                imgMouseDown = function(e)
                {
                    stopEvent(e);

                    obj.state.dragable = true;
                    obj.state.mouseX = e.clientX;
                    obj.state.mouseY = e.clientY;
                },
                imgMouseMove = function(e)
                {
                    stopEvent(e);

                    if (obj.state.dragable)
                    {
                        var x = e.clientX - obj.state.mouseX;
                        var y = e.clientY - obj.state.mouseY;

                        var bg = el.style.backgroundPosition.split(' ');

                        var bgX = x + parseInt(bg[0]);
                        var bgY = y + parseInt(bg[1]);

                        el.style.backgroundPosition = bgX +'px ' + bgY + 'px';

                        obj.state.mouseX = e.clientX;
                        obj.state.mouseY = e.clientY;
                    }
                },
                imgMouseUp = function(e)
                {
                    stopEvent(e);
                    obj.state.dragable = false;
                },
                zoomImage = function(e)
                {
                    var evt=window.event || e;
                    var delta=evt.detail? evt.detail*(-120) : evt.wheelDelta;
                    delta > -120 ? obj.ratio*=1.1 : obj.ratio*=0.9;
                    setBackground();
                }

            obj.spinner.style.display = 'block';
            obj.image.onload = function() {
                obj.spinner.style.display = 'none';
                setBackground();

                attachEvent(el, 'mousedown', imgMouseDown);
                attachEvent(el, 'mousemove', imgMouseMove);
                attachEvent(document.body, 'mouseup', imgMouseUp);
                var mousewheel = (/Firefox/i.test(navigator.userAgent))? 'DOMMouseScroll' : 'mousewheel';
                attachEvent(el, mousewheel, zoomImage);
            };
            obj.image.src = options.imgSrc;
            attachEvent(el, 'DOMNodeRemoved', function(){detachEvent(document.body, 'DOMNodeRemoved', imgMouseUp)});

            return obj;
        };

        window.onload = function() {
            var options =
                {
                    imageBox: '.imageBox',
                    thumbBox: '.thumbBox',
                    spinner: '.spinner',
                    imgSrc: 'avatar.png'
                };
            var cropper;
            document.querySelector('#file').addEventListener('change', function(){
                var reader = new FileReader();
                reader.onload = function(e) {
                    options.imgSrc = e.target.result;
                    cropper = new cropbox(options);
                };
                reader.readAsDataURL(this.files[0]);
                this.files = [];
            });
            document.querySelector('#btnCrop').addEventListener('click', function(){

                this.setAttribute("disabled", "");

                var img = cropper.getDataURL();
                document.querySelector('.cropped').innerHTML = "";
                document.querySelector('.cropped').innerHTML += '<img src="'+img+'">';
                document.querySelector("textarea").innerHTML += img;

            });
            // Zoom in
            document.querySelector('#btnZoomIn').addEventListener('click', function(){
                cropper.zoomIn();
            });
            // Zoom out
            document.querySelector('#btnZoomOut').addEventListener('click', function(){
                cropper.zoomOut();
            })
        };


    </script>
@endsection