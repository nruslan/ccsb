@extends('layouts.admin')

@section('title')
    New Account
@endsection

@section('hTitle')
    Add New Account
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('admin/accounts/users') }}"><i class="fa fa-users"></i> Users Accounts</a></li>
    <li class="active">Add New User Account</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/accounts/users', 'id' => 'myForm']) !!}
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                {!! Form::label('username', 'Last Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                                <div class="input-group">
                                    <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify username"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                                    {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'last name', 'autocomplete' => 'off', 'autofocus']) !!}
                                </div>
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('email', 'Email') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                                <div class="input-group">
                                    <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify the email"><i class="fa fa-envelope fa-fw" aria-hidden="true"></i></span>
                                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'email address', 'autocomplete' => 'off']) !!}
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('since') ? ' has-error' : '' }}">
                                {!! Form::label('since', 'Church Member Since Date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                                <div class="input-group date" id="datepicker">
                                    <span class="input-group-addon"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                                    {!! Form::text('since', null, ['class' => 'form-control', 'placeholder' => 'date', 'autocomplete' => 'off']) !!}
                                </div>
                                @if ($errors->has('since'))
                                    <span class="help-block"><strong>{{ $errors->first('since') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url('admin/accounts/users')])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({format: 'MM/DD/YYYY'});
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection