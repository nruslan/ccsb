{!! Form::open(['url' => "/admin/accounts/users/$id/reset-password", 'class' => 'form-inline', 'id' => 'resetForm']) !!}
<button type="submit" class="btn btn-warning"
        data-toggle="confirmation"
        data-placement="top"
        data-popout="true" title="Reset account's password?">
    <i class="fa fa-fw fa-retweet" aria-hidden="true"></i>
</button>
{!! Form::close() !!}