<html>
<body>
<div style="margin: 0;padding: 0;background-color: #ffffff; min-height: 100%;">
    <div style="margin: 0 auto;padding:0;background-color: #A2A791; width: 590px;">
        <table width="100%" style="min-width: 348px;" border="0" cellspacing="0" cellpadding="0">
            <tbody>
            <tr height="32px">
                <td></td>
            </tr>
            <tr>
                <td align="center">
                    <img style="vertical-align: top;" src="http://ccsb.us/media/images/logo-gray-sm.png" alt="Community Church at SaddleBrooke">
                    <hr>
                    <h1 style="margin:0; padding-top:20px; text-align:center;">Hello Dear <?= $user->username ?></h1>
                </td>
            </tr>
            <tr height="16px">
                <td></td>
            </tr>
            <tr>
                <td style="padding: 7px;">
                    <p style="text-align: center;font-family:Helvetica, Arial, sans-serif; font-size: 20px;">
                        Your log-in: <strong><?= $user->email ?></strong><br>
                        Your password: <strong><?= $password ?></strong>
                    <div style="text-align: center;font-family:Helvetica, Arial, sans-serif;">
                        <strong>This is an auto-generated password. You can reset it by following to this link:
                            <a href="http://ccsb.us/password/reset" target="_blank">www.ccsb.us/password/reset</a>.</strong>
                    </div>
                    </p>
                    <p style="text-align: center; font-family:Helvetica, Arial, sans-serif; font-size: 25px;">
                        Like and follow us on <a href="https://www.facebook.com/CommunityChurchSaddleBrooke/">facebook</a>
                    </p>
                    <hr>
                    <p style="text-align: center; font-family:Helvetica, Arial, sans-serif; font-size: 14px;">Community Church at Saddlebrooke &copy; 2016</p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>