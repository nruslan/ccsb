<tr>
    <td colspan="8">
        @include('admin.parts.btnBack', ['url' => url("/admin/accounts/users"), 'btnClass' => 'btn-xs', 'title' => "users list"]) <strong>Search Results</strong> <span class="badge">{{ $users->count() }}</span>
    </td>
</tr>
@foreach($users as $user)
    <tr>
        <th scope="row">{{ $i++ }}</th>
        <td class="text-center">
            @if($user->verified == 'yes')
                <a class="btn btn-success btn-xs verify" href="{{ url("/admin/accounts/users/$user->id/verify") }}" role="button">
                    <i class="fa fa-fw fa-smile-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Verified"></i>
                </a>
            @else
                <a class="btn btn-default btn-xs verify" href="{{ url("/admin/accounts/users/$user->id/verify") }}" role="button">
                    <i class="fa fa-fw fa-frown-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Unverified"></i>
                </a>
            @endif
        </td>
        <td>
            <a href="{{ url("/admin/accounts/users/$user->id") }}" class="aBtnLink" data-toggle="tooltip" data-placement="left" title="view details">
                <i class="fa fa-fw fa-user-o" aria-hidden="true"></i> {{ $user->username }}
            </a>
        </td>
        <td>{{ $user->email }}</td>
        <td>{{ date('F d, Y', strtotime($user->created_at)) }}</td>
        <td class="text-center">{!! $user->picture_checker !!}</td>
        <td class="text-center">{{ count($user->members) }}</td>
        <td>
            <ul class="list-inline">
                <li>
                    <a href="{{ url("/admin/accounts/users/$user->id") }}" class="btn btn-default btn-xs aBtn" data-toggle="tooltip" data-placement="left" title="view details">
                        <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </td>
    </tr>
@endforeach