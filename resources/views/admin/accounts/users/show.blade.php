@extends('layouts.admin')

@section('title')
    User: {{ $user->username }}
@endsection

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/accounts/users?page=$pageId"), 'title' => 'accounts list'])
    Users account
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('admin/accounts/users') }}"><i class="fa fa-users"></i> Users Accounts</a></li>
    <li class="active">{{ $user->username }}'s Account</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('admin.parts.message')
            <div class="media">
                <div class="media-left media-top">
                    @if($user->profilePicture)
                        <img src="{{ asset("storage/accounts/users/". $user->profilePicture->filename) }}" class="media-object img-responsive img-thumbnail img-rounded" alt="Responsive image">
                    @else
                        <img src="http://placehold.it/500x500" class="media-object img-responsive img-thumbnail img-rounded" alt="Responsive image">
                    @endif
                    <br>
                    <div class="text-center">
                        <a href="{{ url("admin/accounts/users/picture/create?id=$user->id") }}" class="btn btn-default btn-block btn-lg aBtn"
                           data-toggle="popover"
                           data-trigger="hover"
                           data-placement="top"
                           title="Update account picture"
                           data-content="Min size: 500X500 pixel, 72 ppi">
                            <i class="fa fa-fw fa-camera" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div class="media-body">
                    <ul class="list-inline">
                        <li data-toggle="tooltip" data-placement="bottom" title="Password Reset">
                            @include('admin.accounts.users.btnPasswordReset', ['id' => $user->id])</li>
                        <li>
                            <a href="{{ url("/admin/accounts/users/$user->id/edit") }}" class="btn btn-primary aBtn" title="edit user account">
                                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit
                            </a>
                        </li>
                        <li class="pull-right">
                            @include("admin.parts.deleteBtn", ['action' => ['Admin\Accounts\UserController@destroy', $user->id], 'class' => 'btn-lg', 'btnTitle' => $user->username."'s account"])
                        </li>
                    </ul>
                    <p class="lead">
                        Username: {{ $user->username }}<br>
                        Email: {{ $user->email }}<br>
                        Member Since: {{ $user->since_date }}<br>
                        Registration date: {{ $user->registration_date }}
                    </p>
                    <div class="row">
                        <div class="col-lg-6">
                            <h3>
                                Account Members <span class="badge" data-toggle="tooltip" data-placement="top" title="members related to account">{{ count($user->members) }}</span>
                                <a href="{{ url("/admin/accounts/members/create?user=$user->id") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="add member to account">
                                    add <i class="fa fa-fw fa-user-circle" aria-hidden="true"></i>
                                </a>
                            </h3>
                            <table class="table">
                                <tbody>
                                    @foreach($user->members as $member)
                                        <tr>
                                            <td>
                                                {{ $i++ }}
                                            </td>
                                            <td>
                                                @if($member->deleted_at)
                                                    <del data-toggle="tooltip" data-placement="top" title="marked for deletion">
                                                        <a href="{{ url("/admin/accounts/members/$member->id") }}">
                                                            <i class="fa fa-fw fa-user-circle" aria-hidden="true"></i>
                                                            {{ $member->first_name }}
                                                            @if($member->middle_name){{ $member->middle_name }}@endif
                                                            {{ $member->last_name }}
                                                        </a>
                                                    </del>
                                                    @else
                                                    <a href="{{ url("/admin/accounts/members/$member->id") }}">
                                                        <i class="fa fa-fw fa-user-circle" aria-hidden="true"></i>
                                                        {{ $member->first_name }}
                                                        @if($member->middle_name){{ $member->middle_name }}@endif
                                                        {{ $member->last_name }}
                                                    </a>
                                                @endif
                                            </td>
                                            <td>
                                                @if($member->deleted_at)
                                                    @include("admin.parts.deleteBtn", ['action' => ['Admin\Accounts\MemberController@destroy', $member->id], 'class' => 'btn-xs', 'btnTitle' => 'permanently from the account'])
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <h3>
                                Account Addresses <span class="badge" data-toggle="tooltip" data-placement="top" title="members related to account">{{ count($user->addresses) }}</span>
                                <a href="{{ url("/admin/accounts/users/address/create?id=$user->id") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="add address to account">
                                    add <i class="fa fa-fw fa-map-marker" aria-hidden="true"></i>
                                </a>
                            </h3>
                            <table class="table">
                                <tbody>
                                @foreach($user->addresses as $adr)
                                    <tr>
                                        <td><span class="badge" data-toggle="tooltip" data-placement="top" title="address type">{{ $adr->type->name }}</span></td>
                                        <td>
                                            {{ $adr->address1 }}<br>
                                            @if($adr->address2){{ $adr->address2 }}<br>@endif
                                            {{ $adr->city }}@if($adr->state_id) {{ $adr->state->name }}@endif {{ $adr->zip_code }}<br>
                                            @if($adr->country->id != 244){{ $adr->country->name }}@endif
                                        </td>
                                        <td><span class="badge" data-toggle="tooltip" data-placement="top" title="visibility">{{ $adr->visibility->mode }}</span></td>
                                        <td>
                                            <a href="{{ url("/admin/accounts/users/address/$adr->id/edit") }}" class="btn btn-default btn-xs aBtn" data-toggle="tooltip" data-placement="left" title="edit {{ $adr->type->name }} address">
                                                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>
                                            @include("admin.parts.deleteBtn", ['action' => ['Admin\Accounts\AddressController@destroy', $adr->id], 'class' => 'btn-xs', 'btnTitle' => $adr->type->name.' address'])
                                        </td>
                                    </tr>
                                    @if(isset($adr->homePhone))
                                    <tr>
                                        <td>
                                            <span class="badge" data-toggle="tooltip" data-placement="top" title="phone number type">{{ $adr->homePhone->numberType->name }} phone</span>
                                        </td>
                                        <td>({{ $adr->homePhone->area_code }}) {{ $adr->homePhone->number }}</td>
                                        <td>
                                            <span class="badge" data-toggle="tooltip" data-placement="top" title="visibility">{{ $adr->homePhone->visibility->mode }}</span>
                                        </td>
                                        <td>
                                            <a href="{{ url("/admin/accounts/users/phone/".$adr->homePhone->id."/edit") }}" class="btn btn-default btn-xs pEdit" data-toggle="tooltip" data-placement="left" title="edit home phone number">
                                                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>
                                            @include("admin.parts.deleteBtn", ['action' => ['Admin\Accounts\HomePhoneController@destroy', $adr->homePhone->id], 'class' => 'btn-xs', 'btnTitle' => $adr->homePhone->numberType->name.' phone number'])
                                        </td>
                                    </tr>
                                    @else
                                        <tr>
                                            <td></td>
                                            <td><small>no phone number</small></td>
                                            <td></td>
                                            <td>
                                                <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/accounts/users/phone/create?id=$adr->id") }}" data-toggle="tooltip" data-placement="top" title="add home phone number">
                                                    add <i class="fa fa-fw fa-phone" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header with-border">
            Story
            @if($user->story)
                <a href="{{ url("/admin/accounts/users/story/".$user->story->id."/edit") }}" class="btn btn-primary btn-xs aBtn" data-container="body" data-toggle="tooltip" data-placement="top" title="edit story">
                    <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                </a>
            @else
                <a href="{{ url("/admin/accounts/users/story/create?id=$user->id") }}" class="btn btn-primary btn-xs aBtn" data-container="body" data-toggle="tooltip" data-placement="top" title="add story">
                    <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i>
                </a>
            @endif
        </div>
        <!-- /.box-header -->
        @if($user->story)
            <div class="box-body">
                {!! $user->story->description !!}
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                @include("admin.parts.deleteBtn", ['action' => ['Admin\Accounts\UserStoryController@destroy', $user->story->id], 'class' => 'btn-xs', 'position' => 'right', 'btnTitle' => 'Delete the story'])
            </div>
        @endif
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle="popover"]').popover();

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                // other options
            });

            $('#resetForm').on('submit', function(){
                $(this).children("button").addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection