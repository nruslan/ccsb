@extends('layouts.admin')

@section('title', 'Users')

@section('hTitle', 'Users')

@section('breadcrumbs')
    <li><a href="{{ url("/admin/accounts/users") }}">Accounts</a></li>
    <li class="active">User Accounts</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Total users <span class="badge">{{ $users->total() }}</span></h3>
        <a href="{{ url('/admin/accounts/users/create') }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add New User">
            <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i> add
        </a>
        <div class="box-tools">
            <!-- search form -->
            {!! Form::open(['url' => '/api/user/search', 'id' => 'search', 'onsubmit' => 'return false;']) !!}
            <div class="input-group input-group-sm" style="width: 150px;">
                {!! Form::text('search', null, ['class' => 'form-control pull-right', 'id' => 'searchQuery', 'placeholder' => 'Search...']) !!}
                <span class="input-group-btn"><button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button></span>
            </div>
            {!! Form::close() !!}
            <!-- /.search form -->
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th class="text-center">Verified</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Date</th>
                <th class="text-center">Picture</th>
                <th class="text-center">Members</th>
                <th colspan="2"></th>
            </tr>
            </thead>
            <tbody id="result">
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                    <td class="text-center">
                        @if($user->verified == 'yes')
                            <a class="btn btn-success btn-xs verify" href="{{ url("/admin/accounts/users/$user->id/verify") }}" role="button">
                                <i class="fa fa-fw fa-smile-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Verified"></i>
                            </a>
                        @else
                            <a class="btn btn-default btn-xs verify" href="{{ url("/admin/accounts/users/$user->id/verify") }}" role="button">
                                <i class="fa fa-fw fa-frown-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Unverified"></i>
                            </a>
                        @endif
                    </td>
                    <td>
                        <a href="{{ url("/admin/accounts/users/$user->id") }}" class="aBtnLink" data-toggle="tooltip" data-placement="left" title="view details">
                            <i class="fa fa-fw fa-user-o" aria-hidden="true"></i> {{ $user->username }}
                        </a>
                    </td>
                    <td>
                        @if($user->activity_status)
                            <i class="fa fa-circle text-success" data-toggle="tooltip" data-placement="top" title="Online"></i>
                        @else
                            <i class="fa fa-circle text-default" data-toggle="tooltip" data-placement="top" title="Offline"></i>
                        @endif
                        {{ $user->email }}
                    </td>
                    <td>{{ date('F d, Y', strtotime($user->created_at)) }}</td>
                    <td class="text-center">{!! $user->picture_checker !!}</td>
                    <td class="text-center">{{ count($user->members) }}</td>
                    <td>
                        <ul class="list-inline">
                            <li>
                                <a href="{{ url("/admin/accounts/users/$user->id") }}" class="btn btn-default btn-xs aBtn" data-toggle="tooltip" data-placement="left" title="view details">
                                    <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </td>
                    <td class="text-right">@include('admin.parts.deleteBtn', ['action' => ['Admin\Accounts\UserController@destroy', $user->id], 'class' => 'btn-xs', 'btnTitle' => "Delete $user->username's account"])</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix" id="pagination">
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing {{ $users->firstItem() }} to {{ $users->lastItem() }} of {{ $users->total() }} accounts</div>
            </div>
            <div class="col-sm-7">
                {{ $users->links('vendor.pagination.admin-lte') }}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        // Search function
        (function () {
            let submitBtn = document.getElementById('search-btn');
            submitBtn.onclick = function () {
                submitBtn.setAttribute("disabled", "disabled");
                let query = document.getElementById('searchQuery').value;
                axios.post('/api/user/search', {
                    query : query
                })
                .then(function (response) {
                    document.getElementById('result').innerHTML = response.data;
                    submitBtn.removeAttribute("disabled");
                    document.getElementById('pagination').remove();
                })
                .catch(function (err) {
                    document.getElementById('result').innerHTML = err.message;
                });
            }
        })();

        let vBtns = document.getElementsByClassName("verify");
        for (i = 0; i < vBtns.length; i++) {
            vBtns[i].addEventListener("click", function(e){
                e.preventDefault();
                let btn = this;
                btn.setAttribute("disabled", "disabled");
                btn.innerHTML = '<i class="fa fa-spinner fa-pulse fa-fw"></i>'
                axios.post(this)
                    .then(function (response) {
                        btn.innerHTML = response.data['face'];
                        btn.classList.remove(response.data['class_remove']);
                        btn.classList.add(response.data['class_add']);
                        btn.removeAttribute("disabled");
                    })
                    .catch(function (err) {
                        document.getElementById('result').innerHTML = err.message;
                    });
            });
        }

        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection