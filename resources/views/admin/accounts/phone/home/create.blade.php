@extends('layouts.admin')

@section('title')
    Add home phone number
@endsection

@section('hTitle')
    Add Home Phone number for {{ $address->address1 }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('admin/accounts/users') }}"><i class="fa fa-users"></i> Users Accounts</a></li>
    <li><a href="{{ url("admin/accounts/users/$user->id") }}"><i class="fa fa-user"></i> {{ $user->username }}</a></li>
    <li class="active">Add Home Phone number</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/accounts/users/phone', 'id' => 'myForm']) !!}
                    {!! Form::hidden('address_id', $address->id) !!}
                    @include('admin.accounts.phone.home.formBody')
                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/accounts/users/$address->user_id")])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection