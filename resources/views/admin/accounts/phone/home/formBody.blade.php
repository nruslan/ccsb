{!! Form::hidden('type_id', 2) !!}
<div class="row">
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('area_code') ? ' has-error' : '' }}">
            {!! Form::label('area_code', 'Area code') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::text('area_code', 520, ['class' => 'form-control input-lg', 'placeholder' => '000', 'autocomplete' => 'off', 'autofocus']) !!}
            @if ($errors->has('area_code'))
                <span class="help-block">
                    <strong>{{ $errors->first('area_code') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
            {!! Form::label('number', 'Phone Number') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::text('number', null, ['class' => 'form-control input-lg', 'placeholder' => '0000000', 'autocomplete' => 'off']) !!}
            @if ($errors->has('number'))
                <span class="help-block">
                    <strong>{{ $errors->first('number') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('visibility_id') ? ' has-error' : '' }}">
            {!! Form::label('visibility_id', 'Visibility mode') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('visibility_id', $visibility, null, ['class' => 'form-control input-lg']) !!}
            @if ($errors->has('visibility_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('visibility_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>