@extends('layouts.admin')

@section('title')
    Edit phone number
@endsection

@section('hTitle')
    Edit {{ $phone->member->first_name }}'s phone number
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('admin/accounts/users') }}"><i class="fa fa-users"></i> Users Accounts</a></li>
    <li><a href="{{ url("admin/accounts/users/$member->user_id") }}"><i class="fa fa-user"></i> {{ $member->userAccount->username }}</a></li>
    <li><a href="{{ url("admin/accounts/members/$member->id") }}"><i class="fa fa-user-circle-o"></i> {{ $member->full_name }}</a></li>
    <li class="active">Edit Phone Number</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::model($phone, ['method' => 'PATCH', 'action' => ['Admin\Accounts\PhoneController@update', $phone->id], 'id' => 'myForm']) !!}
                    @include('admin.accounts.phone.formBody')
                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/accounts/members/$phone->member_id")])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection