@extends('layouts.admin')

@section('title')
    Add phone number
@endsection

@section('hTitle')
    Add phone number for {{ $member->first_name }} {{ $member->last_name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('admin/accounts/users') }}"><i class="fa fa-users"></i> Users Accounts</a></li>
    <li><a href="{{ url("admin/accounts/users/$member->user_id") }}"><i class="fa fa-user"></i> {{ $member->userAccount->username }}</a></li>
    <li><a href="{{ url("admin/accounts/members/$member->id") }}"><i class="fa fa-user-circle-o"></i> {{ $member->full_name }}</a></li>
    <li class="active">Add Phone Number</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/accounts/members/phone', 'id' => 'myForm']) !!}
                    {!! Form::hidden('member_id', $member->id) !!}
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('type_id') ? ' has-error' : '' }}">
                                {!! Form::label('type_id', 'Phone number type') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                                {!! Form::select('type_id', $phoneTypes, null, ['class' => 'form-control input-lg', 'autofocus']) !!}
                                @if ($errors->has('type_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('area_code') ? ' has-error' : '' }}">
                                {!! Form::label('area_code', 'Area code') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                                {!! Form::text('area_code', null, ['class' => 'form-control input-lg', 'placeholder' => '000']) !!}
                                @if ($errors->has('area_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('area_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                                {!! Form::label('number', 'Phone Number') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                                {!! Form::text('number', null, ['class' => 'form-control input-lg', 'placeholder' => '0000000']) !!}
                                @if ($errors->has('number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('ext') ? ' has-error' : '' }}">
                                {!! Form::label('ext', 'Extension') !!}
                                {!! Form::text('ext', null, ['class' => 'form-control input-lg', 'placeholder' => 'extension']) !!}
                                @if ($errors->has('ext'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ext') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('visibility_id') ? ' has-error' : '' }}">
                                {!! Form::label('visibility_id', 'Visibility mode') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                                {!! Form::select('visibility_id', $visibility, null, ['class' => 'form-control input-lg']) !!}
                                @if ($errors->has('visibility_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('visibility_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => "/admin/accounts/members/$member->id"])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection