@extends('layouts.admin')

@section('title')
    Member: {{ $member->first_name }}
@endsection

@section('hTitle')
    @include("admin.accounts.members.backBtn")
    Member <small>{{ $member->first_name }} {{ $member->last_name }}</small>
    <a href="{{ url("/admin/accounts/users/$member->user_id") }}" class="btn btn-primary aBtn" data-container="body" data-toggle="tooltip" data-placement="top" title="go to main account">
        <i class="fa fa-fw fa-user-circle-o" aria-hidden="true"></i> {{ $member->userAccount->username }}
    </a>
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('admin/accounts/users') }}"><i class="fa fa-users"></i> Users Accounts</a></li>
    <li><a href="{{ url("admin/accounts/users/$member->user_id") }}"><i class="fa fa-user"></i> {{ $member->userAccount->username }}</a></li>
    <li class="active">Member: {{ $member->full_name }}</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('admin.parts.message')
            <div class="media">
                <div class="media-left media-top">
                    @if($member->profilePicture)
                        <img src="{{ asset("storage/accounts/members/". $member->profilePicture->filename) }}" class="media-object img-responsive img-thumbnail img-rounded" alt="Responsive image">
                    @else
                        <img src="{{ asset("storage/accounts/blank.png") }}" class="media-object img-responsive img-thumbnail img-rounded" alt="Responsive image">
                    @endif
                    <br>
                    <div class="text-center">
                        <a href="{{ url("/admin/accounts/members/picture/create?id=$member->id") }}" class="btn btn-default btn-block btn-lg aBtn"
                           data-toggle="popover"
                           data-trigger="hover"
                           data-placement="top"
                           title="Update account picture"
                           data-content="Min size: 500X500 pixel, 72 ppi">
                            <i class="fa fa-fw fa-camera" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div class="media-body">
                    @if(!$member->deleted_at)
                    <ul class="list-inline">
                        <li class="pull-right">
                            @include("admin.parts.deleteBtn", ['action' => ['Admin\Accounts\MemberController@destroy', $member->id], 'class' => 'btn-lg', 'btnTitle' => $member->full_name])
                        </li>
                    </ul>
                    @else
                        <div class="alert alert-danger text-center lead" role="alert">
                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            This member has been marked for deletion.
                            @include('admin.accounts.members.undoBtn')
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-4">
                            <h4>
                                Members Name
                                <a href="{{ url("/admin/accounts/members/$member->id/edit") }}" class="btn btn-primary btn-xs aBtn" data-container="body" data-toggle="tooltip" data-placement="top" title="edit members name">
                                    <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                                </a>
                            </h4>
                            <p class="lead">
                                <span data-toggle="tooltip" data-placement="right" title="first name">{{ $member->first_name }}</span><br>
                                @if($member->middle_name)<span data-toggle="tooltip" data-placement="right" title="middle name">{{ $member->middle_name }}</span><br>@endif
                                <span data-toggle="tooltip" data-placement="right" title="last name">{{ $member->last_name }}</span>
                            </p>
                            <p>Birth Date: {!! $member->birth_date !!}</p>
                            @foreach($member->roles as $role)
                                <i class="fa fa-shield" aria-hidden="true"></i> {{ $role->name }}
                            @endforeach
                        </div>
                        <div class="col-lg-6">
                            <h4>
                                Title
                                <a href="{{ url("/admin/accounts/members/title/create?id=$member->id") }}" class="btn btn-primary btn-xs aBtn" data-container="body" data-toggle="tooltip" data-placement="top" title="assign a title to the member">
                                    <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i>
                                </a>
                            </h4>
                            @if(count($member->titles) > 0)
                            <table class="table">
                                <tbody>
                                @foreach($member->titles as $t)
                                    <tr>
                                        <td><strong>{{ $t->full_title }}</strong></td>
                                        <td>{{ date("F j, Y", strtotime($t->pivot->term_started)) }} @if($t->pivot->term_ended)- {{ date("F j, Y", strtotime($t->pivot->term_ended)) }}@endif</td>
                                        <td>
                                            <a href="{{ url("/admin/accounts/members/title/".$t->pivot->id."/edit") }}" class="btn btn-default btn-xs aBtn" data-toggle="tooltip" data-placement="left" title="edit title">
                                                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>
                                            @include("admin.parts.deleteBtn", ['action' => ['Admin\Accounts\MembersTitleController@destroy', $t->pivot->id], 'class' => 'btn-xs', 'btnTitle' => $t->full_title.' title'])
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                                <small class="text-muted">
                                    - none of the titles have been assigned
                                </small>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-4">
                            <h4>Address</h4>
                            @foreach($member->userAccount->addresses as $address)
                                <address>
                                    {{ $address->address1 }}<br>
                                    {{ $address->city }}, {{ $address->state->name }} {{ $address->zip_code }}<br>
                                    @if($address->country->id != 244){{ $address->country->name }}<br>@endif
                                    @if(isset($address->homePhone))
                                        <i class="fa fa-fw fa-phone-square" aria-hidden="true"></i> ({{ $address->homePhone->area_code }}) {{ $address->homePhone->number }}
                                    @endif
                                </address>
                            @endforeach
                        </div>
                        <div class="col-lg-4">
                            <h4>
                                @if(count($member->phones->where('deleted_at', null)) > 1)
                                    Phone numbers
                                @else
                                    Phone number
                                @endif
                                <a href="{{ url("/admin/accounts/members/phone/create?id=$member->id") }}" class="btn btn-primary btn-xs aBtn" data-container="body" data-toggle="tooltip" data-placement="top" title="add phone number">
                                    <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i>
                                </a>
                            </h4>
                            @if(count($member->phones->where('deleted_at', null)) > 0)
                                <table class="table">
                                    <tbody>
                                    @foreach($member->phones->where('deleted_at', null) as $phone)
                                        <tr>
                                            <td><span data-toggle="tooltip" data-placement="top" title="{{ $phone->numberType->name }} phone number">{!! $phone->numberType->icon !!}</span></td>
                                            <td>({{ $phone->area_code }}) {{ $phone->number }}</td>
                                            <td>{{ $phone->visibility->mode }}</td>
                                            <td>
                                                <a href="{{ url("/admin/accounts/members/phone/$phone->id/edit") }}" class="btn btn-default btn-xs aBtn" data-toggle="tooltip" data-placement="left" title="edit {{ $phone->numberType->name }} number">
                                                    <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                            <td>
                                                @include("admin.parts.deleteBtn", ['action' => ['Admin\Accounts\PhoneController@destroy', $phone->id], 'class' => 'btn-xs', 'btnTitle' => 'the phone number'])
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @else
                                <small class="text-muted">
                                    - no additional phone numbers has been added
                                </small>
                            @endif
                        </div>
                        <div class="col-lg-4">
                            <h4>
                                Email
                                <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/accounts/members/email/create?id=$member->id") }}" data-container="body" data-toggle="tooltip" data-placement="top" title="add email">
                                    <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i>
                                </a>
                            </h4>
                            @if(count($member->emails) > 0)
                                <table class="table">
                                    <tbody>
                                    @foreach($member->emails as $email)
                                        <tr>
                                            <td>
                                                @if(!$email->verified)
                                                    <i class="fa fa-fw fa-exclamation-circle text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="unverified email"></i>
                                                    @else
                                                    <i class="fa fa-fw fa-check-circle text-success" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="verified email"></i>
                                                @endif
                                            </td>
                                            <td><a href="mailto:{{ $email->address }}">{{ $email->address }}</a></td>
                                            <td>{{ $email->visibility->mode }}</td>
                                            <td>
                                                <a href="{{ url("/admin/accounts/members/email/$email->id/edit") }}" class="btn btn-default btn-xs aBtn" data-toggle="tooltip" data-placement="left" title="edit email">
                                                    <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                            <td>
                                                @include("admin.parts.deleteBtn", ['action' => ['Admin\Accounts\EmailController@destroy', $email->id], 'class' => 'btn-xs', 'btnTitle' => 'the email'])
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @else
                                <small class="text-muted">- no additional emails have been added</small>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header with-border">
            Story
            @if($member->story)
                <a href="{{ url("/admin/accounts/members/story/".$member->story->id."/edit") }}" class="btn btn-primary btn-xs aBtn" data-container="body" data-toggle="tooltip" data-placement="top" title="edit story">
                    <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                </a>
            @else
                <a href="{{ url("/admin/accounts/members/story/create?id=$member->id") }}" class="btn btn-primary btn-xs aBtn" data-container="body" data-toggle="tooltip" data-placement="top" title="add story">
                    <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i>
                </a>
            @endif
        </div>
        <!-- /.box-header -->
        @if($member->story)
        <div class="box-body">
            {!! $member->story->description !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include("admin.parts.deleteBtn", ['action' => ['Admin\Accounts\MemberStoryController@destroy', $member->story->id], 'class' => 'btn-xs', 'position' => 'right', 'btnTitle' => 'Delete the story'])
        </div>
        @endif
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle="popover"]').popover();
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                // other options
            });
        });
    </script>
@endsection