@extends('layouts.admin')

@section('title')
    Edit member
@endsection

@section('hTitle')
    Edit members information
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::model($member, ['method' => 'PATCH', 'action' => ['Admin\Accounts\MemberController@update', $member->id], 'id' => 'myForm']) !!}
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                {!! Form::label('first_name', 'First name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="your first name"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                                    {!! Form::text('first_name', null, ['class' => 'form-control input-lg', 'placeholder' => 'first name', 'autocomplete' => 'off', 'autofocus']) !!}
                                </div>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                {!! Form::label('last_name', 'Last name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="your last name"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                                    {!! Form::text('last_name', null, ['class' => 'form-control input-lg', 'placeholder' => 'last name', 'autocomplete' => 'off']) !!}
                                </div>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                {!! Form::label('middle_name', 'Middle name') !!}
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="your middle name"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                                    {!! Form::text('middle_name', null, ['class' => 'form-control input-lg', 'placeholder' => 'middle name', 'autocomplete' => 'off']) !!}
                                </div>
                                @if ($errors->has('middle_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                                {!! Form::label('dob', 'Birth Date') !!}
                                <div class="input-group date" data-provide="datepicker">
                                    <span class="input-group-addon"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                                    {!! Form::text('dob', null, ['class' => 'form-control', 'placeholder' => 'date', 'autocomplete' => 'off']) !!}
                                </div>
                                @if ($errors->has('dob'))
                                    <span class="help-block"><strong>{{ $errors->first('dob') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => "/admin/accounts/members/$member->id"])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $("#submitBtn").addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection