<div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
    {!! Form::label('role_id', 'Role') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
    {!! Form::select('role_id', $roles, null, ['class' => 'form-control input-lg', 'autofocus']) !!}
    @if ($errors->has('role_id'))
        <span class="help-block">
            <strong>{{ $errors->first('role_id') }}</strong>
        </span>
    @endif
</div>