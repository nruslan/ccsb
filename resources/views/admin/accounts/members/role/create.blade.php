@extends('layouts.admin')

@section('title')
    Assign role to the member
@endsection

@section('hTitle')
    Assign role to {{ $member->first_name }} {{ $member->last_name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/accounts/members/role']) !!}
                    {!! Form::hidden('member_id', $member->id) !!}
                    @include('admin.accounts.members.role.formBody')
                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/accounts/members/$member->id")])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{ Html::script('/media/vendor/jquery-datepicker/jquery-ui.js') }}
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true
            });

            $('[data-toggle="tooltip"]').tooltip();

            $('#cancelBtn').on('click', function(){
                $(this).addClass('disabled').append(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });

            $('#submitBtn').on('click', function(){
                $(this).addClass('disabled').append(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection