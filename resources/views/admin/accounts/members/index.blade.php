@extends('layouts.admin')

@section('title')
    Members
@endsection

@section('hTitle')
    Community Church Members
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/accounts/users") }}">Accounts</a></li>
    <li><a href="{{ url('admin/accounts/users') }}"><i class="fa fa-users"></i> Users Accounts</a></li>
    <li class="active">Members</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Total members <span class="badge">{{ $members->total() }}</span></h3>
            <div class="box-tools">
                <!-- search form -->
                {!! Form::open(['url' => '/api/user/search', 'id' => 'search', 'onsubmit' => 'return false;']) !!}
                <div class="input-group input-group-sm" style="width: 150px;">
                    {!! Form::text('search', null, ['class' => 'form-control pull-right', 'id' => 'searchQuery', 'placeholder' => 'Search...']) !!}
                    <span class="input-group-btn"><button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button></span>
                </div>
            {!! Form::close() !!}
            <!-- /.search form -->
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Last Name</th>
                    <th>Name</th>
                    <th>Date of birth</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="result">
                @foreach($members as $member)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $member->userAccount->username }}</td>
                        <td>
                            <a href="{{ url("/admin/accounts/members/$member->id") }}" class="btn btn-link btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="view details">
                                <i class="fa fa-fw fa-user" aria-hidden="true"></i> {{ $member->full_name }}</a>
                        </td>
                        <td>{!! $member->birth_date !!}</td>
                        <td>
                            <a href="{{ url("/admin/accounts/members/$member->id") }}" class="btn btn-default btn-xs aBtn" data-toggle="tooltip" data-placement="left" title="view details">
                                <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix" id="pagination">
            <div class="row">
                <div class="col-sm-5">
                    <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing {{ $members->firstItem() }} to {{ $members->lastItem() }} of {{ $members->total() }} accounts</div>
                </div>
                <div class="col-sm-7">
                    {{ $members->links('vendor.pagination.admin-lte') }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        // Search function
        (function () {
            let submitBtn = document.getElementById('search-btn');
            submitBtn.onclick = function () {
                submitBtn.setAttribute("disabled", "disabled");
                let query = document.getElementById('searchQuery').value;
                axios.post('/api/member/search', {
                    query : query
                })
                    .then(function (response) {
                        document.getElementById('result').innerHTML = response.data;
                        submitBtn.removeAttribute("disabled");
                        document.getElementById('pagination').remove();
                    })
                    .catch(function (err) {
                        document.getElementById('result').innerHTML = err.message;
                    });
            }
        })();
    </script>
@endsection