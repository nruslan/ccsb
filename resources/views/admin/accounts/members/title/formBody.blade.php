<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="form-group{{ $errors->has('title_id') ? ' has-error' : '' }}">
            {!! Form::label('title_id', 'Title') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            {!! Form::select('title_id', $titles, null, ['class' => 'form-control input-lg', 'autofocus']) !!}
            @if ($errors->has('title_id'))
                <span class="help-block"><strong>{{ $errors->first('title_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('term_started') ? ' has-error' : '' }}">
            {!! Form::label('term_started', 'Term started') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group input-group-lg date" id="datepicker">
                <span class="input-group-addon"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('term_started', null, ['class' => 'form-control input-lg', 'placeholder' => 'term started', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('term_started'))
                <span class="help-block"><strong>{{ $errors->first('term_started') }}</strong></span>
            @endif
        </div>
    </div>
</div>