@extends('layouts.admin')

@section('styles')
    {{ Html::style('/media/vendor/jquery-datepicker/jquery-ui.css') }}
@endsection

@section('title')
    Edit member's title
@endsection

@section('hTitle')
    Edit {{ $memberTitle->member->first_name }} {{ $memberTitle->member->last_name }}'s title
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::model($memberTitle, ['method' => 'PATCH', 'action' => ['Admin\Accounts\MembersTitleController@update', $memberTitle->id]]) !!}
                    @include('admin.accounts.members.title.formBody')
                    <div class="form-group{{ $errors->has('term_ended') ? ' has-error' : '' }}">
                        {!! Form::label('term_ended', 'Term ended') !!}
                        <div class="input-group input-group-lg date" id="datepicker2">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the term is started"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('term_ended', null, ['class' => 'form-control input-lg', 'placeholder' => 'term ended date', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('term_ended'))
                            <span class="help-block">
                                <strong>{{ $errors->first('term_ended') }}</strong>
                            </span>
                        @endif
                    </div>
                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/accounts/members/$memberTitle->member_id")])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({format: 'MM/DD/YYYY'});
            $("#datepicker2").datetimepicker({format: 'MM/DD/YYYY'});

            $('#submitBtn').on('click', function(){
                $(this).addClass('disabled').append(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection