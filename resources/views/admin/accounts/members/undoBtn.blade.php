{!! Form::open(['action' => ['Admin\Accounts\MemberController@undo', $member->id]]) !!}
<button type="submit" class="btn btn-link">
    <i class="fa fa-fw fa-undo" aria-hidden="true"></i> Undo
</button>
{!! Form::close() !!}