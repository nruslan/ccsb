<tr>
    <td colspan="5">
        @include('admin.parts.btnBack', ['url' => url("/admin/accounts/members"), 'btnClass' => 'btn-xs', 'title' => "members list"]) <strong>Search Results</strong> <span class="badge">{{ $members->count() }}</span>
    </td>
</tr>
@foreach($members as $member)
    <tr>
        <td>{{ $i++ }}</td>
        <td>{{ $member->userAccount->username }}</td>
        <td>
            <a href="{{ url("/admin/accounts/members/$member->id") }}" class="btn btn-link btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="view details">
                <i class="fa fa-fw fa-user" aria-hidden="true"></i> {{ $member->full_name }}</a>
        </td>
        <td>{!! $member->birth_date !!}</td>
        <td>
            <a href="{{ url("/admin/accounts/members/$member->id") }}" class="btn btn-default btn-xs aBtn" data-toggle="tooltip" data-placement="left" title="view details">
                <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
            </a>
        </td>
    </tr>
@endforeach