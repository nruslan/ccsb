<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
            {!! Form::label('address1', 'Address line 1') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group input-group-lg">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('address1', null, ['class' => 'form-control input-lg', 'placeholder' => 'address', 'autocomplete' => 'off', 'autofocus']) !!}
            </div>
            @if ($errors->has('address1'))
                <span class="help-block">
                    <strong>{{ $errors->first('address1') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}">
            {!! Form::label('address2', 'Address line 2') !!}
            <div class="input-group input-group-lg">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('address2', null, ['class' => 'form-control input-lg', 'placeholder' => 'address', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('address2'))
                <span class="help-block">
                    <strong>{{ $errors->first('address2') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
            {!! Form::label('city', 'City/Town') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            {!! Form::text('city', 'Tucson', ['class' => 'form-control input-lg', 'placeholder' => 'city']) !!}
            @if ($errors->has('city'))
                <span class="help-block">
                    <strong>{{ $errors->first('city') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('address_type_id') ? ' has-error' : '' }}">
            {!! Form::label('address_type_id', 'Address type') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('address_type_id', $addressTypes, null, ['class' => 'form-control input-lg']) !!}
            @if ($errors->has('address_type_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('address_type_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">
            {!! Form::label('state_id', 'State') !!}
            {!! Form::select('state_id', $states, 3, ['class' => 'form-control input-lg', 'placeholder' => 'choose your state']) !!}
        </div>
        <div class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
            {!! Form::label('Country', 'Country') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group input-group-lg">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Country"><i class="fa fa-flag-o fa-fw" aria-hidden="true"></i></span>
                {!! Form::select('country_id', $countries, 244, ['class' => 'form-control input-lg', 'placeholder' => 'choose your country']) !!}
            </div>
            @if ($errors->has('country_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('country_id') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
            {!! Form::label('zip_code', 'Zip/Postal code') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group input-group-lg">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="zip code"><i class="fa fa-th-large fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('zip_code', 85739, ['class' => 'form-control input-lg', 'placeholder' => 'zip code']) !!}
            </div>
            @if ($errors->has('zip_code'))
                <span class="help-block">
                    <strong>{{ $errors->first('zip_code') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('visibility_id') ? ' has-error' : '' }}">
            {!! Form::label('visibility_id', 'Visibility mode') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('visibility_id', $visibility, null, ['class' => 'form-control input-lg']) !!}
            @if ($errors->has('visibility_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('visibility_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>
@include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/accounts/users/$user_id")])