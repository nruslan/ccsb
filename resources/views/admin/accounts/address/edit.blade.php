@extends('layouts.admin')

@section('title')
    Edit address
@endsection

@section('hTitle')
    Edit address for {{ $address->user->username }}'s account
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('admin/accounts/users') }}"><i class="fa fa-users"></i> Users Accounts</a></li>
    <li><a href="{{ url("admin/accounts/users/$user->id") }}"><i class="fa fa-user"></i> {{ $user->username }}</a></li>
    <li class="active">Edit Address</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::model($address, ['method' => 'PATCH', 'action' => ['Admin\Accounts\AddressController@update', $address->id], 'id' => 'myForm']) !!}
                    @include('admin.accounts.address.formBody', ['user_id' => $address->user_id])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $("#submitBtn").addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection