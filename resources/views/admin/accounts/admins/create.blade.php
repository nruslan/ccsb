@extends('layouts.admin')

@section('title')
    Add new Administrator
@endsection

@section('hTitle')
    Add new website Administrator
@endsection

@section('content')
    <div class="box box-primary">
        <!-- /.box-header -->
        {!! Form::open(['url' => '/admin/accounts/admins', 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        {!! Form::label('first_name', 'First Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify username"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('first_name', null, ['class' => 'form-control input-lg', 'placeholder' => 'first name', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('first_name'))<span class="help-block"><strong>{{ $errors->first('first_name') }}</strong></span>@endif
                    </div>
                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        {!! Form::label('last_name', 'Last Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify username"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('last_name', null, ['class' => 'form-control input-lg', 'placeholder' => 'last name', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('last_name'))<span class="help-block"><strong>{{ $errors->first('last_name') }}</strong></span>@endif
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('email', 'Email') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify the email"><i class="fa fa-envelope fa-fw" aria-hidden="true"></i></span>
                            {!! Form::email('email', null, ['class' => 'form-control input-lg', 'placeholder' => 'email address', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => URL::previous()])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#cancelBtn').on('click', function(){
                $(this).addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });

            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection