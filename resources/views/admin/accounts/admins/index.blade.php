@extends('layouts.admin')

@section('title', 'Admins')

@section('hTitle', 'Website Administrators')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <a href="{{ url('/admin/accounts/admins/create') }}" class="btn btn-primary btn-xs aBtn pull-right" data-toggle="tooltip" data-placement="top" title="add new user">
                <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i> add new
            </a>
            Total Admins <span class="badge">{{ $admins->total() }}</span>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            @include('admin.parts.message')
            <table class="table table-striped table-condensed">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Registration Date</th>
                    <th colspan="3"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($admins as $admin)
                    <tr>
                        <th scope="row">{{ $i++ }}</th>
                        <td>
                            <a href="{{ url("/admin/accounts/admins/$admin->id") }}" class="btn btn-link btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="view details">
                                <i class="fa fa-fw fa-user-o" aria-hidden="true"></i> {{ $admin->name }}
                            </a>
                        </td>
                        <td>{{ $admin->email }}</td>
                        <td>{{ date('F d, Y', strtotime($admin->created_at)) }}</td>
                        <td>
                            <a href="{{ url("/admin/accounts/admins/$admin->id") }}" class="btn btn-default btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="view details">
                                <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            <a href="{{ url("/admin/accounts/admins/$admin->id/edit") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="left" title="edit">
                                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-right">@include('admin.parts.deleteBtn', ['action' => ['Admin\Accounts\AdminsController@destroy', $admin->id], 'class' => 'btn-xs', 'btnTitle' => "Delete $admin->name's account"])</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <div class="row">
                <div class="col-md-6"><div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing {{ $admins->firstItem() }} to {{ $admins->lastItem() }} of {{ $admins->total() }} accounts</div></div>
                <div class="col-md-6">{{ $admins->links() }}</div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection