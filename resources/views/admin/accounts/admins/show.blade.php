@extends('layouts.admin')

@section('title')
    {{ $admin->name }}
@endsection

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/accounts/admins"), 'title' => 'accounts list'])
    Website Administrator Account
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <a href="{{ url("/admin/accounts/admins/$admin->id/edit") }}" class="btn btn-primary btn-xs aBtn pull-right" data-toggle="tooltip" data-placement="left" title="edit">
                Edit <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
            </a>
            {{ $admin->name }}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @include('admin.parts.message')
            <div class="box-body box-profile">
                <h3 class="profile-username text-center">{{ $admin->name }}</h3>
                <p class="text-muted text-center">Website Administrator</p>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <ul class="list-inline">
                <li><a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/accounts/admins/password/$admin->id/edit") }}"><i class="fa fa-key" aria-hidden="true"></i> Change Password</a></li>
                @if($currentAdmin != $admin->id)<li class="pull-right">@include('admin.parts.banBtn', ['action' => ['Admin\Accounts\AdminsController@destroy', $admin->id], 'name' => $admin->name])</li>@endif
            </ul>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                // other options
            });
            $('#banForm').on('submit', function(){
                $(this).children("button").addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection