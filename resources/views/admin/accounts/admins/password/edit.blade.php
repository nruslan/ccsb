@extends('layouts.admin')

@section('title')
    Change Password for {{ $admin->name }}
@endsection

@section('hTitle')
    Change Password for {{ $admin->name }}
@endsection

@section('content')
    <div class="box box-primary">
        <!-- /.box-header -->
        {!! Form::model($admin, ['method' => 'PATCH', 'action' => ['Admin\Accounts\AdminsPasswordController@update', $admin->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                        {!! Form::label('new_password', 'New Password') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify username"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                            {!! Form::password('new_password', ['class' => 'form-control input-lg', 'placeholder' => 'new password', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('new_password'))<span class="help-block"><strong>{{ $errors->first('new_password') }}</strong></span>@endif
                    </div>
                    <div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                        {!! Form::label('new_password_confirmation', 'Confirm New Password') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify username"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                            {!! Form::password('new_password_confirmation', ['class' => 'form-control input-lg', 'placeholder' => 'confirm new password', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('new_password_confirmation'))<span class="help-block"><strong>{{ $errors->first('new_password_confirmation') }}</strong></span>@endif
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Change Password', 'cancelUrl' => URL::previous()])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection