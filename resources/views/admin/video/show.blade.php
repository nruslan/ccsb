@extends('layouts.admin')

@section('title', $video->title)

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/videos?page=$pageId")])
    &nbsp;{{ $video->title }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/videos?page=$pageId") }}">Videos</a></li>
    <li class="active">{{ $video->title }}</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-fw fa-tags" aria-hidden="true"></i> {{ $video->related_categories }} <i class="fa fa-fw fa-calendar" aria-hidden="true"></i> Recorded {{ $video->date }}
            <a class="btn btn-primary btn-xs aBtn pull-right" href="{{ url("/admin/videos/$video->id/edit") }}"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @include('admin.parts.message')
            <div class="row">
                <div class="col-lg-6">
                    <h2 class="text-center">{{ $video->title }}<br><small>by {{ $video->starring_members }}</small></h2>
                    <h4>Description</h4>
                    <p class="lead">
                        {!! $video->description or "<small>n/a</small>" !!}
                    </p>
                </div>
                <div class="col-lg-6">
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $video->filename }}" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <ul class="list-inline">
                <li>
                    <a class="btn btn-primary{{ $prevDisabled or '' }} aBtn" href="{{ url("/admin/videos/$prevId") }}"><i class="fa fa-fw fa-angle-double-left" aria-hidden="true"></i> Previous</a></li>
                <li class="pull-right">
                    <a class="btn btn-primary{{ $nextDisabled or '' }} aBtn" href="{{ url("/admin/videos/$nextId") }}">Next <i class="fa fa-fw fa-angle-double-right" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();

            $('.addNew').on('click', function(){
                $(this).addClass('disabled').append(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection