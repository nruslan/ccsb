@extends('layouts.admin')

@section('title', 'Videos')

@section('hTitle')
    Church Videos
@endsection

@section('breadcrumbs')
    <li class="active">Videos</li>
@endsection

@section('search')
    @include('admin.parts.sidebarSearch', ['url' => 'api/video/search'])
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            Total Videos <span class="badge">{{ $videos->total() }}</span>
            <div class="pull-right">
                <a href="{{ url('/admin/videos/create') }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="add new video">
                    <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i> Add
                </a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            @include('admin.parts.message')
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Starring</th>
                    <th>Categories</th>
                    <th colspan="3"></th>
                </tr>
                </thead>
                <tbody id="result">
                @foreach($videos as $video)
                    <tr>
                        <td scope="col">{{ $i++ }}</td>
                        <td>{{ $video->date }}</td>
                        <td><a href="{{ url("/admin/videos/$video->id") }}">{{ $video->short_title }}</a></td>
                        <td>{{ $video->type->service }}</td>
                        <td>{{ $video->starring_members }}</td>
                        <td>{{ $video->related_categories }}</td>
                        <td class="text-center">
                            <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/videos/$video->id") }}" data-toggle="tooltip" data-placement="left" title="view details">
                                <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/videos/$video->id/edit") }}" data-toggle="tooltip" data-placement="left" title="edit">
                                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-right">
                            @include('admin.parts.deleteBtn', ['action' => ['Admin\Video\IndexController@destroy', $video->id], 'class' => 'btn-xs', 'btnTitle' => "Delete $video->title"])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $videos->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        // Search function
        (function () {
            let submitBtn = document.getElementById('search-btn');
            submitBtn.onclick = function () {
                submitBtn.setAttribute("disabled", "disabled");
                let query = document.getElementById('searchQuery').value;
                axios.post('/api/video/search', {
                    query : query
                })
                    .then(function (response) {
                        console.log(response);
                        document.getElementById('result').innerHTML = response.data;
                        submitBtn.removeAttribute("disabled");
                    })
                    .catch(function (err) {
                        document.getElementById('result').innerHTML = err.message;
                    });
            }
        })();

        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            //Delete button
            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection