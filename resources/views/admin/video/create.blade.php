@extends('layouts.admin')

@section('title', 'Add new video')

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/videos?page=$pageId"), 'title' => 'video list'])
    &nbsp;Add new video
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/videos') }}">Videos</a></li>
    <li class="active">Add New Video</li>
@endsection

@section('content')
    <div class="box box-primary">
        {!! Form::open(['url' => 'admin/videos', 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group{{ $errors->has('video_type_id') ? ' has-error' : '' }}">
                        {!! Form::label('video_type_id', 'Video type') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                        {!! Form::select('video_type_id', $videoTypes, null, ['class' => 'form-control input-lg']) !!}
                        @if ($errors->has('video_type_id'))<span class="help-block"><strong>{{ $errors->first('video_type_id') }}</strong></span>@endif
                    </div>
                    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                        {!! Form::label('date', 'Recorded date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg date" id="datepicker">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the video was recorded"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('date', null, ['class' => 'form-control input-lg', 'placeholder' => 'recorded date', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('date'))<span class="help-block"><strong>{{ $errors->first('date') }}</strong></span>@endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group{{ $errors->has('filename') ? ' has-error' : '' }}">
                        {!! Form::label('filename', 'Link to video') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-link fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('filename', null, ['class' => 'form-control input-lg', 'placeholder' => 'link to video', 'autocomplete' => 'off', 'autofocus']) !!}
                        </div>
                        @if ($errors->has('filename'))<span class="help-block"><strong>{{ $errors->first('filename') }}</strong></span>@endif
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        {!! Form::label('title', 'Title') !!}
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('title', null, ['class' => 'form-control input-lg', 'placeholder' => 'video title', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('title'))<span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>@endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <h4>Pastors</h4>
                    <div class="form-group{{ $errors->has('member_id') ? ' has-error' : '' }}">
                        @foreach($title->members as $member)
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('member_id[]', $member->id) !!} {{ $member->full_name }}
                                </label>
                            </div>
                        @endforeach
                    </div>

                    <h4>Video Categories</h4>
                    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                        @foreach($categories as $id => $title)
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('category_id[]', $id) !!} {{ $title }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @include('admin.parts.textarea', ['name' => 'description', 'title' => 'Video Description'])
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url('/admin/videos')])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({format: 'MM/DD/YYYY'});
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection