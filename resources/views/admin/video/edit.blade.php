@extends('layouts.admin')

@section('title')
    Edit video info
@endsection

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/videos?page=$pageId"), 'title' => $video->title])
    &nbsp;Edit video information
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/videos?page=$pageId") }}">Videos</a></li>
    <li><a href="{{ url("/admin/videos/$video->id") }}">{{ $video->title }}</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    <div class="box box-primary">
        {!! Form::model($video, ['method' => 'PATCH', 'action' => ['Admin\Video\IndexController@update', $video->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                        {!! Form::label('date', 'Recorded date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg date" id="datepicker">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the video was recorded"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('date', null, ['class' => 'form-control input-lg', 'placeholder' => 'recorded date', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('date'))<span class="help-block"><strong>{{ $errors->first('date') }}</strong></span>@endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        {!! Form::label('title', 'Title') !!}
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('title', null, ['class' => 'form-control input-lg', 'placeholder' => 'video title', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('title'))<span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>@endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <h4>Pastors</h4>
                    <div class="form-group{{ $errors->has('member_id') ? ' has-error' : '' }}">
                        @foreach($title->members as $member)
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('member_id[]', $member->id) !!} {{ $member->full_name }}
                                </label>
                            </div>
                        @endforeach
                    </div>

                    <h4>Video Categories</h4>
                    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                        @foreach($categories as $id => $title)
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('category_id[]', $id) !!} {{ $title }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @include('admin.parts.textarea', ['name' => 'description', 'title' => 'Video Description'])
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/videos/$video->id")])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({format: 'MM/DD/YYYY'});

            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection