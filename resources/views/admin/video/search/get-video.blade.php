<tr>
    <td colspan="9">@include('admin.parts.btnBack', ['url' => url("/admin/videos"), 'btnClass' => 'btn-xs', 'title' => "video list"]) Search Results <span class="badge">{{ $videos->count() }}</span></td>
</tr>
@foreach($videos as $video)
    <tr>
        <td scope="col">{{ $i++ }}</td>
        <td>{{ $video->date }}</td>
        <td><a href="{{ url("/admin/videos/$video->id") }}">{{ $video->short_title }}</a></td>
        <td>{{ $video->type->service }}</td>
        <td>{{ $video->starring_members }}</td>
        <td>{{ $video->related_categories }}</td>
        <td class="text-center">
            <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/videos/$video->id") }}" data-toggle="tooltip" data-placement="left" title="view details">
                <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
            </a>
        </td>
        <td class="text-center">
            <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/videos/$video->id/edit") }}" data-toggle="tooltip" data-placement="left" title="edit">
                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
            </a>
        </td>
        <td class="text-right">
            @include('admin.parts.deleteBtn', ['action' => ['Admin\Video\IndexController@destroy', $video->id], 'class' => 'btn-xs', 'btnTitle' => "Delete $video->title"])
        </td>
    </tr>
@endforeach