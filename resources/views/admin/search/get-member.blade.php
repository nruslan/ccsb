@if(count($members) > 0)
    @foreach($members as $member)
        <div class="radio">
            <label>
                <input type="radio" name="member_id" id="mId{{ $i++ }}" value="{{ $member->id }}" @if($i == 1) checked @endif>
                {{ $member->last_name }} {{ $member->first_name }}
            </label>
        </div>
    @endforeach
@else
    <p class="text-muted"><i>Sorry, but nothing was found. Please try with different name.</i></p>
@endif