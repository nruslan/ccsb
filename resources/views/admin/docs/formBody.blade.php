<div class="row">
    <div class="col-lg-4">
        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title', 'Title') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group input-group-lg">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Document title"><i class="fa fa-chevron-right fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('title', null, ['class' => 'form-control input-lg', 'placeholder' => 'document title', 'id' => 'title', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('title'))
                <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
                <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
            {!! Form::label('date', 'Date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group input-group-lg date" data-provide="datepicker">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the file was created"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('date', null, ['class' => 'form-control input-lg', 'placeholder' => 'date', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('date'))
                <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                <span class="sr-only">(error)</span>
                <span class="help-block"><strong>{{ $errors->first('date') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group{{ $errors->has('doc_type_id') ? ' has-error' : '' }}">
            {!! Form::label('doc_type_id', 'Document type') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('doc_type_id', $docTypes, null, ['class' => 'form-control input-lg']) !!}
            @if ($errors->has('doc_type_id'))<span class="help-block"><strong>{{ $errors->first('doc_type_id') }}</strong></span>@endif
        </div>
    </div>
</div>

@include('admin.parts.textarea', ['name' => 'description', 'title' => 'File Description'])