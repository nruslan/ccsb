@extends('layouts.admin')

@section('title', 'Edit Document')

@section('hTitle')
    Edit {{ $doc->title }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('admin/documents') }}">Documents</a></li>
    <li><a href="{{ url("admin/documents/$doc->id") }}">{{ $doc->title }}</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    <div class="box box-primary">
        <!-- /.box-header -->
        {!! Form::model($doc, ['method' => 'PATCH', 'action' => ['Admin\Docs\IndexController@update', $doc->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.docs.formBody')
        </div>
        <div class="box-footer clearfix">@include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/documents/$doc->id")])</div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection