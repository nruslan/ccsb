@extends('layouts.admin')

@section('title', $doc->title)

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/documents?page=$pageId"), 'title' => 'documents list'])
    {{ $doc->title }} <small>{{ $doc->date_month_year }}</small>
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('admin/documents') }}">Documents</a></li>
    <li class="active">{{ $doc->title }}</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $doc->title }} <small>{{ $doc->date }}</small></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! $doc->description !!}
            <iframe src="{{ asset("storage/documents/$doc->year/$doc->filename") }}" width="100%" height="750px" frameborder="0"></iframe>
        </div>
        <!-- /.box-body -->
    </div>
@endsection