@extends('layouts.admin')

@section('title', 'Documents')

@section('hTitle')
    Documents
@endsection

@section('breadcrumbs')
    <li class="active">Documents</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            Total Files <span class="badge">{{ $files->total() }}</span>
            <a href="{{ url('/admin/documents/create') }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="add new document">
                <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i> add</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            @include('admin.parts.message')
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Uploaded by</th>
                    <th colspan="4" class="text-center">Action buttons</th>
                </tr>
                </thead>
                <tbody id="result">
                @foreach($files as $file)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $file->date }}</td>
                        <td>
                            <a class="btn btn-link btn-xs aBtn" href="{{ url("/admin/documents/$file->id") }}" data-toggle="tooltip" data-placement="left" title="view details">
                                <i class="fa fa-fw fa-eye"></i> {{ $file->title }}
                            </a>
                        </td>
                        <td>{{ $file->type }}</td>
                        <td>{{ $file->uploaded_by }}</td>
                        <td class="text-center">
                            <a class="btn btn-default btn-xs" href="{{ asset("storage/documents/$file->year/$file->filename") }}" download="{{ $file->filename }}" data-toggle="tooltip" data-placement="left" title="download">
                                <i class="fa fa-fw fa-download" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/documents/$file->id") }}" data-toggle="tooltip" data-placement="left" title="view details">
                                <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/documents/$file->id/edit") }}" data-toggle="tooltip" data-placement="left" title="edit">
                                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-right">
                            @include('admin.parts.deleteBtn', ['action' => ['Admin\Docs\IndexController@destroy', $file->id], 'class' => 'btn-xs', 'btnTitle' => "Delete $file->title"])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">{{ $files->links('vendor.pagination.admin-lte') }}</div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection