@extends('layouts.admin')

@section('title', 'Add Documents')

@section('hTitle')
    Add New Document
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('admin/documents') }}">Documents</a></li>
    <li class="active">Add Document</li>
@endsection

@section('content')
    <div class="box box-primary">
        <!-- /.box-header -->
        {!! Form::open(['url' => 'admin/documents', 'id' => 'myForm', 'files' => true]) !!}
        <div class="box-body">
            @include('admin.docs.formBody')
            <div class="form-group{{ $errors->has('input_file') ? ' has-error has-feedback' : '' }}">
                <span class="fa fa-fw fa-file"></span> {!! Form::label('input_file', 'Document File') !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="required field"></i>
                {!! Form::file('input_file',['class' => 'form-control input-lg', 'id' => 'inputFile']) !!}
                @if ($errors->has('input_file'))
                    <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                    <span class="sr-only">(error)</span>
                    <span class="help-block"><strong>{{ $errors->first('input_file') }}</strong></span>
                @endif
            </div>
        </div>
        <div class="box-footer clearfix">@include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/documents?page=$pageId")])</div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection