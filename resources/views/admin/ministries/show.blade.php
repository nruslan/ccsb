@extends('layouts.admin')

@section('title')
    {{ $ministry->title }}
@endsection

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/ministries?page=$pageId")])
    &nbsp;Ministry: {{ $ministry->name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/ministries?page=$pageId") }}">Ministries</a></li>
    <li class="active">{{ $ministry->name }}</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            {{ $ministry->title or '- title -' }} <small>{{ $ministry->subtitle or '- subtitle -' }}</small>
            <a class="btn btn-primary btn-xs aBtn pull-right" href="{{ url("/admin/ministries/$ministry->id/edit") }}">
                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit
            </a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @include('admin.parts.message')
            <div class="media">
                <div class="media-left">
                    <img class="media-object" src="{{ asset($ministry->logotype_url) }}" alt="logo">
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Description</h4>
                    {!! $ministry->description !!}
                </div>
            </div>
                <hr>
            <!-- Ministry Members -->
            <div class="row">
                <div class="col-lg-6">
                    <h4>
                        Active Ministry Members <span class="badge">{{ $ministry->members_total }}</span>
                        <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/ministries/member/create?id=$ministry->id") }}" data-container="body" data-toggle="tooltip" data-placement="top" title="add member">
                            <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i>
                        </a>
                    </h4>
                    <table class="table">
                        <tbody>
                        @foreach($ministry->members()->where('term_ended', null)->get() as $member)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $member->first_name }} {{ $member->last_name }}</td>
                                <td><small>member since</small> {{ date("F j, Y", strtotime($member->pivot->term_started)) }}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs aBtn" href="{{ url('/admin/ministries/member/'.$member->pivot->id.'/edit') }}">
                                        <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                                    </a>
                                </td>
                                <td>
                                    @include('admin.parts.deleteBtn', ['action' => ['Admin\Ministries\MembersController@destroy', $member->pivot->id], 'btnTitle' => "Delete from the ministry", 'class' => 'btn-xs'])
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-6">
                    <h4>Past Ministry Members</h4>
                    <table class="table">
                        <tbody>
                        @foreach($ministry->past_members as $member)
                            <tr>
                                <td>{{ $ii++ }}</td>
                                <td>{{ $member->first_name }} {{ $member->last_name }}</td>
                                <td>{{ date("F j, Y", strtotime($member->pivot->term_started)) }}</td>
                                <td>{{ date("F j, Y", strtotime($member->pivot->term_ended)) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- /End Ministry Members -->
            <!-- Ministry Liaisons -->
            <div class="row">
                <div class="col-lg-6">
                    <h4>Ministry Liaisons <span class="badge">{{ $ministry->liaisons_total }}</span>
                        <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/ministries/liaison/create?id=$ministry->id") }}" data-container="body" data-toggle="tooltip" data-placement="top" title="add liaison">
                            <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i>
                        </a>
                    </h4>
                    <table class="table">
                        <tbody>
                        @foreach($ministry->liaisons()->where('term_ended', null)->get() as $liaison)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $liaison->first_name }} {{ $liaison->last_name }}</td>
                                <td><small>liaison since</small> {{ date("F j, Y", strtotime($liaison->pivot->term_started)) }}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs aBtn" href="{{ url('/admin/ministries/liaison/'.$liaison->pivot->id.'/edit') }}">
                                        <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                                    </a>
                                </td>
                                <td>
                                    @include('admin.parts.deleteBtn', ['action' => ['Admin\Ministries\LiaisonsController@destroy', $liaison->pivot->id], 'btnTitle' => "Delete from the ministry", 'class' => 'btn-xs'])
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-6">
                    <h4>Past Ministry Liaisons</h4>
                </div>
            </div><!-- /End Ministry Liaisons -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <ul class="list-inline">
                <li class="pull-left"><a href="{{ url("/admin/ministries/create?id=$ministry->id") }}" class="btn btn-primary btn-xs aBtn"><i class="fa fa-fw fa-plus-circle"></i> Related Ministry</a></li>
                <li class="pull-right">@include('admin.parts.deleteBtn', ['action' => ['Admin\Ministries\IndexController@destroy', $ministry->id], 'btnTitle' => "Delete $ministry->name", 'class' => 'btn-xs'])</li>
            </ul>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection