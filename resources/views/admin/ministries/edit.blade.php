@extends('layouts.admin')

@section('title')
    {{ $ministry->name }}
@endsection

@section('hTitle')
    Edit {{ $ministry->name }}'s details
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/ministries?page=$pageId") }}">Ministries</a></li>
    <li><a href="{{ url("/admin/ministries/$ministry->id") }}">{{ $ministry->name }}</a></li>
    <li class="active">Edit {{ $ministry->name }}</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-body">
            {!! Form::model($ministry, ['method' => 'PATCH', 'action' => ['Admin\Ministries\IndexController@update', $ministry->id], 'id' => 'myForm']) !!}
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Ministry Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify name"><i class="fa fa-info fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('name', null, ['class' => 'form-control input-lg', 'placeholder' => 'ministry name', 'autocomplete' => 'off', 'autofocus']) !!}
                        </div>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        {!! Form::label('title', 'Ministry Title') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify title"><i class="fa fa-info fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('title', null, ['class' => 'form-control input-lg', 'placeholder' => 'ministry title', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group{{ $errors->has('subtitle') ? ' has-error' : '' }}">
                        {!! Form::label('subtitle', 'Ministry Subtitle') !!}
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify subtitle"><i class="fa fa-info fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('subtitle', null, ['class' => 'form-control input-lg', 'placeholder' => 'ministry subtitle', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('subtitle'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subtitle') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('meta_d') ? ' has-error' : '' }}">
                        {!! Form::label('meta_d', 'Ministry Meta Description') !!}
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify meta description"><i class="fa fa-info fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('meta_d', null, ['class' => 'form-control input-lg', 'placeholder' => 'ministry meta description', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('meta_d'))
                            <span class="help-block">
                                <strong>{{ $errors->first('meta_d') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            @include('admin.parts.textarea', ['name' => 'description', 'title' => 'Ministry Description'])
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/ministries/$ministry->id")])
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection