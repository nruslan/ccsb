@extends('layouts.admin')

@section('title')
    Add new ministry
@endsection

@section('hTitle')
    Add new ministry
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/ministries') }}">Ministries</a></li>
    <li class="active">Add New Ministry</li>
@endsection

@section('content')
    <div class="box box-primary">
        @isset($ministry)
        <div class="box-header with-border"><h3 class="box-title">Add {{ $ministry->name }}'s related ministry</h3></div>
        <!-- /.box-header -->
        @endisset
        {!! Form::open(['url' => '/admin/ministries', 'id' => 'myForm']) !!}
            @isset($ministry){!! Form::hidden('parent_id', $ministry->id) !!}@endisset
            <div class="box-body">@include('admin.ministries.formBody')</div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">@include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/ministries?page=$pageId")])</div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection