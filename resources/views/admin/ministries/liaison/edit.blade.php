@extends('layouts.admin')

@section('title')
    Edit ministry liaison info
@endsection

@section('hTitle')
    Edit Terms, {{ $ministryMember->ministry->name }} member: {{ $ministryMember->member->full_name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::model($ministryMember, ['method' => 'PATCH', 'action' => ['Admin\Ministries\MembersController@update', $ministryMember->id]]) !!}
                    <div class="form-group{{ $errors->has('term_started') ? ' has-error' : '' }}">
                        {!! Form::label('term_started', 'Term started') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the term is started"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('term_started', null, ['class' => 'form-control input-lg', 'id' => 'datepicker', 'placeholder' => 'term started', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('term_started'))
                            <span class="help-block">
                                <strong>{{ $errors->first('term_started') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('term_ended') ? ' has-error' : '' }}">
                        {!! Form::label('term_ended', 'Term started') !!}
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the term is started"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('term_ended', null, ['class' => 'form-control input-lg', 'id' => 'datepicker2', 'placeholder' => 'term ended', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('term_ended'))
                            <span class="help-block">
                                <strong>{{ $errors->first('term_ended') }}</strong>
                            </span>
                        @endif
                    </div>
                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/ministries/$ministryMember->ministry_id")])
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true
            });

            $( "#datepicker2" ).datepicker({
                changeMonth: true,
                changeYear: true
            });

            $('[data-toggle="tooltip"]').tooltip();

            $('#cancelBtn').on('click', function(){
                $(this).addClass('disabled').append(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });

            $('#submitBtn').on('click', function(){
                $(this).addClass('disabled').append(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection