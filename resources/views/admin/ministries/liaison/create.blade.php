@extends('layouts.admin')

@section('title', 'Add new liaison to the ministry')

@section('hTitle')
    Add new liaison to {{ $ministry->name }} ministry
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/ministries?page=$pageId") }}">Ministries</a></li>
    <li><a href="{{ url("/admin/ministries/$ministry->id") }}">{{ $ministry->name }}</a></li>
    <li class="active">Add New Liaison</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    {!! Form::open(['url' => '/admin/search/member', 'id' => 'search']) !!}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('member_name', 'Get Member') !!}
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="specify username"><i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('member_name', null, ['class' => 'form-control input-lg', 'id' => 'memberName', 'placeholder' => 'name', 'autocomplete' => 'off', 'autofocus']) !!}
                            <span class="input-group-addon"><button type="submit" class="btn btn-default btn-xs" id="searchMember"><i class="fa fa-search fa-fw" aria-hidden="true"></i></button></span>
                        </div>
                        @if ($errors->has('member_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('member_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/ministries/liaison', 'id' => 'myForm']) !!}
                    {!! Form::hidden('ministry_id', $ministry->id) !!}
                    <div class="form-group{{ $errors->has('member_id') ? ' has-error' : '' }}">
                        <div id="memberResult"></div>
                        @if ($errors->has('member_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('member_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('term_started') ? ' has-error' : '' }}">
                        {!! Form::label('term_started', 'Term started') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group input-group-lg date" data-provide="datepicker">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the term is started"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('term_started', null, ['class' => 'form-control input-lg', 'placeholder' => 'term started', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('term_started'))
                            <span class="help-block">
                                <strong>{{ $errors->first('term_started') }}</strong>
                            </span>
                        @endif
                    </div>
                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/ministries/$ministry->id")])
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('.date').datepicker({orientation: "bottom auto"});

            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });

            // Get members by AJAX
            $("#search").on('submit', function(e) {
                $.ajaxSetup({ header:$('meta[name="_token"]').attr('content') });
                e.preventDefault();
                $('#searchMember').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                $.ajax({
                    method: 'POST',
                    url: $("#search").attr('action'),
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data){
                        $('#memberResult').html(data);
                        $('#searchMember').prop('disabled',false).html('<i class="fa fa-search fa-fw" aria-hidden="true"></i>');
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });

        });
    </script>
@endsection