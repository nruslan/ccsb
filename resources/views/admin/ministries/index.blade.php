@extends('layouts.admin')

@section('title')
    Ministries
@endsection

@section('hTitle')
    Church Ministries
@endsection

@section('breadcrumbs')
    <li class="active">Ministries</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            Total ministries <span class="badge">{{ $ministries->total() }}</span>
            <a href="{{ url('/admin/ministries/create') }}" class="btn btn-primary btn-xs aBtn pull-right" data-toggle="tooltip" data-placement="top" title="add new ministry">
                <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i> add new
            </a>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            @include('admin.parts.message')
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Ministry Name</th>
                    <th>Title</th>
                    <th>Subtitle</th>
                    <th class="text-center">Members</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($ministries as $ministry)
                    <tr>
                        <th scope="col">{{ $i++ }}</th>
                        <td>
                            <a class="btn btn-link btn-xs aBtn" href="{{ url("/admin/ministries/$ministry->id") }}" data-toggle="tooltip" data-placement="left" title="view {{ $ministry->name }}'s details">
                                <i class="fa fa-fw fa-eye"></i> {{ $ministry->name }}
                            </a>
                        </td>
                        <td>{{ $ministry->title or '---' }}</td>
                        <td>{{ $ministry->subtitle or '---' }}</td>
                        <td class="text-center">{{ count($ministry->members) }}</td>
                        <td class="text-center">
                            <a class="btn btn-default btn-xs" href="{{ url("/admin/ministries/$ministry->id") }}" data-toggle="tooltip" data-placement="left" title="view {{ $ministry->name }}'s details">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-right">
                            @include('admin.parts.deleteBtn', ['action' => ['Admin\Ministries\IndexController@destroy', $ministry->id], 'class' => 'btn-xs', 'btnTitle' => $ministry->name])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $ministries->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection