@extends('layouts.admin')

@section('title', 'Edit sermon')

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/sermons/$sermon->id"), 'title' => 'sermon list'])
    &nbsp;Edit {!! $sermon->sermons_full_name !!}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/sermons?page=$pageId") }}">Sermons</a></li>
    <li><a href="{{ url("/admin/sermons/$sermon->id") }}">{!! $sermon->sermons_full_name !!}</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    <div class="box box-primary">
        <!-- /.box-header -->
        {!! Form::model($sermon,['method' => 'PATCH', 'action' => ['Admin\Sermons\IndexController@update', $sermon->id], 'id' => 'myForm']) !!}
        @include('admin.sermons.formBody')
        <div class="box-footer clearfix">@include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url('/admin/sermons')])</div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({format: 'MM/DD/YYYY'});
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection