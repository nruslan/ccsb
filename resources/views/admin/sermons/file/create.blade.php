@extends('layouts.admin')

@section('title', 'Add MP3 file')

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/sermons/$sermon->id"), 'title' => "$sermon->title"])
    &nbsp;Add MP3 for {{ $sermon->title }} <small>by {{ $sermon->sermon_pastors }}</small>
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/sermons?page=$pageId") }}">Sermons</a></li>
    <li><a href="{{ url("/admin/sermons/$sermon->id") }}">{{ $sermon->title }}</a></li>
    <li class="active">Add MP3 file</li>
@endsection

@section('content')
    <div class="box box-primary" id="box">
        <!-- /.box-header -->
        {!! Form::open(['url' => 'admin/sermons', 'id' => 'myForm', 'files' => true, 'onsubmit' => 'return false;']) !!}
        {!! Form::hidden('sermon_id', $sermon->id, ['id' => 'sermonId']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-4 col-lg-offset-4">
                    <div class="form-group{{ $errors->has('input_file') ? ' has-error has-feedback' : '' }}">
                        <span class="fa fa-fw fa-music"></span> {!! Form::label('input_file', 'Sermon MP3 File') !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="required field"></i>
                        {!! Form::file('input_file',['class' => 'form-control input-lg', 'id' => 'inputFile']) !!}
                        @if ($errors->has('input_file'))
                            <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                            <span class="sr-only">(error)</span>
                            <span class="help-block"><strong>{{ $errors->first('input_file') }}</strong></span>
                        @endif
                    </div>
                    <audio id="audio"></audio>
                    {!! Form::hidden('duration', null, ['id' => 'f_du']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="progress hidden" id="progress">
                        <div class="progress-bar progress-bar-primary progress-bar-striped" id="output" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                            <span id="completed"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">@include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url('/admin/sermons')])</div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        (function () {
            let output = document.getElementById('output');
            let submitBtn = document.getElementById('submitBtn');
            let progressBar = document.getElementById('progress');
            submitBtn.onclick = function () {
                let data = new FormData();
                let sermon = document.getElementById('inputFile');
                data.append('sermon_id', document.getElementById('sermonId').value);
                data.append('duration', document.getElementById('f_du').value);
                data.append('input_file', sermon.files[0]);
                data.append('file_size', sermon.files[0].size);

                let config = {
                    onUploadProgress: function(progressEvent) {
                        let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                        submitBtn.setAttribute("disabled", "disabled");
                        progressBar.classList.remove('hidden');
                        output.setAttribute("style", "width: "+percentCompleted+"%");
                        document.getElementById('completed').innerHTML = percentCompleted+"%";
                    }
                };
                axios.post('/api/upload', data, config)
                    .then(function (response) {
                        document.getElementById('myForm').remove();
                        //console.log(response);
                        document.getElementById('box').innerHTML = response.data;
                    })
                    .catch(function (err) {
                        output.className = 'container text-danger';
                        output.innerHTML = err.message;
                    });
            };
        })();

        //register canplaythrough event to #audio element to can get duration
        let f_duration =0;  //store duration
        document.getElementById('audio').addEventListener('canplaythrough', function(e){
            //add duration in the input field #f_du
            f_duration = Math.round(e.currentTarget.duration);
            document.getElementById('f_du').value = f_duration;
            URL.revokeObjectURL(obUrl);
        });

        //when select a file, create an ObjectURL with the file and add it in the #audio element
        let obUrl;
        document.getElementById('inputFile').addEventListener('change', function(e){
            let file = e.currentTarget.files[0];
            //check file extension for audio/video type
            if(file.name.match(/\.(avi|mp3|mp4|mpeg|ogg)$/i)){
                obUrl = URL.createObjectURL(file);
                document.getElementById('audio').setAttribute('src', obUrl);
            }
        });
    </script>
@endsection