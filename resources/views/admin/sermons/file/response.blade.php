<div class="box-body">
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-file-audio-o"></i> {{ $sermon->title }}</h4>
        Sermon file has been uploaded to the server successfully!
    </div>
</div>
<!-- /.box-body -->