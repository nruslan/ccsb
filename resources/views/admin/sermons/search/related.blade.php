<h4>Related Sermon</h4>
@foreach($sermons as $sermon)
    <div class="radio">
        <label>
            {!! Form::radio('parent_id', $sermon->id, true) !!}
            {!! $sermon->title_subtitle !!}
        </label>
    </div>
@endforeach