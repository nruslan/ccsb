<tr>
    <td colspan="8">
        @include('admin.parts.btnBack', ['url' => url("/admin/sermons"), 'btnClass' => 'btn-xs', 'title' => "sermons list"]) <strong>Search Results</strong> <span class="badge">{{ $sermons->count() }}</span>
    </td>
</tr>
@foreach($sermons as $sermon)
    <tr>
        <td>{{ $i++ }}</td>
        <td>{{ $sermon->date }}</td>
        <td>{{ $sermon->title }} • <small>{{ $sermon->subtitle }}</small></td>
        <td>{{ $sermon->duration }}</td>
        <td>{{ $sermon->sermon_pastors }}</td>
        <td class="text-center">
            <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/sermons/$sermon->id") }}" data-toggle="tooltip" data-placement="left" title="view details">
                <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
            </a>
            @if(!$sermon->filename)
                &nbsp;
                <a class="btn btn-default btn-xs aBtn" href="{{ url("admin/sermons/file/create?id=$sermon->id") }}" data-toggle="tooltip" data-placement="top" title="MP3 upload">
                    <i class="fa fa-upload fa-fw"></i>
                </a>
            @endif
        </td>
        <td>
            <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/sermons/$sermon->id/edit") }}" data-toggle="tooltip" data-placement="left" title="edit">
                <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
            </a>
        </td>
        <td class="text-right">
            @include('admin.parts.deleteBtn', ['action' => ['Admin\Sermons\IndexController@destroy', $sermon->id], 'class' => 'btn-xs', 'btnTitle' => $sermon->title])
        </td>
    </tr>
@endforeach