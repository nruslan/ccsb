@extends('layouts.admin')

@section('title', 'Sermons')

@section('hTitle')
    Sermons
@endsection

@section('breadcrumbs')
    <li class="active">Sermons</li>
@endsection

@section('search')
    @include('admin.parts.sidebarSearch', ['url' => 'api/sermon/search'])
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        Total Sermons <span class="badge">{{ $sermons->total() }}</span>
        <a href="{{ url('/admin/sermons/create') }}" class="btn btn-primary btn-xs pull-right aBtn" data-toggle="tooltip" data-placement="top" title="add new sermon">
            <i class="fa fa-fw fa-plus-circle" aria-hidden="true"></i> add</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        @include('admin.parts.message')
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Sermon's Date</th>
                <th>Title</th>
                <th>Duration</th>
                <th>Pastor</th>
                <th colspan="3" class="text-center">Action buttons</th>
            </tr>
            </thead>
            <tbody id="result">
            @foreach($sermons as $sermon)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $sermon->date }}</td>
                    <td>{{ $sermon->title }} • <small>{{ $sermon->subtitle }}</small></td>
                    <td>{{ $sermon->duration }}</td>
                    <td>{{ $sermon->sermon_pastors }}</td>
                    <td class="text-center">
                        <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/sermons/$sermon->id") }}" data-toggle="tooltip" data-placement="left" title="view details">
                            <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                        </a>
                        @if(!$sermon->filename)
                            <a class="btn btn-default btn-xs aBtn" href="{{ url("admin/sermons/file/create?id=$sermon->id") }}" data-toggle="tooltip" data-placement="top" title="MP3 upload">
                                <i class="fa fa-upload fa-fw"></i>
                            </a>
                        @endif
                    </td>
                    <td class="text-center">
                        <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/sermons/$sermon->id/edit") }}" data-toggle="tooltip" data-placement="left" title="edit">
                            <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-right">
                        @include('admin.parts.deleteBtn', ['action' => ['Admin\Sermons\IndexController@destroy', $sermon->id], 'class' => 'btn-xs', 'btnTitle' => "Delete $sermon->title"])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">{{ $sermons->links('vendor.pagination.admin-lte') }}</div>
</div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            // Search function
            (function () {
                let submitBtn = document.getElementById('search-btn');
                submitBtn.onclick = function () {
                    submitBtn.setAttribute("disabled", "disabled");
                    let query = document.getElementById('searchQuery').value;
                    axios.post('/api/sermon/search', {
                        query : query
                    })
                    .then(function (response) {
                        document.getElementById('result').innerHTML = response.data;
                        submitBtn.removeAttribute("disabled");
                    })
                    .catch(function (err) {
                        document.getElementById('result').innerHTML = err.message;
                    });
                }
            })();
        });
    </script>
@endsection