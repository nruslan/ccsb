@extends('layouts.admin')

@section('title', 'Add new sermon')

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/sermons?page=$pageId"), 'title' => 'sermon list'])
    &nbsp;Add new sermon
@endsection

@section('breadcrumbs')
    <li><a href="{{ url('/admin/sermons') }}">Sermons</a></li>
    <li class="active">Add New Sermons</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            {!! Form::open(['url' => '/admin/sermons/search/related', 'id' => 'search', 'class' => 'form-inline text-center']) !!}
            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                {!! Form::label('title', 'Get Member', ['class' => 'sr-only']) !!}
                <div class="input-group input-group-sm" data-toggle="tooltip" data-placement="left" title="search for related sermon">
                    {!! Form::text('title', null, ['class' => 'form-control', 'id' => 'sermonTitle', 'placeholder' => 'sermon title', 'autocomplete' => 'off']) !!}
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary btn-flat" id="searchSermon"><i class="fa fa-search fa-fw" aria-hidden="true"></i> Find sermon</button>
                    </span>
                </div>
                @if ($errors->has('title'))<span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>@endif
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => 'admin/sermons', 'id' => 'myForm', 'files' => true]) !!}
        @include('admin.sermons.formBody')
        <div class="box-footer clearfix">@include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url('/admin/sermons')])</div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({format: 'MM/DD/YYYY'});

            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });

            // Get members by AJAX
            $("#search").on('submit', function(e) {
                $.ajaxSetup({ header:$('meta[name="_token"]').attr('content') });
                e.preventDefault();
                $('#searchSermon').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                $('#result').prepend('<div class="text-center text-muted center-block">loading <i class="fa fa-spinner fa-pulse fa-fw"></i></div>');
                $.ajax({
                    method: 'POST',
                    url: $('#search').attr('action'),
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data){
                        $('#result').html(data);
                        $('#searchSermon').prop('disabled',false).html('Find sermon');
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
        });
    </script>
@endsection