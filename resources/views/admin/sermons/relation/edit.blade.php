@extends('layouts.admin')

@section('title', 'Add Related Sermon')

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/sermons/$sermon->id"), 'title' => 'sermon'])
    &nbsp;Add Related Sermon
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/sermons?page=$pageId") }}">Sermons</a></li>
    <li><a href="{{ url("/admin/sermons/$sermon->id") }}">{!! $sermon->sermons_full_name !!}</a></li>
    <li class="active">Add Related Sermon</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            {!! $sermon->sermons_full_name !!}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::open(['url' => '/admin/sermons/search/related', 'id' => 'search', 'class' => 'form-inline text-center']) !!}
            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                {!! Form::label('title', 'Get Member', ['class' => 'sr-only']) !!}
                <div class="input-group input-group-sm" data-toggle="tooltip" data-placement="left" title="search for related sermon">
                    {!! Form::text('title', null, ['class' => 'form-control', 'id' => 'sermonTitle', 'placeholder' => 'sermon title', 'autocomplete' => 'off']) !!}
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary btn-flat" id="searchSermon"><i class="fa fa-search fa-fw" aria-hidden="true"></i> Find sermon</button>
                    </span>
                </div>
                @if ($errors->has('title'))<span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>@endif
            </div>
            {!! Form::close() !!}


            {!! Form::model($sermon,['method' => 'PATCH', 'action' => ['Admin\Sermons\RelationController@update', $sermon->id], 'id' => 'myForm']) !!}
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                        @if ($errors->has('parent_id'))
                            <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                            <span class="sr-only">(error)</span>
                            <span class="help-block"><strong>{{ $errors->first('parent_id') }}</strong></span>
                        @endif
                        <div id="result"></div>
                    </div>
                </div>
            </div>
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("/admin/sermons/$sermon->id")])
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $('#myForm').on('submit', function(){
            $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
        });
        // Get members by AJAX
        $("#search").on('submit', function(e) {
            $.ajaxSetup({ header:$('meta[name="_token"]').attr('content') });
            e.preventDefault();
            $('#searchSermon').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            $('#result').prepend('<div class="text-center text-muted center-block">loading <i class="fa fa-spinner fa-pulse fa-fw"></i></div>');
            $.ajax({
                method: 'POST',
                url: $('#search').attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                success: function(data){
                    $('#result').html(data);
                    $('#searchSermon').prop('disabled',false).html('Find sermon');
                },
                error: function(data){
                    console.log(data);
                }
            });
        });
    </script>
@endsection