<div class="box-body">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                {!! Form::label('title', 'Title') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                <div class="input-group input-group-lg">
                    <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Sermon's title"><i class="fa fa-chevron-right fa-fw" aria-hidden="true"></i></span>
                    {!! Form::text('title', null, ['class' => 'form-control input-lg', 'placeholder' => 'sermons title', 'id' => 'title', 'autocomplete' => 'off']) !!}
                </div>
                @if ($errors->has('title'))
                    <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                    <span class="sr-only">(error)</span>
                    <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('subtitle') ? ' has-error' : '' }}">
                {!! Form::label('subtitle', 'Subtitle') !!}
                <div class="input-group input-group-lg">
                    <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Sermon's subtitle"><i class="fa fa-chevron-right fa-fw" aria-hidden="true"></i></span>
                    {!! Form::text('subtitle', null, ['class' => 'form-control input-lg', 'id' => 'subtitle', 'placeholder' => 'sermons subtitle', 'autocomplete' => 'off']) !!}
                </div>
                @if ($errors->has('subtitle'))
                    <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                    <span class="sr-only">(error)</span>
                    <span class="help-block"><strong>{{ $errors->first('subtitle') }}</strong></span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                {!! Form::label('date', 'Recorded date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                <div class="input-group input-group-lg date" id="datepicker">
                    <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the sermon was recorded"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                    {!! Form::text('date', null, ['class' => 'form-control input-lg', 'placeholder' => 'recorded date', 'autocomplete' => 'off']) !!}
                </div>
                @if ($errors->has('date'))
                    <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                    <span class="sr-only">(error)</span>
                    <span class="help-block"><strong>{{ $errors->first('date') }}</strong></span>
                @endif
            </div>
            <h4>Pastors</h4>
            <div class="form-group{{ $errors->has('member_id') ? ' has-error' : '' }}">
                @foreach($title1->members as $member)
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('member_id[]', $member->id) !!} {{ $member->full_name }}
                        </label>
                    </div>
                @endforeach
                @foreach($title2->members as $member2)
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('member_id[]', $member2->id) !!} {{ $member2->full_name }}
                        </label>
                    </div>
                @endforeach
            </div>
            @include('admin.parts.textarea', ['name' => 'description', 'title' => 'Sermon Description'])
        </div>
        <div class="col-lg-6">
            <div id="result"></div>
        </div>
    </div>
</div>
<!-- /.box-body -->