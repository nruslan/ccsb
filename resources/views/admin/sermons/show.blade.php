@extends('layouts.admin')

@section('title', $sermon->title)

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/sermons?page=$pageId")])
    &nbsp;{{ $sermon->title }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/sermons?page=$pageId") }}">Sermons</a></li>
    <li class="active">{{ $sermon->title }}</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    Recorded {{ $sermon->date }}
                    <a class="btn btn-primary btn-sm pull-right aBtn" href="{{ url("/admin/sermons/$sermon->id/edit") }}"><i class="fa fa-fw fa-pencil"></i> Edit</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <h2>
                        {!! $sermon->title_subtitle !!}<br>
                        <small>by {{ $sermon->sermon_pastors }}</small>
                    </h2>
                    <h4>Description</h4>
                    {!! $sermon->description !!}
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    @include('admin.parts.deleteBtn', ['action' => ['Admin\Sermons\IndexController@destroy', $sermon->id], 'class' => 'btn-xs', 'btnTitle' => "the sermon", 'position' => 'top'])
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-black" style="background: url({{ $sermon->thumbnail }}) center center;">
                    <h3 class="widget-user-username">{{ $sermon->title }} {{ $sermon->subtitle }}</h3>
                    <h5 class="widget-user-desc">by {{ $sermon->sermon_pastors }}</h5>
                </div>
                <div class="widget-user-image">
                    @foreach($sermon->pastors as $pastor)
                    <img class="img-circle" src="{{ $pastor->profile_picture_url }}" alt="User Avatar">
                    @endforeach
                </div>
                <div class="box-footer">
                    <div class="description-block">
                        <h5 class="description-header">{!! $sermon->title_subtitle !!}</h5>
                        @if($sermon->filename)
                            <audio controls="controls" title="{{ $sermon->title }}">
                                Your browser does not support the <code>audio</code> element.
                                <source src="{{ $sermon->sermon_url }}" type="audio/mp3">
                            </audio>
                        @else
                            <a href="{{ url("admin/sermons/file/create?id=$sermon->id") }}" class="btn btn-block btn-primary aBtn" data-toggle="tooltip" data-placement="top" title="upload audio file">
                                <i class="fa fa-upload fa-fw"></i> Upload
                            </a>
                        @endif
                    </div>
                    <!-- /.description-block -->
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{ $sermon->compact_date }}</h5>
                                <span class="description-text">DATE</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{ $sermon->duration }}</h5>
                                <span class="description-text">DURATION</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header">{{ $sermon->file_size }} MB</h5>
                                <span class="description-text">SIZE</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-inline">
                                <li>
                                    <a class="btn btn-default{{ $prevDisabled or '' }} btn-sm aBtn" href="{{ url("/admin/sermons/$prevId") }}" data-toggle="tooltip" data-placement="top" title="go to Previous">
                                        <i class="fa fa-fw fa-angle-double-left" aria-hidden="true"></i> Previous</a></li>
                                <li class="pull-right">
                                    <a class="btn btn-default{{ $nextDisabled or '' }} btn-sm aBtn" href="{{ url("/admin/sermons/$nextId") }}" data-toggle="tooltip" data-placement="top" title="go to Next">
                                        Next <i class="fa fa-fw fa-angle-double-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @if($sermon->filename)
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            @include('admin.parts.deleteBtn', ['action' => ['Admin\Sermons\FileController@destroy', $sermon->id], 'class' => 'btn-xs', 'btnTitle' => "sermon file", 'position' => 'top'])
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            @if($sermon->relatedSermon)
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue">
                        <a href="{{ url("/admin/sermons/".$sermon->relatedSermon->id) }}">
                            <div class="widget-user-image">
                                <img class="img-circle" src="{{ asset("/storage/sermons/headphones.png") }}" alt="User Avatar">
                            </div>
                        </a>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">Related Sermon</h3>
                        <h5 class="widget-user-desc">by {{ $sermon->relatedSermon->sermon_pastors }}</h5>
                    </div>
                    <div class="box-footer text-center">
                        <h4>{!! $sermon->relatedSermon->title_subtitle !!}</h4>
                        @if($sermon->relatedSermon->filename)
                            <audio controls="controls">
                                Your browser does not support the <code>audio</code> element.
                                <source src="{{ asset('storage/sermons/'.date('Y',strtotime($sermon->relatedSermon->date)).'/'.$sermon->relatedSermon->filename) }}" type="audio/mp3">
                            </audio>
                        @endif
                    </div>
                    <div class="box-footer">
                        <ul class="list-inline">
                            <li class="pull-right">@include('admin.parts.deleteBtn', ['action' => ['Admin\Sermons\RelationController@destroy', $sermon->id], 'class' => 'btn-xs', 'btnTitle' => "Remove relation", 'position' => 'left'])</li>
                        </ul>
                    </div>
                </div>
            @else
                <a href="{{ url("/admin/sermons/relation/$sermon->id/edit") }}" class="btn btn-app btn-primary aBtn"><i class="fa fa-chain"></i>Add Related Sermon</a>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.addNew').on('click', function(){
                $(this).addClass('disabled').append(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection