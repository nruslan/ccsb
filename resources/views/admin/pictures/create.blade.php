@extends('layouts.admin')

@section('title')
    Pictures
@endsection

@section('hTitle')
    Upload Picture
@endsection

@section('styles')
    <style type="text/css">
        .imageBox
        {
            position: relative;
            height: 600px;
            width: 600px;
            border:1px solid #aaa;
            background: #fff;
            overflow: hidden;
            background-repeat: no-repeat;
            cursor:move;
        }

        .imageBox .thumbBox
        {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 302px;
            height: 302px;
            margin-top: -151px;
            margin-left: -151px;
            box-sizing: border-box;
            border: 1px solid rgb(102, 102, 102);
            box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
            background: none repeat scroll 0% 0% transparent;
        }

        .imageBox .spinner
        {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            text-align: center;
            line-height: 400px;
            background: rgba(0,0,0,0.7);
        }

        .container
        {
            position: absolute;
            top: 10%; left: 10%; right: 0; bottom: 0;
        }
        .action
        {
            width: 600px;
            height: 50px;
            margin: 10px 0;
        }
        .cropped>img
        {
            margin-right: 10px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Images <span class="badge"></span>
                </div>
                <div class="panel-body text-center">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="imageBox">
                                <div class="thumbBox"></div>
                                <div class="spinner" style="display: none">Loading...</div>
                            </div>
                            <div class="action">
                                <div class="form-group">
                                    <label class="sr-only" for="file">Image</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-file-image-o" aria-hidden="true"></i></div>
                                        <input type="file"  class="form-control input-lg" id="file" autofocus>
                                    </div>
                                </div>
                                <div class="form-inline text-center">
                                    <div class="form-group">
                                        <button class="btn btn-default btn-lg" id="btnZoomIn"><i class="fa fa-fw fa-search-plus" aria-hidden="true"></i></button>
                                        <button class="btn btn-default btn-lg" id="btnZoomOut"><i class="fa fa-fw fa-search-minus" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="form-group pull-right">
                                        <button type="submit" class="btn btn-primary btn-lg" id="btnCrop">crop <i class="fa fa-crop" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="cropped"></div>
                                    {!! Form::open(['url' => '/admin/accounts/members/picture']) !!}
                                    {!! Form::hidden('id',null) !!}
                                    {!! Form::textarea('imageData', null, ['class' => 'form-control hidden', 'readonly']) !!}
                                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection