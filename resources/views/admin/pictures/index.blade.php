@extends('layouts.admin')

@section('styles')
    @include('admin.parts.jquery-confirm.css')
@endsection

@section('title')
    Pictures
@endsection

@section('hTitle')
    Pictures
@endsection

@section('content')
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['url' => '/admin/accounts/members/picture', 'files' => true]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Picture upload form</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <p class="help-block">1 file max. Types supported: jpg, png, gif</p>
                        <label class="sr-only" for="file">File</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-file-image-o" aria-hidden="true"></i></div>
                            {!! Form::file('pictures', ['class' => 'form-control input-lg', 'id' => 'files', 'autofocus']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="alt">Alternate description</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></div>
                            {!! Form::text('alt', null, ['class' => 'form-control input-lg']) !!}
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Images <span class="badge"></span>
                    <div class="pull-right">
                        <a href="{{ url('/admin/pictures/create') }}" class="btn btn-primary btn-xs addNew" data-toggle="tooltip" data-placement="top" title="add new picture">add new</a>
                    </div>
                </div>
                <div class="panel-body text-center">
                    @include('admin.parts.message')
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>file name</th>
                        <th>Alt text</th>
                        <th>date</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pictures as $picture)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    @include('admin.parts.jquery-confirm.script')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();

            $('.addNew').on('click', function(el){
                el.preventDefault();
                var btnValue = $(this).html();
                $(this).addClass('disabled').append(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
                $('#myModal').modal();
                $(this).removeClass('disabled').html(btnValue);
            });

            $('.a-btn').on('click', function(){
                $(this).addClass('disabled').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });

            //Delete button
            $('.delBtn').on('click', function(el){
                el.preventDefault();
                var btn = $(this);
                var btnTitle = btn.attr('data-original-title')
                $.confirm({
                    title: btnTitle.toUpperCase(),
                    content: 'Please confirm that you want to ' + btnTitle,
                    buttons: {
                        confirm: {
                            text: 'delete',
                            btnClass: 'btn-red',
                            action: function (){
                                btn.addClass('disabled').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                                btn.parent('form').submit();
                            }
                        },
                        cancel: function (){},
                    }
                });
            });
        });
    </script>
@endsection