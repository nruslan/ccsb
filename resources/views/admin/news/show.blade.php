@extends('layouts.admin')

@section('title', $aNews->title)

@section('hTitle')
    @include('admin.parts.btnBack', ['url' => url("/admin/news"), 'title' => 'news list'])
    {{ $aNews->title }} <small>{{ $aNews->subtitle }}</small>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="pull-right">
                <small>{!! $aNews->pub_draft !!}</small>
                <a href="{{ url("/admin/news/$aNews->id/edit") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="edit"><i class="fa fa-fw fa-pencil"></i>Edit</a>
            </div>
            Publish date/time <span class="badge">{{ $aNews->publish_date_time }}</span>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @include('admin.parts.message')
            {!! $aNews->text !!}
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="thumbnail">
                        <img class="img-responsive" src="{{ $aNews->thumbnail_picture }}" alt="cover image">
                        <div class="caption">
                            <ul class="list-inline">
                                @if($aNews->picture_id)
                                    <li class="pull-right">@include('admin.parts.deleteBtn', ['action' => ['Admin\News\PictureController@destroy', $aNews->id], 'class' => 'btn-xs', 'btnTitle' => "Delete Cover Image"])</li>
                                    @else
                                    <li class="pull-left"><a href="{{ url("admin/news/picture/create?id=$aNews->id") }}" class="btn btn-primary btn-xs aBtn" role="button"><i class="fa fa-fw fa-upload"></i> upload</a></li>
                                @endif
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix"></div>
    </div><!-- /.row -->
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            //Delete button
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection