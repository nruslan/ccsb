@extends('layouts.admin')

@section('title')
    News
@endsection

@section('hTitle')
    Church News
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">News: total posts <span class="badge">{{ $news->total() }}</span></h3>
            <div class="box-tools">
                <a href="{{ url('/admin/news/create') }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="add news">
                    <i class="fa fa-fw fa-plus-circle"></i> Add</a></div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @include('admin.parts.message')
            <table class="table no-padding">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Status</th>
                    <th>Pub Date / Time</th>
                    <th></th>
                    <th>Title / Subtitle</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($news as $post)
                    <tr @if($post->draft) class="warning" @endif>
                        <th scope="row">{{ $i++ }}</th>
                        <td><small>{!! $post->pub_draft !!}</small></td>
                        <td>
                            <a class="aBtn" href="{{ url("/admin/news/$post->id") }}">
                                <i class="fa fa-fw fa-calendar"></i> {{ $post->pub_date }} {{ $post->pub_time }}</a>
                        </td>
                        <td><img src="{{ $post->thumbnail_picture }}" alt="cover image" height="15"></td>
                        <td>{{ $post->title }} &bullet; <small>{{ $post->subtitle }}</small></td>
                        <td>
                            <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/news/$post->id") }}" data-toggle="tooltip" data-placement="top" title="view details">
                                <i class="fa fa-fw fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $news->links() }}
        </div>
    </div><!-- /.box -->
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            //Delete button
            $('.delBtn').on('click', function(el){
                el.preventDefault();
                var btn = $(this);
                var btnTitle = btn.attr('data-original-title')
                $.confirm({
                    title: btnTitle.toUpperCase(),
                    content: 'Please confirm that you want to ' + btnTitle,
                    buttons: {
                        confirm: {
                            text: 'delete',
                            btnClass: 'btn-red',
                            action: function (){
                                btn.addClass('disabled').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                                btn.parent('form').submit();
                            }
                        },
                        cancel: function (){},
                    }
                });
            });
        });
    </script>
@endsection