@extends('layouts.admin')

@section('title')
    Add News
@endsection

@section('hTitle')
    Add Community Church News
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url' => 'admin/news']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                {!! Form::label('title', 'Title') !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="field is required "></i>
                                {!! Form::text('title', null, ['class' => 'form-control input-lg', 'placeholder' => 'title']) !!}
                                @if ($errors->has('title'))
                                    <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('subtitle') ? ' has-error' : '' }}">
                                {!! Form::label('subtitle', 'Subtitle') !!}
                                {!! Form::text('subtitle', null, ['class' => 'form-control input-lg', 'placeholder' => 'subtitle']) !!}
                                @if ($errors->has('subtitle'))
                                    <span class="help-block"><strong>{{ $errors->first('subtitle') }}</strong></span>
                                @endif
                            </div>
                            @include('admin.parts.textarea', ['name' => 'text', 'title' => 'Content'])
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('pub_date_time') ? ' has-error' : '' }}">
                                {!! Form::label('pub_date_time', 'Publish Date / Time') !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="field is required "></i>
                                <div class="input-group date" id="datepicker">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    {!! Form::text('pub_date_time', null, ['class' => 'form-control input-lg','placeholder' => 'mm/dd/yyyy H:m', 'autocomplete' => 'off']) !!}
                                </div>
                                @if ($errors->has('pub_date_time'))
                                    <span class="help-block"><strong>{{ $errors->first('pub_date_time') }}</strong></span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Committees</h4>
                                    <div class="form-group">
                                        @foreach($committees as $id => $committee)
                                            <div class="checkbox">
                                                <label>
                                                    <input name="committee[]" type="checkbox" value="{{ $id }}"> {{ $committee }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h4>Ministries</h4>
                                    <div class="form-group">
                                        @foreach($ministries as $id => $ministry)
                                            <div class="checkbox">
                                                <label>
                                                    <input name="ministry[]" type="checkbox" value="{{ $id }}"> {{ $ministry }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('draft', 1, false, ['id' => 'draft']) !!} <strong>Save post as draft</strong>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    @include('admin.parts.btnSubmitCancel', ['btnName' => 'Publish', 'cancelUrl' => url('/admin/news')])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker();

            $('#cancelBtn').on('click', function(){
                $(this).addClass('disabled').append(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });

            $('#submitBtn').on('click', function(){
                $(this).addClass('disabled').append(' <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });

            $("#draft").on('change', function() {

                if ( this.checked ) {
                    $('#submitBtn').html('<i class="fa fa-fw fa-floppy-o"></i> Save as draft');
                }
                else {
                    $('#submitBtn').html('<i class="fa fa-fw fa-floppy-o"></i> Publish');
                };

            });


        });
    </script>
@endsection