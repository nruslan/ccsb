@extends('layouts.admin')

@section('title', "Events")

@section('hTitle')
    Church Events
@endsection

@section('breadcrumbs')
    <li class="active">Events</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">List of All Events</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Location Name</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($dates as $date)
                    <tr>
                        <th scope="col">{{ $i++ }}</th>
                        <td>{{ $date->date }}</td>
                        <td>{{ $date->time }}</td>
                        <td>{{ $date->location_name }}</td>
                        <td class="text-center">
                            <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/events/$date->event_id") }}" data-toggle="tooltip" data-placement="top" title="view details">
                                <i class="fa fa-fw fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $dates->links() }}
        </div>
    </div>
@endsection