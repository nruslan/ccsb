@extends('layouts.admin')

@section('title')
    Add Event Date
@endsection

@section('hTitle')
    Add Event Date
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{!! $event->title_subtitle !!}</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => 'admin/events/meta', 'id' => 'myForm']) !!}
        {!! Form::hidden('event_id', $event->id) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                        {!! Form::label('time', "Event Time") !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="field is required "></i>
                        <div class="input-group input-group-lg date" id="datepicker0">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            {!! Form::text('time', null, ['class' => 'form-control input-lg','placeholder' => 'event time']) !!}
                        </div>
                        @if ($errors->has('time'))
                            <span class="help-block"><strong>{{ $errors->first('time') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                        {!! Form::label('date', "Event Date") !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="field is required "></i>
                        <div class="input-group input-group-lg date" id="datepicker">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            {!! Form::text('date', null, ['class' => 'form-control input-lg','placeholder' => 'event date']) !!}
                        </div>
                        @if ($errors->has('date'))
                            <span class="help-block"><strong>{{ $errors->first('date') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('date_end') ? ' has-error' : '' }}">
                        {!! Form::label('date_end', "Event End Date") !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="field is required "></i>
                        <div class="input-group input-group-lg date" id="datepicker2">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            {!! Form::text('date_end', null, ['class' => 'form-control input-lg', 'placeholder' => 'event end date', 'id' => 'dateTimeEnd', 'disabled']) !!}
                        </div>
                        @if ($errors->has('date_end'))<span class="help-block"><strong>{{ $errors->first('date_end') }}</strong></span>@endif
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="repeat_weekly" name="repeat_weekly"> <strong>Repeat Weekly</strong>
                    </label>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("admin/events/$event->id")])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(function () {
            $("#datepicker0").datetimepicker({ format: 'LT' });

            $("#datepicker").datetimepicker({ format: 'MM/DD/YYYY' });
            $("#datepicker2").datetimepicker({
                format: 'MM/DD/YYYY',
                useCurrent: false //Important! See issue #1075
            });
            $("#datepicker").on("dp.change", function (e) {
                $("#datepicker2").data("DateTimePicker").minDate(e.date);
            });
            $("#datepicker2").on("dp.change", function (e) {
                $("#datepicker").data("DateTimePicker").maxDate(e.date);
            });
        });

        $(document).ready(function() {
            $("input[type=checkbox]").change(function(){
                $("#dateTimeEnd").prop("disabled", !$(this).is(':checked'));
            });

            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection