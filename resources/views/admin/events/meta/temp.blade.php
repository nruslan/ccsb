@foreach($eventsMeta as $eventMeta)
    @if($eventMeta->interval)
        @for ($date = strtotime($eventMeta->date); $date <= strtotime($eventMeta->end_date); $date = $date + $eventMeta->interval)
            <tr>
                <td></td>
                <td>{{ $eventMeta->date }}</td>
                <td>{{ $eventMeta->data->title }}</td>
                <td>{{ $eventMeta->data->ministry_name }}</td>
            </tr>
        @endfor
    @else
        <tr>
            <td></td>
            <td>{{ date("F d l, Y", strtotime($eventMeta->date)) }}</td>
            <td>{{ $eventMeta->data->title }}</td>
            <td>{{ $eventMeta->data->ministry_name }}</td>
        </tr>
    @endif
@endforeach

@foreach($events as $event)
    @if((strtotime($today) - strtotime($event->date)) % $event->interval = 0)
        <tr>
            <td></td>
            <td>{{ $event->date }}</td>
            <td>{{ $event->title }}</td>
            <td></td>
        </tr>
    @endif
@endforeach

@for($day = 1; $day <= $numDays; $day++)

    @foreach($events as $event)
        @if(strtotime($event->date) < $event->current_date($day))
            <td colspan="4">{{ $day }}</td>
        @endif
    @endforeach


@endfor