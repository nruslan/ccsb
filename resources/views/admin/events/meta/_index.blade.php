@extends('layouts.admin')

@section('title')
    Events
@endsection

@section('hTitle')
    Church Events
@endsection

@section('breadcrumbs')
    <li class="active">Events</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="pull-right">
                <a href="{{ url('/admin/events/create') }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="left" title="add new event">
                    <i class="fa fa-plus-circle fa-fw"></i> Add</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            @include('admin.parts.message')
            <table class="table table-striped">
                <thead>
                <tr>
                    @foreach(Helper::days_of_week(2) as $id => $dayName)
                        <th>{{ $dayName }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                <tr>
                    @if($dayOfWeek > 0)<td colspan="{{ $dayOfWeek }}"></td>@endif
                    @for($day = 1; $day <= $numDays; $day++)
                        <td>{{ $day }}</td>
                        @if(Helper::day_of_week($day) == 6) </tr><tr> @endif
                    @endfor
                    @if(Helper::day_of_week($day) > 0)<td colspan="{{ 7 - Helper::day_of_week($day) }}"></td>@endif
                </tr>
                </tbody>
            </table>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Title</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($eventsMeta as $em)
                        <tr>
                            <td></td>
                            <td>{{ $em->date_time }}</td>
                            <td>{{ $em->data->title }}</td>
                            <td></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix"></div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            //Delete button
            $('.delBtn').on('click', function(el){
                el.preventDefault();
                var btn = $(this);
                var btnTitle = btn.attr('data-original-title')
                $.confirm({
                    title: btnTitle.toUpperCase(),
                    content: 'Please confirm that you want to ' + btnTitle,
                    buttons: {
                        confirm: {
                            text: 'delete',
                            btnClass: 'btn-red',
                            action: function (){
                                btn.addClass('disabled').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                                btn.parent('form').submit();
                            }
                        },
                        cancel: function (){},
                    }
                });
            });
        });
    </script>
@endsection