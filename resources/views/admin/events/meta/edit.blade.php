@extends('layouts.admin')

@section('title')
    Edit Event Date
@endsection

@section('hTitle')
    Edit Event Date
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{!! $eventMeta->data->title_subtitle !!}</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($eventMeta, ['method' => 'PATCH', 'action' => ['Admin\Events\MetaController@update', $eventMeta->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group{{ $errors->has('date_time') ? ' has-error' : '' }}">
                        {!! Form::label('date_time', "Event's Date & Time") !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="field is required "></i>
                        <div class="input-group date" id="datepicker">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            {!! Form::text('date_time', null, ['class' => 'form-control input-lg','placeholder' => 'Date & Time']) !!}
                        </div>
                        @if ($errors->has('date_time'))
                            <span class="help-block"><strong>{{ $errors->first('date_time') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("admin/events/$eventMeta->event_id")])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker();
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection