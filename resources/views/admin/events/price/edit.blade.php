@extends('layouts.admin')

@section('title', "Edit the Price")

@section('hTitle')
    Edit the Price
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            Edit the Price for Event: {{ $ePrice->event->title }}
        </div><!-- /.box-header -->
        {!! Form::model($ePrice, ['method' => 'PATCH', 'action' => ['Admin\Events\PriceController@update', $ePrice->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.events.price.formBody')
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("admin/events/$ePrice->event_id")])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection