@extends('layouts.admin')

@section('title')
    Define Price
@endsection

@section('hTitle')
    Define the Price
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            Define the Price for Event: {{ $event->title }}
        </div><!-- /.box-header -->

        {!! Form::open(['url' => 'admin/events/price', 'id' => 'myForm']) !!}
        {!! Form::hidden('event_id', $event->id) !!}
        <div class="box-body">
            @include('admin.events.price.formBody')
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("admin/events/$event->id")])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection