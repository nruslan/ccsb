<div class="row">
    <div class="col-lg-3">
        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
            {!! Form::label('price', 'Price') !!}
            <div class="input-group input-group-lg">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Price"><i class="fa fa-usd fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('price', null, ['class' => 'form-control input-lg', 'placeholder' => 'price', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('price'))<span class="help-block"><strong>{{ $errors->first('price') }}</strong></span>@endif
        </div>
    </div>
    <div class="col-lg-9">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', 'Short Price Description') !!}
            <div class="input-group input-group-lg">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Price description"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('name', null, ['class' => 'form-control input-lg', 'placeholder' => 'price description', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('name'))<span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>@endif
        </div>
    </div>
</div>