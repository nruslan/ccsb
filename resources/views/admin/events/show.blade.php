@extends('layouts.admin')

@section('title', "$event->title")

@section('hTitle')
    @include('admin.parts.btnBack', ['title' => 'the list', 'url' => url("/admin/events")])
    {{ $event->title }}
@endsection

@section('breadcrumbs')
    <li class="active">Events</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <a class="btn btn-primary btn-xs aBtn pull-right" href="{{ url("admin/events/$event->id/edit") }}" data-toggle="tooltip" data-placement="left" title="edit"><i class="fa fa-fw fa-pencil"></i> Edit</a>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            @include('admin.parts.message')
            <div class="row">
                <div class="col-md-6">
                    <h4>Description</h4>
                    {!! $event->description !!}
                </div>
                <div class="col-md-6">
                    <h4>Ministries</h4>
                    @foreach($ministries as $ministry)
                        {{ $ministry->title }}
                    @endforeach
                    <h4>Cover Image</h4>
                    <div class="thumbnail">
                        <img class="img-responsive" src="{{ $event->thumbnail_picture }}" alt="cover image">
                        <div class="caption">
                            <ul class="list-inline">
                                @if($event->picture_id)
                                    <li class="pull-right">@include('admin.parts.deleteBtn', ['action' => ['Admin\Events\PictureController@destroy', $event->id], 'class' => 'btn-xs', 'btnTitle' => "Delete Cover Image"])</li>
                                @else
                                    <li class="pull-left"><a href="{{ url("admin/events/picture/create?id=$event->id") }}" class="btn btn-primary btn-xs aBtn" role="button"><i class="fa fa-fw fa-upload"></i> upload</a></li>
                                @endif
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            @if($event->price)
                {{ $event->price_description }}
            @endif
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            @if($event->price)
                <ul class="list-inline no-margin">
                    <li><a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/events/price/".$event->price->id."/edit") }}" data-toggle="tooltip" data-placement="right" title="edit price"><i class="fa fa-fw fa-pencil"></i> Edit Price</a></li>
                    <li>@include('admin.parts.deleteBtn', ['action' => ['Admin\Events\PriceController@destroy', $event->price->id], 'class' => 'btn-xs', 'position' => 'right', 'btnTitle' => "Delete the price"])</li>
                </ul>
            @else
                <a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/events/price/create?id=$event->id") }}" data-toggle="tooltip" data-placement="right" title="define price"><i class="fa fa-fw fa-usd"></i> Price</a>
            @endif
        </div>
    </div>
    <!-- Event Dates -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Dates</h3>
            <a class="btn btn-primary btn-xs aBtn pull-right" href="{{ url("admin/events/meta/create?id=$event->id") }}" data-toggle="tooltip" data-placement="left" title="add dates"><i class="fa fa-fw fa-calendar-plus-o"></i> Add Event Date</a>
        </div><!-- /.box-header -->
        @if($dates->count())
        <div class="box-body no-padding table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Location Name</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($dates as $date)
                    <tr>
                        <th scope="col">{{ $i++ }}</th>
                        <td>{{ $date->date }}</td>
                        <td>{{ $date->time }}</td>
                        <td>{{ $date->location_name }}</td>
                        <td class="text-center">
                            @if($date->location_id)
                                <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/events/meta/location/$date->id/edit") }}" data-toggle="tooltip" data-placement="top" title="edit event location">
                                    <i class="fa fa-fw fa-map-marker"></i></a>
                                @else
                                <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/events/meta/location/create?id=$date->id") }}" data-toggle="tooltip" data-placement="top" title="add event location">
                                    <i class="fa fa-fw fa-map-marker"></i></a>
                            @endif
                        </td>
                        <td class="text-center">
                            <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/events/meta/$date->id/edit") }}" data-toggle="tooltip" data-placement="top" title="edit date & time">
                                <i class="fa fa-fw fa-pencil"></i></a>
                        </td>
                        <td class="text-right">
                            @include('admin.parts.deleteBtn', ['action' => ['Admin\Events\MetaController@destroy', $date->id], 'class' => 'btn-xs', 'btnTitle' => "Delete the date"])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
        @endif
    </div><!-- /End Event Dates -->

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Members <span class="badge">{{ $members->count() }}</span></h3>
            <a class="btn btn-primary btn-xs aBtn pull-right" href="{{ url("admin/events/members/create?id=$event->id") }}" data-toggle="tooltip" data-placement="left" title="add member"><i class="fa fa-fw fa-user-circle-o"></i> Add Members</a>
        </div>
        @if($members->count())
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                @foreach($members as $member)
                <tr>
                    <th scope="col">{{ $ii++ }}</th>
                    <td>{{ $member->full_name }}</td>
                    <td>{{ $member->pivot->title or '---' }}</td>
                    <td class="text-right">
                        @include('admin.parts.deleteBtn', ['action' => ['Admin\Events\MembersController@destroy', $member->pivot->id], 'class' => 'btn-xs', 'btnTitle' => "Delete the member"])
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
        @endif
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection