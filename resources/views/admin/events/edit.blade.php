@extends('layouts.admin')

@section('title')
    Edit new event
@endsection

@section('hTitle')
    Edit Community Church Event
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            Event's information
        </div>
        <!-- /.box-header -->
        {!! Form::model($event, ['method' => 'PATCH', 'action' => ['Admin\Events\IndexController@update', $event->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.events.formBody')
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("admin/events/$event->id")])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection