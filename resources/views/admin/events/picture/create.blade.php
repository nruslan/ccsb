@extends('layouts.admin')

@section('title')
    Add Cover Picture
@endsection

@section('hTitle')
    Add Cover Picture
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">Event: {{ $event->title }}</div><!-- /.box-header -->
    {!! Form::open(['url' => 'admin/events/picture', 'id' => 'myForm', 'files' => true]) !!}
    {!! Form::hidden('event_id', $event->id) !!}
    <div class="box-body">
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <div class="form-group{{ $errors->has('input_file') ? ' has-error has-feedback' : '' }}">
                    <span class="fa fa-fw fa-image"></span> {!! Form::label('input_file', 'Cover Image') !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="required field"></i> Recommended Size 690px by 390px
                    {!! Form::file('input_file',['class' => 'form-control input-lg', 'id' => 'inputFile']) !!}
                    @if ($errors->has('input_file'))
                        <span class="fa fa-exclamation-triangle form-control-feedback" aria-hidden="true"></span>
                        <span class="sr-only">(error)</span>
                        <span class="help-block"><strong>{{ $errors->first('input_file') }}</strong></span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="progress hidden" id="progress">
                    <div class="progress-bar progress-bar-primary progress-bar-striped" id="output" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                        <span id="completed"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">@include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("admin/events/$event->id")])</div>
    {!! Form::close() !!}
</div>
@endsection