@extends('layouts.admin')

@section('title')
    Edit Event Location
@endsection

@section('hTitle')
    Edit Location for Event: {!! $event->title_subtitle !!}
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Date: {{ $eventMeta->date }}</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($eventMeta, ['method' => 'PATCH', 'action' => ['Admin\Events\LocationController@update', $eventMeta->id], 'id' => 'myForm']) !!}

        <div class="box-body">
            @include('admin.events.location.formBody')
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url("admin/events/$event->id")])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection