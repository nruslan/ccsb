<div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
    {!! Form::label('event-location', 'Event Location') !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="required field"></i>
    <div class="row">
        @foreach($locations as $location)
            <div class="col-lg-2">
                <div class="radio">
                    <label>
                        {!! Form::radio('location_id', $location->id) !!}
                        <strong>{{ $location->addressName->name }}</strong><br>
                        {{ $location->address1 }}<br>{{ $location->city }}, {{ $location->state->name }} {{ $location->zip_code }}
                    </label>
                </div>
            </div>
        @endforeach
    </div>
    @if ($errors->has('location_id'))<span class="help-block"><strong>{{ $errors->first('location_id') }}</strong></span>@endif
</div>