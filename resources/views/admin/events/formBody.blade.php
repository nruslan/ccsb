<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title', 'Title') !!} <i class="fa fa-asterisk text-danger" data-toggle="tooltip" data-placement="top" title="field is required "></i>
            {!! Form::text('title', null, ['class' => 'form-control input-lg', 'placeholder' => 'title']) !!}
            @if ($errors->has('title'))
                <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            {!! Form::label('subtitle', 'Subtitle') !!}
            {!! Form::text('subtitle', null, ['class' => 'form-control input-lg', 'placeholder' => 'subtitle']) !!}
        </div>
        <div class="form-group{{ $errors->has('doc_type_id') ? ' has-error' : '' }}">
            {!! Form::label('category_id', 'Event Category') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('category_id', $categories, null, ['class' => 'form-control input-lg']) !!}
            @if ($errors->has('category_id'))<span class="help-block"><strong>{{ $errors->first('category_id') }}</strong></span>@endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('ministry_id') ? ' has-error' : '' }}">
            {!! Form::label('ministry_id', 'Ministries') !!}
            @foreach($ministries as $ministry)
                <div class="radio">
                    <label>
                        {!! Form::checkbox('ministry_id[]', $ministry->id) !!}
                        <strong>{{ $ministry->name }}</strong>
                    </label>
                </div>
            @endforeach
            @if ($errors->has('ministry_id'))<span class="help-block"><strong>{{ $errors->first('ministry_id') }}</strong></span>@endif
        </div>

    </div>
</div>
@include('admin.parts.textarea', ['name' => 'description', 'title' => 'Description'])