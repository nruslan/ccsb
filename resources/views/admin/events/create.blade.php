@extends('layouts.admin')

@section('title')
    Add new event
@endsection

@section('hTitle')
    Add New Community Church Event
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            Event's information
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => 'admin/events', 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.events.formBody')
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            @include('admin.parts.btnSubmitCancel', ['btnName' => 'Save', 'cancelUrl' => url('/admin/events')])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('#myForm').on('submit', function(){
                $('#submitBtn').addClass('disabled').children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection