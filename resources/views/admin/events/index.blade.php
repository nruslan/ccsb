@extends('layouts.admin')

@section('title', "Events")

@section('hTitle')
    Church Events
@endsection

@section('breadcrumbs')
    <li class="active">Events</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <a href="{{ url('admin/events/create') }}" class="btn btn-primary btn-xs aBtn pull-right" data-toggle="tooltip" data-placement="left" title="add new event">
            <i class="fa fa-plus-circle fa-fw"></i> Add</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Category</th>
                <th>Title</th>
                <th>Subtitle</th>
                <th>Date</th>
                <th colspan="2"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($events as $event)
                <tr>
                    <th scope="col">{{ $i++ }}</th>
                    <td>{{ $event->category_title }}</td>
                    <td><a class="btn btn-link btn-xs aBtn" href="{{ url("/admin/events/$event->id") }}" data-toggle="tooltip" data-placement="top" title="view details">
                            <i class="fa fa-fw fa-eye"></i> {{ $event->title }}</a></td>
                    <td>{{ $event->subtitle or '---' }}</td>
                    <td>{{ $event->created_at }}</td>
                    <td>
                        <a class="btn btn-default btn-xs aBtn" href="{{ url("/admin/events/$event->id") }}" data-toggle="tooltip" data-placement="top" title="view details">
                            <i class="fa fa-fw fa-eye"></i></a>
                    </td>
                    <td>
                        <a class="btn btn-primary btn-xs aBtn" href="{{ url("/admin/events/$event->id/edit") }}" data-toggle="tooltip" data-placement="top" title="edit event">
                            <i class="fa fa-fw fa-pencil"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        {{ $events->links() }}
    </div>
</div>
@endsection