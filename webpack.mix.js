let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Admin stylesheets
mix.js('resources/assets/js/admin.js', 'public/js').sass('resources/assets/sass/admin.scss', 'public/css');

// Master Stylesheet
mix.sass('resources/assets/sass/jango-mandatory.scss', 'public/css');
mix.sass('resources/assets/sass/jango-base-plugins.scss', 'public/css');
mix.sass('resources/assets/sass/jango-theme.scss', 'public/css');

mix.js('resources/assets/js/app.js', 'public/js')
//mix.js('resources/assets/js/layout.js', 'public/js')