<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMinistryMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ministry_members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ministry_id', false, true)->nullable();
            $table->integer('member_id', false, true)->nullable();
            $table->date('term_started')->nullable();
            $table->date('term_ended')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ministry_members');
    }
}
