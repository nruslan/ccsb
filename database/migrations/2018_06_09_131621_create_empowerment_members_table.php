<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpowermentMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empowerment_members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empowerment_group_id', false, true)->nullable();
            $table->integer('member_id', false, true)->nullable();
            $table->tinyInteger('leader', false, true)->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empowerment_members');
    }
}
