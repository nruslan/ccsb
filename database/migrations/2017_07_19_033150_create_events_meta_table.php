<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id', false, true)->nullable();
            $table->integer('location_id', false, true)->nullable()->comment('address id');
            $table->dateTime('date_time')->nullable()->comment('events date and time');
            $table->text('comments')->nullable();
            //$table->date('end_date')->nullable()->comment('events end date');
            //$table->integer('interval')->unsigned()->nullable()->comment('recurring interval, UNIX timestamp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events_meta');
    }
}
