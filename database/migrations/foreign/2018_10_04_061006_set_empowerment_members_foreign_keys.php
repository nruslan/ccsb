<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetEmpowermentMembersForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empowerment_members', function (Blueprint $table) {
            $table->foreign('empowerment_group_id')->references('id')->on('empowerment_groups')->onDelete('cascade');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empowerment_members', function (Blueprint $table) {
            $table->dropForeign('empowerment_members_empowerment_group_id_foreign');
            $table->dropForeign('empowerment_members_member_id_foreign');
        });
    }
}
