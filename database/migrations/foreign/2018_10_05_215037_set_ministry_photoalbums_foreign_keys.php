<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetMinistryPhotoalbumsForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ministry_photoalbums', function (Blueprint $table) {
            $table->foreign('ministry_id')->references('id')->on('ministries')->onDelete('cascade');
            $table->foreign('photoalbum_id')->references('id')->on('photoalbums')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ministry_photoalbums', function (Blueprint $table) {
            $table->dropForeign('ministry_photoalbums_ministry_id_foreign');
            $table->dropForeign('ministry_photoalbums_photoalbum_id_foreign');
        });
    }
}
