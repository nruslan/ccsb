<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetPhoneForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phones', function (Blueprint $table) {
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('phone_types')->onDelete('restrict');
            $table->foreign('visibility_id')->references('id')->on('visibilities')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phones', function (Blueprint $table) {
            $table->dropForeign('phones_member_id_foreign');
            $table->dropForeign('phones_address_id_foreign');
            $table->dropForeign('phones_type_id_foreign');
            $table->dropForeign('phones_visibility_id_foreign');
        });
    }
}
