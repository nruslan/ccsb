<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetMinistriesForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ministries', function (Blueprint $table) {
            $table->foreign('ministry_type_id')->references('id')->on('ministry_types')->onDelete('set null');
            $table->foreign('picture_id')->references('id')->on('pictures')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ministries', function (Blueprint $table) {
            $table->dropForeign('ministries_ministry_type_id_foreign');
            $table->dropForeign('ministries_picture_id_foreign');
        });
    }
}
