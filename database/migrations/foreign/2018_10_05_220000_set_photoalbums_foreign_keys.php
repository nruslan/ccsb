<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetPhotoalbumsForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photoalbums', function (Blueprint $table) {
            $table->foreign('location_id')->references('id')->on('addresses')->onDelete('set null');
            $table->foreign('picture_id')->references('id')->on('pictures')->onDelete('set null');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photoalbums', function (Blueprint $table) {
            $table->dropForeign('photoalbums_location_id_foreign');
            $table->dropForeign('photoalbums_picture_id_foreign');
            $table->dropForeign('photoalbums_admin_id_foreign');
        });
    }
}
