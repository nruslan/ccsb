<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetPicturePhotoalbumsForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('picture_photoalbums', function (Blueprint $table) {
            $table->foreign('picture_id')->references('id')->on('pictures')->onDelete('cascade');
            $table->foreign('photoalbum_id')->references('id')->on('photoalbums')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('picture_photoalbums', function (Blueprint $table) {
            $table->dropForeign('picture_photoalbums_picture_id_foreign');
            $table->dropForeign('picture_photoalbums_photoalbum_id_foreign');
        });
    }
}
