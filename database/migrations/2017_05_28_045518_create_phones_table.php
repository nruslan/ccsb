<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('member_id')->unsigned()->nullable();
            $table->integer('address_id')->unsigned()->nullable();
            $table->integer('type_id')->unsigned()->comment('phone type ID');
            $table->integer('country_code')->unsigned()->nullable();
            $table->integer('area_code')->unsigned();
            $table->integer('number')->unsigned();
            $table->integer('ext')->unsigned()->nullable()->comment('phone number extension');
            $table->integer('visibility_id')->unsigned()->comment('phone number visibility');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phones');
    }
}
