<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMinistryLiaisonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ministry_liaisons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ministry_id', false, true)->nullable();
            $table->integer('member_id', false, true)->nullable();
            $table->date('term_started')->nullable();
            $table->date('term_ended')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ministry_liaisons');
    }
}
