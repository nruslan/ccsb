<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id', false, true)->nullable();
            $table->string('title')->nullable()->comment('news title');
            $table->string('subtitle')->nullable();
            $table->string('slug')->unique();
            $table->text('text');
            $table->integer('picture_id', false, true)->nullable()->comment('cover picture for the post');
            $table->dateTime('pub_date_time')->nullable();
            $table->tinyInteger('draft', false, true)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
