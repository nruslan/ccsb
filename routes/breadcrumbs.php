<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

// Home > Media --------------------------------------------------------------------------------------------------------
Breadcrumbs::register('media', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Media', route('media'));
});
// Home > Media > Videos
Breadcrumbs::register('videos', function ($breadcrumbs) {
    $breadcrumbs->parent('media');
    $breadcrumbs->push('Videos', route('media.videos'));
});
// Home > Media > Videos > [Video]
Breadcrumbs::register('video', function ($breadcrumbs, $video) {
    $breadcrumbs->parent('videos');
    $breadcrumbs->push($video->title, route('media.video.show', $video->id));
});
// Home > Media > Videos > Search Result
Breadcrumbs::register('video_search', function ($breadcrumbs) {
    $breadcrumbs->parent('videos');
    $breadcrumbs->push('Search Video', route('media.video.search'));
});
// Home > Videos > [Category]
Breadcrumbs::register('category', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('videos');
    $breadcrumbs->push($category->title, route('media.videos.category', $category->id));
});
// Home > Videos > [Category] > [Video]
Breadcrumbs::register('show_video', function ($breadcrumbs, $video) {
    $breadcrumbs->parent('category', $video['category']);
    $breadcrumbs->push($video->title, route('videos.category.show', $video));
});
// Home > Media > Videos > [By Pastors]
Breadcrumbs::register('pastor', function ($breadcrumbs, $pastor) {
    $breadcrumbs->parent('videos');
    $breadcrumbs->push($pastor->full_name, route('media.videos.pastor', $pastor->id));
});
// Home > Media > Sermons ----------------------------------------------------------------------------------------------
Breadcrumbs::register('sermons', function ($breadcrumbs) {
    $breadcrumbs->parent('media');
    $breadcrumbs->push('Sermons', route('media.sermons'));
});
// Home > Media > Sermons > [Sermon]
Breadcrumbs::register('sermon', function ($breadcrumbs, $sermon) {
    $breadcrumbs->parent('sermons');
    $breadcrumbs->push($sermon->title, route('media.sermon.show', $sermon->id));
});
// Home > Media > Sermons > [By Pastors]
Breadcrumbs::register('sermons_pastor', function ($breadcrumbs, $pastor) {
    $breadcrumbs->parent('sermons');
    $breadcrumbs->push($pastor->full_name, route('media.sermons.pastor', $pastor->id));
});
// Home > Media > Sermons > Search Result
Breadcrumbs::register('sermon_search', function ($breadcrumbs) {
    $breadcrumbs->parent('sermons');
    $breadcrumbs->push('Search Sermon', route('media.sermon.search'));
});

// Home > Happenings ---------------------------------------------------------------------------------------------------
Breadcrumbs::register('happenings', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Church Happenings', route('happenings'));
});
// Home > Happenings > News
Breadcrumbs::register('news', function ($breadcrumbs) {
    $breadcrumbs->parent('happenings');
    $breadcrumbs->push('News', route('happenings.news.index'));
});
// Home > Happenings > News > [Show]
Breadcrumbs::register('news_show', function ($breadcrumbs, $newsPost) {
    $breadcrumbs->parent('news');
    $breadcrumbs->push($newsPost->title, route('happenings.news.show', $newsPost->slug));
});
// Home > Happenings > Events
Breadcrumbs::register('events', function ($breadcrumbs) {
    $breadcrumbs->parent('happenings');
    $breadcrumbs->push('Events', route('happenings.events.index'));
});
// Home > Happenings > Events > [Show]
Breadcrumbs::register('event_show', function ($breadcrumbs, $event) {
    $breadcrumbs->parent('events');
    $breadcrumbs->push($event->title, route('happenings.event.show', $event->slug));
});
// Home > Happenings > Calendar
Breadcrumbs::register('calendar', function ($breadcrumbs) {
    $breadcrumbs->parent('happenings');
    $breadcrumbs->push('Calendar', route('happenings.calendar'));
});

// Home > About --------------------------------------------------------------------------------------------------------
Breadcrumbs::register('about', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('About', route('about'));
});
// Home > About > Church
Breadcrumbs::register('church', function ($breadcrumbs) {
    $breadcrumbs->parent('about');
    $breadcrumbs->push('Church', route('about.church'));
});
// Home > About > Staff
Breadcrumbs::register('staff', function ($breadcrumbs) {
    $breadcrumbs->parent('about');
    $breadcrumbs->push('Staff', route('about.staff'));
});
// Home > About > Staff > [Member]
Breadcrumbs::register('staff_member', function ($breadcrumbs, $member) {
    $breadcrumbs->parent('staff');
    $breadcrumbs->push($member->full_name, route('about.staff.show', $member->slug));
});
// Home > About > Leaders
Breadcrumbs::register('leaders', function ($breadcrumbs) {
    $breadcrumbs->parent('about');
    $breadcrumbs->push('Leaders', route('about.leaders'));
});
// Home > About > Leaders > [Member]
Breadcrumbs::register('leaders_member', function ($breadcrumbs, $member) {
    $breadcrumbs->parent('leaders');
    $breadcrumbs->push($member->full_name, route('about.leaders.show', $member->slug));
});
// Home > About > Committees
Breadcrumbs::register('committees', function ($breadcrumbs) {
    $breadcrumbs->parent('about');
    $breadcrumbs->push('Committees', route('about.committees'));
});
// Home > About > Committees > [Committee]
Breadcrumbs::register('committee', function ($breadcrumbs, $committee) {
    $breadcrumbs->parent('committees');
    $breadcrumbs->push($committee->name, route('about.committees', $committee->slug));
});
// Home > About > Bylaws
Breadcrumbs::register('bylaws', function ($breadcrumbs) {
    $breadcrumbs->parent('about');
    $breadcrumbs->push('Bylaws', route('about.bylaws'));
});
// Home > About > Contact us
Breadcrumbs::register('contact', function ($breadcrumbs) {
    $breadcrumbs->parent('about');
    $breadcrumbs->push('Contact Us', route('about.contact'));
});

// Home > Ministries ---------------------------------------------------------------------------------------------------
Breadcrumbs::register('ministries', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Ministries', route('ministries'));
});
// Home > Ministries > Missions
Breadcrumbs::register('missions', function ($breadcrumbs) {
    $breadcrumbs->parent('ministries');
    $breadcrumbs->push('Missions Ministry', route('ministries.missions'));
});
// Home > Ministries > Missions > [Supported Ministry]
Breadcrumbs::register('supported_ministry', function ($breadcrumbs, $ministry) {
    $breadcrumbs->parent('missions');
    $breadcrumbs->push($ministry->title, route('ministries.missions.ministry', $ministry->slug));
});
// Home > Ministries > Missions > [Member]
Breadcrumbs::register('missions_member', function ($breadcrumbs, $member) {
    $breadcrumbs->parent('missions');
    $breadcrumbs->push($member->full_name, route('ministries.missions.member', $member->slug));
});

// Home > Ministries > Men's
Breadcrumbs::register('mens', function ($breadcrumbs) {
    $breadcrumbs->parent('ministries');
    $breadcrumbs->push('Men\'s Ministry', route('ministries.mens'));
});
// Home > Ministries > Women's
Breadcrumbs::register('womens', function ($breadcrumbs) {
    $breadcrumbs->parent('ministries');
    $breadcrumbs->push('Women\'s Ministry', route('ministries.womens'));
});
Breadcrumbs::register('womens_ministry', function ($breadcrumbs, $ministry) {
    $breadcrumbs->parent('womens');
    $breadcrumbs->push($ministry->name, route('ministries.womens.show', $ministry->slug));
});
// Home > Ministries > Care
Breadcrumbs::register('care', function ($breadcrumbs) {
    $breadcrumbs->parent('ministries');
    $breadcrumbs->push('Care Ministry', route('ministries.care'));
});
// Home > Ministries > Care > [Supported Ministry]
Breadcrumbs::register('care_supported_ministry', function ($breadcrumbs, $ministry) {
    $breadcrumbs->parent('care');
    $breadcrumbs->push($ministry->title, route('ministries.care.ministry', $ministry->slug));
});

// Home > Give ---------------------------------------------------------------------------------------------------------
Breadcrumbs::register('give', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Give', route('give'));
});

// Home > Photoalbums --------------------------------------------------------------------------------------------------
Breadcrumbs::register('photoalbums', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Photo Albums', route('photoalbums'));
});
// Home > Photoalbums > Photoalbum
Breadcrumbs::register('photoalbum', function ($breadcrumbs, $album) {
    $breadcrumbs->parent('photoalbums');
    $breadcrumbs->push($album->title, route('photoalbums.show', $album->slug));
});

// Home > Directory ----------------------------------------------------------------------------------------------------
Breadcrumbs::register('directory', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Directory', route('directory'));
});
Breadcrumbs::register('show-member', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('directory');
    $breadcrumbs->push("Member $user->username", route('directory.member', $user->id));
});

// Home > Profile ------------------------------------------------------------------------------------------------------
Breadcrumbs::register('profile', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("$user->username's Profile", route('profile'));
});

// Home > Login --------------------------------------------------------------------------------------------------------
Breadcrumbs::register('login', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Login', route('login'));
});

Breadcrumbs::register('reset-password', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Reset Password', route('password.request'));
});



























