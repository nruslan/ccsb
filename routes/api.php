<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('sermon/search', 'Admin\Sermons\SearchController@get_sermon');
Route::post('video/search', 'Admin\Video\SearchController@get_video');
Route::post('/upload', 'Admin\Sermons\FileController@upload');
Route::post('user/search', 'Admin\Accounts\UserController@find_user');
Route::post('member/search', 'Admin\Accounts\MemberController@find_member');
Route::post('/photoalbum/pictures/upload', 'Admin\Sermons\FileController@upload');
Route::post('/subscribe', 'Master\ContactUsController@subscribe');
Route::post('/about/contact', 'Master\ContactUsController@contact');

Route::post('/member/message', 'Master\Directory\IndexController@message');
