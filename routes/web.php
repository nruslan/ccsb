<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', function () {
    return redirect(route('home'));
});

Route::get('/', 'Master\Home\IndexController@index')->name('home');

Route::get('/phpinfo', 'Master\Home\IndexController@pinfo');

Route::group(['prefix' => 'media'], function () {
    Route::get('/', 'Master\Media\IndexController@index')->name('media');
    Route::group(['prefix' => 'videos'], function () {
        Route::get('/', 'Master\Media\VideoController@index')->name('media.videos');
        Route::get('/category/{category_slug}', 'Master\Media\VideoCategoriesController@category')->name('media.videos.category');
        Route::get('/category/{category_slug}/{video_slug}', 'Master\Media\VideoCategoriesController@category_video')->name('media.videos.category.video');
        Route::get('/pastor/{name}', 'Master\Media\VideoCategoriesController@pastor')->name('media.videos.pastor');
        Route::get('/pastor/{name}/{video_slug}', 'Master\Media\VideoCategoriesController@pastor_video')->name('media.videos.pastor.show');
        Route::get('/search', 'Master\Media\VideoController@search')->name('media.video.search');
        Route::get('/{slug}', 'Master\Media\VideoController@show')->name('media.video.show');
    });

    Route::group(['prefix' => 'sermons'], function () {
        Route::get('/', 'Master\Media\SermonController@index')->name('media.sermons');
        Route::get('/pastor/{name}', 'Master\Media\SermonCategoriesController@pastor')->name('media.sermons.pastor');
        Route::get('/pastor/{name}/{sermon_slug}', 'Master\Media\SermonCategoriesController@pastor_sermon')->name('media.sermons.pastor.show');
        Route::get('/search', 'Master\Media\SermonController@search')->name('media.sermon.search');
        Route::get('/{slug}', 'Master\Media\SermonController@show')->name('media.sermon.show');
    });
});

Route::group(['prefix' => 'about'], function () {
    Route::get('/', 'Master\AboutController@index')->name('about');
    Route::get('/church', 'Master\AboutController@church')->name('about.church');
    Route::get('/staff', 'Master\AboutController@staff')->name('about.staff');
    Route::get('/staff/{slug}', 'Master\AboutController@show')->name('about.staff.show');
    Route::get('/leaders', 'Master\AboutController@leaders')->name('about.leaders');
    Route::get('/leaders/{slug}', 'Master\AboutController@show')->name('about.leaders.show');
    Route::get('/bylaws', 'Master\AboutController@doc')->name('about.bylaws');
    Route::get('/contact-us', 'Master\ContactUsController@index')->name('about.contact');

    Route::group(['prefix' => 'committees'], function () {
        Route::get('/', 'Master\Committees\IndexController@index')->name('about.committees');
        Route::get('/{slug}', 'Master\Committees\IndexController@show')->name('about.committees.show');
    });
});

// Old Routes
Route::group(['prefix' => 'current'], function () {
    Route::get('/news', function () { return redirect(route('happenings.news_events')); });
    Route::get('/events', function () { return redirect(route('happenings.news_events')); });
    Route::get('/calendar', function () { return redirect(route('happenings.calendar')); });
});

Route::group(['prefix' => 'happenings'], function () {
    Route::get('/', 'Master\Happenings\IndexController@index')->name('happenings');
    //NEWS
    Route::get('news', 'Master\Happenings\NewsController@index')->name('happenings.news.index');
    Route::get('news/{slug}', 'Master\Happenings\NewsController@show')->name('happenings.news.show');
    //EVENTS
    Route::get('events', 'Master\Happenings\EventsController@index')->name('happenings.events.index');
    Route::get('events/{slug}', 'Master\Happenings\EventsController@show')->name('happenings.event.show');
    //CALENDAR
    Route::get('calendar', 'Master\Happenings\IndexController@calendar')->name('happenings.calendar');
});

// Old Routes
Route::group(['prefix' => 'ministry'], function () {
    Route::get('/missions', function () { return redirect(route('ministries.missions')); });
    Route::get('/mans', function () { return redirect(route('ministries.mens')); });
    Route::get('/womans', function () { return redirect(route('ministries.womens')); });
    Route::get('/care', function () { return redirect(route('ministries.care')); });
});

Route::group(['prefix' => 'ministries'], function () {
    Route::get('/', 'Master\Ministries\IndexController@index')->name('ministries');
    Route::get('/missions/member/{slug}', 'Master\Ministries\MissionsController@member')->name('ministries.missions.member');
    Route::get('/missions/{slug}', 'Master\Ministries\MissionsController@ministry')->name('ministries.missions.ministry');
    Route::get('/missions', 'Master\Ministries\MissionsController@index')->name('ministries.missions');
    Route::get('/mens', 'Master\Ministries\MensController@index')->name('ministries.mens');
    Route::get('/women', 'Master\Ministries\WomensController@index')->name('ministries.womens');
    Route::get('/women/{slug}', 'Master\Ministries\WomensController@show')->name('ministries.womens.show');
    Route::get('/care/{slug}', 'Master\Ministries\CareController@ministry')->name('ministries.care.ministry');
    Route::get('/care', 'Master\Ministries\CareController@index')->name('ministries.care');
});
Route::get('/give', 'Master\GiveController@index')->name('give');
Route::get('/photoalbums/{slug}', 'Master\PhotoalbumsController@show')->name('photoalbums.show');
Route::get('/photoalbums', 'Master\PhotoalbumsController@index')->name('photoalbums');


Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/directory', 'Master\Directory\IndexController@index')->name('directory');
    Route::get('/directory/member/{id}', 'Master\Directory\IndexController@show')->name('directory.member');

    Route::get('profile', 'Master\User\IndexController@index')->name('profile');
});

//Admin routes----------------------------------------------------------------------------------------------------------
Route::group(['prefix' => 'admin'], function(){

    Route::get('check', function(){
        return new App\Mail\AdministratorPasswordChanged(App\Admin::find(1), '123456');
    });

    Route::group(['middleware' => ['guest:admin']], function () {
        Route::get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
        Route::post('/login', 'Admin\Auth\LoginController@login')->name('admin.login.submit');
        //Reset Admin Password
        Route::post('/password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
        Route::get('/password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
        Route::post('/password/reset', 'Admin\Auth\ResetPasswordController@reset')->name('admin.password.update');
        Route::get('/password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
    });

    Route::group(['middleware' => ['auth:admin', 'pchecker']], function () {
        Route::post('/logout', 'Admin\Auth\LoginController@logout')->name('admin.logout');
        Route::get('/', 'Admin\Dashboard\IndexController@index')->name('admin.dashboard');
        Route::get('/dashboard', function () { return redirect('/admin');});

        //Search for member
        Route::post('/search/member/', 'Admin\SearchController@getMember');
        //Documents
        Route::resource('/documents', 'Admin\Docs\IndexController');
        //Committees
        Route::resource('/committees', 'Admin\Committees\IndexController');
        Route::resource('/committees/member', 'Admin\Committees\MembersController');
        //Ministries
        Route::resource('/ministries', 'Admin\Ministries\IndexController');
        Route::resource('/ministries/member', 'Admin\Ministries\MembersController', ['except' =>['index', 'show']]);
        Route::resource('/ministries/liaison', 'Admin\Ministries\LiaisonsController', ['except' =>['index', 'show']]);
        //Empowerment
        Route::resource('groups/{id}/member', 'Admin\Empowerment\MembersController', ['except' =>['index', 'show', 'edit']]);
        Route::resource('groups', 'Admin\Empowerment\GroupsController');

        //Videos
        Route::resource('/videos', 'Admin\Video\IndexController');
        //Events
        Route::group(['prefix' => 'events'], function () {
            Route::resource('/meta/location', 'Admin\Events\LocationController', ['except' => ['index', 'show']]);
            Route::resource('/meta', 'Admin\Events\MetaController', ['except' => ['show']]);
            Route::resource('/members', 'Admin\Events\MembersController', ['only' => ['create', 'store', 'destroy']]);
            Route::resource('/price', 'Admin\Events\PriceController', ['except' => ['index', 'show']]);
            Route::resource('/picture', 'Admin\Events\PictureController', ['only' =>['create', 'store', 'destroy']]);
        });
        Route::resource('/events', 'Admin\Events\IndexController');
        //News
        Route::resource('/news/picture', 'Admin\News\PictureController', ['only' =>['create', 'store', 'destroy']]);
        Route::resource('/news', 'Admin\News\IndexController');
        //Sermons
        Route::resource('/sermons', 'Admin\Sermons\IndexController');
        Route::group(['prefix' => 'sermons'], function () {
            Route::post('/search/related', 'Admin\Sermons\SearchController@related');
            Route::resource('/file', 'Admin\Sermons\FileController');
            Route::resource('/relation', 'Admin\Sermons\RelationController', ['only' =>['edit', 'update', 'destroy']]);
        });
        //Photoalbum
        Route::resource('/photoalbums', 'Admin\Photoalbum\IndexController');
        Route::group(['prefix' => 'photoalbums'], function () {
            Route::resource('/pictures', 'Admin\Photoalbum\PictureController');
        });
        //Picture
        //Route::resource('/pictures', 'Admin\Pictures\IndexController');
        //Route::group(['prefix' => 'pictures'], function () {
            //Route::post('/search/related', 'Admin\Sermons\SearchController@related');
        //});

        //Accounts
        Route::group(['prefix' => 'accounts'], function () {

            Route::get('/', function () { return redirect('/admin/accounts/users');});

            Route::resource('/admins/password', 'Admin\Accounts\AdminsPasswordController', ['only' =>['edit', 'update']]);
            Route::resource('/admins', 'Admin\Accounts\AdminsController');

            Route::match(['get', 'post'], '/users/{id}/verify', 'Admin\Accounts\UserController@verifyUser');
            Route::post('/users/{id}/reset-password', 'Admin\Accounts\IndexController@resetPassword');

            Route::resource('/users', 'Admin\Accounts\UserController');
            Route::group(['prefix' => 'users'], function () {
                Route::resource('/picture', 'Admin\Accounts\UserPictureController');
                Route::resource('/address', 'Admin\Accounts\AddressController');
                Route::resource('/phone', 'Admin\Accounts\HomePhoneController');
                Route::resource('/story', 'Admin\Accounts\UserStoryController', ['except' =>['index', 'show']]);
            });

            Route::resource('/members', 'Admin\Accounts\MemberController');
            Route::group(['prefix' => 'members'], function () {
                Route::resource('/picture', 'Admin\Accounts\MemberPictureController');
                Route::resource('/phone', 'Admin\Accounts\PhoneController');
                Route::resource('/email', 'Admin\Accounts\EmailController');
                Route::resource('/title', 'Admin\Accounts\MembersTitleController');
                Route::resource('/story', 'Admin\Accounts\MemberStoryController', ['except' =>['index', 'show']]);
                Route::post('/{id}/undo', 'Admin\Accounts\MemberController@undo');
            });

            //Route::resource('/members/email', 'Admin\Accounts\EmailController');


            //Route::get('/accounts/members', 'Admin\Dashboard\IndexController@index');
        });
    });
});
