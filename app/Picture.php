<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;


class Picture extends Model
{
    //use SoftDeletes;
    //protected $dates = ['deleted_at'];

    protected $fillable = [
        'filename', 'alt', 'description', 'location'
    ];

    public function photoalbums()
    {
        return $this->belongsToMany('App\Photoalbum');
    }

    public function setFilenameAttribute($value)
    {
        $this->attributes['filename'] = strtolower($value);
    }
}
