<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpowermentMember extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'empowerment_group_id', 'member_id', 'leader'
    ];

    //DB Relationships -------------------------------------------------------------------------------------------------
    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }
}
