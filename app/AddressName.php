<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressName extends Model
{
    public function address()
    {
        return $this->belongsTo('App\Address', 'id', 'address_id');
    }
}
