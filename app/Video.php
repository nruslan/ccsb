<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_type_id', 'parent_id', 'title', 'description', 'slug', 'filename', 'date'
    ];

    // DB Relationship -------------------------------------------------------------------------------------------------
    public function members()
    {
        return $this->belongsToMany('App\Member', 'member_videos', 'video_id', 'member_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'video_categories', 'video_id', 'category_id');
    }

    public function type()
    {
        return $this->belongsTo('App\VideoType', 'video_type_id');
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = date("Y-m-d", strtotime($value));
    }

    public function setSlugAttribute($value)
    {
        $shuffleString = substr(str_shuffle(rand().'qwertyuiopasdfghjklzxcvbnm'.rand()),0,5);

        $this->attributes['slug'] = str_slug($value, '-').'-'.$shuffleString;
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = title_case($value);
    }

    public function setFilenameAttribute($value)
    {
        $needle = '=';
        $result = strpos($value, $needle);

        if($result === false) {
            $uri = explode('/',trim($value));

            if(count($uri) == 1) {
                $this->attributes['filename'] = $uri[0];
            } else {
                $this->attributes['filename'] = $uri[3];
            }
        } else {
            $uri = explode($needle,trim($value));

            $ytUri = strstr($uri[1], '&', true);

            if($ytUri === false) {
                $this->attributes['filename'] = $uri[1];
            } else {
                $this->attributes['filename'] = $ytUri;
            }
        }
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getDateAttribute($value)
    {
        return date("m/d/Y", strtotime($value));
    }

    //<--- Delete
    public function getVideoMembersAttribute()
    {
        $members = null;
        foreach($this->members as $member) {
            $members .= $member->full_name . ', ';
        }
        return substr($members, 0, -2);
    }//<--- Delete

    public function getTextDateAttribute()
    {
        return date("l, F d Y", strtotime($this->date));
    }

    public function getMdyDateAttribute()
    {
        return date("F d Y", strtotime($this->date));
    }

    public function getVideoUrlAttribute()
    {
        return $this->type->embed_url.$this->filename;
    }

    public function getImageVideoUrlAttribute()
    {
        $filename = $this->filename;
        return "http://img.youtube.com/vi/$filename/0.jpg";
    }

    public function getShortDescriptionAttribute()
    {
        return str_limit(strip_tags($this->description), 97);
    }

    public function getShortTitleAttribute()
    {
        $limit = 25;
        if(strlen($this->title) > $limit)
            return str_limit($this->title, $limit);
        else
            return $this->title;
    }

    public function getStarringMembersAttribute()
    {
        $names = null;
        foreach($this->members as $member){
            $names .= $member->full_name.' &amp; ';
        }

        return substr($names, 0, -7);
    }

    public function getStarringMemberSlugAttribute()
    {
        if($this->members->count())
            return $this->members->first()->slug;
        else
            return '#';
    }

    public function getRelatedCategoriesAttribute()
    {
        $names = null;
        foreach($this->categories as $category){
            $names .= "$category->title, ";
        }
        return substr($names, 0, -2);
    }

    // Custom Master Navigation
    public function getNextVideoAttribute()
    {
        $video = Video::where('id', '>', $this->id)
            ->where('id', '!=', 1)
            ->orderBy('id','asc')
            ->first();

        return $video ? url("/media/videos/$video->slug") : null;
    }
    public  function getPreviousVideoAttribute()
    {
        $video = Video::where('id', '<', $this->id)
            ->where('id', '!=', 1)
            ->orderBy('id','desc')
            ->first();

        return $video ? url("/media/videos/$video->slug") : null;
    }

    public function getYoutubeVideoViewsCountAttribute()
    {
        /*$api_key = 'AIzaSyCg0punbe6hMfpyCAr5rrKdkhY5phlaayU'; // Unencripted key
        $videoID = $this->filename; // view id here
        $json = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=statistics&id=$videoID&key=$api_key");
        $jsonData = json_decode($json);*/
        $views = 0;//$jsonData->items[0]->statistics->viewCount;
        return number_format($views);
    }

    // Custom
    public function next()
    {
        return Video::where('id', '>', $this->id)->orderBy('id','asc')->first();
    }
    public  function previous()
    {
        return Video::where('id', '<', $this->id)->orderBy('id','desc')->first();
    }
}
