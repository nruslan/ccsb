<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    /**
     * Get Title with parent title
     * @return mixed|string
     */
    public function getFullTitleAttribute()
    {
        if($this->parent_id)
            return $this->title.' - '.Title::find($this->parent_id)->title;
        else
            if($this->description)
                return "$this->title - $this->description";
            else
                return $this->title;

    }

    public function members()
    {
        return $this->belongsToMany('App\Member', 'member_titles')->withPivot('term_started', 'term_ended');
    }

    public function active_members()
    {
        return $this->belongsToMany('App\Member', 'member_titles')->wherePivot('term_ended', null);
    }

    public function parent()
    {
        return $this->belongsTo('App\Title', 'parent_id');
    }
}
