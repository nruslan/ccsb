<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Photoalbum extends Model
{
    //use SoftDeletes;
    //protected $dates = ['deleted_at'];

    protected $fillable = [
        'title', 'description', 'date', 'location_id', 'admin_id', 'picture_id', 'folder_name', 'slug'
    ];

    public function locationName()
    {
        return $this->belongsTo('App\AddressName', 'location_id', 'address_id');
    }

    public function pictures()
    {
        return $this->belongsToMany('App\Picture', 'picture_photoalbums', 'photoalbum_id', 'picture_id');
    }

    public function picture()
    {
        return $this->belongsTo('App\Picture','picture_id');
    }

    public function ministries()
    {
        return $this->belongsToMany('App\Ministry', 'ministry_photoalbums', 'photoalbum_id', 'ministry_id');
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = date("Y-m-d", strtotime($value));
    }

    public function setSlugAttribute($value)
    {
        $shuffleString = substr(str_shuffle(rand().'qwertyuiopasdfghjklzxcvbnm'.rand()),0,5);
        $this->attributes['slug'] = str_slug($value, '-').'-'.$shuffleString;
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getDateAttribute($value)
    {
        return date("m/d/Y", strtotime($value));
    }

    public function getYearAttribute()
    {
        return date("Y", strtotime($this->date));
    }

    public function getAlbumUrlAttribute()
    {
        $year = date('Y', strtotime($this->date));
        $folder = $this->folder_name;

        return "storage/photoalbums/$year/$folder/";
    }

    public function getUrlAttribute()
    {
        return url("photoalbums/$this->slug");
    }

    public function getAlbumThumbnailUriAttribute()
    {
        if($this->picture_id)
            return asset($this->album_url.'thumbnails/'.$this->picture->filename);
        else
            return asset('storage/photoalbums/blank.png');
    }
}
