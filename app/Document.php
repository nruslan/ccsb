<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'user_id', 'doc_type_id', 'parent_id', 'title', 'description', 'filename', 'size', 'date'
    ];

    //DB Relationships -------------------------------------------------------------------------------------------------
    public function adminUser()
    {
        return $this->belongsTo('App\Admin', 'user_id');
    }

    public function docType()
    {
        return $this->belongsTo('App\DocType', 'doc_type_id');
    }

    //Mutators ---------------------------------------------------------------------------------------------------------
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = date("Y-m-d", strtotime($value));
    }


    // Accessors -------------------------------------------------------------------------------------------------------
    public function getTypeAttribute()
    {
        return $this->docType->name;
    }

    public function getUploadedByAttribute()
    {
        return $this->adminUser->name;
    }

    public function getDateMonthYearAttribute()
    {
        return date('F Y', strtotime($this->date));
    }

    public function getYearAttribute()
    {
        return date('Y', strtotime($this->date));
    }

    public function getDateAttribute($value)
    {
        return date('m/d/Y', strtotime($value));
    }
}
