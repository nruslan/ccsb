<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Phone extends Model
{
    //use SoftDeletes;

    //protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id', 'address_id', 'type_id', 'county_code', 'area_code', 'number', 'ext', 'visibility_id', 'deleted_at'
    ];

    public function address()
    {
        return $this->belongsTo('App\Address', 'address_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function numberType()
    {
        return $this->belongsTo('App\PhoneType', 'type_id');
    }

    public function visibility()
    {
        return $this->belongsTo('App\Visibility', 'visibility_id');
    }

    // Accessors
    public function getFullNumberAttribute()
    {
        $partA = substr($this->number, 0, 3);
        $partB = substr($this->number, -4);
        return "($this->area_code) $partA-$partB";
    }
}
