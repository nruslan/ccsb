<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventMeta extends Model
{
    protected $table = 'events_meta';

    protected $fillable = [
        'event_id', 'location_id', 'date_time', 'comments'
    ];

    public function data()
    {
        return $this->belongsTo('App\Event', 'event_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Address', 'location_id');
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setDateTimeAttribute($value)
    {
        $this->attributes['date_time'] = date("Y-m-d H:i:s", strtotime($value));
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getDateTimeAttribute($value)
    {
        return date("m/d/y g:i A", strtotime($value));
    }

    public function getDateAttribute()
    {
        return date("l, F d, Y", strtotime($this->date_time));
    }

    public function getTimeAttribute()
    {
        return date("g:i A", strtotime($this->date_time));
    }

    public function getLocationNameAttribute()
    {
        if($this->location_id)
            return $this->location->addressName->name;
        else
            return "-TBD-";
    }
}
