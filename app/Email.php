<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id', 'address', 'visibility_id', 'verified', 'deleted_at'
    ];

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    /**
     * Get email visibility mode
     */
    public function visibility()
    {
        return $this->belongsTo('App\Visibility', 'visibility_id');
    }
}
