<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPrice extends Model
{
    protected $fillable = [
        'event_id', 'name', 'price'
    ];

    public function event()
    {
        return $this->belongsTo('App\Event', 'event_id');
    }
}
