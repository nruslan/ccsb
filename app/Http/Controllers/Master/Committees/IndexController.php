<?php

namespace App\Http\Controllers\Master\Committees;

use App\Committee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $committees = Committee::orderBy('name')->get();
        $i = 1;
        return view('master.about.committees.index', compact('committees', 'i'));
    }

    public function show($slug)
    {
        $committee = Committee::where('slug', $slug)->first();
        $i = 1;
        return view('master.about.committees.show', compact('committee', 'i'));
    }
}
