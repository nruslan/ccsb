<?php

namespace App\Http\Controllers\Master\Directory;

use App\Mail\SendMessageToMember;
use App\Member;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $users = User::orderBy('username')
            ->paginate(12);
        $i = 1;
        return view('master.directory.index', compact('users', 'i'));
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        $i = 1;
        $addresses = $user->addresses->where('address_type_id', 1)->where('visibility_id', 1);

        $currentUserEmail = request()->user()->email;
        $currentUserName = request()->user()->members_pretty_name;

        return view('master.directory.show', compact('user', 'i', 'addresses', 'currentUserEmail', 'currentUserName'));
    }

    public function message(Request $request)
    {
        // Email To Data
        $message = $request->message;
        $member = Member::findOrFail($request->id);
        $name = $member->full_name;
        $email = $member->emails->count() > 0 ? $member->emails->first()['address'] : $member->userAccount->email;
        // From Data
        $fromEmail = $request->from_email;
        $fromName = $request->from_name;
        // Sending Email
        Mail::to($email)->send(new SendMessageToMember($name, $email, $message, $fromEmail, $fromName));
        // Response
        $data = view('master.directory.member.message')->render();
        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
