<?php

namespace App\Http\Controllers\Master\Home;

use App\Category;
use App\News;
use App\Sermon;
use App\User;
use App\Video;
use App\PicturePhotoalbum;

use Carbon\Carbon;
use App\Event;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $welcomingVideo = Video::find(76);

        $weeklyVideo = Category::find(2)
            ->videos()
            ->orderBy('date', 'desc')
            ->first();

        $i = 1;
        $latestSermons = Sermon::orderBy('date', 'desc')->limit(3)->get();

        $latestPics = PicturePhotoalbum::inRandomOrder()->limit(6)->get();

        $users = User::whereNull('deleted_at')
            ->where('verified', 'yes')
            ->orderBy('since', 'desc')
            ->limit(12)->get();

        // Latest news
        $currentDate = Carbon::now();
        $news = News::orderBy('pub_date_time', 'desc')
            ->where('pub_date_time', '<', $currentDate)
            ->whereNull('draft')
            ->limit(5)
            ->get();


        //$startDateOfThisMonth = new Carbon('first day of this month');
        //$endDateOfThisMonth  = new Carbon('last day of this month');
        //$events = Event::get($startDateOfThisMonth, $endDateOfThisMonth);

        return view('master.home.index', compact(
            'welcomingVideo',
            'weeklyVideo',
            'latestSermons',
            'i',
            'users',
            'news',
            'latestPics'));
    }

    public function pinfo()
    {
        return view('master.home.phpinfo');
    }
}
