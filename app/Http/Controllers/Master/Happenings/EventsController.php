<?php

namespace App\Http\Controllers\Master\Happenings;

use App\Event;
use App\EventMeta;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EventsController extends IndexController
{
    private $view_template;

    public function __construct()
    {
        parent::__construct();
        $this->view_template = "$this->template.events";
    }

    public function index()
    {
        $currentDate = Carbon::now();
        $today = Carbon::today();
        $start = Carbon::now();
        $end = $next4weeks = Carbon::now()->addWeeks(10);

        /*
        $events = DB::table('events_meta')
            ->join('events', 'events_meta.event_id', '=', 'events.id')
            ->whereBetween('date_time', [$start, $end])
            //->whereRaw("($today - events_meta.date) % interval = 0")
            ->paginate();
        */

        $events = EventMeta::whereBetween('date_time', [$currentDate, $end])
            ->orderBy('date_time')
            ->paginate(5);

        return view("$this->view_template.index", compact('events', 'start', 'end'));
    }

    public function show($slug)
    {
        $event = Event::where('slug', $slug)->first();

        $dates = $event->dates()
            ->where('date_time', '>', Carbon::now())
            ->orderBy('date_time')
            ->get();

        return view("$this->view_template.show", compact('event', 'dates'));
    }
}
