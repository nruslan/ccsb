<?php

namespace App\Http\Controllers\Master\Happenings;

use App\News;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NewsController extends IndexController
{
    private $view_template;

    public function __construct()
    {
        parent::__construct();
        $this->view_template = "$this->template.news";
    }

    public function index()
    {
        $currentDate = Carbon::now();
        $news = News::orderBy('pub_date_time', 'desc')
            ->where('pub_date_time', '<', $currentDate)
            ->paginate(5);

        return view("$this->view_template.index", compact('news'));
    }

    public function show($slug)
    {
        $newsPost = News::where('slug', $slug)->first();
        return view("$this->view_template.show", compact('newsPost'));
    }
}
