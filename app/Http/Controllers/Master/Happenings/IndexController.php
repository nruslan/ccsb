<?php

namespace App\Http\Controllers\Master\Happenings;

use App\EventMeta;
use App\News;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    protected $template;

    public function __construct()
    {
        $this->template = "master.happenings";
    }

    public function index()
    {
        $currentDate = Carbon::now();
        $next4weeks = Carbon::now()->addWeeks(10);

        $news = News::where('pub_date_time', '<', $currentDate)
            ->orderBy('pub_date_time', 'desc')
            ->limit(5)->get();

        $events = EventMeta::whereBetween('date_time', [$currentDate, $next4weeks])
            ->orderBy('date_time')
            ->limit(5)
            ->get();

        return view("$this->template.index", compact('news', 'events'));
    }

    public function calendar()
    {
        return view("$this->template.calendar");
    }
}
