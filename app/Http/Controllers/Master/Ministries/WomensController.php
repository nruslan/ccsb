<?php

namespace App\Http\Controllers\Master\Ministries;

use App\Ministry;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class WomensController extends Controller
{
    public function index()
    {
        $currentDate = Carbon::now();
        $ministry = Ministry::findOrFail(3);
        $members = $ministry->active_members;
        $photoalbums = $ministry->photoalbums;
        $news = $ministry->news()
            ->orderBy('pub_date_time', 'desc')
            ->limit(5)
            ->get();

        // Bible Studies Events
        $bibleStudies = $ministry->events->where('category_id', 7);
        // Retreats Events
        $retreats = $ministry->events->where('category_id', 8);

        $_events = DB::table('events_meta')->where('date_time', '>', $currentDate)
            ->orderBy('date_time')
            //->join('ministries', 'events_meta.event_id', '=', 'contacts.user_id')
            ->join('events', function ($join) {
                $join->on('events_meta.event_id', '=', 'events.id')
                    ->where('events.category_id', 7);
            })
            ->get();

        $i = 1;
        return view('master.ministries.womens.index', compact('ministry', 'photoalbums', 'members', 'i', 'news', 'bibleStudies', 'retreats'));
    }

    public function show($slug)
    {
        $ministry = Ministry::where('slug', $slug)->first();

        return view('master.ministries.womens.show', compact('ministry'));
    }
}
