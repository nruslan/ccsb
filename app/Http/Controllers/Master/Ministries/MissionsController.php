<?php

namespace App\Http\Controllers\Master\Ministries;

use App\Committee;
use App\Ministry;
use App\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MissionsController extends Controller
{
    public function index()
    {
        $i = 1;
        $ministry = Ministry::findOrFail(1);
        $photoalbums = $ministry->photoalbums;
        $committee = Committee::findOrFail(1);
        $members = $committee->active_members;
        $news = $ministry->news()->orderBy('pub_date_time', 'desc')->limit(5)->get();
        return view('master.ministries.missions.index', compact('ministry', 'members', 'i', 'photoalbums', 'news'));
    }

    public function ministry($slug)
    {
        $ministry = Ministry::where('slug', $slug)->first();
        $liaisons = $ministry->liaisons()->where('term_ended', null)->get();

        $news = $ministry->news()->orderBy('pub_date_time', 'desc')->limit(3)->get();

        return view('master.ministries.missions.ministry', compact('ministry', 'liaisons', 'news'));
    }

    public function member($slug)
    {
        $member = Member::where('slug', $slug)->first();
        $segment = request()->segment(2);
        return view('master.ministries.missions.member', compact('member', 'segment'));
    }
}
