<?php

namespace App\Http\Controllers\Master\Ministries;

use App\EmpowermentGroup;
use App\Ministry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MensController extends Controller
{
    public function index()
    {
        $ministry = Ministry::findOrFail(2);
        $groups = EmpowermentGroup::all();
        $photoalbums = $ministry->photoalbums;
        $news = $ministry->news()->orderBy('pub_date_time', 'desc')->limit(5)->get();
        return view('master.ministries.mens.index', compact('ministry', 'photoalbums', 'groups', 'news'));
    }
}
