<?php

namespace App\Http\Controllers\Master\Ministries;

use App\Ministry;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CareController extends Controller
{
    public function index()
    {
        $ministry = Ministry::findOrFail(4);
        //$members = $ministry->active_members;
        //$photoalbums = $ministry->photoalbums;
        $i = 1;
        $news = $ministry->news()->orderBy('pub_date_time', 'desc')->limit(5)->get();
        return view('master.ministries.care.index', compact('ministry', 'news'));
    }

    public function ministry($slug)
    {
        $ministry = Ministry::where('slug', $slug)->first();
        $members = $ministry->active_members;
        $liaison = User::findOrFail(4);
        //dd($ministry->slug);
        return view('master.ministries.care.ministry', compact('ministry', 'members', 'liaison'));
    }
}
