<?php

namespace App\Http\Controllers\Master\Ministries;

use App\Ministry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    protected $template;

    public function __construct()
    {
        $this->template = "master.ministries";
    }

    public function index()
    {
        $ministries = Ministry::whereNull('parent_id')->get();
        $i = 1;
        return view("$this->template.index", compact('ministries', 'i'));
    }
}
