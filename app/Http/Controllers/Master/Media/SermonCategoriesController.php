<?php

namespace App\Http\Controllers\Master\Media;

use App\Member;
use App\Sermon;

class SermonCategoriesController extends IndexController
{
    public function pastor($name)
    {
        $pastor = Member::where('slug', $name)->first();
        $sermons = $pastor->sermons()
            ->orderBy('date', 'desc')
            ->paginate(6);

        return view('master.media.sermon.category.pastor', compact('sermons', 'pastor'));
    }

    public function pastor_sermon($name, $sermon_slug)
    {
        $pastor = Member::where('slug', $name)->first();
        $sermon = Sermon::where('slug', $sermon_slug)->first();
        $sermons = $pastor->sermons()->inRandomOrder()->limit(6)->get();
        return view('master.media.sermon.category.pastors-sermon', compact('sermon', 'sermons', 'pastor'));
    }
}
