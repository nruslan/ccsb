<?php

namespace App\Http\Controllers\Master\Media;

use App\Member;
use App\Video;
use App\Category;

class VideoCategoriesController extends IndexController
{
    public function category($category_slug)
    {
        $category = Category::where('slug', $category_slug)
            ->first();

        $videos = $category->videos()
            ->orderBy('date', 'desc')
            ->paginate(6);

        return view('master.media.video.category.index', compact('videos', 'category'));
    }

    public function category_video($category_slug, $video_slug)
    {
        $category = Category::where('slug', $category_slug)->first();
        $video = Video::where('slug', $video_slug)->first();

        $videos = $category->videos()->inRandomOrder()->limit(6)->get();

        return view('master.media.video.category.category-video', compact('video', 'videos', 'category'));
    }

    public function pastor($name)
    {
        $pastor = Member::where('slug', $name)->first();
        $videos = $pastor->videos()
            ->orderBy('date', 'desc')
            ->paginate(6);

        return view('master.media.video.category.pastor', compact('videos', 'pastor'));
    }

    public function pastor_video($name, $video_slug)
    {
        $pastor = Member::where('slug', $name)->first();
        $video = Video::where('slug', $video_slug)->first();
        $videos = $pastor->videos()->inRandomOrder()->limit(6)->get();
        return view('master.media.video.category.pastors-video', compact('video', 'videos', 'pastor'));
    }
}
