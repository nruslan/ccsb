<?php

namespace App\Http\Controllers\Master\Media;

use App\Video;
use Illuminate\Http\Request;

class VideoController extends IndexController
{
    public function index()
    {
        $videos = Video::orderBy('date', 'desc')->paginate(6);
        return view('master.media.video.index', compact('videos'));
    }

    public function show($slug)
    {
        $video = Video::where('slug', $slug)->first();
        $randVideos = Video::inRandomOrder()->limit(6)->get();

        $prevDisabled = ($video->previous_video ? null : ' disabled');
        $nextDisabled = ($video->next_video ? null : ' disabled');

        return view('master.media.video.show', compact('video', 'randVideos', 'prevDisabled', 'nextDisabled'));
    }

    public function search()
    {
        $search = request('keywords');

        $videos = Video::where('title', 'LIKE', '%'.$search.'%')
            ->orderBy('date', 'desc')
            ->paginate(6);
        $i = 1;
        return view('master.media.video.search.result', compact('videos', 'search', 'i'));
    }
}
