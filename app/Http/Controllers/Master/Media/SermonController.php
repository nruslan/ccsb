<?php

namespace App\Http\Controllers\Master\Media;

use App\Sermon;
use Illuminate\Http\Request;

class SermonController extends IndexController
{
    public function index()
    {
        $sermons = Sermon::orderBy('date', 'desc')
            ->paginate(6); // it has to be even number

        $i = 1;//$sermons->firstItem();

        return view('master.media.sermon.index', compact('sermons', 'i'));
    }

    public function show($slug)
    {
        $sermon = Sermon::where('slug', $slug)
            ->first();

        $sermons = Sermon::inRandomOrder()->limit(6)->get();
        // Next & Prev links
        $prevDisabled = !$sermon->previous() ? ' disabled' : null;
        $nextDisabled = !$sermon->next() ? ' disabled' : null;

        return view('master.media.sermon.show', compact('sermon', 'sermons', 'prevDisabled', 'nextDisabled'));
    }

    public function search()
    {
        $search = request('keywords');

        $sermons = Sermon::where('title', 'LIKE', '%'.$search.'%')
            ->orderBy('date', 'desc')
            ->paginate(6);
        $i = 1;
        return view('master.media.sermon.search.result', compact('sermons', 'search', 'i'));
    }
}
