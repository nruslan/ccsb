<?php

namespace App\Http\Controllers\Master;

use App\Photoalbum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhotoalbumsController extends Controller
{
    public function index()
    {
        $photoalbums = Photoalbum::orderBy('date', 'desc')->paginate(6);

        return view('master.photoalbums.index', compact('photoalbums'));
    }

    public function show($slug)
    {
        $album = Photoalbum::whereSlug($slug)->first();

        return view('master.photoalbums.show', compact('album'));
    }
}
