<?php

namespace App\Http\Controllers\Master;

use App\Member;
use App\Title;
use App\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index()
    {
        return view('master.about.index');
    }

    public function church()
    {
        return view('master.about.church');
    }

    public function staff()
    {
        $title = Title::find(1); //Active Pastor
        $pastors = $title->active_members;

        $titles = Title::find([3,4,5]);


        return view('master.about.staff', compact('pastors', 'titles', 'title'));
    }

    public function leaders()
    {
        $title = Title::find(6); // Board of directors

        $mainTitles = Title::where('parent_id', $title->id)->get();

        return view('master.about.leaders', compact('title', 'mainTitles'));
    }

    public function show($slug)
    {
        $member = Member::where('slug', $slug)->first();
        $segment = request()->segment(2);
        $sermons = $member->sermons->count();
        $videos = $member->videos->count();
        $media = $sermons + $videos;

        return view('master.about.show', compact('member', 'sermons', 'videos', 'media', 'segment'));
    }

    public function doc()
    {
        $doc = Document::findOrFail(13);
        return view('master.about.document', compact('doc'));
    }
}
