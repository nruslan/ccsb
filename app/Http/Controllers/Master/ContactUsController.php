<?php

namespace App\Http\Controllers\Master;

use App\Mail\Subscribe;
use App\Mail\ContactUsForm;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    public function index()
    {
        return view('master.about.contact.index');
    }

    public function subscribe(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        $email = $request->email;
        Mail::to('ccsbsaddlebrooke@gmail.com')->send(new Subscribe($email));
        $data = view('master.about.contact.subscribe')->render();
        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    public function contact(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'name' => 'required',
            'message' => 'required'
        ]);

        $name = $request->name;
        $email = $request->email;
        $message = $request->message;
        Mail::to('ccsbsaddlebrooke@gmail.com')->send(new ContactUsForm($name, $email, $message));
        //Mail::to('ruslanwd@gmail.com')->send(new ContactUsForm($name, $email, $message));
        $data = view('master.about.contact.response')->render();
        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
