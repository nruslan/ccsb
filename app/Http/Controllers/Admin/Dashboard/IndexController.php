<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Member;
use App\PicturePhotoalbum;
use App\Sermon;
use App\Video;
use App\EventMeta;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $members = Member::all();
        $sermons = Sermon::all();
        $videos = Video::all();

        $sermon = Sermon::orderBy('date', 'desc')->first();
        $video = Video::orderBy('date', 'desc')->first();

        $latestPics = PicturePhotoalbum::orderBy('id')->limit(6)->get();
        $i = 0;

        $currentDate = Carbon::now();
        $end = $next4weeks = Carbon::now()->addWeeks(10);
        $events = EventMeta::whereBetween('date_time', [$currentDate, $end])->get();

        return view('admin.dashboard.index', compact('members', 'sermons', 'videos', 'sermon', 'video', 'events', 'latestPics', 'i'));
    }
}
