<?php

namespace App\Http\Controllers\Admin\Photoalbum;

use App\Photoalbum;
use App\AddressType;
use App\Ministry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $photoalbums = Photoalbum::orderBy('date', 'desc')->paginate();
        session()->put('pageId', $photoalbums->currentPage());
        $i = $photoalbums->firstItem();

        return view('admin.photoalbum.index', compact('photoalbums', 'i'));
    }

    public function create()
    {
        $locations = AddressType::find(3)->addresses;
        $ministries = Ministry::where('ministry_type_id', 1)->orderBy('name')->get();
        return view('admin.photoalbum.create', compact('locations', 'ministries'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required'
        ]);

        $request['slug'] = $request->title;
        $request['admin_id'] = request()->user()->id;
        $album = Photoalbum::create($request->all());
        if($request->ministry_id)
            $album->ministries()->sync($request->ministry_id);

        return redirect("admin/photoalbums/$album->id");
    }

    public function show($id)
    {
        $album = Photoalbum::findOrFail($id);
        return view('admin.photoalbum.show', compact('album'));
    }

    public function edit($id)
    {
        $album = Photoalbum::findOrFail($id);
        $locations = AddressType::find(3)->addresses;
        $ministries = Ministry::where('ministry_type_id', 1)->orderBy('name')->get();

        //dd($album->ministries);

        return view('admin.photoalbum.edit', compact('album', 'locations', 'ministries'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required'
        ]);

        $album = Photoalbum::findOrFail($id);
        $album->update($request->all());

        return redirect("admin/photoalbums/$album->id");
    }

    public function destroy($id)
    {
        //
    }
}
