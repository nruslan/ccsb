<?php

namespace App\Http\Controllers\Admin\Photoalbum;

use App\Photoalbum;
use App\Picture;
use Image;
//use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $album = Photoalbum::findOrFail(request()->id);
        return view('admin.photoalbum.picture.create', compact('album'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'input_files' => 'required'
        ]);

        $album = Photoalbum::findOrFail($request->album_id);
            $i = $album->pictures->count();

        foreach($request->input_files as $file) {
            // Photoalbum Year
            $year = date('Y',strtotime($album->date));
            // Photoalbum's folder name
            $title = date('Ymd',strtotime($album->date)).'-'.camel_case($album->title);
            // upload path
            $destinationPath = "public/photoalbums/$year/$title";
            $destinationPathThumbnail = "public/photoalbums/$year/$title/thumbnails";
            // getting file extension
            $extension = strtolower($file->getClientOriginalExtension());
            // Make filename
            $fileName = "photo-$i.$extension";
            // uploading file to given path
            $file->storeAs($destinationPath, $fileName);
            $file->storeAs($destinationPathThumbnail, $fileName);
            // Make image thumbnail size
            $this->image_manipulation($fileName, $destinationPathThumbnail);
            // Store in DB table
            $pic = Picture::create(['filename' => $fileName]);
            // Create keys in table
            $album->pictures()->attach($pic->id);
            // Save folder name
            if(!$album->folder_name)
                $album->update(['folder_name' => $title]);

            $i++;
        }

        return redirect("admin/photoalbums/$album->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $picture = Picture::findOrFail($id);

        return view('admin.photoalbum.picture.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $picture = Picture::findOrFail($id);
        $picture->delete();
        return redirect()->back();
    }


    public function image_manipulation($img, $path)
    {
        $image = Image::make(storage_path("app/$path/$img"));

        $width = $image->width();
        $height = $image->height();

        if($width > 300 and $height > 300) {
            $image->resize(null, 500, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            // crop image
            $image->crop(500, 500);
        }

        $image->save(storage_path("app/$path/$img"), 80);

        return true;
    }
}
