<?php

namespace App\Http\Controllers\Admin\Video;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function get_video(Request $request)
    {
        $search = $request['query'];

        $videos = Video::where('title', 'LIKE', '%'.$search.'%')
            ->orderBy('date', 'desc')
            ->limit(25)
            ->get();
        $i = 1;
        $data = view('admin.video.search.get-video', compact('videos', 'i'))->render();

        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
