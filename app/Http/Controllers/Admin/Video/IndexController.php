<?php

namespace App\Http\Controllers\Admin\Video;

use App\CategoryType;
use App\Title;
use App\Video;
use App\VideoType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::orderBy('date', 'desc')->paginate(12);
        $i = $videos->firstItem();
        session()->put('pageId', $videos->currentPage());
        return view('admin.video.index', compact('videos', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $videoTypes = VideoType::all()->pluck('service', 'id');
        $type = CategoryType::findOrFail(1);
        $categories = $type->categories()->pluck('title', 'id');
        $title = Title::find(1); //Pastor
        $pageId = session()->get('pageId');
        return view('admin.video.create', compact('videoTypes', 'title', 'categories', 'pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'video_type_id' => 'required',
            'filename' => 'required',
            'date' => 'required',
            'title' => 'required'
        ],[
           'filename.required' => 'Link to video is required field. Specify the URL.'
        ]);

        $request['slug'] = $request['title'];

        $video = Video::create($request->all());

        $video->members()->sync($request['member_id']);
        $video->categories()->sync($request['category_id']);

        return redirect("/admin/videos/$video->id")->with('message_success', 'The video has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video = Video::find($id);
        $pageId = session()->get('pageId');

        // Previous button
        if($video->previous()) {
            $prevId = $video->previous()->id;
        } else {
            $prevId = null;
            $prevDisabled = ' disabled';
        }
        // Next button
        if($video->next()) {
            $nextId = $video->next()->id;
        } else {
            $nextId = null;
            $nextDisabled = ' disabled';
        }

        return view('admin.video.show', compact('video', 'pageId', 'prevId', 'nextId', 'prevDisabled', 'nextDisabled'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Load the video
        $video = Video::find($id);
            $video->member_id = $video->members->pluck('id')->all();
            $video->category_id = $video->categories->pluck('id')->all();
        // Video Types
        $videoTypes = VideoType::all()->pluck('service', 'id');
        // Video Categories
        $type = CategoryType::findOrFail(1);
        $categories = $type->categories()->pluck('title', 'id');
        // Pastors
        $title = Title::find(1); //Pastor
        // Page ID
        $pageId = session()->get('pageId');
        return view('admin.video.edit', compact('video', 'title', 'pageId', 'videoTypes', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date' => 'required',
            'title' => 'required'
        ]);

        $video = Video::find($id);
            $video->update($request->all());
            $video->members()->detach();
            $video->categories()->detach();

        $video->members()->attach($request['member_id']);
        $video->categories()->attach($request['category_id']);

        return redirect("/admin/videos/$id")->with('message_success', 'The video has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::find($id);
            $video->members()->detach();
            $video->categories()->detach();
        $video->delete();
        return redirect('/admin/videos')->with('message_info', 'The video has been deleted.');
    }
}
