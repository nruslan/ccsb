<?php

namespace App\Http\Controllers\Admin\Committees;

use App\Committee;
use App\CommitteeMember;
use App\Member;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = request()->get('id');
        if($id)
        {
            $committee = Committee::find($id);

            return view('admin.committees.member.create', compact('committee'));
        }
        else
        {
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'member_id' => 'required',
            'term_started' => 'required|date'
        ], [
            'member_id.required' => 'You need to add member\'s name. Use search field above ⬏'
        ]);

        CommitteeMember::create(array_filter($request->all()));

        $id = $request['committee_id'];

        return redirect("/admin/committees/$id")->with('message_success', 'Member has been added to the committee successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $committeeMember = CommitteeMember::find($id);
        return view('admin.committees.member.edit', compact('committeeMember'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'term_started' => 'required|date',
        ]);

        $committeeMember = CommitteeMember::find($id);
        $committeeMember->update(array_filter($request->all()));

        return redirect("/admin/committees/$committeeMember->committee_id")->with('message_success', "Member's terms have been updated successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
