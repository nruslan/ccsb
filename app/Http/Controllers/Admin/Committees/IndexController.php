<?php

namespace App\Http\Controllers\Admin\Committees;

use App\Committee;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $committees = Committee::orderBy('id')->paginate(12);
        $i = $committees->firstItem();
        session()->put('pageId', $committees->currentPage());
        return view('admin.committees.index', compact('committees', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.committees.create', compact('committees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $request['slug'] = $request->name;
        $committee = Committee::create($request->all());

        return redirect("/admin/committees/$committee->id")->with('message_success', "$committee->name committee has been added successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $committee = Committee::find($id);
        $i = 1;
        $ii = $i;
        $pageId = session()->get('pageId');

        return view('admin.committees.show', compact('committee', 'i', 'pageId', 'ii'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $committee = Committee::find($id);
        return view('admin.committees.edit', compact('committee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $committee = Committee::find($id);
            $committee->update($request->all());

        return redirect("/admin/committees/$committee->id")->with('message_success', "$committee->name committee has been updated successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $committee = Committee::find($id);
            $name = $committee->name;
        $committee->delete();

        return redirect("/admin/committees")->with('message_info', "$name committee has been deleted.");
    }
}
