<?php

namespace App\Http\Controllers\Admin\Events;

use App\Event;
use App\EventPrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PriceController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = Event::findOrFail(request()->id);
        return view('admin.events.price.create', compact('event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'price' => 'required',
            'name' => 'required|max:191'
        ], [
            'name.required' => 'The Short Price Description field is required.'
        ]);

        EventPrice::create($request->all());

        return redirect("admin/events/$request->event_id")->with('message_success', 'The Price has been added to the event successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ePrice = EventPrice::findOrFail($id);
        return view('admin.events.price.edit', compact('ePrice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'price' => 'required',
            'name' => 'required|max:191'
        ], [
            'name.required' => 'The Short Price Description field is required.'
        ]);

        $ePrice = EventPrice::findOrFail($id);
        $ePrice->update($request->all());

        return redirect("admin/events/$ePrice->event_id")->with('message_success', 'The Price has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ePrice = EventPrice::findOrFail($id);
        $eventId = $ePrice->event_id;
        $ePrice->delete();
        return redirect("admin/events/$eventId")->with('message_success', 'The Price has been deleted.');
    }
}
