<?php

namespace App\Http\Controllers\Admin\Events;

use App\Event;
use App\Picture;
use function GuzzleHttp\Psr7\str;
use Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PictureController extends Controller
{
    public function create()
    {
        $event = Event::findOrFail(request()->id);
        return view('admin.events.picture.create', compact('event'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'input_file' => 'required'
        ]);
        $file = $request->input_file;
        $event = Event::findOrFail($request->event_id);
        // Photoalbum Year
        $year = date('Y',strtotime($event->created_at));
        // Photoalbum's folder name
        $title = date('Ymd',strtotime($event->created_at)).'-'.strtolower(camel_case(str_replace("'", '', $event->title)));
        // upload path
        $destinationPath = "public/events/$year";
        $destinationPathThumbnail = "public/events/$year/thumbnails";
        // getting file extension
        $extension = strtolower($file->getClientOriginalExtension());
        // Make filename
        $fileName = "$title.$extension";
        // uploading file to given path
        //$file->storeAs($destinationPath, $fileName);
        $file->storeAs($destinationPathThumbnail, $fileName);
        // Make image thumbnail size
        $this->image_manipulation($fileName, $destinationPathThumbnail);
        // Store in DB table
        $pic = Picture::create(['filename' => $fileName]);
        // Create keys in table
        $event->update(['picture_id' => $pic->id]);

        return redirect("admin/events/$event->id");
    }

    public function destroy($id)
    {
        Event::findOrFail($id)->update(['picture_id' => NULL]);
        return redirect("admin/events/$id");
    }

    public function image_manipulation($img, $path)
    {
        $image = Image::make(storage_path("app/$path/$img"));

        $width = $image->width();
        $height = $image->height();

        if($width > $height) { //horizontal image
            $image->resize(519, NULL, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
        } elseif ($width < $height) { //Vertical image
            $image->resize(NULL, 346, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
        } else {
            $image->resize(519, NULL, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
        }
        // crop image
        $image->crop(519, 346);
        $image->save(storage_path("app/$path/$img"), 90);
        return true;
    }
}
