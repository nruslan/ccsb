<?php

namespace App\Http\Controllers\Admin\Events;

use App\CategoryType;
use App\Event;
use App\Ministry;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $events = Event::orderBy('id', 'desc')->paginate(12);
        $i = $events->firstItem();
        return view("admin.events.index", compact('events', 'i'));
    }

    public function create()
    {
        $ministries = Ministry::where('ministry_type_id', 1)
            ->orderBy('name')
            ->get();

        $type = CategoryType::findOrFail(2);
        $categories = $type->categories()->pluck('title', 'id');

        return view("admin.events.create", compact('ministries', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\EventRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\EventRequest $request)
    {
        $inputs = $request->all();

        // Convert string into slug
        $inputs['slug'] = $inputs['title'];

        $event = Event::create($inputs);

        // Assign event to the ministry if exists
        if(!is_null($inputs['ministry_id']))
            $event->ministries()->attach($inputs['ministry_id']);

        return redirect("admin/events/$event->id")->with('message_success', 'Event has been saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        $dates = $event->dates;
        $ministries = $event->ministries;
        $members = $event->members;
        $i = 1;
        $ii = $i;
        return view("admin.events.show", compact('event', 'dates', 'ministries', 'members', 'i', 'ii'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        $event['ministry_id'] = $event->ministries->pluck('id', 'title')->toArray();
        $type = CategoryType::findOrFail(2);
        $categories = $type->categories()->pluck('title', 'id');
        $ministries = Ministry::where('ministry_type_id', 1)
            ->orderBy('name')
            ->get();
        return view("admin.events.edit", compact('event', 'ministries', 'categories'));
    }

    public function update(Requests\EventRequest $request, $id)
    {
        $event = Event::findOrFail($id);
        $event->update($request->all());

        if(!is_null($request->ministry_id))
            $event->ministries()->sync($request->ministry_id);
        else
            $event->ministries()->detach();

        return redirect("admin/events/$event->id")->with('message_success', 'Event has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
