<?php

namespace App\Http\Controllers\Admin\Events;

use App\AddressType;
use App\EventMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eventMeta = EventMeta::findOrFail(request()->id);
        $event = $eventMeta->data;
        $locations = AddressType::find(3)->addresses;
        return view('admin.events.location.create', compact('eventMeta', 'event', 'locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'location_id' => 'required'
        ], [
            'location_id.required' => 'You need to specify the event location.'
        ]);

        $eventMeta = EventMeta::findOrFail($request->meta_id);
        $eventMeta->update(['location_id' => $request->location_id]);
        return redirect("admin/events/$eventMeta->event_id")->with('message_success', 'Event Location has been saved successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventMeta = EventMeta::findOrFail($id);
        $event = $eventMeta->data;
        $locations = AddressType::find(3)->addresses;
        return view('admin.events.location.edit', compact('eventMeta', 'event', 'locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'location_id' => 'required'
        ], [
            'location_id.required' => 'You need to specify the event location.'
        ]);

        $eventMeta = EventMeta::findOrFail($id);
        $eventMeta->update(['location_id' => $request->location_id]);
        return redirect("admin/events/$eventMeta->event_id")->with('message_success', 'Event Location has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
