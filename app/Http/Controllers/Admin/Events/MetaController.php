<?php

namespace App\Http\Controllers\Admin\Events;

use App\Event;
use App\EventMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MetaController extends Controller
{
    public function index()
    {
        $dates = EventMeta::orderBy('date_time', 'desc')->paginate(12);
        $i = $dates->firstItem();
        return view("admin.events.meta.index", compact('dates', 'i'));
    }

    public function create()
    {
        $event = Event::findOrFail(request()->id);
        return view('admin.events.meta.create', compact('event'));
    }

    public function store(Request $request)
    {
        if($request->repeat_weekly) {
            $request->validate([
                'date' => 'required|date',
                'time' => 'required',
                'date_end' => 'required|date'
            ]);
        } else {
            $request->validate([
                'date' => 'required|date',
                'time' => 'required'
            ]);
        }

        $dateTime = "$request->date $request->time";
        $dateTimeEnd = "$request->date_end $request->time";
        // Get the interval for recurring event
        if($request->repeat_weekly) {
            $interval = 604800;
            do {
                $evInput['event_id'] = $request->event_id;
                $evInput['date_time'] = $dateTime;
                $em = EventMeta::create($evInput);
                $dateTime = date("m/d/Y g:i A",strtotime($dateTime) + $interval);

            } while (strtotime($em->date_time) <= strtotime($dateTimeEnd) );
        } else {
            EventMeta::create([
                'date_time' => $dateTime,
                'event_id' => $request->event_id
            ]);
        }

        return redirect("admin/events/$request->event_id")->with('message_success', 'Event has been saved successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventMeta = EventMeta::findOrFail($id);
        return view('admin.events.meta.edit', compact('eventMeta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'date_time' => 'required'
        ]);

        $eventMeta = EventMeta::findOrFail($id);
        $eventMeta->update($request->all());

        return redirect("admin/events/$eventMeta->event_id")->with('message_success', "Event's Date & Time have been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventMeta = EventMeta::findOrFail($id);
        $eventId = $eventMeta->event_id;
        $eventMeta->delete();

        return redirect("admin/events/$eventId")->with('message_info', "Event's Date & Time have been deleted.");
    }
}
