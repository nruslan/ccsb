<?php

namespace App\Http\Controllers\Admin\Events;

use App\Event;
use App\EventMember;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = Event::findOrFail(request()->id);
        return view('admin.events.members.create', compact('event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'event_id' => 'required',
            'member_id' => 'required'
        ], [
            'member_id.required' => 'You have to specify the member. Use the search field.'
        ]);

        $event = Event::findOrFail($request->event_id);

        $event->members()->attach($request->member_id, ['title' => $request->title]);

        return redirect("admin/events/$event->id")->with('message_success', 'The member has been assigned successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = EventMember::findOrFail($id);
        $eventId = $member->event_id;
        $member->delete();
        return redirect("admin/events/$eventId");
    }
}
