<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Member;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function getMember(Request $request)
    {
        $search = $request['member_name'];

        $members = Member::whereRaw("concat(first_name, ' ', last_name) like '%$search%'")
            ->orWhereRaw("concat(last_name, ' ', first_name) like '%$search%'")
            ->orderBy('last_name')
            ->get();

        $i = 0;

        $data = view('admin.search.get-member', compact('members', 'i'))->render();

        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
