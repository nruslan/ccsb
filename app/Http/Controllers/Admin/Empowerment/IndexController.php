<?php

namespace App\Http\Controllers\Admin\Empowerment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    protected $template;
    public function __construct()
    {
        $this->template = "admin.empowerment";
    }
}
