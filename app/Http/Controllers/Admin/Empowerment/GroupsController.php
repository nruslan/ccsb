<?php

namespace App\Http\Controllers\Admin\Empowerment;

use App\EmpowermentGroup;
use Illuminate\Http\Request;

class GroupsController extends IndexController
{
    protected $view_template;

    public function __construct()
    {
        parent::__construct();
        $this->view_template = "$this->template.groups";
    }

    public function index()
    {
        $groups = EmpowermentGroup::orderBy('id')->paginate(14);
        $i = 1;
        return view("$this->view_template.index", compact('groups', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("$this->view_template.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'time' => 'required'
        ]);
        $group = EmpowermentGroup::create($request->all());
        return redirect("admin/groups/$group->id")->with('message_success', 'The group has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = EmpowermentGroup::findOrFail($id);
        $leaders = $group->leaders;
        $members = $group->members;
        $i = 1;
        return view("$this->view_template.show", compact('group', 'members', 'leaders', 'i'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = EmpowermentGroup::findOrFail($id);
        return view("$this->view_template.edit", compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'time' => 'required'
        ]);

        $group = EmpowermentGroup::findOrFail($id);
        $group->update($request->all());

        return redirect("admin/groups/$id")->with('message_success', 'The group information has been updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = EmpowermentGroup::findOrFail($id);
        $group->delete();
        return redirect("admin/groups")->with('message_success', 'The group has been deleted from the list!');
    }
}
