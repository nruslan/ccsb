<?php

namespace App\Http\Controllers\Admin\Empowerment;

use App\EmpowermentGroup;
use App\EmpowermentMember;
use Illuminate\Http\Request;

class MembersController extends IndexController
{
    protected $view_template;

    public function __construct()
    {
        parent::__construct();
        $this->view_template = "$this->template.members";
    }

    /**
     * Show the form for creating a new resource.
     * @param $id int
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if($id) {
            $group = EmpowermentGroup::find($id);
            $pageId = session()->get('pageId');
            return view("$this->view_template.create", compact('group', 'pageId'));
        }
        else {
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'member_id' => 'required'
        ], [
            'member_id.required' => 'You need to add member\'s name. Use search field above ⬏'
        ]);

        $id = request()->id;

        EmpowermentMember::create([
            'empowerment_group_id' => $id,
            'member_id' => $request->member_id
        ]);

        return redirect("admin/groups/$id")->with('message_success', 'Member has been added to the group successfully!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  int  $memberId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $memberId)
    {
        $eMember = EmpowermentMember::findOrFail($memberId);
        $eMember->leader ? $eMember->update(['leader' => null]) : $eMember->update(['leader' => true]);

        return redirect("admin/groups/$id")->with('message_success', "Member's leadership has been set successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  int  $memberId
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $memberId)
    {
        $eMember = EmpowermentMember::findOrFail($memberId);
        $name = $eMember->member->full_name;
        $eMember->delete();
        return redirect("admin/groups/$id")->with('message_success', "$name has been removed from the group successfully!");
    }
}
