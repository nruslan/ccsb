<?php

namespace App\Http\Controllers\Admin\News;

use App\Committee;
use App\Ministry;
use App\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $news = News::orderBy('pub_date_time', 'desc')
            ->paginate(12);

        $i = $news->firstItem();

        return view('admin.news.index', compact('news', 'i'));
    }

    public function create()
    {
        $committees = Committee::all()->pluck('name', 'id');
        $ministries = Ministry::all()->pluck('title', 'id');
        return view('admin.news.create', compact('committees', 'ministries'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            //'subtitle' => 'required',
            'text' => 'required',
            'pub_date_time' => 'required'
        ]);
        $inputs = $request->all();

        $inputs['slug'] = $inputs['title'].' '.$inputs['subtitle'];

        $post = News::create(array_filter($inputs));

        if(isset($inputs['committee']) && !empty($inputs['committee']))
            $post->committees()->sync($inputs['committee']);

        if(isset($inputs['ministry']) && !empty($inputs['ministry']))
            $post->ministries()->sync($inputs['ministry']);

        return redirect("admin/news/$post->id")->with('message_success', 'Post has been published successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aNews = News::find($id);

        return view('admin.news.show', compact('aNews'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
