<?php

namespace App\Http\Controllers\Admin\News;

use App\News;
use App\Picture;
use Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PictureController extends Controller
{
    public function create()
    {
        $newsPost = News::findOrFail(request()->id);
        return view('admin.news.picture.create', compact('newsPost'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'input_file' => 'required'
        ]);
        $file = $request->input_file;
        $newsPost = News::findOrFail($request->newspost_id);
        // Photoalbum Year
        $year = date('Y',strtotime($newsPost->created_at));
        // Photoalbum's folder name
        $title = date('Ymd',strtotime($newsPost->created_at)).'-'.strtolower(camel_case($newsPost->title));
        // upload path
        $destinationPath = "public/news/$year";
        $destinationPathThumbnail = "public/news/$year/thumbnails";
        // getting file extension
        $extension = strtolower($file->getClientOriginalExtension());
        // Make filename
        $fileName = "$title.$extension";
        // uploading file to given path
        //$file->storeAs($destinationPath, $fileName);
        $file->storeAs($destinationPathThumbnail, $fileName);
        // Make image thumbnail size
        $this->image_manipulation($fileName, $destinationPathThumbnail);
        // Store in DB table
        $pic = Picture::create(['filename' => $fileName]);
        // Create keys in table
        $newsPost->update(['picture_id' => $pic->id]);

        return redirect("admin/news/$newsPost->id");
    }

    public function destroy($id)
    {
        News::findOrFail($id)->update(['picture_id' => NULL]);
        return redirect("admin/news/$id");
    }

    public function image_manipulation($img, $path)
    {
        $image = Image::make(storage_path("app/$path/$img"));

        $width = $image->width();
        $height = $image->height();

        if($width > $height) { //horizontal image
            $image->resize(519, NULL, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
        } elseif ($width < $height) { //Vertical image
            $image->resize(NULL, 346, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
        } else {
            $image->resize(519, NULL, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
        }
        // crop image
        $image->crop(519, 346);
        $image->save(storage_path("app/$path/$img"), 90);
        return true;
    }
}
