<?php

namespace App\Http\Controllers\Admin\Docs;

use App\DocType;
use App\Document;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = Document::orderBy('date', 'desc')->paginate();
        session()->put('pageId', $files->currentPage());
        $i = $files->firstItem();

        return view('admin.docs.index', compact('files', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageId = session()->get('pageId');
        $docTypes = DocType::all()->pluck('name', 'id');
        return view('admin.docs.create', compact('pageId', 'docTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required',
            'input_file' => 'required'
        ], [
            'input_file.required' => 'The Document File field is required.'
        ]);
        // all incoming inputs
        $inputs = $request->all();

        // Check if file exists
        if($request->hasFile('input_file'))
        {
            // Year
            $year = date('Y',strtotime($inputs['date']));
            // upload path
            $destinationPath = "public/documents/$year";

            // getting file extension
            $extension = $request->file('input_file')->getClientOriginalExtension();

            // getting file size
            $size = $request->file('input_file')->getSize();

            // Make filename
            $fileName = strtotime($inputs['date']).'-'.str_slug($inputs['title'], '-').'.'.$extension;

            // uploading file to given path
            $request->file('input_file')->storeAs($destinationPath, $fileName);

            $inputs['filename'] = $fileName;
            $inputs['user_id'] = Auth::user()->id;
            $inputs['size'] = $size;

            $newDoc = Document::create($inputs);

            return redirect("admin/documents/$newDoc->id");
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doc = Document::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('admin.docs.show', compact('doc', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doc = Document::findOrFail($id);
        $docTypes = DocType::all()->pluck('name', 'id');
        $pageId = session()->get('pageId');
        return view('admin.docs.edit', compact('doc', 'docTypes', 'pageId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required'
        ]);

        $doc = Document::findOrFail($id);
        $doc->update($request->all());

        return redirect("admin/documents/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doc = Document::findOrFail($id);
            //Delete the file
            Storage::delete("public/documents/$doc->year/$doc->filename");
            // Delete the DB entry
            $doc->delete();

        $pageId = session()->get('pageId');
        return redirect("admin/documents?page=$pageId");
    }
}
