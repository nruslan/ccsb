<?php

namespace App\Http\Controllers\Admin\Sermons;

use App\Title;
use App\Sermon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sermons = Sermon::orderBy('date', 'desc')->paginate(12);
        $i = $sermons->firstItem();
        session()->put('pageId', $sermons->currentPage());
        return view('admin.sermons.index', compact('sermons', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title1 = Title::find(1); //Pastor
        $title2 = Title::find(2); // Guest Pastor
        $pageId = session()->get('pageId');
        return view('admin.sermons.create', compact('title1', 'title2', 'pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required',
            //'member_id' => 'required'
        ]);

        // all incoming inputs
        $inputs = $request->all();
        $inputs['slug'] = $inputs['title'];
        // Create data for new sermon
        $sermon = Sermon::create($inputs);
        // Save pastors related to the sermon
        if(isset($inputs['member_id']))
            $sermon->pastors()->sync($inputs['member_id']);

        $data = $sermon->id;
        if(isset($inputs['parent_id']) && !empty($inputs['parent_id'])) {
            // Set parent ID
            Sermon::find($inputs['parent_id'])->update(['parent_id' => $data]);
        }
        return redirect("admin/sermons/$data");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sermon = Sermon::find($id);
        $pageId = session()->get('pageId');

        if($sermon->previous()) {
            $prevId = $sermon->previous()->id;
        } else {
            $prevId = null;
            $prevDisabled = ' disabled';
        }

        if($sermon->next()) {
            $nextId = $sermon->next()->id;
        } else {
            $nextId = null;
            $nextDisabled = ' disabled';
        }

        return view('admin.sermons.show', compact('sermon', 'pageId', 'prevId', 'nextId', 'prevDisabled', 'nextDisabled'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sermon = Sermon::findOrFail($id);
        $pageId = session()->get('pageId');
        $title1 = Title::find(1); //Pastor
        $title2 = Title::find(2); // Guest Pastor
        $sermon['member_id'] = $sermon->pastors->pluck('id')->toArray();
        return view('admin.sermons.edit', compact('sermon', 'pageId', 'title1', 'title2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'date' => 'required',
            'member_id' => 'required'
        ]);
        // all incoming inputs
        $inputs = $request->all();
        $sermon = Sermon::findOrFail($id);
            $sermon->update($inputs);
        // Save pastors related to the sermon
        $sermon->pastors()->detach();
        $sermon->pastors()->sync($inputs['member_id']);
        return redirect("admin/sermons/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Set parent ID
        $sermon = Sermon::find($id);
        $sermon->delete();
        $pageId = session()->get('pageId');
        return redirect("/admin/sermons?page=$pageId")->with('message_info', 'The sermon has been deleted.');
    }
}
