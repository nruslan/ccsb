<?php

namespace App\Http\Controllers\Admin\Sermons;

use App\Sermon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sermon = Sermon::findOrFail(request()->id);
        $pageId = session()->get('pageId');
        return view('admin.sermons.file.create', compact('sermon', 'pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // all incoming inputs
        $inputs = $request->all();

        // Check if file exists
        if($request->hasFile('input_file'))
        {
            // Sermon Year
            $year = date('Y',strtotime($inputs['date']));
            // upload path
            $destinationPath = "public/sermons/$year";

            // getting file extension
            $extension = $request->file('input_file')->getClientOriginalExtension();

            // Make filename
            $fileName = strtotime($inputs['date']).'-'.str_slug($inputs['title'], '-').'.'.$extension;

            // uploading file to given path
            $request->file('input_file')->storeAs($destinationPath, $fileName);

            $inputs['filename'] = $fileName;
            $inputs['slug'] = $inputs['title'];
        }

        // Create data for new sermon
        $sermon = Sermon::create($inputs);

        // Save pastors related to the sermon
        $sermon->pastors()->sync($inputs['member_id']);

        $data = $sermon->id;

        if(isset($inputs['parent_id']) && !empty($inputs['parent_id']))
        {
            // Set parent ID
            Sermon::find($inputs['parent_id'])->update(['parent_id' => $data]);
        }

        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sermon = Sermon::findOrFail($id);
        $year = date('Y',strtotime($sermon->date));
            Storage::delete("public/sermons/$year/$sermon->filename");
        $sermon->update(['filename' => null, 'duration' => null, 'size' => null]);

        return redirect("/admin/sermons/$id");
    }

    public function upload(Request $request)
    {
        if($request->hasFile('input_file'))
        {
            // Set parent ID
            $sermon = Sermon::findOrFail($request->sermon_id);
            // Sermon Year
            $year = date('Y',strtotime($sermon->date));
            // upload path
            $destinationPath = "public/sermons/$year";

            // getting file extension
            $extension = $request->file('input_file')->getClientOriginalExtension();

            // Make filename
            $fileName = $sermon->sermon_file_name.'.'.$extension;

            // uploading file to given path
            $request->file('input_file')->storeAs($destinationPath, $fileName);
            $sermon->update([
                'filename' => $fileName,
                'duration' => $request->duration,
                'size' => $request->file_size
            ]);

            return view("admin.sermons.file.response", compact('sermon'))->render();

        }else {
            return 'error happend';
        }
    }
}
