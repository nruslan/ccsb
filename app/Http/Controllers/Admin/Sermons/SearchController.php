<?php

namespace App\Http\Controllers\Admin\Sermons;

use App\Sermon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function related(Request $request)
    {
        $search = $request['title'];
        $sermons = Sermon::where('title', 'LIKE', '%'.$search.'%')
            ->orWhere('subtitle', 'LIKE', '%'.$search.'%')
            ->orderBy('date', 'desc')
            ->get();
        $i = 1;
        $data = view('admin.sermons.search.related', compact('sermons', 'i'))->render();
        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    public function get_sermon(Request $request)
    {
        $search = $request['query'];
        $sermons = Sermon::where('title', 'LIKE', '%'.$search.'%')
            ->orWhere('subtitle', 'LIKE', '%'.$search.'%')
            ->orderBy('date', 'desc')
            ->limit(25)
            ->get();
        $i = 1;
        $data = view('admin.sermons.search.get-sermon', compact('sermons', 'i'))->render();
        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
