<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\User;
use Illuminate\Http\Request;
use App\Member;
use App\Http\Requests;

class MemberController extends IndexController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::orderBy('last_name')
            ->where('deleted_at', null)
            ->paginate(12);
        $i = $members->firstItem();
        return view('admin.accounts.members.index', compact('members', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::find(request()->get('user'));

        return view('admin.accounts.members.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha'
        ]);

        $inputs = array_filter($request->all());
        $inputs['slug'] = $inputs['first_name'].'-'.$inputs['last_name'].'-'.substr(mt_rand(99999,1000000), 0, 3);
        Member::create($inputs);

        return redirect("/admin/accounts/users/".$inputs['user_id'])->with('message_success', 'Member has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::find($id);

        return view('admin.accounts.members.show', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);

        return view('admin.accounts.members.edit', compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha'
        ]);

        Member::find($id)->update(array_filter($request->all()));

        return redirect("/admin/accounts/members/$id")->with('message_success', 'The member was updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::find($id);
            $userId = $member->userAccount->id;
            $membersName = $member->first_name.' '.$member->last_name;

        if($member->deleted_at)
        {
            $member->delete();
            return redirect("/admin/accounts/users/$userId")->with('message_info', "$membersName has been deleted from the account.");
        }
        else
        {
            $member->update(['deleted_at' => date("Y-m-d H:i:s")]);
            return redirect("/admin/accounts/users/$userId")->with('message_info', "$membersName has been marked for deletion.");
        }
    }

    public function undo($id)
    {
        Member::find($id)->update(['deleted_at' => null]);
        return redirect("/admin/accounts/members/$id")->with('message_info', 'Member deletion has been canceled.');
    }

    public function find_member(Request $request)
    {
        $search = $request['query'];

        $members = Member::whereRaw("concat(first_name, ' ', last_name) like '%$search%'")
            ->orWhereRaw("concat(last_name, ' ', first_name) like '%$search%'")
            ->orderBy('last_name')
            ->get();

        $i = 1;

        $data = view('admin.accounts.members.search.result', compact('members', 'i'))->render();

        //sleep(3);

        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
