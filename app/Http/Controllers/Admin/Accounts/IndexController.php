<?php

namespace App\Http\Controllers\Admin\Accounts;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\State;
use App\Country;
use App\Visibility;
use App\AddressType;
use Image;

class IndexController extends Controller
{
    protected $template;

    public function __construct()
    {
        $this->template = "admin.accounts";
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function resetPassword($id)
    {
        // Get user
        $user = User::find($id);
        //automatically generated password
        $newPassword = substr(str_shuffle('123456789q$wertyuiopasdfghj!kLzxcvbnmQWERTYUIOPKJHGFDSAZXCVBNM'),0,9);
        // Update user table
        $user->update(['password' => bcrypt($newPassword)]);

        // Send e-mail notification
        Mail::send('admin.accounts.users.email.password-reset',['user' => $user, 'password' => $newPassword],function ($m) use ($user){
            $m->from('office@communitychurchSaddlebrooke.com','CCSB');
            $m->to($user->email, $user->name)
                ->subject('Your password was reset');
        });

        // Redirect back
        return redirect()->back()->with('message_success', 'Password was reset successfully. Notification email with a new password has been sent.');
    }

    //Custom functions
    public function image_manipulation($img, $path)
    {
        $bg_image = Image::make(storage_path("app/public/accounts/blank.png"));
        $bg_image->insert(storage_path("app/$path/$img"));
        $bg_image->save(storage_path("app/$path/$img"), 100);
        return true;
    }
    /**
     * Set verification status of the user
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function verifyUser($id)
    {
        // Load users data
        $user = User::find($id);

        if($user->verified)
        {
            $user->update(['verified' => 0]);
        }
        else
        {
            $user->update(['verified' => 1]);
        }

        return redirect(URL::previous());
    }

    public static function states()
    {
        return State::where('hidden', '0')->pluck('full_name', 'id');
    }

    public static function countries()
    {
        return Country::where('hidden', '0')->pluck('full_name', 'id');
    }

    public static function visibility()
    {
        return Visibility::all()->pluck('mode', 'id');
    }

    public static function addressType()
    {
        return AddressType::all()->pluck('name', 'id');
    }

}
