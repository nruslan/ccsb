<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\Member;
use App\MemberTitle;
use App\Title;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MembersTitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = request()->get('id');
        if($id)
        {
            $member = Member::find($id);

            $titles = Title::all()->pluck('full_title', 'id');

            return view('admin.accounts.members.title.create', compact('member', 'titles'));
        }
        else
        {
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title_id' => 'required',
            'term_started' => 'required|date'
        ]);

        $mTitle = MemberTitle::create($request->all());

        return redirect("/admin/accounts/members/$mTitle->member_id")->with('message_success', 'The title has been assigned to the member successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $memberTitle = MemberTitle::find($id);
        $titles = Title::all()->pluck('full_title', 'id');
        return view('admin.accounts.members.title.edit', compact('memberTitle', 'titles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title_id' => 'required',
            'term_started' => 'required|date',
            'term_ended' => 'date'
        ]);

        $mTitle = MemberTitle::find($id);
            $mTitle->update(array_filter($request->all()));

        return redirect("/admin/accounts/members/$mTitle->member_id")->with('message_success', 'The title has been updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MemberTitle::find($id)->delete();

        return redirect()->back()->with('message_success', 'The title has been deleted successfully.');
    }
}
