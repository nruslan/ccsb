<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\Email;
use App\Member;
use Illuminate\Http\Request;

use App\Http\Requests;

class EmailController extends IndexController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = request()->get('id');
        if($id) {
            $member = Member::find($id);
            $visibility = parent::visibility();
            return view('admin.accounts.email.create', compact('member', 'visibility'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\EmailRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Requests\EmailRequest $request)
    {
        Email::create($request->all());

        return redirect("/admin/accounts/members/". $request['member_id'])->with('message_success', 'The email address has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $email = Email::find($id);
        $member = $email->member;
        $visibility = parent::visibility();
        return view('admin.accounts.email.edit', compact('email', 'member', 'visibility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\EmailRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Requests\EmailRequest $request, $id)
    {
        $email = Email::find($id);
        $member_id = $email->member->id;
        $email->update($request->all());
        return redirect("/admin/accounts/members/$member_id")->with('message_success', 'The email address has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $email = Email::find($id);
            $memberId = $email->member_id;
            $emailAddress = $email->address;
        $email->delete();
        return redirect("/admin/accounts/members/$memberId")->with('message_info', "$emailAddress has been deleted.");
    }
}
