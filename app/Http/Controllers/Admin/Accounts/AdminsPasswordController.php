<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\Admin;
use App\Mail\AdministratorPasswordChanged;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AdminsPasswordController extends IndexController
{
    private $view_template;
    public function __construct()
    {
        parent::__construct();
        $this->view_template = "$this->template.admins.password";
    }

    public function edit($id)
    {
        $admin = Admin::findOrFail($id);
        return view("$this->view_template.edit", compact('admin'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'new_password' => 'required|string|min:12|confirmed',
            'new_password_confirmation' => 'required|string',
        ]);


        $admin = Admin::findOrFail($id);

        $psw = $request->new_password;

        $admin->update([
            'password' => bcrypt($psw),
            'temp_password' => 0
        ]);

        $num = strlen($psw)-2;
        $asterisk = str_repeat('*', $num);
        $firstChar = substr($psw, 0, 1);
        $lastChar = substr($psw, -1);
        $hint = $firstChar.$asterisk.$lastChar;
        $mail = Mail::to($admin->email)->send(new AdministratorPasswordChanged($admin, $hint));
        return redirect("admin/accounts/admins/$id")->with('message_success', "Account's password has been changed successfully!");
    }
}
