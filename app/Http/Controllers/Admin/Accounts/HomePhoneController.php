<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\Address;
//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Phone;

class HomePhoneController extends PhoneController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = request()->get('id');
        if($id) {
            $address = Address::find($id);

            $user = $address->user;

            if(!isset($address->homePhone)) {
                $phoneTypes = parent::phoneTypes();
                $visibility = parent::visibility();

                return view('admin.accounts.phone.home.create', compact('user', 'address', 'phoneTypes', 'visibility'));
            } else {
                return redirect()->back()->with('message_info', 'You can add only one number for each address');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\PhoneRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PhoneRequest $request)
    {
        $phone = Phone::create(array_filter($request->all()));

        $id = $phone->address->user_id;

        return redirect("/admin/accounts/users/$id")->with('message_success', 'Phone number has been saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phone = Phone::find($id);
        $visibility = $this->visibility();
        $user = $phone->address->user;
        return view('admin.accounts.phone.home.edit', compact('user', 'phone', 'visibility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\PhoneRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PhoneRequest $request, $id)
    {
        $phone = Phone::find($id);
        $phone->update($request->all());

        return redirect("/admin/accounts/users/". $phone->address->user_id)->with('message_success', 'Phone number has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Phone::find($id)->delete();

        return redirect()->back()->with('message_success', 'Phone number has been deleted.');
    }
}
