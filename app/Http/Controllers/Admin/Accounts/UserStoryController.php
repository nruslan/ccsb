<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\User;
use App\UserStory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserStoryController extends Controller
{
    public function create()
    {
        $user = User::findOrFail(request()->id);
        return view('admin.accounts.users.story.create', compact('user'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required'
        ]);

        UserStory::create($request->all());

        return redirect("admin/accounts/users/$request->user_id")->with('message_success', "Users story has been added successfully.");
    }

    public function edit($id)
    {
        $story = UserStory::findOrFail($id);
        $user = $story->user;
        return view('admin.accounts.members.story.edit', compact('user', 'story'));
    }

    public function update(Request $request, $id)
    {
        $story = UserStory::findOrFail($id);
        $story->update($request->all());

        return redirect("admin/accounts/users/$story->user_id")->with('message_success', "Users story has been updated successfully.");;
    }

    public function destroy($id)
    {
        $story = UserStory::findOrFail($id);
        $userId = $story->user_id;
        $usersName = $story->user->members_pretty_name;

        $story->delete();
        return redirect("admin/accounts/users/$userId")->with('message_info', "The story has been deleted from $usersName's profile.");;
    }
}
