<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\MemberStory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;

class MemberStoryController extends Controller
{
    public function create()
    {
        $member = Member::findOrFail(request()->id);
        return view('admin.accounts.members.story.create', compact('member'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required'
        ]);

        MemberStory::create($request->all());

        return redirect("admin/accounts/members/$request->member_id")->with('message_success', "Member's story has been added successfully.");
    }

    public function edit($id)
    {
        $story = MemberStory::findOrFail($id);
        $member = $story->member;
        return view('admin.accounts.members.story.edit', compact('member', 'story'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $story = MemberStory::findOrFail($id);
        $story->update($request->all());

        return redirect("admin/accounts/members/$story->member_id")->with('message_success', "Member's story has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $story = MemberStory::findOrFail($id);
        $memberId = $story->member_id;
        $membersName = $story->member->full_name;

        $story->delete();
        return redirect("admin/accounts/members/$memberId")->with('message_info', "The story has been deleted from $membersName's profile.");
    }
}
