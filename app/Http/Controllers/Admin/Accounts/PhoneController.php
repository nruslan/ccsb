<?php

namespace App\Http\Controllers\Admin\Accounts;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Phone;
use App\Member;
use App\PhoneType;
use App\Visibility;

class PhoneController extends IndexController
{
    public static function phoneTypes()
    {
        return PhoneType::where('id', '!=', 2)->pluck('name', 'id');
    }

    public static function visibility()
    {
        return Visibility::all()->pluck('mode', 'id');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $member = Member::find(request()->get('id'));

        $phoneTypes = $this->phoneTypes();
        $visibility = $this->visibility();

        return view('admin.accounts.phone.create', compact('member', 'phoneTypes', 'visibility'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\PhoneRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PhoneRequest $request)
    {
        Phone::create(array_filter($request->all()));
        return redirect("/admin/accounts/members/".$request['member_id'])->with('message_success', 'Phone number has been saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phone = Phone::find($id);
        $member = $phone->member;
        $phoneTypes = $this->phoneTypes();
        $visibility = $this->visibility();

        return view('admin.accounts.phone.edit', compact('member', 'phone', 'phoneTypes', 'visibility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\PhoneRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PhoneRequest $request, $id)
    {
        $phone = Phone::find($id);
            $phone->update(array_filter($request->all()));

        return redirect("/admin/accounts/members/$phone->member_id")->with('message_success', 'Phone number has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phone = Phone::find($id);
            $phone->delete();

        return redirect("/admin/accounts/members/$phone->member_id")->with('message_success', 'Phone number has been deleted.');
    }
}
