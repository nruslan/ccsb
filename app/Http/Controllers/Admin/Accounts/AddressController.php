<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Address;

class AddressController extends IndexController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Load user data
        $user = User::find(request()->get('id'));
            $countries = parent::countries();
            $states = parent::states();
            $visibility = parent::visibility();
            $addressTypes = parent::addressType();
        return view('admin.accounts.address.create', compact('user', 'countries', 'states', 'visibility', 'addressTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\AddressRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\AddressRequest $request)
    {
        Address::create(array_filter($request->all()));

        return redirect("/admin/accounts/users/". $request['user_id'])->with('message_success', 'The address has been added to the account.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $address = Address::find($id);
        $user = $address->user;
        $countries = parent::countries();
        $states = parent::states();
        $visibility = parent::visibility();
        $addressTypes = parent::addressType();

        return view('admin.accounts.address.edit', compact('user', 'address', 'countries', 'states', 'visibility', 'addressTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\AddressRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\AddressRequest $request, $id)
    {
        $address = Address::find($id);
            $address->update(array_filter($request->all()));

        return redirect("/admin/accounts/users/" . $address->user_id)->with('message_success', 'The address has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
