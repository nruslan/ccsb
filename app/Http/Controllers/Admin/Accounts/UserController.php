<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\User;
use App\Mail\UserCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class UserController extends IndexController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('username')
            ->paginate(12);
        $i = $users->firstItem();
        session()->put('pageId', $users->currentPage());
        return view('admin.accounts.users.index', compact('users','i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.accounts.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|alpha_dash',
            'email' => 'required|email|unique:users,email',
            'since' => 'required|date'
        ], [
            'username.required' => 'The Last Name field is required.',
            'since.required' => 'Church Member Since Date field is required',
        ]);

        $temp_password = $this->shuffled_string();

        $user = User::create([
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => bcrypt($temp_password),
            'since' => $request['since']
        ]);

        //$mail = Mail::to($request['email'])->send(new UserCreated($user, $temp_password));

        return redirect("/admin/accounts/users/$user->id")->with('message_success', 'The user account was created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        $i = 1;
        $pageId = session()->get('pageId');
        return view('admin.accounts.users.show', compact('user', 'i', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.accounts.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'required|alpha_dash',
            'email' => "required|email|unique:users,email,$id"
        ]);

        // Load user data
        $user = User::find($id);
            // Update account
            $user->update($request->all());

        // Redirect back
        return redirect("/admin/accounts/users/$user->id")->with('message_success', 'User account has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Load user data
        $user = User::find($id);
        // Update account
        $user->delete();
        $pageId = session()->get('pageId');
        // Redirect back
        return redirect("/admin/accounts/users?page=$pageId")->with('message_success', 'User account has been deleted successfully.');
    }

    public function verifyUser($id)
    {
        // Load users data
        $user = User::find($id);
        if($user->verified == 'yes') {
            $user->update(['verified' => 'no']);
            $result = [
                'face' => '<i class="fa fa-fw fa-frown-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title data-original-title="Unverified"></i>',
                'class_add' => 'btn-default',
                'class_remove' => 'btn-success'
            ];
        } else {
            $user->update(['verified' => 'yes']);
            $result = [
                'face' => '<i class="fa fa-fw fa-smile-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title data-original-title="Verified"></i>',
                'class_add' => 'btn-success',
                'class_remove' => 'btn-default'
            ];
        }
        $data = $result;
        $pageId = session()->get('pageId');
        //sleep(3);
        if (request()->isMethod('post'))
            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        else
            return redirect("admin/accounts/users?page=$pageId");

    }

    /**
     * Get randomly shuffled string
     * @return mixed
     */
    public function shuffled_string()
    {
        $user = new User();
        return $user->temp_password;
    }

    public function find_user(Request $request)
    {
        $search = $request['query'];
        $users = User::where('username', 'LIKE', '%'.$search.'%')
            ->orderBy('username')
            ->get();
        $i = 1;
        $data = view('admin.accounts.users.search.result', compact('users', 'i'))->render();
        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
