<?php

namespace App\Http\Controllers\Admin\Accounts;

use App\Admin;
use App\Mail\AdministratorCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Requests;

class AdminsController extends IndexController
{
    private $view_template;
    public function __construct()
    {
        parent::__construct();
        $this->view_template = "$this->template.admins";
    }

    public function index()
    {
        $admins = Admin::orderBy('id')->paginate(12);
        $i = $admins->firstItem();

        return view("$this->view_template.index", compact('admins','i'));
    }

    public function create()
    {
        return view('admin.accounts.admins.create');
    }

    public function store(Requests\AdminsRequest $request)
    {
        $temp_password = $this->shuffled_string();
        $admin = Admin::create([
            'username' => [$request['first_name'], $request['last_name']],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'password' => bcrypt($temp_password),
            'temp_password' => 1
        ]);

        $mail = Mail::to($request['email'])->send(new AdministratorCreated($admin, $temp_password));

        return redirect('/admin/accounts/admins');
    }

    public function show($id)
    {
        $admin = Admin::find($id);
        $currentAdmin = request()->user()->id;
        return view("$this->view_template.show", compact('admin', 'currentAdmin'));
    }

    public function edit($id)
    {
        $admin = Admin::findOrFail($id);
        return view("$this->view_template.edit", compact('admin'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'username' => "required|alpha|unique:admins,username,$id",
            'first_name' => 'required|alpha_dash',
            'last_name' => 'required|alpha_dash',
            'email' => "required|string|email|max:255|unique:admins,email,$id"
        ]);

        $admin = Admin::findOrFail($id);
            $admin->update($request->all());

        return redirect("admin/accounts/admins/$id")->with('message_success', "Account's details has been updated successfully!");
    }

    public function destroy($id)
    {
        if(request()->user()->id == $id)
            return redirect("admin/accounts/admins")->with('message_info', "You CANNOT delete yourself!");

        $admin = Admin::find($id);
            $admin->delete();
        return redirect("admin/accounts/admins");
    }

    public function shuffled_string()
    {
        $admin = new Admin();
        return $admin->temporary_password;
    }
}
