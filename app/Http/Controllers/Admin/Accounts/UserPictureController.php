<?php

namespace App\Http\Controllers\Admin\Accounts;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Picture;
use Illuminate\Support\Facades\Storage;

class UserPictureController extends IndexController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = request()->get('id');
        $user = User::find($id);

        return view('admin.accounts.users.picture.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uid = $request['id'];
        $photo = $request['imageData'];
        $destinationPath = "public/accounts/users";
        if(strlen($photo) > 128) {
            list($ext, $data)   = explode(';', $photo);
            list(, $data)       = explode(',', $data);
            $data = base64_decode($data);

            $user = User::find($uid);
            $fileName = $user->picture_filename;
            Storage::disk('local')->put("$destinationPath/$fileName", $data);
            $pic = Picture::create(['filename' => $fileName]);
            $user->update(['picture_id' => $pic->id]);

            parent::image_manipulation($fileName, $destinationPath);
        }
        return redirect("/admin/accounts/users/$uid");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
