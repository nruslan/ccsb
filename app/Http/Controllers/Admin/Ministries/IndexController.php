<?php

namespace App\Http\Controllers\Admin\Ministries;

use App\Ministry;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ministries = Ministry::orderBy('id')->paginate(12);
        $i = $ministries->firstItem();
        session()->put('pageId', $ministries->currentPage());
        return view('admin.ministries.index', compact('ministries', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageId = session()->get('pageId');
        $id = request()->id;
        if ($id)
            $ministry = Ministry::findOrFail($id);
        return view('admin.ministries.create', compact('id', 'ministry', 'pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:50'
        ]);
        $inputs = $request->all();
        $inputs['slug'] = $inputs['name'];
        $ministry = Ministry::create($inputs);

        return redirect("/admin/ministries/$ministry->id")->with('message_success', "$ministry->name has been created successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ministry = Ministry::find($id);

        $pageId = session()->get('pageId');
        $i = 1;
        $ii = $i;
        return view('admin.ministries.show', compact('ministry', 'pageId', 'i', 'ii'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ministry = Ministry::find($id);
        $pageId = session()->get('pageId');
        return view('admin.ministries.edit', compact('ministry', 'pageId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|alpha|max:50',
            'title' => 'required|max:100',
            'subtitle' => 'max:100',
            'meta_d' => 'max:150',
        ]);
        $ministry = Ministry::find($id);
        $ministry->update($request->all());
        return redirect("/admin/ministries/$ministry->id")->with('message_success', "$ministry->name has been UPDATED successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ministry = Ministry::findOrFail($id);
        $name = $ministry->name;
        $ministry->delete();
        $pageId = session()->get('pageId');
        return redirect("/admin/ministries?page=$pageId")->with('message_info', "$name ministry has been deleted.");
    }
}
