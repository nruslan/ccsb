<?php

namespace App\Http\Controllers\Admin\Ministries;

use App\Ministry;
use App\MinistryMember;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    public function create()
    {
        $id = request()->get('id');
        if($id) {
            $ministry = Ministry::find($id);
            $pageId = session()->get('pageId');
            return view('admin.ministries.member.create', compact('ministry', 'pageId'));
        }
        else {
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'member_id' => 'required',
            'term_started' => 'required|date'
        ], [
            'member_id.required' => 'You need to add member\'s name. Use search field above ⬏'
        ]);

        MinistryMember::create(array_filter($request->all()));

        $id = $request['ministry_id'];

        return redirect("/admin/ministries/$id")->with('message_success', 'Member has been added to the ministry successfully!');
    }

    public function edit($id)
    {
        $ministryMember = MinistryMember::find($id);
        return view('admin.ministries.member.edit', compact('ministryMember'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'term_started' => 'required|date',
            'term_ended' => 'date'
        ]);

        $ministryMember = MinistryMember::find($id);
            $ministryMember->update(array_filter($request->all()));

        return redirect("/admin/ministries/$ministryMember->ministry_id")->with('message_success', "Member's terms have been updated successfully!");
    }

    public function destroy($id)
    {
        $ministryMember = MinistryMember::find($id);

        $ministryMember->delete();

        return redirect()->back();
    }
}
