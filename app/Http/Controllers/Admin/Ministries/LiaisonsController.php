<?php

namespace App\Http\Controllers\Admin\Ministries;

use App\Ministry;
use App\MinistryLiaison;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LiaisonsController extends Controller
{
    public function create()
    {
        $id = request()->get('id');
        if($id) {
            $ministry = Ministry::find($id);
            $pageId = session()->get('pageId');
            return view('admin.ministries.liaison.create', compact('ministry', 'pageId'));
        }
        else {
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'member_id' => 'required',
            'term_started' => 'required|date'
        ], [
            'member_id.required' => 'You need to add liaison\'s name. Use search field above ⬏'
        ]);

        MinistryLiaison::create($request->all());

        $id = $request['ministry_id'];

        return redirect("/admin/ministries/$id")->with('message_success', 'Liaison has been added to the ministry successfully!');
    }

    public function edit($id)
    {
        $ministryMember = MinistryLiaison::find($id);
        return view('admin.ministries.liaison.edit', compact('ministryMember'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'term_started' => 'required|date',
            'term_ended' => 'date'
        ]);

        $ministryMember = MinistryLiaison::find($id);
        $ministryMember->update($request->all());

        return redirect("/admin/ministries/$ministryMember->ministry_id")->with('message_success', "Liaison's terms have been updated successfully!");
    }

    public function destroy($id)
    {
        $ministryMember = MinistryLiaison::find($id);

        $ministryMember->delete();

        return redirect()->back();
    }
}
