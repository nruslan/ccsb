<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PhoneRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_id' => 'required',
            'area_code' => 'required|numeric',
            'number' => 'required|numeric',
            'visibility_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'type_id.required' => 'The Phone Number Type field is required.'
        ];
    }
}
