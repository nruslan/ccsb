<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'category_id' => 'required',
            //'date_time' => 'required',
            //'time' => 'required',
            //'location_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            //'location_id.required' => "Please specify the Event's Location."
        ];
    }
}
