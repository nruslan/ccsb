<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address1' => 'required',
            'city' => 'required',
            'country_id' => 'required',
            'zip_code' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'address1.required' => 'Please specify the address. The field is required.',
            'country_id.required' => 'Please specify the country. The field is required.'
        ];
    }
}
