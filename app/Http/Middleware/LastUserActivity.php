<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Cache;
use Carbon\Carbon;


class LastUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $expire_at = Carbon::now()->addMinutes(2);
            Cache::put('user-'.Auth::user()->id.'-is-online', true, $expire_at);
        }
        return $next($request);
    }
}
