<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckTempPassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Auth::user()->username;

        if(Auth::user()->temp_password)
        {
            //redirect();
        }
        return $next($request);
    }
}
