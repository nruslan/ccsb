<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(!$user)
            return redirect('/login');

        $roles = $user->roles()->pluck('name', 'role_id');

        if(array_has($roles, '1') or array_has($roles, '2'))
            return $next($request);
        else
            return redirect('/')->with('', 'You dont have permission');
    }
}
