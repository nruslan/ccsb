<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Committee extends Model
{
    //use SoftDeletes;
    //protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'picture_id', 'slug', 'deleted_at'
    ];

    /**
     * Members that belong to the committee.
     */
    public function members()
    {
        return $this->belongsToMany('App\Member', 'committee_members')->withPivot('id', 'term_started', 'term_ended');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getActiveMembersAttribute()
    {
        return $this->members()->where('term_ended', null)->get();
    }

    public function getIconClassAttribute()
    {
        $icons = [
            'missions-committee' => 'c-icon-29',
            'blue-dot-sunday-committee' => 'c-icon-6',
            'fellowship-committee' => 'c-icon-2',
            'communications-committee' => 'c-icon-47',
            'stewardship-committee' => 'c-icon-1',
            'altar-guild-committee' => 'c-icon-12',
            'communion-committee' => 'c-icon-10',
            'ushergreeter-committee' => 'c-icon-49',
            'sound-booth-committee' => 'c-icon-34',
            'office-assistance-committee' => 'c-icon-24',
            'library-committee' => 'c-icon-39',
            'road-clean-up-committee' => 'c-icon-4',
            'church-signs-committee' => 'c-icon-43',
            'website-committee' => 'c-icon-18'
        ];

        return $icons[$this->slug];
    }

    public function getTotalActiveMembersAttribute()
    {
       return count($this->members()->where('term_ended', null)->get());
    }

    public function getPastMembersAttribute()
    {
        return $this->members()->where('term_ended', '!=' , null)->get();
    }

    // Mutators -------------------------------------------------------------------------------------------------------
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value, '-').'-'.'committee';
    }
}
