<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventMember extends Model
{
    protected $fillable = [
        'event_id', 'member_id', 'title', 'leader', 'order'
    ];
}
