<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'address_type_id', 'address1', 'address2', 'city', 'state_id', 'country_id', 'zip_code', 'visibility_id'
    ];

    /**
     * Get User
     */
    public function homePhone()
    {
        return $this->hasOne('App\Phone', 'address_id');
    }

    /**
     * Get User
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get US State
     */
    public function state()
    {
        return $this->belongsTo('App\State', 'state_id');
    }

    /**
     * Get US State
     */
    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }

    /**
     * Get Address Visibility
     */
    public function visibility()
    {
        return $this->belongsTo('App\Visibility', 'visibility_id');
    }

    //Get Address Type
    public function type()
    {
        return $this->belongsTo('App\AddressType', 'address_type_id');
    }

    //Get Address Name
    public function addressName()
    {
        return $this->belongsTo('App\AddressName', 'id','address_id');
    }
}
