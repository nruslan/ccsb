<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'category_id', 'title', 'subtitle', 'slug', 'description', 'picture_id'
    ];

    public function ministries()
    {
        return $this->belongsToMany('App\Ministry', 'ministry_events', 'event_id','ministry_id');
    }

    public function dates()
    {
        return $this->hasMany('App\EventMeta', 'event_id','id')->where('date_time', '>', Carbon::now());
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function members()
    {
        return $this->belongsToMany('App\Member', 'event_members', 'event_id','member_id')->withPivot('id', 'leader', 'title', 'order');
    }

    public function price()
    {
        return $this->hasOne('App\EventPrice', 'event_id');
    }

    public function picture()
    {
        return $this->belongsTo('App\Picture','picture_id');
    }

    // Mutators -------------------------------------------------------------------------------------------------------
    public function setSlugAttribute($value)
    {
        $shuffleString = substr(str_shuffle(rand().'qwertyuiopasdfghjklzxcvbnm'.rand()),0,5);

        $this->attributes['slug'] = str_slug($value, '-').'-'.$shuffleString;
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getMinistryNameAttribute()
    {
        if(is_null($this->ministries)) {
            return $this->ministries->first()->name;
        } else {
            return "Church Event";
        }
    }

    public function getTitleSubtitleAttribute()
    {
        if($this->subtitle) {
            return "$this->title <small>$this->subtitle</small>";
        } else {
            return "$this->title";
        }
    }

    public function getUrlAttribute()
    {
        return url("happenings/events/$this->slug");
    }

    public function getCategoryTitleAttribute()
    {
        return $this->category->title;
    }

    // Main leader/member for the event
    public function getMainMemberAttribute()
    {
        $members = $this->members;
        if($members)
            return $members->first();
        else
            return false;
    }

    public function getMainMemberTitleAttribute()
    {
        $members = $this->members;
        if($members)
            return $members->first();
        else
            return false;
    }

    public function getPriceDescriptionAttribute()
    {
        if($this->price->price) {
            $price = $this->price->price;
            $description = $this->price->name;
            return "$ $price, $description";
        } else {
            return false;
        }
    }

    public function getThumbnailPictureAttribute()
    {
        $year = date('Y', strtotime($this->created_at));
        if($this->picture_id)
            return asset("storage/events/$year/thumbnails/".$this->picture->filename);
        else
            return asset("storage/events/thumbnail-default.jpg");
    }

    // Custom ---------------------------------------------------------------------------------------------------------
    public static function current_date($day)
    {
        return strtotime(date("Y-m-$day"));
    }

    public static function text_limit($text, $start = 0, $strLength = 105)
    {
        //Remove any titles <h1> - <h6>
        $no_title_txt = preg_replace('/<h[0-6][^>]*>([\s\S]*?)<\/h[0-6][^>]*>/', '', $text);

        // Remove HTML tags from content
        $txt = strip_tags($no_title_txt);

        if(strlen($txt) > $strLength)
            $result = substr($txt, $start, strrpos($txt, " ", -(strlen($txt) - $strLength))); // Return trimmed string
        else
            $result = $txt;

        return $result;
    }
}
