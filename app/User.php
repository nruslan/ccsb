<?php

namespace App;

use Cache;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    //use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'picture_id', 'verified', 'since', 'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // DB  relationships
    public function members()
    {
        return $this->hasMany('App\Member', 'user_id');
    }

    public function profilePicture()
    {
        return $this->belongsTo('App\Picture', 'picture_id');
    }

    public function addresses()
    {
        return $this->hasMany('App\Address', 'user_id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id');
    }

    public function story()
    {
        return $this->belongsTo('App\UserStory', 'id', 'user_id');
    }

    /**
     * Mutators
     ******************************************************************************************************************/
    public function setUsernameAttribute($value)
    {
        $this->attributes['username'] = ucfirst(strtolower($value));
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function setSinceAttribute($value)
    {
        $this->attributes['since'] = date("Y-m-d", strtotime($value));
    }

    /**
     * Accessors
     ******************************************************************************************************************/
    public function getSinceAttribute($value)
    {
        return date("F d, Y", strtotime($value));
    }

    public function getSinceDateAttribute()
    {
        return date("F d, Y", strtotime($this->since));
    }

    public function getSinceYearAttribute()
    {
        return date("Y", strtotime($this->since));
    }

    public function getRegistrationDateAttribute()
    {
        return date("F d, Y", strtotime($this->created_at));
    }

    public function getMembersPrettyNameAttribute()
    {
        $result = null;
        $lastNames = [];

        foreach($this->members as $member) {
            $lastNames[$member->id] = $member->last_name;
        }

        $acv = count(array_count_values($lastNames));

        if($acv > 1) {
            foreach($this->members as $member) {
                $result .= $member->first_name.' '.$member->last_name.' &amp; ';
            }

        } else {
            foreach($this->members as $member) {
                $result .= $member->first_name. ' &amp; ';
            }

            $result = $this->username.', '.$result;
        }

        return substr($result, 0, -7);
    }

    public function getJustMembersNamesAttribute()
    {
        $result = null;
        foreach($this->members as $member) {
            $result .= $member->first_name. ' &amp; ';
        }

        return substr($result, 0, -7);
    }

    public function getPictureCheckerAttribute()
    {
        if($this->picture_id)
            $icon = '<i class="fa fa-fw fa-picture-o" aria-hidden="true"></i>';
        else
            $icon = '---';

        return $icon;
    }

    public function getTempPasswordAttribute()
    {
        $digits = rand(100000000,999999999);
        $lowerCaseLetters = 'qwertasdfgzxcvbyuiophjknm'; // no lower case L
        $upperCaseLetters = 'QWERTASDFGZXCVBYUIOPHJKLNM';
        $specialCharacters = '!@$%';
        // united string
        $string = str_shuffle($digits.$lowerCaseLetters.$digits.$upperCaseLetters.$specialCharacters.$digits);

        return substr($string,0,9);
    }

    public function getPictureFilenameAttribute()
    {
        return strtolower($this->username.'-'.mt_rand(9999,100000).'.png');
    }

    public function getPictureFilenameUrlAttribute()
    {
        if($this->picture_id)
            return "storage/accounts/users/".$this->profilePicture->filename;
        else
            return "storage/accounts/blank.jpg";
    }

    public function getActivityStatusAttribute()
    {
        return Cache::has("user-$this->id-is-online");
    }
}
