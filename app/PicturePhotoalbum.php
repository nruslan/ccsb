<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PicturePhotoalbum extends Model
{
    public function picture()
    {
        return $this->belongsTo('App\Picture', 'picture_id');
    }

    public function photoalbum()
    {
        return $this->belongsTo('App\Photoalbum', 'photoalbum_id');
    }

    public function getPictureThumbUrlAttribute()
    {
        return $this->photoalbum->album_url.'thumbnails/'.$this->picture->filename;
    }

    public function getPictureUrlAttribute()
    {
        return $this->photoalbum->album_url.$this->picture->filename;
    }
}
