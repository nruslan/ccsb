<?php
namespace App\Helpers;

class Helper
{
    public static function text_limit($text, $start = 0, $strLength = 155)
    {
        //Remove any titles <h1> - <h6>
        $no_title_txt = preg_replace('/<h[0-6][^>]*>([\s\S]*?)<\/h[0-6][^>]*>/', '', $text);

        // Remove HTML tags from content
        $txt = strip_tags($no_title_txt);

        if(strlen($txt) > $strLength) {
            // Return trimmed string
            $result = substr($txt, $start, strrpos($txt, " ", -(strlen($txt) - $strLength)));
        }
        else {
            $result = $txt;
        }

        return $result;
    }


    // Calendar Functions
    public static function days_of_week($value)
    {
        $dw = [
            1 => ['S','M','T','W','Th','F','S'],
            2 => ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            3 => []
        ];

        return $dw[$value];
    }

    public static function day_of_week($day)
    {
        $month = date('m');
        $year = date('Y');
        $dateInfo = getdate(mktime(0,0,0,$month,$day, $year));

        return $dateInfo['wday'];
    }

    public function days_in_month($month, $year)
    {
        return cal_days_in_month(CAL_GREGORIAN, $month, $year);
    }

}