<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'middle_name', 'slug', 'user_id', 'dob', 'picture_id', 'deleted_at'
    ];

    //DB Relationships -------------------------------------------------------------------------------------------------
    public function userAccount()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function profilePicture()
    {
        return $this->belongsTo('App\Picture', 'picture_id');
    }

    public function phones()
    {
        return $this->hasMany('App\Phone', 'member_id');
    }

    public function titles()
    {
        return $this->belongsToMany('App\Title', 'member_titles', 'member_id')->withPivot('id', 'term_started', 'term_ended');
    }

    public function story()
    {
        return $this->belongsTo('App\MemberStory', 'id', 'member_id');
    }

    public function emails()
    {
        return $this->hasMany('App\Email', 'member_id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles', 'member_id', 'role_id');
    }

    public function videos()
    {
        return $this->belongsToMany('App\Video', 'member_videos', 'member_id', 'video_id');
    }

    public function sermons()
    {
        return $this->belongsToMany('App\Sermon', 'member_sermons', 'member_id', 'sermon_id');
    }

    //Missions Supported Ministries
    public function ministries()
    {
        return $this->belongsToMany('App\Ministry', 'ministry_liaisons')->withPivot('id', 'term_started', 'term_ended');
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = strtolower($value);
    }

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucfirst(strtolower($value));
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = ucfirst(strtolower($value));
    }

    public function setMiddleNameAttribute($value)
    {
        $this->attributes['middle_name'] = ucfirst(strtolower($value));
    }

    public function setDobAttribute($value)
    {
        $this->attributes['dob'] = date('Y-m-d', strtotime($value));
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getDobAttribute($value)
    {
        if($value)
            return date('m/d/Y', strtotime($value));
        else
            return null;
    }

    public function getBirthDateAttribute()
    {
        if($this->dob)
            return date('m/d/Y', strtotime($this->dob));
        else
            return "<em>- not specified -</em>";
    }

    public function getFullNameAttribute()
    {
        if($this->middle_name)
            return "$this->first_name $this->middle_name $this->last_name";
        else
            return "$this->first_name $this->last_name";
    }

    public function getTitlesListAttribute()
    {
        $result = null;
        foreach($this->titles as $title){
            if($title->parent_id)
                $result .= $title->parent->title. ' &bullet; '. $title->title. ', ';
            else
                $result .= $title->title. ', ';
        }

        return substr($result, 0, -2);
    }

    public function getPictureFilenameAttribute()
    {
        return strtolower($this->last_name.'-'.$this->first_name.'-'.mt_rand(9999,100000).'.png');
    }

    public function getProfilePictureUrlAttribute()
    {
        if($this->picture_id)
            return asset("/storage/accounts/members/".$this->profilePicture->filename);
        else
            return asset("/storage/accounts/members/default.png");
    }

    public function getAboutStoryAttribute()
    {
        return $this->story->description;
    }

    // Custom Function -------------------------------------------------------------------------------------------------
    /**
     * Offset members picture in the Directory
     * @param $value
     * @return null|string
     */
    public static function offset($value)
    {
        if ($value == 3)
            return null;
        elseif ($value == 2)
            return "col-md-offset-2";
        elseif ($value == 1)
            return "col-md-offset-4";
        else
            return null;
    }


}
