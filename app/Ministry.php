<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Ministry extends Model
{
    //use SoftDeletes;

    //protected $dates = ['deleted_at'];

    protected $fillable = [
        'parent_id', 'name', 'title', 'subtitle', 'meta_d', 'slug', 'description', 'picture_id'
    ];

    /**
     * Members that belong to the committee.
     */
    public function members()
    {
        return $this->belongsToMany('App\Member', 'ministry_members')->withPivot('id', 'term_started', 'term_ended');
    }

    public function liaisons()
    {
        return $this->belongsToMany('App\Member', 'ministry_liaisons')->withPivot('id', 'term_started', 'term_ended');
    }

    public function photoalbums()
    {
        return $this->belongsToMany('App\Photoalbum', 'ministry_photoalbums', 'ministry_id', 'photoalbum_id');
    }

    public function logo()
    {
        return $this->belongsTo('App\Picture', 'picture_id');
    }

    public function supportedMinistries()
    {
        return $this->hasMany('App\Ministry', 'parent_id');
    }

    public function news()
    {
        return $this->belongsToMany('App\News', 'news_ministries', 'ministry_id', 'news_id');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event', 'ministry_events', 'ministry_id', 'event_id');
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setSlugAttribute($value)
    {
        //$shuffleString = substr(str_shuffle(rand().'qwertyuiopasdfghjklzxcvbnm'.rand()),0,5);

        $this->attributes['slug'] = str_slug($value, '-');//.'-'.$shuffleString;
    }

    // Accessor --------------------------------------------------------------------------------------------------------
    public function getActiveMembersAttribute()
    {
        return $this->members()->where('term_ended', null)->get();
    }

    public function getPastMembersAttribute()
    {
        return $this->members()->where('term_ended', '!=' , null)->get();
    }

    public function getLogotypeUrlAttribute()
    {
        if($this->picture_id)
            return "storage/logotypes/".$this->logo->filename;
        else
            return "storage/logotypes/_blank.jpg";
    }

    public function getTitleSubtitleAttribute()
    {
        if($this->subtitle)
            return "$this->title<br><small>$this->subtitle</small>";
        else
            return $this->title;
    }

    public function getMembersTotalAttribute()
    {
        return count($this->members()->where('term_ended', null)->get());
    }

    public function getLiaisonsTotalAttribute()
    {
        return count($this->liaisons()->where('term_ended', null)->get());
    }
}
