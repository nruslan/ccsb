<?php

namespace App\Providers;

use App\Title;
use App\Category;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Public Service Providers
        view()->composer('master.media.video.partials.sidenav', function($view)
        {
            $categories = Category::where('category_type_id', 1)
                ->orderBy('sort')
                ->get();
            $view->with('categories', $categories);

            $pastors = Title::find(1)->members; //Pastor
            $view->with('pastors', $pastors);
        });

        view()->composer('master.media.sermon.partials.sidenav', function($view)
        {
            $pastors = Title::find(1)->members; //Pastor
            $view->with('pastors', $pastors);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
