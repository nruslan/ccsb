<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    //use SoftDeletes;
    protected $table = 'news';
    //protected $dates = ['deleted_at'];

    protected $fillable = [
        'parent_id', 'title', 'subtitle', 'slug', 'text', 'picture_id', 'pub_date_time', 'draft', 'deleted_at'
    ];

    // DB Relationship -------------------------------------------------------------------------------------------------
    public function ministries()
    {
        return $this->belongsToMany('App\Ministry', 'news_ministries', 'news_id', 'ministry_id');
    }

    public function committees()
    {
        return $this->belongsToMany('App\Committee', 'news_committees', 'news_id', 'committee_id');
    }

    public function picture()
    {
        return $this->belongsTo('App\Picture','picture_id');
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setSlugAttribute($value)
    {
        $shuffleString = substr(str_shuffle(rand().'qwertyuiopasdfghjklzxcvbnm'.rand()),0,5);

        $this->attributes['slug'] = str_slug($value, '-').'-'.$shuffleString;
    }

    public function setPubDateTimeAttribute($value)
    {
        $this->attributes['pub_date_time'] = date('Y-m-d H:i:s', strtotime($value));
    }

    // Accessor --------------------------------------------------------------------------------------------------------
    public function getPubDateTimeAttribute($value)
    {
        return date('m/d/Y h:i A', strtotime($value));
    }

    public function getPubDateAttribute()
    {
        return date('F d, Y', strtotime($this->pub_date_time));
    }

    public function getPubTimeAttribute()
    {
        return date('h:i A', strtotime($this->pub_date_time));
    }

    public function getPublishDateTimeAttribute()
    {
        return date('F d, Y - h:i A', strtotime($this->pub_date_time));
    }

    public function getPubDraftAttribute()
    {
        if($this->draft)
            $result = '<span class="text-danger"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Draft</span>';
        else
            $result = '<span class="text-success"><i class="fa fa-fw fa-thumbs-o-up" aria-hidden="true"></i> Published</span>';

        return $result;
    }

    public function getTitleSubtitleAttribute()
    {
        if($this->subtitle)
            return "$this->title<br><small>$this->subtitle</small>";
        else
            return $this->title;
    }

    public function getThumbnailPictureAttribute()
    {
        $year = date('Y', strtotime($this->created_at));
        if($this->picture_id)
            return asset("storage/news/$year/thumbnails/".$this->picture->filename);
        else
            return asset("storage/news/thumbnail-default.jpg");
    }

    public function getThePictureAttribute()
    {
        $year = date('Y', strtotime($this->created_at));
        if($this->picture_id)
            return asset("/storage/news/$year/".$this->picture->filename);
        else
            return asset("/storage/news/default.jpg");
    }

    public function getUrlAttribute()
    {
        return url("happenings/news/$this->slug");
    }


    public static function text_limit($text, $start = 0, $strLength = 105)
    {
        //Remove any titles <h1> - <h6>
        $no_title_txt = preg_replace('/<h[0-6][^>]*>([\s\S]*?)<\/h[0-6][^>]*>/', '', $text);

        // Remove HTML tags from content
        $txt = strip_tags($no_title_txt);

        if(strlen($txt) > $strLength)
            $result = substr($txt, $start, strrpos($txt, " ", -(strlen($txt) - $strLength))); // Return trimmed string
        else
            $result = $txt;

        return $result;
    }
}
