<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Sermon extends Model
{
    //use SoftDeletes;
    //protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'parent_id', 'title', 'subtitle', 'description', 'slug', 'filename', 'duration', 'size', 'picture_id', 'date', 'deleted_at'
    ];

    // DB Relationship -------------------------------------------------------------------------------------------------
    public function pastors()
    {
        return $this->belongsToMany('App\Member','member_sermons','sermon_id', 'member_id');
    }

    public function relatedSermon()
    {
        return $this->belongsTo('App\Sermon','parent_id');
    }

    public function picture()
    {
        return $this->belongsTo('App\Picture','picture_id');
    }

    //Mutators ---------------------------------------------------------------------------------------------------------
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = date("Y-m-d", strtotime($value));
    }

    public function setSlugAttribute($value)
    {
        $shuffleString = substr(str_shuffle(rand().'qwertyuiopasdfghjklzxcvbnm'.rand()),0,5);

        $this->attributes['slug'] = str_slug($value, '-').'-'.$shuffleString;
    }

    //Accessors --------------------------------------------------------------------------------------------------------
    public function getDateAttribute($value)
    {
        return date("m/d/Y", strtotime($value));
    }

    public function getTextDateAttribute()
    {
        return date("l, F d Y", strtotime($this->date));
    }

    public function getCompactDateAttribute()
    {
        return date("m/d/Y", strtotime($this->date));
    }

    public function getSermonPastorsAttribute()
    {
        if($this->pastors->count()) {
            $pastors = null;
            foreach($this->pastors as $member) {
                $pastors .= $member->full_name . ', ';
            }
            return substr($pastors, 0, -2);
        } else {
            return '- - -';
        }
    }

    public function getSermonPastorSlugAttribute()
    {
        if($this->pastors->count())
            return $this->pastors->first()->slug;
        else
            return '#';
    }

    public function getSermonFileNameAttribute()
    {
        return date('Ymd',strtotime($this->date)).'-'.str_slug($this->title, '-');
    }

    public function getSermonUrlAttribute()
    {
        $year = date('Y',strtotime($this->date));
        return asset("storage/sermons/$year/$this->filename");
    }

    public function getTitleSubtitleAttribute()
    {
        if($this->subtitle)
            return "$this->title<br><small>$this->subtitle</small>";
        else
            return $this->title;
    }

    public function getSermonsFullNameAttribute()
    {
        if($this->subtitle)
            return "$this->title <small>$this->subtitle</small>";
        else
            return $this->title;
    }

    public function getDurationAttribute($value)
    {
        return gmdate("H:i:s", $value);
    }

    public function getThumbnailAttribute()
    {
        if($this->picture_id)
            return asset("/storage/sermons/".$this->picture->filename);
        else
            return asset("/storage/sermons/default.jpg");
    }

    public function getFileSizeAttribute()
    {
        return round($this->size / 1048576, 2);
    }

    // Custom Master Navigation
    public function getNextSermonAttribute()
    {
        $sermon = Sermon::where('id', '>', $this->id)->orderBy('id','asc')->first();
        if(!$sermon)
            $path_to_file = null;
        else
            $path_to_file = $sermon->slug;

        return url("media/sermons/$path_to_file");
    }
    public  function getPreviousSermonAttribute()
    {
        $sermon = Sermon::where('id', '<', $this->id)->orderBy('id','desc')->first();

        if(!$sermon)
            $path_to_file = null;
        else
            $path_to_file = $sermon->slug;

        return url("media/sermons/$path_to_file");
    }

    // Custom Admin Navigation
    public function next()
    {
        return Sermon::where('id', '>', $this->id)->orderBy('id','asc')->first();
    }
    public function previous()
    {
        return Sermon::where('id', '<', $this->id)->orderBy('id','desc')->first();
    }
}
