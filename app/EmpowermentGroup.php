<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class EmpowermentGroup extends Model
{
    //use SoftDeletes;

    //protected $dates = ['deleted_at'];

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'time', 'location_id', 'description'
    ];

    public function members()
    {
        return $this->hasMany('App\EmpowermentMember', 'empowerment_group_id')->whereNull('leader');
    }

    public function leaders()
    {
        return $this->hasMany('App\EmpowermentMember', 'empowerment_group_id')->whereNotNull('leader');
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setTimeAttribute($value)
    {
        $this->attributes['time'] = date("H:i:s", strtotime($value));
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getTimeAttribute($value)
    {
        return date("h:i A", strtotime($value));
    }

    public function getTotalMembersAttribute()
    {
        return $this->members()->count() +  $this->leaders()->count();
    }

    public function getGroupLeadersAttribute()
    {
        $gLeaders = null;
        foreach($this->leaders as $leader) {
            $gLeaders .= '<a href="">'.$leader->member->full_name.'</a>, ';
        }

        return substr($gLeaders, 0, -2);
    }
}
