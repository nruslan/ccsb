<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Database\Eloquent\SoftDeletes;
use App\Notifications\AdminResetPasswordNotification;

class Admin extends Authenticatable
{
    use Notifiable;
    //use SoftDeletes;

    protected $guard = 'admin';

    //protected $dates = ['deleted_at'];

    protected $fillable = [
        'username', 'first_name', 'middle_name', 'last_name', 'email', 'password', 'temp_password', 'verified'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }

    // Mutators -------------------------------------------------------------------------------------------------------
    public function setUsernameAttribute($value)
    {
        $lastNameFirstChar = substr($value[1], 0, 1);
        $this->attributes['username'] = strtolower($lastNameFirstChar.$value[0]);
    }


    // Accessors -------------------------------------------------------------------------------------------------------
    public function getNameAttribute()
    {
        return $this->first_name .' '. $this->last_name;
    }

    public function getTemporaryPasswordAttribute()
    {
        $digits = rand(100000000,999999999);
        $lowerCaseLetters = 'qwertasdfgzxcvbyuiophjknm'; // no lower case L
        $upperCaseLetters = 'QWERTASDFGZXCVBYUIOPHJKLNM';
        $specialCharacters = '!@$%#';
        // united string
        $string = str_shuffle($digits.$lowerCaseLetters.$digits.$upperCaseLetters.$specialCharacters.$digits);

        return substr($string,0,12);
    }
}