<?php

namespace App\Mail;

use App\Admin;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdministratorPasswordChanged extends Mailable
{
    use Queueable, SerializesModels;

    protected $admin;
    protected $hint;

    /**
     * AdministratorCreated constructor.
     * @param Admin $admin
     * @param string
     */
    public function __construct(Admin $admin, $hint)
    {
        $this->admin = $admin;
        $this->hint = $hint;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Password Changed')
            ->markdown('admin.emails.admins.password-changed')
            ->with([
                'hint' => $this->hint,
                'name' => $this->admin->name
            ]);
    }
}
