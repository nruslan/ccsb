<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Subscribe extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;
    //public $message;

    /**
     * Create a new message instance.
     * @param $email
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
        //$this->message = ['image' => 'https://ccsb.us/storage/logo-brand.png'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New Subscriber')
            ->markdown('master.emails.subscribe')
            //->attach(asset('storage/logo-brand.png'))
            ->with(['email' => $this->email]);
    }
}
