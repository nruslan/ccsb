<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $temp_password;

    /**
     * Create a new message instance.
     * @param User $user
     * @param $temp_password
     */
    public function __construct(User $user, $temp_password)
    {
        $this->user = $user;
        $this->temp_password = $temp_password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome to CCSB')
            ->markdown('admin.emails.users.create')
            ->with([
                'temp_password' => $this->temp_password,
                'name' => $this->user->username
            ]);
    }
}
