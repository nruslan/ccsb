<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUsForm extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;
    protected $email;
    public $message;

    /**
     * Create a new message instance.
     *
     * @param $name
     * @param $email
     * @param $message
     */
    public function __construct($name, $email, $message)
    {
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email, $this->name)
            ->subject("Message from $this->name")
            ->markdown('master.emails.contactUs')
            ->with([
                'message' => $this->message,
                'name' => $this->name,
                'email' => $this->email
            ]);
    }
}
