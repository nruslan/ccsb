<?php

namespace App\Mail;

use App\Admin;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdministratorCreated extends Mailable
{
    use Queueable, SerializesModels;

    protected $admin;
    protected $temp_password;

    /**
     * AdministratorCreated constructor.
     * @param Admin $admin
     * @param string
     */
    public function __construct(Admin $admin, $temp_password)
    {
        $this->admin = $admin;
        $this->temp_password = $temp_password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome to AdminCCSB')
            ->markdown('admin.emails.admins.create')
            ->with([
                'temp_password' => $this->temp_password,
                'name' => $this->admin->name
            ]);
    }
}
