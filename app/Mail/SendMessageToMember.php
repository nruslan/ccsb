<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMessageToMember extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;
    protected $email;
    protected $from_name;
    protected $from_email;
    public $message;

    /**
     * Create a new message instance.
     * @param $name
     * @param $email
     * @param $message
     * @param $fromEmail
     * @param $fromName
     *
     */
    public function __construct($name, $email, $message, $fromEmail, $fromName)
    {
        $this->name = $name;
        $this->email = $email;
        $this->from_name = $fromName;
        $this->from_email = $fromEmail;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->from_email, $this->from_name)
            ->subject("Message from $this->from_name")
            ->markdown('master.emails.memberMessage')
            ->with([
                'message' => $this->message,
                'name' => $this->name,
                'email' => $this->email,
                'from_name' => $this->from_name,
                'from_email' => $this->from_email
            ]);
    }
}
