<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberStory extends Model
{
    protected $fillable = [
        'member_id', 'description'
    ];

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }
}
