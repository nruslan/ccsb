<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MinistryMember extends Model
{
    protected $fillable = [
        'ministry_id', 'member_id', 'term_started', 'term_ended'
    ];

    // DB Relationships ------------------------------------------------------------------------------------------------

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function ministry()
    {
        return $this->belongsTo('App\Ministry', 'ministry_id');
    }

    //Mutators----------------------------------------------------------------------------------------------------------

    public function setTermStartedAttribute($value)
    {
        $this->attributes['term_started'] = date("Y-m-d", strtotime($value));
    }

    public function setTermEndedAttribute($value)
    {
        $this->attributes['term_ended'] = date("Y-m-d", strtotime($value));
    }

    //Accessors---------------------------------------------------------------------------------------------------------

    public function getTermStartedAttribute($value)
    {
        return date("d/m/Y", strtotime($value));
    }

    public function getTermEndedAttribute($value)
    {
        if($value)
            return date("d/m/Y", strtotime($value));
        else
            return '';
    }
}
